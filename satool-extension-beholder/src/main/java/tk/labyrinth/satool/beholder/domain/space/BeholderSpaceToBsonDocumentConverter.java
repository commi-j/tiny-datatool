package tk.labyrinth.satool.beholder.domain.space;

import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.storage.mongodb.SelfProvidingConverter;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

@Component
@RequiredArgsConstructor
public class BeholderSpaceToBsonDocumentConverter implements SelfProvidingConverter<BeholderSpace, Document> {

	private final ObjectProvider<ConversionService> conversionServiceProvider;

	@Override
	public Document convert(BeholderSpace source) {
		ConversionService conversionService = conversionServiceProvider.getObject();
		//
		Document result = new Document();
		result.put("_id", source.getUid());
		result.put("key", source.getKey());
		result.put("updatedAt", source.getUpdatedAt() != null ? source.getUpdatedAt().toString() : null);
		result.put("version", source.getVersion());
		source.getObjects().groupBy(RootObjectUtils::getModelReference).forEach(entry -> {
			CodeObjectModelReference objectModelReference = entry._1();
			if (objectModelReference == null) {
				throw new IllegalArgumentException();
			}
			result.put(
					objectModelReference.getCode(),
					entry._2().map(object -> conversionService.convert(
							object
									.withoutAttribute(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME)
									.withoutAttribute(BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME),
							Document.class)));
		});
		return result;
	}
}
