package tk.labyrinth.satool.beholder.domain.object.table;

import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.Map;
import lombok.Value;
import lombok.With;

import javax.annotation.Nullable;

@Value(staticConstructor = "of")
@With
public class TablePredicate {

	Map<String, ColumnPredicate<?>> columns;

	public ColumnPredicate<?> getColumn(String columnName) {
		return columns.get(columnName).getOrNull();
	}

	public TablePredicate withColumn(String columnName, @Nullable ColumnPredicate<?> columnPredicate) {
		return withColumns(columnPredicate != null
				? columns.put(columnName, columnPredicate)
				: columns.remove(columnName));
	}

	public static TablePredicate empty() {
		return new TablePredicate(LinkedHashMap.empty());
	}

	public static String render(TablePredicate predicate) {
		return predicate.columns
				.map(column -> "%s %s %s".formatted(
						column._1(),
						column._2().getOperator().getDescriptor().getText(),
						column._2().getValue()))
				.mkString(" AND ");
	}
}
