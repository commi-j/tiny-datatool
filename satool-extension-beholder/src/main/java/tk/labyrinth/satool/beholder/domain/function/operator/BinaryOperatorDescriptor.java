package tk.labyrinth.satool.beholder.domain.function.operator;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;

// TODO: Think how to determine what suggests to use for this operator.
public interface BinaryOperatorDescriptor<S> {

	Datatype getSecondOperandDatatype(Datatype firstOperandDatatype);

	String getText();

	boolean supports(Class<?> firstOperandClass);

	Predicate toPredicate(String attributeName, S value);
}
