package tk.labyrinth.satool.beholder.stores;

import com.mongodb.ConnectionString;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.PropertyPredicate;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.genericobject.mongodb.MongoDbDatabaseLocator;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;

import java.util.Objects;
import java.util.stream.Collectors;

@LazyComponent
@RequiredArgsConstructor
public class BeholderMongoDbDatabaseLocator implements MongoDbDatabaseLocator {

	@Override
	public Pair<GenericObject, @Nullable String> locateDatabase(
			StoreConfiguration storeConfiguration,
			GenericObject object) {
		Pair<GenericObject, String> result;
		{
			GenericObjectAttribute spaceReferenceAttribute = object.findAttribute(
					BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME);
			//
			result = Pair.of(
					object.withoutAttribute(BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME),
					spaceReferenceAttribute != null
							? KeySpaceReference.from(spaceReferenceAttribute.getValue().asSimple().getValue()).getKey()
							: null);
		}
		return result;
	}

	@Override
	public DatabaseContext locateDatabase(StoreConfiguration storeConfiguration, @Nullable Predicate predicate) {
		DatabaseContext result;
		{
			result = DatabaseContext.of(
					new ConnectionString(storeConfiguration.getMongoDbSettings().getUri()).getDatabase(),
					storeConfiguration.getDesignation() == StoreDesignation.COMMON
							? List.of(
							GenericObjectAttribute.ofSimple(
									BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
									KeySpaceReference
											.from((String) ((ObjectPropertyPredicate) storeConfiguration.getPredicate())
													.value())
											.toString()))
							: List.empty(),
					predicate != null ? removeSpaceReferencePredicate(predicate) : null);
		}
		return result;
	}

	@Nullable
	public static ObjectPropertyPredicate findSpaceReferencePredicate(Predicate predicate) {
		ObjectPropertyPredicate result;
		{
			if (predicate instanceof JunctionPredicate junctionPredicate) {
				if (junctionPredicate.isAnd()) {
					List<ObjectPropertyPredicate> predicates = junctionPredicate.predicates().stream()
							.map(BeholderMongoDbDatabaseLocator::findSpaceReferencePredicate)
							.filter(Objects::nonNull)
							.collect(List.collector());
					//
					if (predicates.size() > 1) {
						throw new NotImplementedException();
					} else {
						result = !predicates.isEmpty() ? predicates.single() : null;
					}
				} else {
					throw new NotImplementedException();
				}
			} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
				if (Objects.equals(
						objectPropertyPredicate.property(),
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME)) {
					result = objectPropertyPredicate;
				} else {
					result = null;
				}
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Nullable
	public static Predicate removeSpaceReferencePredicate(Predicate predicate) {
		Predicate result;
		{
			if (predicate instanceof JunctionPredicate junctionPredicate) {
				result = new JunctionPredicate(
						junctionPredicate.operator(),
						junctionPredicate.predicates().stream()
								.map(BeholderMongoDbDatabaseLocator::removeSpaceReferencePredicate)
								.filter(Objects::nonNull)
								.collect(Collectors.toList()));
			} else if (predicate instanceof PropertyPredicate propertyPredicate) {
				if (Objects.equals(
						propertyPredicate.property(),
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME)) {
					result = null;
				} else {
					result = propertyPredicate;
				}
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
