package tk.labyrinth.satool.beholder.domain.store.directory;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class DirectoryModelMapping {

	List<DirectoryAttributeMapping> attributeMappings;

	DirectoryModelMappingKind kind;

	String location;

	String modelCode;
}
