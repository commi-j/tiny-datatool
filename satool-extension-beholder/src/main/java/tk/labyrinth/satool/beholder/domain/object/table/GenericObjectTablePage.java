package tk.labyrinth.satool.beholder.domain.object.table;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import io.vavr.collection.HashMap;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.misc4j2.java.io.IoUtils;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.TablePathNameObjectModelReference;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.component.selector.SelectorBoxRenderer;
import tk.labyrinth.pandora.ui.paginator.Paginator;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.domain.space.reference.VaadinUiSpaceReferenceExecutionContextAttributeContributor;
import tk.labyrinth.satool.beholder.root.BeholderRootLayout;

import javax.annotation.CheckForNull;
import java.time.Instant;
import java.util.Objects;

@RequiredArgsConstructor
@Route(value = "", layout = BeholderRootLayout.class)
@Slf4j
public class GenericObjectTablePage extends CssVerticalLayout implements HasUrlParameter<String> {

	private final Label fetchTimeWindowLabel = new Label();

	private final H4 nameHeader = new H4();

	private final ObjectMapper objectMapper;

	private final Paginator paginator = new Paginator();

	private final TablePredicateHandler predicateHandler;

	private final SelectorBoxRenderer selectorBoxRenderer;

	private final CssHorizontalLayout selectorLayout = new CssHorizontalLayout();

	private final VaadinUiSpaceReferenceExecutionContextAttributeContributor spaceReferenceProvider;

	private final Observable<State> stateObservable = Observable.notInitialized();

	private final ObjectProvider<GenericObjectTable> tableProvider;

	@CheckForNull
	private ObjectModel currentModel = null;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	private GenericObjectTable table;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			stateObservable.subscribe(nextState -> {
				logger.debug("State change: {}", nextState);
				//
				if (!Objects.equals(nextState.getObjectModel(), currentModel)) {
					rerender();
					currentModel = nextState.getObjectModel();
				}
				//
				{
					updateFetchTimeWindowLabel(fetchTimeWindowLabel, nextState.getFetchTimeWindow());
				}
				{
					nameHeader.setText(nextState.getObjectModel().getName() + "s");
				}
				{
					paginator.render(Paginator.Properties.builder()
							.onSelectedPageIndexChange(nextPageIndex -> stateObservable.update(currentState ->
									currentState.withPageIndex(nextPageIndex)))
							.pageCount((int) Math.ceil(nextState.getTotalCount() / (double) nextState.getPageSize()))
							.selectedPageIndex(nextState.getPageIndex())
							.build());
				}
				{
					selectorLayout.removeAll();
					//
					Component selectorComponent = selectorBoxRenderer.render(builder -> builder
							.onValueChange(nextValue -> stateObservable.update(currentState ->
									currentState.withPredicate(nextValue)))
							.value(nextState.getPredicate())
							.build());
					CssFlexItem.setFlexGrow(selectorComponent, 1);
					selectorLayout.add(selectorComponent);
				}
				table.render(createTableProperties(nextState, stateObservable));
				//
				// FIXME: Update uiState instead?
				UI.getCurrent().getPage().getHistory().replaceState(null, stateToLocation(nextState));
			});
		}
	}

	private void rerender() {
		{
			removeAll();
		}
		{
			{
				CssHorizontalLayout header = new CssHorizontalLayout();
				{
					header.addClassNames(PandoraStyles.LAYOUT);
					//
					header.setAlignItems(AlignItems.BASELINE);
				}
				{
					header.add(nameHeader);
				}
				{
					CssFlexItem.setFlexGrow(selectorLayout, 1);
					header.add(selectorLayout);
				}
				{
					Button refreshButton = new Button(
							VaadinIcon.REFRESH.create(),
							event -> table.render(createTableProperties(
									stateObservable.get().withFetchRequestedAt(Instant.now()),
									stateObservable)));
					refreshButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
					refreshButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
					header.add(refreshButton);
				}
				add(header);
			}
			{
				table = tableProvider.getObject();
				CssFlexItem.setFlexGrow(table.asVaadinComponent(), 1);
				add(table.asVaadinComponent());
			}
			{
				CssGridLayout footer = new CssGridLayout();
				{
					footer.setAlignItems(tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.AlignItems.BASELINE);
					footer.setGridTemplateColumns("1fr 1fr 1fr");
				}
				{
					{
						footer.add(fetchTimeWindowLabel);
					}
					{
						footer.add(paginator);
					}
				}
				add(footer);
			}
		}
	}

	public Map<String, String> predicateToMap(Predicate predicate) {
		Map<String, String> result;
		{
			JunctionPredicate junctionPredicate = (JunctionPredicate) predicate;
			//
			if (!junctionPredicate.isAnd()) {
				throw new IllegalArgumentException("Require AND: %s".formatted(predicate));
			}
			//
			if (!junctionPredicate.getOperands().isEmpty()) {
				String predicateString = IoUtils.unchecked(() -> objectMapper.writeValueAsString(predicate));
				//
				return !predicateString.isEmpty()
						? LinkedHashMap.of("predicate", predicateString)
						: HashMap.empty();
			} else {
				result = HashMap.empty();
			}
		}
		return result;
	}

	@Override
	public void setParameter(BeforeEvent event, String parameter) {
		ObjectModel objectModel = objectModelSearcher.findSingle(TablePathNameObjectModelReference.of(parameter));
		if (objectModel != null) {
			stateObservable.set(State.builder()
					.fetchRequestedAt(Instant.now())
					.gridSortOrders(List.empty())
					.objectModel(objectModel)
					.pageIndex(0)
					.pageSize(20)
					.predicate(predicateHandler.extractPredicate(event.getLocation()))
					.spaceReference(spaceReferenceProvider.provideSpaceReference())
					.totalCount(0)
					.build());
		} else {
			removeAll();
			//
			add("Not found: " + parameter);
		}
	}

	public Location stateToLocation(State state) {
		return new Location(
				"beholder/%s".formatted(state.getObjectModel().getTablePathName()),
				new QueryParameters(predicateToMap(state.getPredicate())
						.mapValues(value -> List.of(value).asJava())
						.toJavaMap()));
	}

	public static GenericObjectTable.Properties createTableProperties(
			State state,
			Observable<State> stateObservable) {
		return GenericObjectTable.Properties.builder()
				.fetchRequestedAt(state.getFetchRequestedAt())
				.gridSortOrders(state.getGridSortOrders())
				.objectModel(state.getObjectModel())
				.onFetchTimeWindowChange(nextFetchTimeWindow -> stateObservable.update(currentState ->
						currentState.withFetchTimeWindow(nextFetchTimeWindow)))
				.onGridSortOrdersChange(nextGridSortOrders -> stateObservable.update(currentState ->
						currentState.withGridSortOrders(nextGridSortOrders)))
				.onPredicateChange(nextPredicate -> stateObservable.update(currentState ->
						currentState.withPredicate(nextPredicate)))
				.onTotalCountChange(nextTotalCount -> stateObservable.update(currentState ->
						currentState.withTotalCount(nextTotalCount)))
				.pageIndex(state.getPageIndex())
				.pageSize(state.getPageSize())
				.predicate(state.getPredicate())
				.spaceReference(state.getSpaceReference())
				.build();
	}

	public static void updateFetchTimeWindowLabel(Label label, @CheckForNull TimeWindow fetchTimeWindow) {
		{
			if (fetchTimeWindow != null) {
				if (fetchTimeWindow.getFinishedAt() != null) {
					label.setText("Fetch finished at %s".formatted(fetchTimeWindow.getFinishedAt()));
					ElementUtils.setTitle(label, "It took %s ms".formatted(fetchTimeWindow.getDuration().toMillis()));
				} else {
					label.setText("Fetch started at %s".formatted(fetchTimeWindow.getStartedAt()));
					ElementUtils.setTitle(label, null);
				}
			} else {
				label.setText("No fetch");
				ElementUtils.setTitle(label, null);
			}
		}
	}

	// TODO: Sync with UI state.
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	private static class PrivateState {

		/**
		 * External fetch request;
		 */
		Instant fetchRequestedAt;

		/**
		 * Internal time window reported by table.
		 */
		@CheckForNull
		TimeWindow fetchTimeWindow;

		ObjectModel objectModel;

		PublicState publicState;

		int totalCount;
	}

	// TODO: Sync with UI state.
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	private static class PublicState {

		List<GridSortOrder<?>> gridSortOrders;

		int pageIndex;

		int pageSize;

		Predicate predicate;

		KeySpaceReference spaceReference;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	private static class State {

		/**
		 * External fetch request;
		 */
		Instant fetchRequestedAt;

		/**
		 * Internal time window reported by table.
		 */
		@CheckForNull
		TimeWindow fetchTimeWindow;

		List<GridSortOrder<?>> gridSortOrders;

		ObjectModel objectModel;

		int pageIndex;

		int pageSize;

		Predicate predicate;

		KeySpaceReference spaceReference;

		int totalCount;
	}
}
