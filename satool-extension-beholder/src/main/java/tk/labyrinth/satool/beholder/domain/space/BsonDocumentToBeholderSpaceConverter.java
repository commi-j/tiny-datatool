package tk.labyrinth.satool.beholder.domain.space;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.storage.mongodb.SelfProvidingConverter;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.time.Instant;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class BsonDocumentToBeholderSpaceConverter implements SelfProvidingConverter<Document, BeholderSpace> {

	private final ObjectProvider<ConversionService> conversionServiceProvider;

	private final Set<String> hardcodedKeys = HashSet.of("_id", "key", "updatedAt", "version");

	private GenericObject readObject(
			ConversionService conversionService,
			KeySpaceReference spaceReference,
			CodeObjectModelReference objectModelReference,
			Object object) {
		GenericObject beholderObject = conversionService.convert(object, GenericObject.class);
		Objects.requireNonNull(beholderObject, "beholderObject");
		return beholderObject
				.withNonNullSimpleAttribute(
						RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME,
						objectModelReference.toString())
				.withNonNullSimpleAttribute(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
						spaceReference.toString());
	}

	@Override
	@SuppressWarnings("unchecked")
	public BeholderSpace convert(Document source) {
		ConversionService conversionService = conversionServiceProvider.getObject();
		//
		String key = source.getString("key");
		String updatedAtString = source.getString("updatedAt");
		//
		KeySpaceReference spaceReference = KeySpaceReference.of(key);
		Instant updatedAt = updatedAtString != null ? Instant.parse(updatedAtString) : null;
		//
		return BeholderSpace.builder()
				.key(key)
				.objects(Stream.ofAll(source.entrySet())
						.filter(entry -> !hardcodedKeys.contains(entry.getKey()))
						.flatMap(entry -> {
							CodeObjectModelReference objectModelReference = CodeObjectModelReference.of(entry.getKey());
							java.util.List<Object> values = (java.util.List<Object>) entry.getValue();
							return Stream.ofAll(values).map(value ->
									readObject(conversionService, spaceReference, objectModelReference, value));
						})
						.toList())
				.uid(source.getString("_id"))
				.updatedAt(updatedAt)
				.version(source.getString("version"))
				.build();
	}
}
