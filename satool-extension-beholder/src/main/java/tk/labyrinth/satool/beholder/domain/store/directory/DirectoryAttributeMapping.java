package tk.labyrinth.satool.beholder.domain.store.directory;

import lombok.Value;

@Value(staticConstructor = "of")
public class DirectoryAttributeMapping {

	String name;

	String path;
}
