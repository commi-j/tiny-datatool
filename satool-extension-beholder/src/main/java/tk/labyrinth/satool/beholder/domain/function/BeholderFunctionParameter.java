package tk.labyrinth.satool.beholder.domain.function;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;

@Builder(toBuilder = true)
@Value
public class BeholderFunctionParameter {

	String name;

	Datatype type;
}
