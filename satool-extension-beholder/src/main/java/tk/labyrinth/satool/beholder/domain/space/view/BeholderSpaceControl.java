package tk.labyrinth.satool.beholder.domain.space.view;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.domain.space.reference.VaadinUiSpaceReferenceExecutionContextAttributeContributor;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class BeholderSpaceControl extends CssVerticalLayout {

	private final VaadinUiSpaceReferenceExecutionContextAttributeContributor spaceReferenceProvider;

	private final ValueBoxRegistry valueBoxRegistry;

	@PostConstruct
	@SuppressWarnings("unchecked")
	private void postConstruct() {
		KeySpaceReference keySpaceReference = spaceReferenceProvider.getSpaceReferenceObservable().get();
		{
			MutableValueBox<KeySpaceReference> spaceReferenceBox = (MutableValueBox<KeySpaceReference>) valueBoxRegistry
					.getValueBox(KeySpaceReference.class);
			//
			spaceReferenceBox.render(MutableValueBox.Properties.<KeySpaceReference>builder()
					.currentValue(keySpaceReference)
					.initialValue(keySpaceReference)
					.label("Space")
					.onValueChange(next -> {
						spaceReferenceProvider.getSpaceReferenceObservable().set(next);
//					UI.getCurrent().getPage().reload(); // FIXME: Keep spaceKey in url.
					})
					.build());
			//
			add(spaceReferenceBox.asVaadinComponent());
		}
	}
}
