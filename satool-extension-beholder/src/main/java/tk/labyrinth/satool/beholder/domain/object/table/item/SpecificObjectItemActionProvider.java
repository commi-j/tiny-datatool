package tk.labyrinth.satool.beholder.domain.object.table.item;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Color;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import javax.annotation.Nullable;
import java.util.Objects;

@Deprecated // See TypedObjectActionProvider in Pandora.
public abstract class SpecificObjectItemActionProvider<T> implements GenericObjectItemActionProvider, TypeAware<T> {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	private Class<T> javaClass;

	private CodeObjectModelReference objectModelReference;

	@PostConstruct
	private void postConstruct() {
		javaClass = getParameterClass();
		objectModelReference = javaBaseTypeRegistry.getNormalizedObjectModelReference(javaClass);
	}

	@Nullable
	protected abstract Component provideItemAction(T item);

	@Nullable
	@Override
	public Component provideItemAction(GenericObject item) {
		Component result;
		{
			if (Objects.equals(RootObjectUtils.getModelReference(item), objectModelReference)) {
				result = Try
						.ofSupplier(() -> provideItemAction(converterRegistry.convert(item, javaClass)))
						.recover(fault -> {
							Label label = new Label(fault.toString());
							label.setWidth("8em");
							StyleUtils.setCssProperty(label, Color.RED);
							return label;
						})
						.get();
			} else {
				result = null;
			}
		}
		return result;
	}
}
