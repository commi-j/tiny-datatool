package tk.labyrinth.satool.beholder.domain.uiconfiguration;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;

@Builder(toBuilder = true)
@Model(BeholderUiConfiguration.MODEL_CODE)
@Value
public class BeholderUiConfiguration {

	public static final String MODEL_CODE = "uiconfiguration";

	public static final String PRIORITY_ATTRIBUTE_NAME = "priority";

	public static final String SCOPE_ATTRIBUTE_NAME = "scope";

	List<BeholderMenuConfiguration> menuConfigurations;

	Integer priority;

	String scope;
}
