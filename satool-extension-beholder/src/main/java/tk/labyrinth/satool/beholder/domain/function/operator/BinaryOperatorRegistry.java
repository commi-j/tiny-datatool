package tk.labyrinth.satool.beholder.domain.function.operator;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;

@Component
@RequiredArgsConstructor
public class BinaryOperatorRegistry {

	private final java.util.List<BinaryOperatorDescriptor<?>> operatorDescriptors;

	public BinaryOperatorDescriptor<?> getOperatorDescriptor(Class<?> firstOperandClass, String operatorText) {
		return Stream.ofAll(operatorDescriptors)
				.filter(operatorDescriptor -> operatorDescriptor.supports(firstOperandClass))
				.filter(operatorDescriptor -> StringUtils.containsIgnoreCase(operatorDescriptor.getText(), operatorText))
				.get();
	}

	public <T> List<BinaryOperatorDescriptor<T>> getOperatorDescriptors(Class<T> firstOperandClass) {
		return List.ofAll(operatorDescriptors)
				.filter(operatorDescriptor -> operatorDescriptor.supports(firstOperandClass))
				.map(ClassUtils::castInferred);
	}
}
