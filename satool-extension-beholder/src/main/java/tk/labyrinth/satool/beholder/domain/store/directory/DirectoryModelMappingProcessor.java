package tk.labyrinth.satool.beholder.domain.store.directory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.vavr.collection.List;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.value.wrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.exception.ExceptionUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.text.TextUtils;
import tk.labyrinth.pandora.pattern.PandoraPattern;

import javax.annotation.CheckForNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;

@Component
public class DirectoryModelMappingProcessor {

	private final ObjectMapper yamlObjectMapper = new ObjectMapper(new YAMLFactory());

	public File getFileFor(DirectoryModelMapping mapping, GenericObject object) {
		throw new NotImplementedException();
	}

	public List<GenericObject> readAll(DirectoryModelMapping mapping, Path directory) {
		List<GenericObject> result;
		{
			if (mapping.getKind() == DirectoryModelMappingKind.OBJECT_PER_FILE) {
				List<File> objectFiles = getMatchingFiles(directory, mapping.getLocation());
				result = objectFiles.map(objectFile -> readObjectFile(mapping, objectFile));
			} else {
				throw new NotImplementedException(mapping.getKind() + "");
			}
		}
		return result;
	}

	public GenericObject readObjectFile(DirectoryModelMapping mapping, File objectFile) {
		ObjectNode objectNode = IoUtils.unchecked(() -> yamlObjectMapper.readValue(objectFile, ObjectNode.class));
		return GenericObject.of(mapping.getAttributeMappings()
				.toList()
				.map(attributeMapping -> readAttribute(objectNode, attributeMapping))
				.filter(Objects::nonNull));
	}

	public static List<File> getMatchingFiles(Path rootPath, String location) {
		List<File> result;
		{
			try {
				String discoveryPattern = replacePlaceholdersWithWildcards(location);
				result = Files.walk(rootPath)
						.filter(path -> TextUtils.matchesMask(
								normalizeRelativePath(rootPath.relativize(path)),
								discoveryPattern))
						.map(Path::toFile)
						.sorted(Comparator.comparing(File::toString))
						.collect(List.collector());
				//
				System.out.println();
			} catch (IOException ex) {
				throw new RuntimeException();
			}
		}
		return result;
	}

	public static String normalizeRelativePath(Path relativePath) {
		String relativePathString = relativePath.toString();
		return !relativePathString.isEmpty()
				? "./%s".formatted(relativePathString.replace("\\", "/"))
				: ".";
	}

	@CheckForNull
	public static GenericObjectAttribute readAttribute(
			ObjectNode objectNode,
			DirectoryAttributeMapping attributeMapping) {
		ValueWrapper value;
		{
			JsonNode finalNode = Stream.of(attributeMapping.getPath().split("\\.")).reduce(
					(JsonNode) objectNode,
					(currentNode, attributeSegmentName) ->
							currentNode != null ? currentNode.get(attributeSegmentName) : null,
					FunctionUtils::throwUnreachableStateException);
			if (finalNode != null) {
				value = readValue(finalNode);
			} else {
				value = null;
			}
		}
		return value != null ? GenericObjectAttribute.of(attributeMapping.getName(), value) : null;
	}

	public static ValueWrapper readValue(JsonNode jsonNode) {
		ValueWrapper result;
		{
			if (jsonNode.isArray()) {
				result = ListValueWrapper.of(List.ofAll(StreamUtils.from(jsonNode.elements()))
						.map(DirectoryModelMappingProcessor::readValue));
			} else if (jsonNode.isObject()) {
				result = ObjectValueWrapper.of(GenericObject.of(List.ofAll(StreamUtils.from(jsonNode.fields()))
						.map(field -> GenericObjectAttribute.of(field.getKey(), readValue(field.getValue())))));
			} else if (jsonNode.isValueNode()) {
				String text = jsonNode.asText();
				result = !text.isEmpty() ? SimpleValueWrapper.of(text) : null;
			} else {
				throw new UnsupportedOperationException(ExceptionUtils.render(jsonNode));
			}
		}
		return result;
	}

	public static String replacePlaceholdersWithWildcards(String patternString) {
		return PandoraPattern.from(patternString).render(variableName -> "*");
	}
}
