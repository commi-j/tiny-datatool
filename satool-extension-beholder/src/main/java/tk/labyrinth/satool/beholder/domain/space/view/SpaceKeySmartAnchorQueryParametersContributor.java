package tk.labyrinth.satool.beholder.domain.space.view;

import com.vaadin.flow.router.QueryParameters;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.UiScopedComponent;
import tk.labyrinth.pandora.ui.component.anchor.SmartAnchorQueryParametersContributor;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.domain.space.reference.VaadinUiSpaceReferenceExecutionContextAttributeContributor;

@RequiredArgsConstructor
@UiScopedComponent
public class SpaceKeySmartAnchorQueryParametersContributor implements SmartAnchorQueryParametersContributor {

	private final VaadinUiSpaceReferenceExecutionContextAttributeContributor spaceReferenceProvider;

	@Override
	public QueryParameters contributeQueryParameters() {
		KeySpaceReference spaceReference = spaceReferenceProvider.provideSpaceReference();
		return spaceReference != null
				? QueryParameters.fromString("space=%s".formatted(spaceReference.getKey()))
				: QueryParameters.empty();
	}
}
