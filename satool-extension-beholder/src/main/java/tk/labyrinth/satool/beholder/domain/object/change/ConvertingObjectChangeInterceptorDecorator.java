package tk.labyrinth.satool.beholder.domain.object.change;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;

// FIXME: Could not make bean because if we do Spring would try to instantiate it to inject in List<Interceptor>.
//@PrototypeScopedComponent
@RequiredArgsConstructor
public class ConvertingObjectChangeInterceptorDecorator<E, I> implements ObjectChangeInterceptor<E> {

	private final ConversionService conversionService;

	private final Class<E> externalClass;

	private final Class<I> internalClass;

	private final ObjectChangeInterceptor<I> internalInterceptor;

	@Override
	public E interceptObjectChange(E current, E next) {
		I internalCurrent = conversionService.convert(current, internalClass);
		I internalNext = conversionService.convert(next, internalClass);
		//
		I internalResult = internalInterceptor.interceptObjectChange(internalCurrent, internalNext);
		//
		E result = conversionService.convert(internalResult, externalClass);
		//
		return result;
	}
}
