package tk.labyrinth.satool.beholder.domain.function;

import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;

@Value
public class BeholderFunction {

	String name;

	List<BeholderFunctionParameter> parameters;

	Datatype scopeType;

	String signature;
}
