package tk.labyrinth.satool.beholder.domain.object.table.item;

import com.vaadin.flow.component.Component;
import tk.labyrinth.pandora.datatypes.object.GenericObject;

import javax.annotation.Nullable;

@Deprecated // See Pandora.
public interface GenericObjectItemActionProvider {

	@Nullable
	Component provideItemAction(GenericObject item);
}
