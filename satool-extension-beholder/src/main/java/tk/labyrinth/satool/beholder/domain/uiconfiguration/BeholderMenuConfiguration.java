package tk.labyrinth.satool.beholder.domain.uiconfiguration;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class BeholderMenuConfiguration {

	String filterTag;
}
