package tk.labyrinth.satool.beholder.base;

public class BeholderConstants {

	public static final String BEHOLDER_TAG = "beholder";

	public static final String DEFAULT_SPACE = "default";

	public static final String UTILITY_SPACE = "utility";
}
