package tk.labyrinth.satool.beholder.domain.accessibleresource;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.core.PandoraConstants;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.util.UUID;

/**
 * <a href="https://en.wikipedia.org/wiki/Web_resource">https://en.wikipedia.org/wiki/Web_resource</a>
 */
@Builder(builderClassName = "Builder", toBuilder = true)
@Model
@ModelTag(PandoraConstants.PANDORA_MODEL_TAG_NAME)
@RenderAttribute({"name", "url"})
@Value
@With
public class WebResource implements HasUid {

	@Nullable
	KeyCredentialsReference credentialsReference;

	// TODO: Maintain list of possible values.
	// TODO: Make suggests from url.
	// TODO: Probably rework into tags.
	String kind;

	String name;

	UUID uid;

	String url;
}
