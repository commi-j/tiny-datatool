package tk.labyrinth.satool.beholder.domain.object.table;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.router.Location;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.misc4j2.java.io.IoUtils;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodelattribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperator;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperatorDescriptor;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperatorRegistry;
import tk.labyrinth.satool.beholder.domain.function.operator.EqualsOperatorDescriptor;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class TablePredicateHandler {

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ObjectMapper objectMapper;

	private final BinaryOperatorRegistry operatorRegistry;

	public Predicate extractPredicate(Location location) {
		Predicate result;
		{
			java.util.List<String> queryString = location.getQueryParameters().getParameters().get("predicate");
			if (queryString != null) {
				result = queryStringToPredicate(List.ofAll(queryString).single());
			} else {
				result = Predicates.and();
			}
		}
		return result;
	}

	public TablePredicate extractTablePredicate(ObjectModel objectModel, Location location) {
		TablePredicate result;
		{
			java.util.List<String> queryString = location.getQueryParameters().getParameters().get("predicate");
			if (queryString != null) {
				result = queryStringToTablePredicate(objectModel, List.ofAll(queryString).single());
			} else {
				result = TablePredicate.empty();
			}
		}
		return result;
	}

	public String predicateToPrettyString(TablePredicate predicate) {
		throw new NotImplementedException();
	}

	public String predicateToQueryString(TablePredicate predicate) {
		return predicate.getColumns()
				.map(entry -> "%s_%s_%s".formatted(
						entry._1(),
						entry._2().getOperator().getDescriptor().getText(),
						entry._2().getValue()))
				.mkString("_AND_");
	}

	public TablePredicate prettyStringToPredicate(String prettyString) {
		throw new NotImplementedException();
	}

	public Predicate queryStringToPredicate(String queryString) {
		return IoUtils.unchecked(() -> objectMapper.readValue(queryString, Predicate.class));
	}

	public TablePredicate queryStringToTablePredicate(ObjectModel objectModel, String queryString) {
		List<String> segments = List.of(queryString.split("_AND_"));
		//
		return TablePredicate.of(segments
				.map(segment -> resolveColumnPredicate(objectModel, segment))
				.toLinkedMap(Pair::getLeft, Pair::getRight));
	}

	public String referenceToQueryString(GenericReference<?> reference) {
		return predicateToQueryString(TablePredicate.of(reference.getAttributes().toLinkedMap(
				GenericReferenceAttribute::getName,
				attribute -> ColumnPredicate.of(
						BinaryOperator.of(ClassUtils.castInferred(operatorRegistry.getOperatorDescriptor(
								String.class,
								EqualsOperatorDescriptor.TEXT))),
						attribute.getValue()))));
	}

	public String referenceToQueryString(Reference<?> reference) {
		return referenceToQueryString(GenericReference.from(reference));
	}

	public Pair<String, ColumnPredicate<?>> resolveColumnPredicate(
			ObjectModel objectModel,
			String columnPredicateString) {
		List<String> segments = List.of(columnPredicateString.split("_", 3));
		//
		String attributeName = segments.get(0);
		ObjectModelAttribute attribute = objectModel.getAttributes()
				.find(attributeElement -> Objects.equals(attributeElement.getName(), attributeName))
				.get();
		Class<?> attributeClass = javaBaseTypeRegistry.getJavaClass(attribute.getDatatype().getBaseReference());
		String operatorText = segments.get(1);
		BinaryOperatorDescriptor<?> operatorDescriptor = operatorRegistry.getOperatorDescriptor(
				attributeClass,
				operatorText);
		//
		return Pair.of(attributeName, createColumnPredicate(operatorDescriptor, segments.get(2)));
	}

	@SuppressWarnings("unchecked")
	private static <T> ColumnPredicate<T> createColumnPredicate(
			BinaryOperatorDescriptor<?> operatorDescriptor,
			Object value) {
		return ColumnPredicate.of(
				BinaryOperator.of((BinaryOperatorDescriptor<T>) operatorDescriptor),
				(T) value);
	}
}
