package tk.labyrinth.satool.beholder.root;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.activities.activity.PandoraActivityFeedView;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BackgroundColor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.ZIndex;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.PlaceSelf;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.anchor.SmartAnchorRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;
import tk.labyrinth.pandora.uiapplication.layout.PandoraRootLayout;
import tk.labyrinth.pandora.uiapplication.navbar.ProvidesNavbarComponents;
import tk.labyrinth.pandora.uiapplication.sidebar.ProvidesSidebarComponents;
import tk.labyrinth.satool.beholder.domain.space.reference.VaadinUiSpaceReferenceExecutionContextAttributeContributor;
import tk.labyrinth.satool.beholder.domain.space.view.BeholderSpaceControl;
import tk.labyrinth.satool.beholder.tool.menu.BeholderMenuFolderView;

@ParentLayout(PandoraRootLayout.class)
@RequiredArgsConstructor
@RoutePrefix("beholder")
public class BeholderRootLayout extends CssGridLayout implements
		ProvidesNavbarComponents,
		ProvidesSidebarComponents,
		RouterLayout {

	private final ObjectProvider<PandoraActivityFeedView> activityFeedViewProvider;

	private final SmartAnchorRenderer anchorRenderer;

	private final BeholderMenuFolderView menuFolderView;

	private final BeholderSpaceControl spaceControl;

	private final VaadinUiSpaceReferenceExecutionContextAttributeContributor spaceReferenceProvider;

	@PostConstruct
	private void postConstruct() {
		setGridTemplateAreas("content");
	}

	@Override
	public List<PandoraAttachableComponentWrapper> provideNavbarComponents() {
		Observable<Boolean> activityFeedVisibleObservable = Observable.withInitialValue(false);
		//
		activityFeedVisibleObservable.subscribe(next -> {
			if (next) {
				PandoraActivityFeedView activityFeedView = activityFeedViewProvider.getObject();
				{
					activityFeedView.addClassNames(PandoraStyles.CARD);
					//
					StyleUtils.setCssProperty(activityFeedView, BackgroundColor.WHITE);
					CssGridItem.setJustifySelf(activityFeedView, PlaceSelf.END);
					activityFeedView.setMinWidth("10em");
					{
						// FIXME: This is a layout crutch. We need >0 because of position static-vs-relative,
						//  but >1 because of z-index=1 for Tab button.
						StyleUtils.setCssProperty(activityFeedView, ZIndex.of(2));
					}
				}
				add(activityFeedView, "content");
			} else {
				List<Component> activityFeedViews = getChildren()
						.filter(child -> child.hasClassName(PandoraActivityFeedView.CLASS_NAME))
						.collect(List.collector());
				//
				remove(activityFeedViews.asJava());
			}
		});
		//
		return List.of(
				PandoraAttachableComponentWrapper.builder()
						.component(FunctionalComponent.createWithExternalObservable(
								activityFeedVisibleObservable,
								(state, sink) -> ButtonRenderer.render(builder -> builder
										.onClick(event -> sink.accept(!state))
										.text("Activities")
										.themeVariants((state
												? List.of(ButtonVariant.LUMO_PRIMARY)
												: List.<ButtonVariant>empty())
												.asJava())
										.build())
						))
						.placeAtEnd(true)
						.build()
		);
	}

	@Override
	public List<PandoraAttachableComponentWrapper> provideSidebarComponents() {
		return List.of(
				PandoraAttachableComponentWrapper.builder()
						.component(spaceControl)
						.build(),
				PandoraAttachableComponentWrapper.builder()
						.component(anchorRenderer.render(SmartAnchorRenderer.Properties.builder()
								.navigationTarget(BeholderRootPage.class)
								.text("Home")
								.build()))
						.build(),
				PandoraAttachableComponentWrapper.builder()
						.component(menuFolderView)
						.build());
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
//		CssFlexItem.setFlexGrow(content, 1);
		add((Component) content, "content");
	}
}
