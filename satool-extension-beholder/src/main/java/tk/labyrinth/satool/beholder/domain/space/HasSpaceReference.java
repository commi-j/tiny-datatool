package tk.labyrinth.satool.beholder.domain.space;

public interface HasSpaceReference {

	KeySpaceReference getSpaceReference();
}
