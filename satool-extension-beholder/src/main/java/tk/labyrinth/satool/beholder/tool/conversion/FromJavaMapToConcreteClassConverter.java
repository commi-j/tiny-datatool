package tk.labyrinth.satool.beholder.tool.conversion;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.Set;

/**
 * Map<String,?> -> (!abstract class) ?
 */
// TODO: Check parameters of source.
// TODO: Support parameters of target.
@Component
@Deprecated // Yet it is used to convert bson Document to GenericObject in MongoStores.
@Order
@RequiredArgsConstructor
public class FromJavaMapToConcreteClassConverter implements ConditionalGenericConverter {

	private final ObjectMapper objectMapper;

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		return objectMapper.convertValue(source, targetType.getType());
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return null;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		Class<?> targetClass = targetType.getType();
		//
		return Map.class.isAssignableFrom(sourceType.getType()) &&
				!targetClass.isInterface() &&
				!Modifier.isAbstract(targetClass.getModifiers());
	}
}
