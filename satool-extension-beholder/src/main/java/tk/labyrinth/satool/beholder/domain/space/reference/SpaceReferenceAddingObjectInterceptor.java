package tk.labyrinth.satool.beholder.domain.space.reference;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectmanipulator.GenericObjectManipulatorInterceptor;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class SpaceReferenceAddingObjectInterceptor implements GenericObjectManipulatorInterceptor {

	private final ExecutionContextHandler executionContextHandler;

	private GenericObject enrichWithSpaceReference(GenericObject object) {
		GenericObject result;
		{
			KeySpaceReference contextSpaceReference = executionContextHandler.getExecutionContext()
					.findAttributeValueInferred(BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME);
			//
			if (contextSpaceReference != null) {
				GenericObjectAttribute spaceReferenceAttribute = object.findAttribute(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME);
				//
				if (spaceReferenceAttribute == null) {
					// Adding.
					//
					result = object.withNonNullSimpleAttribute(
							BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
							contextSpaceReference.toString());
				} else if (Objects.equals(
						spaceReferenceAttribute.getValue().asSimple().unwrap(),
						contextSpaceReference.toString())) {
					// Already exists.
					//
					result = object;
				} else {
					// Different spaces.
					//
					throw new IllegalArgumentException("Different spaces TODO");
				}
			} else {
				// Nothing to add.
				//
				result = object;
			}
		}
		return result;
	}

	@Override
	public GenericObject onCreate(GenericObject object) {
		return enrichWithSpaceReference(object);
	}

	@Override
	public GenericObject onCreateOrUpdate(GenericObject object) {
		return enrichWithSpaceReference(object);
	}

	@Override
	public GenericObject onUpdate(GenericObject object) {
		return enrichWithSpaceReference(object);
	}
}
