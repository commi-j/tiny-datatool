package tk.labyrinth.satool.beholder.domain.space;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.ui.component.reference.ReferenceItem;
import tk.labyrinth.pandora.ui.component.reference.ReferenceRendererBase;
import tk.labyrinth.pandora.ui.component.reference.ReferenceTool;

import javax.annotation.Nullable;

@Component
@Lazy
public class BeholderSpaceReferenceTool extends ReferenceRendererBase<BeholderSpace, KeySpaceReference> implements
		ReferenceTool<BeholderSpace, KeySpaceReference> {

	@Nullable
	@Override
	public Predicate buildSearchPredicate(String text) {
		return Predicates.contains("key", text, false);
	}

	@Nullable
	@Override
	public ReferenceItem<KeySpaceReference> renderNonNullObject(BeholderSpace value) {
		return ReferenceItem.<KeySpaceReference>builder()
				.reference(KeySpaceReference.of(value.getKey()))
				.text(value.getKey())
				.build();
	}
}
