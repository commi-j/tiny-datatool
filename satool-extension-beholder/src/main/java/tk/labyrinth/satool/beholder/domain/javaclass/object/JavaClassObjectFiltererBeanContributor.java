package tk.labyrinth.satool.beholder.domain.javaclass.object;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

import javax.annotation.CheckForNull;
import java.lang.reflect.Type;

@Component
@RequiredArgsConstructor
public class JavaClassObjectFiltererBeanContributor extends
		UnaryParameterizedBeanContributor<JavaClassObjectFilterer<?>> {

	@CheckForNull
	@Override
	protected Object doContributeBean(
			BeanFactory beanFactory,
			Class<? extends JavaClassObjectFilterer<?>> base,
			Type parameter) {
		return beanFactory
				.withBean(TypeUtils.getClass(parameter))
				.withSuppressedBeanContributor(this)
				.getBean(JavaClassObjectFilterer.class);
	}

	@CheckForNull
	@Override
	protected Integer doGetSupportDistance(Class<? extends JavaClassObjectFilterer<?>> base, Type parameter) {
		return Integer.MAX_VALUE;
	}
}
