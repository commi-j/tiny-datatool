package tk.labyrinth.satool.beholder.domain.object.table;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.provider.SortDirection;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.task.TaskExecutor;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodelattribute.ObjectModelAttribute;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.extra.bulkoperations.BulkEditCommand;
import tk.labyrinth.pandora.stores.extra.bulkoperations.BulkOperator;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.pandora.ui.component.bulkedit.BulkEditViewRenderer;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectMultiView;
import tk.labyrinth.pandora.ui.component.object.custom.CustomizeTable;
import tk.labyrinth.pandora.ui.component.objectaction.GenericObjectActionProvider;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;

import javax.annotation.CheckForNull;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

@Deprecated
@PrototypeScopedComponent
@RequiredArgsConstructor
@Slf4j
public class GenericObjectTable implements RenderableView<GenericObjectTable.Properties> {

	private final BeanContext beanContext;

	private final BulkEditViewRenderer bulkEditViewRenderer;

	private final BulkOperator bulkOperator;

	private final GenericObjectManipulator genericObjectManipulator;

	private final GenericObjectSearcher genericObjectSearcher;

	private final Grid<GenericObject> grid = new Grid<>();

	private final ObjectProvider<ColumnHeaderComponent<?>> headerProvider;

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	@Autowired(required = false)
	private final java.util.List<GenericObjectActionProvider> newActionProviders;

	private final ObjectMapper objectMapper;

	private final Observable<State> stateObservable = Observable.withInitialValue(State.builder()
			.fetchTimeWindow(null)
			.items(Try.success(List.empty()))
			.properties(null)
			.build());

	private final TableCellRendererRegistry tableCellRendererRegistry;

	private final TaskExecutor taskExecutor;

	boolean initialized = false;

	private List<Pair<ObjectModelAttribute, ColumnHeaderComponent<?>>> attributeAndHeaderPairs;

	@Deprecated
	private Button createButton(String name, VaadinIcon icon, Runnable action) {
		Button button = new Button(icon.create(), event -> action.run());
		button.getElement().setAttribute("name", name);
		button.addThemeVariants(ButtonVariant.LUMO_SMALL);
		button.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		//
		StyleUtils.setMargin(button, "0px");
		return button;
	}

	private Button createButton(String name, VaadinIcon icon, Runnable action, @CheckForNull String nonEditableReason) {
		return createButton(name, icon, action, null, nonEditableReason);
	}

	private Component createButtonsHeader(KeySpaceReference spaceReference, ObjectModel objectModel) {
		CodeObjectModelReference objectModelReference = CodeObjectModelReference.from(objectModel);
		GenericObject newObject = createNewObject(spaceReference, objectModel);
		//
		//
		CssHorizontalLayout result = new CssHorizontalLayout();
		{
			result.add(createButton(
					"create-object-button",
					VaadinIcon.PLUS,
					() -> showCreateFormDialog(objectModel, newObject),
					genericObjectManipulator.canManipulate(newObject)
							? null
							: "No StoreRoute to create new object specified"));
		}
		{
			result.add(createButton(
					"import-object-button",
					VaadinIcon.FILE_ADD,
					() -> {
						TextArea textArea = new TextArea("Import from JSON");
						textArea.setHeight("60vh");
						textArea.setWidth("60vw");
						//
						ConfirmationViews.showComponentDialog(textArea).subscribeAlwaysAccepted(next -> {
							GenericObject object = IoUtils.unchecked(() ->
									objectMapper.readValue(textArea.getValue(), GenericObject.class));
							//
							genericObjectManipulator.create(object
									.withMergedAttributes(newObject.getAttributes())
									.withAddedAttributes(RootObjectUtils.createNewUidAttribute()));
						});
					},
					genericObjectManipulator.canManipulate(newObject)
							? null
							: "No StoreRoute to import new object specified"));
		}
		{
			result.add(createButton(
					"bulk-edit-object-button",
					VaadinIcon.EDIT,
					() -> {
						Observable<BulkEditCommand> stateObservable = Observable.withInitialValue(BulkEditCommand.builder()
								.attributeName(null)
								.attributeValue(null)
								.modelReference(objectModelReference)
								.selector(this.stateObservable.get().getProperties().getPredicate())
								.build());
						ConfirmationViews
								.showFunctionDialog(
										stateObservable,
										nextState -> bulkEditViewRenderer.render(
												BulkEditViewRenderer.Properties.builder()
														.attributeName(nextState.getAttributeName())
														.attributeValue(nextState.getAttributeValue())
														.onAttributeNameChange(nextAttributeName ->
																stateObservable.update(currentState ->
																		currentState.withAttributeName(
																				nextAttributeName)))
														.onAttributeValueChange(nextNextAttributeValue ->
																stateObservable.update(currentState ->
																		currentState.withAttributeValue(
																				nextNextAttributeValue)))
														.onSelectorChange(nextSelector -> stateObservable.update(
																currentState ->
																		currentState.withSelector(nextSelector)))
														.selector(nextState.getSelector())
														.build()))
								.subscribeAlwaysAccepted(success -> {
									bulkOperator.bulkEdit(stateObservable.get());
									refresh();
								});
					},
					"Bulk Edit",
					genericObjectManipulator.canManipulate(newObject)
							? null
							: "No StoreRoute to import new object specified"));
		}
		return result;
	}

	@WithSpan
	private Pair<Integer, List<GenericObject>> doFetch(
			ObjectModel objectModel,
			Predicate predicate,
			List<GridSortOrder<?>> gridSortOrders,
			int offset,
			int limit) {
		ParameterizedQuery<CodeObjectModelReference> countQuery = ParameterizedQuery.<CodeObjectModelReference>builder()
				.parameter(CodeObjectModelReference.from(objectModel))
				.predicate(predicate)
				.build();
		//
		return Pair.of(
				(int) genericObjectSearcher.count(countQuery),
				genericObjectSearcher.search(countQuery.toBuilder()
						.limit((long) limit)
						.offset((long) offset)
						.sort(gridSortOrdersToSort(gridSortOrders))
						.build()));
	}

	@WithSpan
	private void doRender(State state) {
		if (!initialized) {
			configureGrid(
					grid,
					state.getProperties().getSpaceReference(),
					state.getProperties().getObjectModel());
			initialized = true;
		}
		//
		// TODO: If items is not Try.success, render error.
		List<GenericObject> items = state.getItems().getOrElse(List.empty());
		grid.setItems(items.asJava());
		grid.sort(ClassUtils.castInferred(state.getProperties().getGridSortOrders().asJava()));
		//
		renderColumnHeaders(state.getProperties(), attributeAndHeaderPairs);
	}

	@WithSpan
	private void fetchAndUpdateState(UI ui, Properties properties, Instant startedAt) {
		Try<Pair<Integer, List<GenericObject>>> fetchedItemsTry = Try.of(() -> doFetch(
				properties.getObjectModel(),
				properties.getPredicate(),
				properties.getGridSortOrders(),
				properties.getPageIndex() * properties.getPageSize(),
				properties.getPageSize()));
		{
			if (fetchedItemsTry.isFailure()) {
				logger.warn("", fetchedItemsTry.getCause());
			}
		}
		//
		TimeWindow nextFetchTimeWindow = TimeWindow.builder()
				.startedAt(startedAt)
				.finishedAt(Instant.now())
				.build();
		//
		ui.access(() -> {
			State stateAfterUpdate = stateObservable.update(currentState -> {
				State result;
				if (currentState.getFetchTimeWindow().getStartedAt() == startedAt) {
					logger.debug("Fetch successful: {}", startedAt);
					//
					result = currentState.toBuilder()
							.fetchTimeWindow(nextFetchTimeWindow)
							.items(fetchedItemsTry.map(Pair::getRight))
							.build();
				} else {
					logger.debug("Fetch outdated: {}", startedAt);
					//
					result = currentState;
				}
				return result;
			});
			//
			if (stateAfterUpdate.getFetchTimeWindow() == nextFetchTimeWindow) {
				properties.getOnTotalCountChange().accept(fetchedItemsTry.map(Pair::getLeft).getOrElse(0));
			}
		});
	}

	@WithSpan
	private void fetchAsync(Properties properties, Instant startedAt) {
		logger.debug("#fetchAsync: properties = {}, startedAt = {}", properties, startedAt);
		//
		UI ui = UI.getCurrent();
		taskExecutor.execute(() -> fetchAndUpdateState(ui, properties, startedAt));
	}

	@PostConstruct
	private void postConstruct() {
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT);
			grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
		}
		{
			grid.addSortListener(event -> stateObservable.get().getProperties().getOnGridSortOrdersChange()
					.accept(List.ofAll(event.getSortOrder())));
		}
		{
			stateObservable.getFlux()
					.filter(nextState -> nextState.getProperties() != null)
					.subscribe(nextState -> {
						logger.debug("State change: {}", nextState);
						//
						doRender(nextState);
					});
		}
		{
			stateObservable.getFlux()
					.filter(nextState -> nextState.getProperties() != null)
					.distinctUntilChanged(GenericObjectTable::createFetchComparisonObject)
					.subscribe(nextState -> {
						logger.debug("Before refresh: nextState.properties = {}", nextState.getProperties());
						//
						refresh();
					});
		}
		{
			stateObservable.getFlux()
					.distinctUntilChanged(FunctionUtils.getOrDummy(State::getFetchTimeWindow))
					.filter(nextState -> nextState.getProperties() != null)
					.subscribe(nextState -> nextState.getProperties().getOnFetchTimeWindowChange().accept(
							nextState.getFetchTimeWindow()));
		}
		{
			stateObservable.getFlux()
					.distinctUntilChanged(nextState -> Optional.of(nextState)
							.map(State::getFetchTimeWindow)
							.map(TimeWindow::getStartedAt))
					.subscribe(nextState -> {
						Properties properties = nextState.getProperties();
						if (properties != null) {
							fetchAsync(properties, nextState.getFetchTimeWindow().getStartedAt());
						} else {
							// TODO: Send out signal that properties are null and search is undergoing.
						}
					});
		}
	}

	@WithSpan
	private void refresh() {
		stateObservable.update(currentState -> {
			Instant fetchStartedAt = Instant.now();
			//
			logger.debug("Refresh: fetchStartedAt = {}", fetchStartedAt);
			//
			return currentState.withFetchTimeWindow(TimeWindow.builder()
					.startedAt(fetchStartedAt)
					.build());
		});
	}

	private <T> void renderColumnHeader(
			ColumnHeaderComponent<T> columnHeaderComponent,
			ObjectModelAttribute attribute,
			Consumer<ColumnPredicate<?>> onPredicateChange,
			@CheckForNull ColumnPredicate<?> predicate) {
		columnHeaderComponent.render(ColumnHeaderComponent.Properties.<T>builder()
				.datatype(attribute.getDatatype())
				.name(attribute.getName())
				.onPredicateChange(onPredicateChange::accept)
				.predicate(ClassUtils.castNullableInferred(predicate))
				.build());
	}

	private void renderColumnHeaders(
			Properties properties,
			List<Pair<ObjectModelAttribute, ColumnHeaderComponent<?>>> columnHeaderComponents) {
		columnHeaderComponents.forEach(pair -> {
			ObjectModelAttribute attribute = pair.getLeft();
			//
			renderColumnHeader(
					pair.getRight(),
					attribute,
					next -> {
						// FIXME: Make it work after TablePredicate -> Predicate rework.
//						properties.getOnPredicateChange().accept(properties.getPredicate().withColumn(
//								attribute.getName(),
//								next));
					},
					// FIXME: Make it work after TablePredicate -> Predicate rework.
//					properties.getPredicate().getColumn(attribute.getName())
					null
			);
		});
	}

	private List<ObjectModelAttribute> selectAttributes(ObjectModel objectModel) {
		List<ObjectModelAttribute> result;
		{
			JavaBaseType javaBaseType = javaBaseTypeRegistry.findJavaBaseType(objectModel);
			if (javaBaseType != null) {
				MergedAnnotation<CustomizeTable> customizeTable = MergedAnnotations.from(javaBaseType.resolveClass())
						.get(CustomizeTable.class);
				if (customizeTable.isPresent()) {
					List<String> attributesOrder = List.of(customizeTable.getStringArray(
							"attributesOrder"));
					boolean hideNotDeclaredAttributes = customizeTable.getBoolean(
							"hideNotDeclaredAttributes");
					//
					result = attributesOrder
							.map(attributeName ->
									objectModel.getAttributes()
											.find(attribute -> Objects.equals(attribute.getName(), attributeName))
											.getOrElseThrow(() -> new IllegalArgumentException(
													"Require attribute: %s".formatted(attributeName))))
							.appendAll(!hideNotDeclaredAttributes
									? objectModel.getAttributes().filter(attribute -> !attributesOrder.contains(
									attribute.getName()))
									: List.empty());
				} else {
					result = objectModel.getAttributes();
				}
			} else {
				result = objectModel.getAttributes();
			}
		}
		return result;
	}

	private void showCreateFormDialog(ObjectModel objectModel, GenericObject object) {
		if (RootObjectUtils.findUid(object) != null) {
			throw new IllegalArgumentException("Require null: object.uid, object = %s".formatted(object));
		}
		//
		showFormDialog(
				objectModel,
				object,
				next -> genericObjectManipulator.create(next.withAddedAttribute(
						RootObjectUtils.createNewUidAttribute())));
	}

	private void showEditFormDialog(ObjectModel objectModel, GenericObject object) {
		// TODO: uid must be preserved in box, not here.
		showFormDialog(
				objectModel,
				object,
				next -> {
					if (!Objects.equals(RootObjectUtils.getUid(next), RootObjectUtils.getUid(object))) {
						throw new IllegalArgumentException("Require equal uid");
					}
					genericObjectManipulator.update(next);
				});
	}

	private void showFormDialog(ObjectModel objectModel, GenericObject object, Consumer<GenericObject> callback) {
		GenericObjectMultiView view = beanContext.getBeanFactory().withBean(objectModel).getBean(
				GenericObjectMultiView.class);
		view.render(GenericObjectMultiView.Properties.builder()
				.currentValue(object)
				.initialValue(object)
				.build());
		ConfirmationViews.showViewDialog(view).subscribeAlwaysAccepted(next -> {
			callback.accept(view.getValue());
			refresh();
		});
	}

	protected void configureGrid(Grid<GenericObject> grid, KeySpaceReference spaceReference, ObjectModel objectModel) {
		{
			grid.setSelectionMode(Grid.SelectionMode.NONE);
		}
		{
			List<ObjectModelAttribute> attributes = selectAttributes(objectModel);
			//
			attributeAndHeaderPairs = attributes.map(attribute -> Pair.of(attribute, headerProvider.getObject()));
			//
			attributes.forEach(attribute -> grid
					.addColumn(tableCellRendererRegistry.getVaadinRenderer(attribute))
					.setHeader(attributeAndHeaderPairs
							.find(pair -> Objects.equals(pair.getLeft().getName(), attribute.getName()))
							.get()
							.getRight())
					.setKey(attribute.getName())
					.setResizable(true)
					.setSortable(false)); // TODO: datatypeTool.isComparable(attribute.getDatatype())
		}
		{
			grid
					.addComponentColumn(item -> {
						CssHorizontalLayout layout = new CssHorizontalLayout();
						{
							layout.setAlignItems(AlignItems.BASELINE);
						}
//						{
//							layout.add(createButton(
//									"edit-object-button",
//									VaadinIcon.EDIT,
//									() -> showEditFormDialog(objectModel, item),
//									genericObjectManipulator.canManipulate(item)
//											? null
//											: "No StoreRoute to edit this object specified"));
//						}
						{
							layout.add(createButton(
									"copy-object-button",
									VaadinIcon.COPY,
									() -> showCreateFormDialog(
											objectModel,
											item.withoutAttributes(
													RootObjectUtils.UID_ATTRIBUTE_NAME,
													// FIXME: Are we sure we want to remove it here? Looks like we need per-attribute setting if it is copyable.
													ObjectModel.HARDCODED_ATTRIBUTE_NAME))));
						}
//						{
//							layout.add(createButton(
//									"delete-object-button",
//									VaadinIcon.TRASH,
//									() -> ConfirmationViews
//											.showTextDialog("Delete?")
//											.subscribeAlwaysAccepted(next -> {
//												genericObjectManipulator.delete(GenericObjectUidReference.from(item));
//												refresh();
//											})));
//						}
						{
							Stream.ofAll(newActionProviders)
									.map(actionProvider -> actionProvider.provideActionForGenericObject(
											GenericObjectActionProvider.Context.<GenericObject>builder()
													.object(item)
													.objectModel(objectModel)
													.parentRefreshCallback(this::refresh)
													.build()))
									.filter(Objects::nonNull)
									.forEach(layout::add);
						}
						return layout;
					})
					.setAutoWidth(true)
					.setFlexGrow(0)
					.setFrozenToEnd(true)
					.setHeader(createButtonsHeader(spaceReference, objectModel));
		}
		{
			grid.addItemDoubleClickListener(event -> showEditFormDialog(objectModel, event.getItem()));
		}
	}

	@Override
	public Component asVaadinComponent() {
		return grid;
	}

	@Override
	public void render(Properties properties) {
		logger.debug("#render: properties = {}", properties);
		//
		stateObservable.update(currentState -> currentState.toBuilder()
				.properties(properties)
				.build());
	}

	private static Button createButton(
			String name,
			VaadinIcon icon,
			Runnable action,
			@CheckForNull String title,
			@CheckForNull String nonEditableReason) {
		Button button = new Button(icon.create(), event -> action.run());
		//
		if (nonEditableReason != null) {
			button.setEnabled(false);
		}
		button.getElement().setAttribute("name", name);
		button.addThemeVariants(ButtonVariant.LUMO_SMALL);
		button.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		//
		{
			String titleToUse;
			//
			if (title != null) {
				if (nonEditableReason != null) {
					button.setEnabled(false);
					titleToUse = "%s\n%s".formatted(title, nonEditableReason);
				} else {
					titleToUse = title;
				}
			} else {
				if (nonEditableReason != null) {
					titleToUse = nonEditableReason;
				} else {
					titleToUse = null;
				}
			}
			//
			if (titleToUse != null) {
				ElementUtils.setTitle(button, titleToUse);
			}
		}
		//
		StyleUtils.setMargin(button, "0px");
		return button;
	}

	private static GenericObject createNewObject(KeySpaceReference spaceReference, ObjectModel objectModel) {
		return GenericObject.of(
				RootObjectUtils.createModelReferenceAttribute(objectModel),
				GenericObjectAttribute.ofSimple(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
						spaceReference.toString()));
	}

	private static <T> Predicate createPredicate(String attributeName, ColumnPredicate<T> columnPredicate) {
		return columnPredicate.getOperator().getDescriptor().toPredicate(attributeName, columnPredicate.getValue());
	}

	public static Object createFetchComparisonObject(State state) {
		Properties properties = state.getProperties();
		return properties != null
				? List.of(
				properties.getGridSortOrders(),
				properties.getObjectModel(),
				properties.getPageIndex(),
				properties.getPageSize(),
				properties.getPredicate(),
				properties.getSpaceReference())
				: FunctionUtils.DUMMY;
	}

	@CheckForNull
	public static Sort gridSortOrdersToSort(List<GridSortOrder<?>> gridSortOrders) {
		return !gridSortOrders.isEmpty()
				? gridSortOrders
				.map(gridSortOrder -> Sort.by(
						gridSortOrder.getSorted().getKey(),
						vaadinSortDirectionToExpressoSortDirection(gridSortOrder.getDirection())))
				.reduce(Sort::then)
				: null;
	}

	public static Sort.Direction vaadinSortDirectionToExpressoSortDirection(SortDirection sortDirection) {
		return sortDirection == SortDirection.ASCENDING ? Sort.Direction.ASCENDING : Sort.Direction.DESCENDING;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		Instant fetchRequestedAt;

		List<GridSortOrder<?>> gridSortOrders;

		ObjectModel objectModel;

		Consumer<TimeWindow> onFetchTimeWindowChange;

		Consumer<List<GridSortOrder<?>>> onGridSortOrdersChange;

		Consumer<Predicate> onPredicateChange;

		Consumer<Integer> onTotalCountChange;

		int pageIndex;

		int pageSize;

		Predicate predicate;

		KeySpaceReference spaceReference;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State {

		@CheckForNull
		TimeWindow fetchTimeWindow;

		@NonNull
		Try<List<GenericObject>> items;

		@CheckForNull
		Properties properties;
	}
}
