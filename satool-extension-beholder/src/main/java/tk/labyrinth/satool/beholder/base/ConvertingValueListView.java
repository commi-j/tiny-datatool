package tk.labyrinth.satool.beholder.base;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.component.value.listview.ValueListView;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

/**
 * @param <E> External
 * @param <I> Internal
 */
@PrototypeScopedComponent
@RequiredArgsConstructor
public class ConvertingValueListView<E, I> implements ValueListView<E> {

	private final Type externalElementType;

	private final Type internalElementType;

	private final ValueListView<I> internalListView;

	@Autowired
	private ConversionService conversionService;

	@Nullable
	@SuppressWarnings("unchecked")
	private E elementToExternal(@Nullable I value) {
		return (E) conversionService.convert(
				value,
				TypeDescriptor.forObject(value),
				new TypeDescriptor(ResolvableType.forType(externalElementType), null, null));
	}

	@Nullable
	@SuppressWarnings("unchecked")
	private I elementToInternal(@Nullable E value) {
		return (I) conversionService.convert(
				value,
				TypeDescriptor.forObject(value),
				new TypeDescriptor(ResolvableType.forType(internalElementType), null, null));
	}

	@Nullable
	private List<E> toExternal(@Nullable List<I> value) {
		return value != null ? value.map(this::elementToExternal) : null;
	}

	@Nullable
	private List<I> toInternal(@Nullable List<E> value) {
		return value != null ? value.map(this::elementToInternal) : null;
	}

	@Override
	public Component asVaadinComponent() {
		return internalListView.asVaadinComponent();
	}

	@Override
	public Type getParameterType() {
		return externalElementType;
	}

	@Override
	public Properties<E> getProperties() {
		throw new NotImplementedException();
	}

	@Override
	public void setProperties(Properties<E> properties) {
		internalListView.setProperties(Properties.<I>builder()
				.changeListener(properties.getChangeListener() != null
						? next -> properties.getChangeListener().accept(toExternal(next))
						: null)
				.currentValue(toInternal(properties.getCurrentValue()))
				.initialValue(toInternal(properties.getInitialValue()))
				.build());
	}
}
