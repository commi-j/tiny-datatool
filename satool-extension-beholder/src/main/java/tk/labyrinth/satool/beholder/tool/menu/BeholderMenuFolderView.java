package tk.labyrinth.satool.beholder.tool.menu;

import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.details.DetailsVariant;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.component.anchor.SmartAnchorRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.domain.object.table.GenericObjectTablePage;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.domain.space.reference.VaadinUiSpaceReferenceExecutionContextAttributeContributor;
import tk.labyrinth.satool.beholder.domain.uiconfiguration.BeholderUiConfiguration;

import jakarta.annotation.PostConstruct;
import java.util.Objects;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class BeholderMenuFolderView extends CssVerticalLayout {

	private final SmartAnchorRenderer anchorRenderer;

	private final VaadinUiSpaceReferenceExecutionContextAttributeContributor spaceReferenceProvider;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@SmartAutowired
	private TypedObjectSearcher<BeholderUiConfiguration> uiConfigurationSearcher;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			spaceReferenceProvider.getSpaceReferenceObservable().getFlux().subscribe(this::render);
		}
	}

	@WithSpan
	private void render(KeySpaceReference spaceReference) {
		removeAll();
		//
		BeholderUiConfiguration uiConfiguration = uiConfigurationSearcher
				.search(PlainQuery.builder()
						.sort(Sort.descending(BeholderUiConfiguration.PRIORITY_ATTRIBUTE_NAME))
						.build())
				.getOrNull();
		//
		if (uiConfiguration != null && uiConfiguration.getMenuConfigurations() != null) {
			add(List.ofAll(uiConfiguration.getMenuConfigurations())
					.filter(Objects::nonNull)
					.map(menuConfiguration -> {
						AccordionPanel accordionPanel = new AccordionPanel(
								menuConfiguration.getFilterTag(),
								// FIXME: Replace with ListContains predicate.
								objectModelSearcher.search(PlainQuery.builder().build())
										.filter(objectModel -> objectModel.getTagsOrEmpty().exists(tag ->
												Objects.equals(tag.getName(), menuConfiguration.getFilterTag())))
										.filter(objectModel -> objectModel.getTablePathName() != null)
										.map(objectModel -> anchorRenderer.render(
												SmartAnchorRenderer.Properties.builder()
														.navigationTarget(GenericObjectTablePage.class)
														.navigationTargetParameter(objectModel.getTablePathName())
														.text(objectModel.getName() + "s")
														.build()))
										.toJavaStream()
										.reduce(
												new CssVerticalLayout(),
												(layout, link) -> {
													layout.add(link);
													return layout;
												},
												FunctionUtils::throwUnreachableStateException));
						accordionPanel.addThemeVariants(DetailsVariant.REVERSE);
						return accordionPanel;
					})
					.toJavaStream()
					.reduce(
							new Accordion(),
							(accordion, accordionPanel) -> {
								accordion.add(accordionPanel);
								return accordion;
							},
							FunctionUtils::throwUnreachableStateException));
		}
	}
}
