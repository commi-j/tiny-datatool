package tk.labyrinth.satool.beholder;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.labyrinth.pandora.storage.mongodb.EnablePandoraStorageMongoDb;
import tk.labyrinth.pandora.storage.mongodb.MongoDbJavaListToVavrListConverter;
import tk.labyrinth.pandora.stores.PandoraStoresModule;

@EnablePandoraStorageMongoDb
@EnableScheduling
@Import({
		MongoDbJavaListToVavrListConverter.class,
		PandoraStoresModule.class,
})
@Slf4j
@SpringBootApplication
public class BeholderExtensionConfiguration {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
