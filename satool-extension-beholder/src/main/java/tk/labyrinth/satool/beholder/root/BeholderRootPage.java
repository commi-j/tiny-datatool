package tk.labyrinth.satool.beholder.root;

import com.vaadin.flow.router.Route;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import jakarta.annotation.PostConstruct;

@Route(value = "", layout = BeholderRootLayout.class)
public class BeholderRootPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		//
	}
}
