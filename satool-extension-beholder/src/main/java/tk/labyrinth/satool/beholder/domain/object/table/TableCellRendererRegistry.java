package tk.labyrinth.satool.beholder.domain.object.table;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodelattribute.ObjectModelAttribute;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.value.view.FaultValueViewRenderer;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRenderer;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;

@LazyComponent
@RequiredArgsConstructor
public class TableCellRendererRegistry {

	private final ConverterRegistry converterRegistry;

	private final ToStringRendererRegistry toStringRendererRegistry;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	private <T> Renderer<GenericObject> createVaadinComponentRenderer(
			ObjectModelAttribute objectModelAttribute,
			ToVaadinComponentRenderer<T> renderer) {
		return new ComponentRenderer<>(item -> {
			Object attributeValue = item.findAttributeValueAsRaw(objectModelAttribute.getName());
			try {
				T value = ClassUtils.castNullableInferred(converterRegistry.convert(
						attributeValue,
						renderer.getParameterType()));
				//
				Component renderedComponent = renderer.render(
						ToVaadinComponentRenderer.Context.builder()
								.datatype(objectModelAttribute.getDatatype())
								.hints(List.empty())
								.rendererRegistry(toVaadinComponentRendererRegistry)
								.build(),
						value);
				if (renderedComponent == null) {
					throw new IllegalArgumentException("Require non-null: renderer = %s".formatted(renderer));
				}
				return renderedComponent;
			} catch (RuntimeException ex) {
				return FaultValueViewRenderer.render(FaultValueViewRenderer.Properties.builder()
						.fault(ex)
						.rawValue(toStringRendererRegistry.render(
								ToStringRendererRegistry.Context.builder()
										.datatype(null)
										.hints(List.empty())
										.build(),
								attributeValue))
						.build());
			}
		});
	}

	public Renderer<GenericObject> getVaadinRenderer(ObjectModelAttribute objectModelAttribute) {
		ToVaadinComponentRenderer<Object> renderer = toVaadinComponentRendererRegistry.getRenderer(
				ToVaadinComponentRendererRegistry.Context.builder()
						.datatype(objectModelAttribute.getDatatype())
						.hints(List.empty())
						.build());
		return createVaadinComponentRenderer(objectModelAttribute, renderer);
	}
}
