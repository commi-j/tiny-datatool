package tk.labyrinth.satool.beholder.base;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.value.box.ValueBox;
import tk.labyrinth.pandora.ui.component.value.listview.ValueListView;
import tk.labyrinth.satool.core.CustomClassPathScanningCandidateComponentProvider;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
@RequiredArgsConstructor
@Slf4j
public class BeholderCustomViewRegistry implements ApplicationRunner {

	private final ApplicationContext context;

	// TODO: Key should be JavaTypeSignatureReference.
	private final ConcurrentMap<Class<?>, Class<? extends ValueBox<?>>> customBoxes = new ConcurrentHashMap<>();

	private final ConcurrentMap<CodeObjectModelReference, Class<? extends ObjectForm<?>>> customForms =
			new ConcurrentHashMap<>();

	private final ConcurrentMap<CodeObjectModelReference, Class<? extends ValueListView<?>>> customListViews =
			new ConcurrentHashMap<>();

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	@Nullable
	public Class<? extends ObjectForm<?>> findCustomFormType(CodeObjectModelReference modelReference) {
		return customForms.get(modelReference);
	}

	@Nullable
	public Class<? extends ValueListView<?>> findCustomListViewType(CodeObjectModelReference elementModelReference) {
		return customListViews.get(elementModelReference);
	}

	@Nullable
	public Class<? extends ValueBox<?>> findCustomValueBoxClass(Class<?> valueClass) {
		return customBoxes.get(valueClass);
	}

	@Override
	public void run(ApplicationArguments args) {
		try {
			CustomViewDiscoverer customViewDiscoverer = new CustomViewDiscoverer(context);
			//
			Set<BeanDefinition> candidateComponents = customViewDiscoverer
					.findCandidateComponents("tk.labyrinth");
			candidateComponents.forEach(candidateComponent -> {
				Class<?> type = ClassUtils.get(candidateComponent.getBeanClassName());
				//
				if (ValueBox.class.isAssignableFrom(type)) {
					Type valueType = ParameterUtils.getFirstActualParameter(type, ValueBox.class);
					Class<?> valueClass = TypeUtils.findClass(valueType);
					//
					if (valueClass != null) {
						customBoxes.put(valueClass, ClassUtils.castInferred(type));
					}
				}
				if (ObjectForm.class.isAssignableFrom(type)) {
					Type valueType = ParameterUtils.getFirstActualParameter(type, ObjectForm.class);
					Class<?> valueClass = TypeUtils.findClass(valueType);
					//
					if (valueClass != null) {
						customForms.put(
								javaBaseTypeRegistry.getNormalizedObjectModelReference(valueClass),
								ClassUtils.castInferred(type));
					}
				}
				if (ValueListView.class.isAssignableFrom(type)) {
					Type valueType = ParameterUtils.getFirstActualParameter(type, ValueListView.class);
					Class<?> valueClass = TypeUtils.findClass(valueType);
					//
					if (valueClass != null) {
						customListViews.put(
								javaBaseTypeRegistry.getNormalizedObjectModelReference(valueClass),
								ClassUtils.castInferred(type));
					}
				}
			});
			//
			logger.info("Discovered {} customViews", candidateComponents.size());
		} catch (RuntimeException ex) {
			logger.error("", ex);
		}
	}

	public static class CustomViewDiscoverer extends CustomClassPathScanningCandidateComponentProvider {

		public CustomViewDiscoverer(ApplicationContext context) {
			super(false);
			//
			setEnvironment(context.getEnvironment());
			setResourceLoader(context);
			//
			addIncludeFilter(new AssignableTypeFilter(ObjectForm.class));
			addIncludeFilter(new AssignableTypeFilter(ValueBox.class));
			addIncludeFilter(new AssignableTypeFilter(ValueListView.class));
			//
			setResourceProcessingFaultHandler((resource, fault) -> {
				if ((fault instanceof IndexOutOfBoundsException) &&
						resource.toString().contains("/org/atmosphere/inject/")) {
					logger.debug("Failed to read metadata of Atmosphere class: %s".formatted(resource));
				} else {
					throw new RuntimeException(fault);
				}
			});
		}
	}
}
