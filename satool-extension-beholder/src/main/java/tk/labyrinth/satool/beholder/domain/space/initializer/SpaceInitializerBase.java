package tk.labyrinth.satool.beholder.domain.space.initializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.stores.objectmanipulator.WildcardObjectManipulator;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.domain.uiconfiguration.BeholderUiConfiguration;

import javax.annotation.CheckForNull;

public abstract class SpaceInitializerBase implements ApplicationRunner {

	@Autowired
	private WildcardObjectManipulator objectManipulator;

	protected abstract String getSpaceKey();

	@CheckForNull
	protected abstract BeholderUiConfiguration getUiConfiguration();

	@Override
	public void run(ApplicationArguments args) throws Exception {
		{
			// Creating Space, listener will create StoreConfiguration and StoreRoute.
			//
			BeholderSpace space = BeholderSpace.builder()
					.key(getSpaceKey())
					.build();
			objectManipulator.createOrUpdate(space, space.getKey());
		}
		{
			// Creating UiConfiguration.
			//
			BeholderUiConfiguration uiConfiguration = getUiConfiguration();
			if (uiConfiguration != null) {
				objectManipulator.createOrUpdate(
						uiConfiguration,
						uiConfiguration.getScope(),
						genericObject -> genericObject.withAddedAttribute(GenericObjectAttribute.ofSimple(
								BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
								KeySpaceReference.of(getSpaceKey()).toString())));
			}
		}
	}
}
