package tk.labyrinth.satool.beholder.domain.space.reference;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttribute;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttributeContributor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.UiScopedComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;

@RequiredArgsConstructor
@UiScopedComponent
public class SpaceReferenceSearchPredicateAttributeContributor implements ExecutionContextAttributeContributor<Predicate> {

	private final VaadinUiSpaceReferenceExecutionContextAttributeContributor spaceReferenceExecutionContextAttributeContributor;

	@Override
	public ExecutionContextAttribute<Predicate> contributeAttribute() {
		return ExecutionContextAttribute.<Predicate>builder()
				.name(GenericObjectSearcher.SEARCH_PREDICATE_EXECUTION_CONTEXT_ATTRIBUTE_NAME)
				.propagatable(true)
				.value(Predicates.in(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
						spaceReferenceExecutionContextAttributeContributor.provideSpaceReference(),
						null))
				.build();
	}
}
