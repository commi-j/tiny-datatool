package tk.labyrinth.satool.beholder.domain.object.table;

import com.vaadin.flow.component.grid.Grid;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;

// TODO: Make useful
public interface GenericObjectTableConfigurer {

	void configure(Grid<GenericObject> grid, ObjectModel objectModel);

	boolean supports(CodeObjectModelReference objectModelReference);
}
