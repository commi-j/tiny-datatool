package tk.labyrinth.satool.beholder.domain.object.reference;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.objectreferencemodel.ObjectReferenceModel;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.GenericReferenceAttribute;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.pandora.ui.component.reference.ReferenceItem;
import tk.labyrinth.pandora.ui.component.reference.ReferenceRendererBase;
import tk.labyrinth.pandora.ui.component.reference.ReferenceTool;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;

import javax.annotation.Nullable;

@Deprecated
@PrototypeScopedComponent
@RequiredArgsConstructor
public class BeholderObjectReferenceTool extends ReferenceRendererBase<GenericObject, GenericObjectReference> implements
		ReferenceTool<GenericObject, GenericObjectReference> {

	@Setter
	// TODO: Should be BeholderModelReferenceReference.
	private GenericReference modelReferenceReference;

	@Setter
	private ObjectModel objectModel;

	@Autowired
	private ToStringRendererRegistry toStringRendererRegistry;

	@Nullable
	@Override
	public Predicate buildSearchPredicate(String text) {
//		String trimmedText = text.trim();
//		return !trimmedText.isEmpty()
//				? Predicates.equalTo("modelReference", BeholderModelCodeReference.from(model))
//				: null;
		return Predicates.equalTo(
				RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME,
				CodeObjectModelReference.from(objectModel));
	}

	@Nullable
	@Override
	public ReferenceItem<GenericObjectReference> renderNonNullObject(GenericObject value) {
		String renderedValue = toStringRendererRegistry.render(
				ToStringRendererRegistry.Context.builder()
						.datatype(ObjectModelUtils.createDatatype(objectModel))
						.hints(null)
						.build(),
				value);
		return ReferenceItem.of(
				createReference(objectModel, modelReferenceReference, value),
				renderedValue != null ? renderedValue : "NULL_RENDERED_VALUE");
	}

	public static GenericObjectReference createReference(
			ObjectModel objectModel,
			GenericReference modelReferenceReference,
			GenericObject object) {
		String attributeNamesString = modelReferenceReference.getAttributeValue(
				ObjectReferenceModel.ATTRIBUTE_NAMES_ATTRIBUTE_NAME);
		List<String> attributeNames = List.of(attributeNamesString
				.substring(1, attributeNamesString.length() - 1)
				.split(","));
		return GenericObjectReference.from(GenericReference.of(
				objectModel.getCode(),
				attributeNames
						.map(attributeName -> {
							Object attributeValue = object.findAttributeValue(attributeName);
							//
							return GenericReferenceAttribute.of(
									attributeName,
									// TODO: Convert to string in a safe way.
									attributeValue != null ? attributeValue.toString() : null);
						})));
	}
}
