package tk.labyrinth.satool.beholder.domain.space;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class KeySpaceReference implements Reference<BeholderSpace> {

	String key;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				BeholderSpace.MODEL_CODE,
				BeholderSpace.KEY_ATTRIBUTE_NAME,
				key);
	}

	public static KeySpaceReference from(BeholderSpace object) {
		return of(object.getKey());
	}

	@JsonCreator
	public static KeySpaceReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), BeholderSpace.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(BeholderSpace.KEY_ATTRIBUTE_NAME));
	}
}
