package tk.labyrinth.satool.beholder.domain.space.reference;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttribute;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttributeContributor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.UiScopedComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.satool.beholder.base.BeholderConstants;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;

@Order(1001)
@UiScopedComponent
public class VaadinUiSpaceReferenceExecutionContextAttributeContributor implements
		ExecutionContextAttributeContributor<KeySpaceReference>,
		SpaceReferenceProvider {

	@Value("${beholder.uiInitialSpaceKey:#{null}}")
	private String initialSpace;

	@Getter
	private Observable<KeySpaceReference> spaceReferenceObservable;

	@PostConstruct
	private void postConstruct() {
		spaceReferenceObservable = Observable.withInitialValue(KeySpaceReference.of(initialSpace != null
				? initialSpace
				: BeholderConstants.DEFAULT_SPACE));
	}

	@Override
	public ExecutionContextAttribute<KeySpaceReference> contributeAttribute() {
		return ExecutionContextAttribute.<KeySpaceReference>builder()
				.name(BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME)
				.propagatable(true)
				.value(spaceReferenceObservable.get())
				.build();
	}

	@Override
	public KeySpaceReference provideSpaceReference() {
		return spaceReferenceObservable.get();
	}
}
