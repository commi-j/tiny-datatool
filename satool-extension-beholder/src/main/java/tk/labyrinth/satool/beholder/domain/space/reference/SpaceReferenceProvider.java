package tk.labyrinth.satool.beholder.domain.space.reference;

import tk.labyrinth.pandora.context.executioncontext.ExecutionContext;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;

import javax.annotation.CheckForNull;

/**
 * @see ExecutionContext
 */
@Deprecated
public interface SpaceReferenceProvider {

	@CheckForNull
	KeySpaceReference provideSpaceReference();
}
