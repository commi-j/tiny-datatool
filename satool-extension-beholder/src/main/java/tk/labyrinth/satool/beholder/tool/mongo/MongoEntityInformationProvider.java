package tk.labyrinth.satool.beholder.tool.mongo;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
public class MongoEntityInformationProvider {

	private final MongoRepositoryFactory mongoRepositoryFactory;

	public MongoEntityInformationProvider(MongoOperations mongoOperations) {
		mongoRepositoryFactory = new MongoRepositoryFactory(mongoOperations);
	}

	public <T> MongoEntityInformation<T, ?> provide(Class<T> entityType) {
		return mongoRepositoryFactory.getEntityInformation(entityType);
	}

	@SuppressWarnings("unchecked")
	public <T> MongoEntityInformation<T, ?> provide(T entity) {
		MongoEntityInformation<T, ?> result;
		if (entity instanceof GenericObject) {
			// FIXME:
			result = (MongoEntityInformation<T, ?>) mongoRepositoryFactory.getEntityInformation(entity.getClass());
		} else {
			result = (MongoEntityInformation<T, ?>) provide(entity.getClass());
		}
		return result;
	}
}
