package tk.labyrinth.satool.beholder.domain.function.operator;

import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class BinaryOperator<S> {

	@NonNull
	BinaryOperatorDescriptor<S> descriptor;
}
