package tk.labyrinth.satool.beholder.domain.object.change;

public interface ObjectChangeInterceptor<T> {

	T interceptObjectChange(T current, T next);
}
