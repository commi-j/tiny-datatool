package tk.labyrinth.satool.beholder.domain.job;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.jobs.model.job.Job;
import tk.labyrinth.pandora.jobs.model.job.JobRunnerClassJobReference;
import tk.labyrinth.pandora.jobs.model.jobrun.UidJobRunReference;
import tk.labyrinth.pandora.jobs.model.jobrunindex.JobRunIndex;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.jobs.tool.JobRunnerRegistry;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.objectaction.TypedObjectActionProvider;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;

@RequiredArgsConstructor
@SpringComponent
public class BeholderJobRunIndexItemActionProvider extends TypedObjectActionProvider<JobRunIndex> {

	private final BeanContext beanContext;

	private final ConverterRegistry converterRegistry;

	private final JobRunnerRegistry jobRunnerRegistry;

	@SmartAutowired
	private TypedObjectSearcher<Job> jobSearcher;

	private <P extends HasSpaceReference> void showParametersDialog(Job job, Object parameters) {
		Class<? extends JobRunner<P>> jobRunnerClass = job.getJobRunnerClass();
		//
		Class<P> parametersClass = ClassUtils.castInferred(jobRunnerRegistry.getInitialParameters(jobRunnerClass)
				.getClass());
		P parametersToEdit = converterRegistry.convert(parameters, parametersClass);
		Observable<P> parametersObservable = Observable.withInitialValue(parametersToEdit);
		//
		ObjectForm<P> parametersForm = beanContext.getBeanFactory().getBean(
				org.apache.commons.lang3.reflect.TypeUtils.parameterize(ObjectForm.class, parametersClass));
		parametersForm.render(ObjectForm.Properties.<P>builder()
				.currentValue(parametersToEdit)
				.initialValue(parametersToEdit)
				.onValueChange(parametersObservable::set)
				.build());
		//
		ConfirmationViews.showViewDialog(parametersForm).subscribeAlwaysAccepted(success -> {
			P parametersToUse = parametersObservable.get();
			//
			UidJobRunReference jobRunReference = jobRunnerRegistry.runJob(jobRunnerClass, parametersToUse);
			//
			// TODO: Show notification with link to job run.
			System.out.println();
		});
	}

	@Nullable
	@Override
	protected Component provideActionForTypedObject(Context<JobRunIndex> context) {
		Component result;
		{
			JobRunIndex jobRunIndex = context.getObject();
			//
			Class<? extends JobRunner<?>> jobRunnerClass = ClassUtils.findInferred(jobRunIndex.getJobRunnerClassName());
			//
			if (jobRunnerClass != null) {
				Job job = jobSearcher.findSingle(JobRunnerClassJobReference.of(jobRunnerClass));
				if (job != null) {
					result = new Button(
							"Repeat Job",
							event -> showParametersDialog(job, jobRunIndex.getParameters()));
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}
}
