package tk.labyrinth.satool.beholder.domain.object.table;

import lombok.Value;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperator;

@Value(staticConstructor = "of")
public class ColumnPredicate<T> {

	BinaryOperator<T> operator;

	T value;
}
