package tk.labyrinth.satool.beholder.domain.space;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttribute;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.jobs.tool.JobContextAttributeContributor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class SpaceReferenceJobContextAttributeContributor implements JobContextAttributeContributor {

	@Override
	public <P> List<ExecutionContextAttribute<?>> contributeJobContextAttributes(
			Class<? extends JobRunner<P>> jobRunnerClass,
			P parameters) {
		return parameters instanceof HasSpaceReference hasSpaceReference
				? List.of(ExecutionContextAttribute.builder()
				.name(BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME)
				.propagatable(true)
				.value(hasSpaceReference.getSpaceReference())
				.build())
				: List.empty();
	}
}
