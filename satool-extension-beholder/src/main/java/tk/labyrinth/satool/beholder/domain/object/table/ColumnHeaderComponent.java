package tk.labyrinth.satool.beholder.domain.object.table;

import com.awesomecontrols.quickpopup.QuickPopup;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lang.Accessor;
import tk.labyrinth.pandora.misc4j.lang.SimpleAccessor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoVariables;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperatorRegistry;
import tk.labyrinth.satool.beholder.domain.object.table.filter.FilterView;

import javax.annotation.Nullable;
import java.util.function.Consumer;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class ColumnHeaderComponent<T> extends CssHorizontalLayout implements
		RenderableView<ColumnHeaderComponent.Properties<T>> {

	private final BinaryOperatorRegistry binaryOperatorRegistry;

	private final Button filterButton = new Button(VaadinIcon.FILTER.create());

	private final ObjectProvider<FilterView> filterViewProvider;

	private final Label nameLabel = new Label();

	private Properties<T> properties;

	@PostConstruct
	private void postConstruct() {
		{
			setAlignItems(AlignItems.BASELINE);
		}
		{
			CssFlexItem.setFlexGrow(nameLabel, 1);
			add(nameLabel);
		}
		{
//			Button sortButton = new Button(VaadinIcon.SORT.create());
//			{
//				sortButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
//				sortButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
//			}
//			{
//				sortButton.setEnabled(false);
//			}
//			add(sortButton);
		}
		{
			{
				{
					filterButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
					filterButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
				}
				{
					filterButton.addClickListener(event -> {
						FilterView filterView = filterViewProvider.getObject();
						//
						Accessor<QuickPopup> popupAccessor = new SimpleAccessor<>();
						Observable<FilterView.Properties> state = Observable.withInitialValue(
								FilterView.Properties.builder()
										.datatype(properties.getDatatype())
										.onValueChange(nextValue -> {
											popupAccessor.get().hide();
											properties.getOnPredicateChange().accept(ClassUtils.castNullableInferred(nextValue));
										})
										.value(properties.getPredicate())
										.build());
						//
						state.getFlux().subscribe(filterView::render);
						//
						QuickPopup popup = new QuickPopup(filterButton.getElement(), filterView);
						popupAccessor.set(popup);
						popup.setAlign(QuickPopup.Align.BOTTOM_RIGHT);
						popup.show();
					});
				}
				add(filterButton);
			}
		}
	}

	@Override
	public Component asVaadinComponent() {
		return this;
	}

	@Override
	public void render(Properties<T> properties) {
		this.properties = properties;
		//
		{
			filterButton.getElement().setAttribute("name", "filter.%s".formatted(properties.getName()));
			StyleUtils.setColor(
					filterButton,
					properties.getPredicate() != null
							? LumoVariables.PRIMARY_COLOUR
							: LumoVariables.DISABLED_TEXT_COLOUR);
		}
		{
			nameLabel.setText(properties.getName());
		}
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties<T> {

		Datatype datatype;

		String name;

		Consumer<ColumnPredicate<T>> onPredicateChange;

		@Nullable
		ColumnPredicate<T> predicate;
	}
}
