package tk.labyrinth.satool.beholder.domain.space.initializer;

import io.vavr.collection.HashMap;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.pattern.PandoraPattern;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.MongoDbStoreSettings;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.domain.storesummary.StoreSummary;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreRegistry;
import tk.labyrinth.pandora.stores.init.temporal.PandoraTemporalConstants;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;
import tk.labyrinth.pandora.stores.objectindex.ObjectIndexer;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;

@LazyComponent
@RequiredArgsConstructor
public class BeholderSpaceChangeListener extends ObjectChangeListener<BeholderSpace> {

	private final ConverterRegistry converterRegistry;

	private final GenericObjectManipulator genericObjectManipulator;

	private final GenericObjectStoreRegistry genericObjectStoreRegistry;

	private final ObjectIndexer objectIndexer;

	@Value("${beholder.spaceStoreConnectionString:#{null}}")
	private String spaceStoreConnectionString;

	@Override
	protected void onObjectChange(ObjectChangeEvent<BeholderSpace> event) {
		if (event.hasPreviousObject()) {
			// TODO: Remove
		}
		if (event.hasNextObject()) {
			KeySpaceReference spaceReference = KeySpaceReference.from(event.getNextObject());
			String storeName = "%s_main_store".formatted(spaceReference.getKey());
			//
			{
				StoreConfiguration storeConfiguration = StoreConfiguration.builder()
						.designation(StoreDesignation.COMMON)
						.kind(StoreKind.MONGO_DB)
						.name(storeName)
						.mongoDbSettings(MongoDbStoreSettings.builder()
								.uri(PandoraPattern.render(
										spaceStoreConnectionString,
										HashMap.of("spaceKey", spaceReference.getKey())))
								.build())
						.predicate(Predicates.equalTo(
								BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
								spaceReference))
						.build();
				//
				genericObjectManipulator.create(converterRegistry.convert(storeConfiguration, GenericObject.class)
						.withAddedAttributes(
								RootObjectUtils.createHashCodeBasedUidAttribute(storeConfiguration.getName()),
								PandoraTemporalConstants.ATTRIBUTE));
			}
			{
				StoreRoute storeRoute = StoreRoute.builder()
						.distance(1000)
						.name("%s_main_route".formatted(spaceReference.getKey()))
						.storeConfigurationPredicate(Predicates.equalTo(
								StoreConfiguration.NAME_ATTRIBUTE_NAME,
								storeName))
						.testPredicate(Predicates.equalTo(
								BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
								spaceReference))
						.build();
				//
				genericObjectManipulator.create(converterRegistry.convert(storeRoute, GenericObject.class)
						.withAddedAttributes(
								RootObjectUtils.createHashCodeBasedUidAttribute(storeRoute.getName()),
								PandoraTemporalConstants.ATTRIBUTE));
			}
			{
				// Triggering Store indexing.
				//
				GenericObjectStore objectStore = genericObjectStoreRegistry.getStore(storeName);
				StoreSummary storeSummary = objectStore.getSummary();
				storeSummary.getObjectModelSummaries().forEach(objectModelSummary -> objectStore
						.search(PlainQuery.builder()
								.predicate(Predicates.equalTo(
										RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME,
										objectModelSummary.getObjectModelReference()))
								.build())
						.forEach(objectIndexer::createIndices));
			}
		}
	}
}
