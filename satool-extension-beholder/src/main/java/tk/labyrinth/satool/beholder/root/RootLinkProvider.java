package tk.labyrinth.satool.beholder.root;

import com.vaadin.flow.router.RouterLink;
import io.vavr.collection.Stream;

public interface RootLinkProvider {

	Stream<RouterLink> provideLinks();
}
