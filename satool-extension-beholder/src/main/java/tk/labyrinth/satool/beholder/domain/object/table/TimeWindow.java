package tk.labyrinth.satool.beholder.domain.object.table;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

import javax.annotation.CheckForNull;
import java.time.Duration;
import java.time.Instant;

@Builder(toBuilder = true)
@Value
@With
public class TimeWindow {

	@CheckForNull
	Instant finishedAt;

	@NonNull
	Instant startedAt;

	Duration getDuration() {
		return Duration.between(startedAt, finishedAt);
	}
}
