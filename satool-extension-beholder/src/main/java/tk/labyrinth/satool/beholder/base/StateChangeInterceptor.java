package tk.labyrinth.satool.beholder.base;

public interface StateChangeInterceptor<S> {

	S intercept(S previous, S next);
}
