package tk.labyrinth.satool.beholder.domain.job;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.jobs.model.job.Job;
import tk.labyrinth.pandora.jobs.model.jobrun.UidJobRunReference;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.jobs.tool.JobRunnerRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.objectaction.TypedObjectActionProvider;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

@LazyComponent
@RequiredArgsConstructor
public class BeholderJobItemActionProvider extends TypedObjectActionProvider<Job> {

	private final BeanContext beanContext;

	private final JobRunnerRegistry jobRunnerRegistry;

	private <P> void showParametersDialog(Job job) {
		Class<? extends JobRunner<P>> jobRunnerClass = job.getJobRunnerClass();
		P initialParameters = jobRunnerRegistry.getInitialParameters(jobRunnerClass);
		Observable<P> currentValueObservable = Observable.withInitialValue(initialParameters);
		//
		ObjectForm<P> parametersForm = beanContext.getBeanFactory().getBean(
				org.apache.commons.lang3.reflect.TypeUtils.parameterize(
						ObjectForm.class,
						initialParameters.getClass()));
		currentValueObservable.subscribe(nextCurrentValue -> parametersForm.render(ObjectForm.Properties.<P>builder()
				.currentValue(nextCurrentValue)
				.initialValue(initialParameters)
				.onValueChange(currentValueObservable::set)
				.build()));
		//
		ConfirmationViews.showViewDialog(parametersForm).subscribeAlwaysAccepted(success -> {
			P parametersToUse = currentValueObservable.get();
			//
			UidJobRunReference jobRunReference = jobRunnerRegistry.runJob(jobRunnerClass, parametersToUse);
			//
			// TODO: Show notification with link to job run.
			System.out.println();
		});
	}

	@Nullable
	@Override
	protected Component provideActionForTypedObject(Context<Job> context) {
		return new Button(
				"Run Job",
				event -> showParametersDialog(context.getObject()));
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}
}
