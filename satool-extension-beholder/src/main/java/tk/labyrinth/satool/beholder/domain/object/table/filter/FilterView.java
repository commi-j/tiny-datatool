package tk.labyrinth.satool.beholder.domain.object.table.filter;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.JustifyContent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.component.value.box.simple.SuggestBox;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperator;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperatorDescriptor;
import tk.labyrinth.satool.beholder.domain.function.operator.BinaryOperatorRegistry;
import tk.labyrinth.satool.beholder.domain.object.table.ColumnPredicate;

import java.util.function.Consumer;

@PrototypeScopedComponent
@RequiredArgsConstructor
@Slf4j
public class FilterView extends CssVerticalLayout implements RenderableView<FilterView.Properties> {

	private final BinaryOperatorRegistry binaryOperatorRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final SuggestBox<BinaryOperatorDescriptor<?>> operatorBox = new SuggestBox<>();

	private final Observable<State> stateObservable = Observable.notInitialized();

	private final CssHorizontalLayout valueBoxLayout = new CssHorizontalLayout();

	private final ValueBoxRegistry valueBoxRegistry;

	{
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			CssHorizontalLayout boxLayout = new CssHorizontalLayout();
			{
				boxLayout.addClassNames(PandoraStyles.LAYOUT);
			}
			{
				CssHorizontalLayout horizontalLayout = new CssHorizontalLayout();
				{
					horizontalLayout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					operatorBox.asVaadinComponent().getElement().setAttribute("name", "operator-box");
					horizontalLayout.add(operatorBox.asVaadinComponent());
				}
				boxLayout.add(horizontalLayout);
			}
			{
				valueBoxLayout.addClassNames(PandoraStyles.LAYOUT);
				boxLayout.add(valueBoxLayout);
			}
			add(boxLayout);
		}
		{
			CssHorizontalLayout buttonLayout = new CssHorizontalLayout();
			{
				buttonLayout.addClassNames(PandoraStyles.LAYOUT);
				//
				buttonLayout.setJustifyContent(JustifyContent.FLEX_END);
			}
			{
				Button resetButton = new Button(
						"RESET",
						event -> stateObservable.get().getProperties().getOnValueChange().accept(null));
				resetButton.getElement().setAttribute("name", "reset-button");
				buttonLayout.add(resetButton);
			}
			{
				Button confirmButton = new Button(
						"CONFIRM",
						event -> {
							State currentState = stateObservable.get();
							BinaryOperatorDescriptor<?> currentOperator = currentState.getOperator();
							Object currentValue = currentState.getValue();
							//
							currentState.getProperties().getOnValueChange().accept(currentValue != null
									? ColumnPredicate.of(
									BinaryOperator.of(currentOperator),
									ClassUtils.castInferred(currentValue))
									: null);
						});
				confirmButton.getElement().setAttribute("name", ConfirmationViews.CONFIRM_BUTTON_NAME);
				buttonLayout.add(confirmButton);
			}
			add(buttonLayout);
		}
	}

	private <T> Component getConfiguredValueBox(
			Datatype datatype,
			BinaryOperatorDescriptor<T> operatorDescriptor,
			Object value) {
		MutableValueBox<T> valueBox = getValueBox(datatype, operatorDescriptor);
		valueBox.render(MutableValueBox.Properties.<T>builder()
				.currentValue(ClassUtils.castNullableInferred(value))
				.initialValue(ClassUtils.castNullableInferred(value))
				.label("Value")
				.onValueChange(nextValue -> stateObservable.update(currentState -> currentState.withValue(nextValue)))
				.build());
		return valueBox.asVaadinComponent();
	}

	private <T> MutableValueBox<T> getValueBox(Datatype datatype, BinaryOperatorDescriptor<T> operatorDescriptor) {
		Datatype secondOperandDatatype = operatorDescriptor.getSecondOperandDatatype(datatype);
		return ClassUtils.castInferred(valueBoxRegistry.getValueBox(secondOperandDatatype));
	}

	@PostConstruct
	private void postConstruct() {
		stateObservable.getFlux().subscribe(nextState -> {
			logger.info("nextState: {}", nextState);
			{
				operatorBox.render(SuggestBox.Properties.<BinaryOperatorDescriptor<?>>builder()
						.label("Operator")
						.onValueChange(nextValue -> stateObservable.update(currentState -> currentState.toBuilder()
								.operator(nextValue)
								.value(null)
								.build()))
						.suggestFunction(text -> nextState.getOperatorDescriptors())
						.toStringRenderFunction(BinaryOperatorDescriptor::getText)
						.value(nextState.getOperator())
						.build());
			}
			{
				Datatype datatype = nextState.getProperties().getDatatype();
				//
				valueBoxLayout.removeAll();
				//
				Component valueBox = getConfiguredValueBox(datatype, nextState.getOperator(), nextState.getValue());
				valueBox.getElement().setAttribute("name", "value-box");
				valueBoxLayout.add(valueBox);
			}
		});
	}

	@Override
	public Component asVaadinComponent() {
		return this;
	}

	@Override
	public void render(Properties properties) {
		Class<?> javaClass = javaBaseTypeRegistry.getJavaClass(properties.getDatatype().getBaseReference());
		//
		List<? extends BinaryOperatorDescriptor<?>> operatorDescriptors = binaryOperatorRegistry
				.getOperatorDescriptors(javaClass);
		//
		stateObservable.set(State.builder()
				.operatorDescriptors(operatorDescriptors)
				.operator(properties.getValue() != null
						? properties.getValue().getOperator().getDescriptor()
						: operatorDescriptors.get(0))
				.properties(properties)
				.value(properties.getValue() != null
						? properties.getValue().getValue()
						: null)
				.integer(0)
				.build());
	}

	@Builder(toBuilder = true)
	@Value
	@With
	private static class State {

		Integer integer;

		@NonNull
		BinaryOperatorDescriptor<?> operator;

		List<? extends BinaryOperatorDescriptor<?>> operatorDescriptors;

		Properties properties;

		@Nullable
		Object value;
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties {

		Datatype datatype;

		Consumer<ColumnPredicate<?>> onValueChange;

		@Nullable
		ColumnPredicate<?> value;
	}
}
