package tk.labyrinth.satool.beholder.stores;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.genericobject.mongodb.MongoDbCollectionLocator;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import javax.annotation.CheckForNull;
import java.util.Objects;
import java.util.stream.Collectors;

@LazyComponent
@RequiredArgsConstructor
public class BeholderMongoDbCollectionLocator implements MongoDbCollectionLocator {

	@Override
	public Pair<GenericObject, String> locateCollection(StoreConfiguration storeConfiguration, GenericObject object) {
		Pair<GenericObject, String> result;
		{
			GenericObjectAttribute modelReferenceAttribute = object.getAttribute(
					RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME);
			//
			result = Pair.of(
					object.withoutAttribute(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME),
					CodeObjectModelReference.from(modelReferenceAttribute.getValue().asSimple().getValue()).getCode());
		}
		return result;
	}

	@Override
	public CollectionContext locateCollection(StoreConfiguration storeConfiguration, Predicate predicate) {
		CollectionContext result;
		{
			ObjectPropertyPredicate modelReferencePredicate = findModelReferencePredicate(predicate);
			Objects.requireNonNull(modelReferencePredicate, "modelReferencePredicate");
			//
			CodeObjectModelReference modelReference = CodeObjectModelReference.from(
					modelReferencePredicate.value().toString());
			//
			result = CollectionContext.of(
					modelReference.getCode(),
					List.of(
							GenericObjectAttribute.ofSimple(
									RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME,
									modelReference.toString())),
					removeModelReferencePredicate(predicate));
		}
		return result;
	}

	@Override
	public CodeObjectModelReference objectModelReferenceFromCollectionName(String collectionName) {
		return CodeObjectModelReference.of(collectionName);
	}

	@CheckForNull
	public static ObjectPropertyPredicate findModelReferencePredicate(Predicate predicate) {
		ObjectPropertyPredicate result;
		{
			if (predicate instanceof JunctionPredicate junctionPredicate) {
				if (junctionPredicate.isAnd()) {
					List<ObjectPropertyPredicate> predicates = junctionPredicate.predicates().stream()
							.map(BeholderMongoDbCollectionLocator::findModelReferencePredicate)
							.filter(Objects::nonNull)
							.collect(List.collector());
					//
					if (predicates.size() > 1) {
						throw new NotImplementedException();
					} else {
						result = !predicates.isEmpty() ? predicates.single() : null;
					}
				} else {
					throw new NotImplementedException();
				}
			} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
				if (Objects.equals(
						objectPropertyPredicate.property(),
						RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME)) {
					result = objectPropertyPredicate;
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@CheckForNull
	public static Predicate removeModelReferencePredicate(Predicate predicate) {
		Predicate result;
		{
			if (predicate instanceof JunctionPredicate junctionPredicate) {
				JunctionPredicate nextJunctionPredicate = new JunctionPredicate(
						junctionPredicate.operator(),
						junctionPredicate.predicates().stream()
								.map(BeholderMongoDbCollectionLocator::removeModelReferencePredicate)
								.filter(Objects::nonNull)
								.collect(Collectors.toList()));
				//
				result = !nextJunctionPredicate.predicates().isEmpty() ? nextJunctionPredicate : null;
			} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
				if (Objects.equals(
						objectPropertyPredicate.property(),
						RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME)) {
					result = null;
				} else {
					result = objectPropertyPredicate;
				}
			} else {
				result = predicate;
			}
		}
		return result;
	}
}
