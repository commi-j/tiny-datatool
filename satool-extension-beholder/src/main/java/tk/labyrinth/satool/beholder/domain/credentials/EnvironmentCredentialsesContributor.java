package tk.labyrinth.satool.beholder.domain.credentials;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.objectcontributor.ObjectContributor;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.util.UUID;

@Component
@ConditionalOnProperty("beholder.environmentCredentialses")
//@Priority(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
@Slf4j
public class EnvironmentCredentialsesContributor implements ObjectContributor<Credentials> {

	private final ConverterRegistry converterRegistry;

	private final GenericObjectManipulator genericObjectManipulator;

	private final ObjectMapper objectMapper;

	@Value("${beholder.environmentCredentialses}")
	private String environmentCredentialses;

	@Override
	public UUID computeUid(Credentials object) {
		return RootObjectUtils.computeUidFromValueHashCode(object.getKey());
	}

	@Override
	public List<Credentials> contributeObjects() {
		List<Credentials> credentialses = IoUtils.unchecked(() -> objectMapper.readValue(
				environmentCredentialses,
				objectMapper.constructType(TypeUtils.parameterize(List.class, Credentials.class))));
		//
		logger.info("Initialized: environmentCredentialses.keys = %s".formatted(
				credentialses.map(Credentials::getKey).mkString(",")));
		//
		return credentialses;
	}
}
