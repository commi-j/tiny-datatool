package tk.labyrinth.satool.beholder.domain.javaclass.object;

import io.vavr.collection.List;
import io.vavr.collection.Traversable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.datatypes.reference.ReferenceTool;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectFilterer;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import jakarta.annotation.PostConstruct;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class JavaClassObjectFilterer<T> {

	private final ConverterRegistry converterRegistry;

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final GenericObjectFilterer objectFilterer;

	@SmartAutowired
	private Class<T> javaClass;

	private CodeObjectModelReference objectModelReference;

	private Predicate adjustPredicate(@Nullable Predicate predicate) {
		Predicate result;
		{
			Predicate modelReferencePredicate =
					Predicates.equalTo(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME, objectModelReference);
			//
			if (predicate != null) {
				result = Predicates.and(predicate, modelReferencePredicate);
			} else {
				result = modelReferencePredicate;
			}
		}
		return result;
	}

	@PostConstruct
	private void postConstruct() {
		objectModelReference = javaBaseTypeRegistry.getNormalizedObjectModelReference(javaClass);
	}

	public long count(Traversable<GenericObject> objects, PlainQuery query) {
		return objectFilterer.count(objects, query);
	}

	public List<T> filter(Traversable<GenericObject> objects, PlainQuery query) {
		return objectFilterer
				.filter(
						objects,
						query.toBuilder()
								.predicate(adjustPredicate(query.getPredicate()))
								.build())
				.map(object -> converterRegistry.convert(object, javaClass));
	}

	public List<T> filterAll(Traversable<GenericObject> objects) {
		return objectFilterer
				.filter(
						objects,
						PlainQuery.builder()
								.predicate(adjustPredicate(null))
								.build())
				.map(object -> converterRegistry.convert(object, javaClass));
	}

	@CheckForNull
	public T find(Traversable<GenericObject> objects, @NonNull Reference<T> reference) {
		Predicate predicate = ReferenceTool.referenceToPredicate(reference);
		//
		List<T> found = filter(
				objects,
				PlainQuery.builder()
						.limit(2L)
						.predicate(predicate)
						.build());
		//
		if (found.size() > 1) {
			// TODO: Implement.
			throw new NotImplementedException("reference = %s".formatted(reference));
		}
		//
		return !found.isEmpty() ? found.get(0) : null;
	}
}
