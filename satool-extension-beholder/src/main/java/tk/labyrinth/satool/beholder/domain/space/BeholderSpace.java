package tk.labyrinth.satool.beholder.domain.space;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.datatypes.value.wrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import javax.annotation.CheckForNull;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Builder(toBuilder = true)
@Model(BeholderSpace.MODEL_CODE)
@ModelTag("setup")
@RenderAttribute(BeholderSpace.KEY_ATTRIBUTE_NAME)
@Value
@With
public class BeholderSpace {

	public static final String GENERATED_OBJECT_ATTRIBUTE_NAME = "generated";

	public static final String KEY_ATTRIBUTE_NAME = "key";

	public static final String MODEL_CODE = "space";

	public static final String SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME = "spaceReference";

	String key;

	@Deprecated
	List<GenericObject> objects;

	String uid;

	/**
	 * Null for old spaces.
	 */
	@CheckForNull
	Instant updatedAt;

	String version;

	@CheckForNull
	public GenericObject findByUid(UUID objectUid) {
		ValueWrapper objectUidWrapper = SimpleValueWrapper.of(objectUid.toString());
		return objects.find(object -> Objects.equals(getUidWrapper(object), objectUidWrapper)).getOrNull();
	}

	public GenericObject getByUid(UUID objectUid) {
		GenericObject result = findByUid(objectUid);
		if (result == null) {
			throw new IllegalArgumentException("Not found: objectUid = %s".formatted(objectUid));
		}
		return result;
	}

	public BeholderSpace withUpdatedObjects(List<GenericObject> objects) {
		return withObjects(objects.map(object -> {
			validate(object);
			return object.withNonNullSimpleAttribute(
					SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME,
					KeySpaceReference.of(key).toString());
		}));
	}

	public static ValueWrapper getUidWrapper(GenericObject object) {
		return object.getAttributeValue(RootObjectUtils.UID_ATTRIBUTE_NAME);
	}

	@Deprecated
	public static void validate(GenericObject object) {
		Objects.requireNonNull(getUidWrapper(object), "object.uid, object = %s".formatted(object));
		Objects.requireNonNull(
				RootObjectUtils.findModelReference(object),
				"object.modelReference, object = %s".formatted(object));
	}
}
