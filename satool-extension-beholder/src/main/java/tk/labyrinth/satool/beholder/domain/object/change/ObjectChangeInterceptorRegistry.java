package tk.labyrinth.satool.beholder.domain.object.change;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

import java.util.Objects;

@Component
@Lazy
public class ObjectChangeInterceptorRegistry {

	private final List<Entry<?>> entries;

	@Autowired
	private ConversionService conversionService;

	@SuppressWarnings({"rawtypes", "unchecked"})
	public ObjectChangeInterceptorRegistry(
			@Autowired(required = false) java.util.List<ObjectChangeInterceptor<?>> interceptors,
			JavaBaseTypeRegistry javaBaseTypeRegistry) {
		this.entries = (List<Entry<?>>) (List) Stream.ofAll(interceptors)
				.map(interceptor -> {
					Class<?> objectClass = TypeUtils.getClass(ParameterUtils.getFirstActualParameter(
							interceptor.getClass(),
							ObjectChangeInterceptor.class));
					return new Entry(
							interceptor,
							objectClass,
							javaBaseTypeRegistry.getNormalizedObjectModelReference(objectClass));
				})
				.toList();
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public <T> List<ObjectChangeInterceptor<T>> getInterceptors(
			CodeObjectModelReference objectModelReference,
			Class<T> objectClass) {
		return entries
				.filter(entry -> Objects.equals(entry.getObjectModelReference(), objectModelReference))
				.map(entry -> {
					ObjectChangeInterceptor<?> result;
					if (Objects.equals(entry.getJavaClass(), objectClass)) {
						result = entry.getInterceptor();
					} else {
						result = new ConvertingObjectChangeInterceptorDecorator(
								conversionService,
								objectClass,
								entry.getJavaClass(),
								entry.getInterceptor());
					}
					return (ObjectChangeInterceptor<T>) result;
				});
	}

	@Value
	public static class Entry<T> {

		ObjectChangeInterceptor<T> interceptor;

		Class<T> javaClass;

		CodeObjectModelReference objectModelReference;
	}
}
