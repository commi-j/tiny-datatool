package tk.labyrinth.satool.beholder.domain.value;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.value.wrapper.SimpleValueWrapper;

import jakarta.annotation.PostConstruct;
import java.util.Set;

@Component
@RequiredArgsConstructor
@Slf4j
public class FromSimpleValueWrapperToEnumConverter implements ConditionalGenericConverter {

	private final ObjectProvider<ObjectMapper> objectMapperProvider;

	@PostConstruct
	private void postConstruct() {
		logger.debug("Initialized");
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		return objectMapperProvider.getObject().convertValue(
				((SimpleValueWrapper) source).getValue(),
				targetType.getType());
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return null;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return sourceType.getType() == SimpleValueWrapper.class && targetType.getType().isEnum();
	}
}
