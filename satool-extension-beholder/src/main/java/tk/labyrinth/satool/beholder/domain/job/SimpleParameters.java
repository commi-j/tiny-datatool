package tk.labyrinth.satool.beholder.domain.job;

import lombok.Value;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;

@Value
public class SimpleParameters implements HasSpaceReference {

	KeySpaceReference spaceReference;
}
