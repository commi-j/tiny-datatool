package tk.labyrinth.satool.beholder.entry;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import tk.labyrinth.satool.beholder.BeholderExtensionConfiguration;

@Configuration
@Import(BeholderExtensionConfiguration.class)
@Profile("beholder")
public class BeholderExtensionEntry {
	// empty
}
