package tk.labyrinth.satool.beholder.domain.function.operator;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;

@Component
@EqualsAndHashCode
@RequiredArgsConstructor
public class ContainsOperatorDescriptor implements BinaryOperatorDescriptor<String> {

	@Override
	public Datatype getSecondOperandDatatype(Datatype firstOperandDatatype) {
		return JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(String.class);
	}

	@Override
	public String getText() {
		return "CONTAINS";
	}

	@Override
	public boolean supports(Class<?> firstOperandClass) {
		// TODO: true if we have function that transforms operand into string for contains function.
		return firstOperandClass == String.class;
	}

	@Override
	public Predicate toPredicate(String attributeName, String value) {
		return Predicates.contains(attributeName, value, false);
	}
}
