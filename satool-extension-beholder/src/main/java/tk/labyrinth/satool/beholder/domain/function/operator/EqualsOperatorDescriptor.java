package tk.labyrinth.satool.beholder.domain.function.operator;

import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;

@Component
@EqualsAndHashCode
public class EqualsOperatorDescriptor implements BinaryOperatorDescriptor<Object> {

	public static final String TEXT = "EQUALS";

	@Override
	public Datatype getSecondOperandDatatype(Datatype firstOperandDatatype) {
		return firstOperandDatatype;
	}

	@Override
	public String getText() {
		return TEXT;
	}

	@Override
	public boolean supports(Class<?> firstOperandClass) {
		return true;
	}

	@Override
	public Predicate toPredicate(String attributeName, Object value) {
		return Predicates.equalTo(attributeName, value);
	}
}
