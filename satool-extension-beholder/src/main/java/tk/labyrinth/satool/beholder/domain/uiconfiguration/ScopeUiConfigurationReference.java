package tk.labyrinth.satool.beholder.domain.uiconfiguration;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class ScopeUiConfigurationReference implements Reference<BeholderUiConfiguration> {

	String scope;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				BeholderUiConfiguration.MODEL_CODE,
				BeholderUiConfiguration.SCOPE_ATTRIBUTE_NAME,
				scope);
	}

	@JsonCreator
	public static ScopeUiConfigurationReference from(BeholderUiConfiguration object) {
		return of(object.getScope());
	}

	@JsonCreator
	public static ScopeUiConfigurationReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), BeholderUiConfiguration.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(BeholderUiConfiguration.SCOPE_ATTRIBUTE_NAME));
	}
}
