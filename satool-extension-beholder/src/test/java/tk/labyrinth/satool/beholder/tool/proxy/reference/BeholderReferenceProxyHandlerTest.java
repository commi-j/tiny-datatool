package tk.labyrinth.satool.beholder.tool.proxy.reference;

// TODO: Make useful or remove?
//@ExtendWithSpring
//@Import({
//		BeholderObjectProxyRegistry.class,
//		BeholderObjectValueObjectMapperConfigurer.class,
//		ObjectProxyHandler.class,
//		BeholderReferenceProxyHandler.class,
//		ConversionHandler.class,
//		ReflectionPandoraModelFactory.class,
//})
class BeholderReferenceProxyHandlerTest {
//	@Autowired
//	private BeholderReferenceProxyHandler referenceProxyHandler;
//
//	@Test
//	void testEqualsAndHashCodeOfReferenceProxy() {
//		// Given:
//		//
//		// When:
//		MyProxyReference reference0 = referenceProxyHandler.create(
//				MyProxyReference.class,
//				LinkedHashMap.of(
//						"code", "CD",
//						"codeSystem", "CDSM"));
//		MyProxyReference reference1 = referenceProxyHandler.create(
//				MyProxyReference.class,
//				LinkedHashMap.of(
//						"code", "CD",
//						"codeSystem", "CDSM"));
//		//
//		// Then:
//		Assertions.assertEquals(reference0.hashCode(), reference1.hashCode());
//		Assertions.assertEquals(reference0, reference1);
//	}
//
//	@Test
//	void testToString() {
//		// Given:
//		//
//		// When:
//		MyProxyReference reference0 = referenceProxyHandler.create(
//				MyProxyReference.class,
//				LinkedHashMap.of(
//						"code", "CD",
//						"codeSystem", "CDSM"));
//		//
//		// Then:
//		Assertions.assertEquals(
//				"MyProxyReference(modelCode=myproxy,(code=CD),(codeSystem=CDSM))",
//				reference0.toString());
//	}
//
//	public interface MyProxyReference extends Reference<MyProxy> {
//
//		String getCode();
//
//		String getCodeSystem();
//	}
//
//	public interface MyProxy {
//		// empty
//	}
}