package tk.labyrinth.satool.beholder.base;

import org.atmosphere.inject.AtmosphereRequestIntrospector;
import org.atmosphere.inject.annotation.RequestScoped;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.MergedAnnotation;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

import java.util.Map;

class BeholderCustomViewRegistryTest {

	/**
	 * Because of this one we can not simply scan for all packages.
	 */
	@Test
	void testMergedAnnotationOfAtmosphereRequestIntrospectorThrowsException() {
		ContribAssertions.assertThrows(
				() -> MergedAnnotation.of(
						ClassLoader.getSystemClassLoader(),
						new AtmosphereRequestIntrospector(),
						RequestScoped.class,
						Map.of()),
				fault -> Assertions.assertEquals(
						"java.lang.IndexOutOfBoundsException: Index 0 out of bounds for length 0",
						fault.toString()));
	}
}