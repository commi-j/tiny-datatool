package tk.labyrinth.satool.freemarker;

import freemarker.template.Configuration;
import freemarker.template.Version;

public class FreemarkerConfigurationFactory {

	public static final Version DEFAULT_VERSION = Configuration.VERSION_2_3_31;

	public static Configuration createDefault() {
		Configuration configuration = new Configuration(DEFAULT_VERSION);
		configuration.setFallbackOnNullLoopVariable(false);
		return configuration;
	}
}
