package tk.labyrinth.satool.freemarker;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
public class SatoolFreemarkerObjectWrapper extends DefaultObjectWrapper {

	private final ConverterRegistry converterRegistry;

	public SatoolFreemarkerObjectWrapper(Version incompatibleImprovements, ConverterRegistry converterRegistry) {
		super(incompatibleImprovements);
		this.converterRegistry = converterRegistry;
	}

	@Override
	protected TemplateModel handleUnknownType(Object obj) throws TemplateModelException {
		TemplateModel result;
		{
			if (obj instanceof GenericObject genericObject) {
				TemplateGenericObjectModel templateGenericObjectModel = TemplateGenericObjectModel.of(genericObject);
				templateGenericObjectModel.setObjectWrapper(this);
				result = templateGenericObjectModel;
			} else if (obj instanceof List<?> vavrList) {
				TemplateVavrListModel templateVavrListModel = TemplateVavrListModel.of(vavrList);
				templateVavrListModel.setObjectWrapper(this);
				result = templateVavrListModel;
			} else if (obj instanceof Map<?, ?> vavrMap) {
				// FIXME: Ensure all keys are Strings.
				@SuppressWarnings("unchecked")
				TemplateVavrMapModel templateVavrMapModel = TemplateVavrMapModel.of((Map<String, ?>) vavrMap);
				templateVavrMapModel.setObjectWrapper(this);
				result = templateVavrMapModel;
			} else if (obj instanceof ValueWrapper valueWrapper) {
				result = wrap(valueWrapper.unwrap());
			} else {
				result = wrap(converterRegistry.convert(obj, ValueWrapper.class));
			}
		}
		return result;
	}
}
