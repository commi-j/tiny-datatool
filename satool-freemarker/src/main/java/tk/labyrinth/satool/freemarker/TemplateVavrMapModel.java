package tk.labyrinth.satool.freemarker;

import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;
import io.vavr.collection.Map;
import lombok.Value;

@Value(staticConstructor = "of")
public class TemplateVavrMapModel extends WrappingTemplateModel implements TemplateHashModel {

	Map<String, ?> map;

	@Override
	public TemplateModel get(String key) throws TemplateModelException {
		return wrap(map.get(key).getOrNull());
	}

	@Override
	public boolean isEmpty() throws TemplateModelException {
		return map.isEmpty();
	}
}
