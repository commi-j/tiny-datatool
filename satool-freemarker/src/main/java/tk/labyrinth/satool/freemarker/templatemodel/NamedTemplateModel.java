package tk.labyrinth.satool.freemarker.templatemodel;

import freemarker.template.TemplateModel;

public interface NamedTemplateModel extends TemplateModel {

	String getName();
}
