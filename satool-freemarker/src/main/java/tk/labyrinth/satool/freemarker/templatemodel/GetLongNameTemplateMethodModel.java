package tk.labyrinth.satool.freemarker.templatemodel;

import freemarker.template.SimpleScalar;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.List;

@LazyComponent
public class GetLongNameTemplateMethodModel implements NamedTemplateMethodModel {

	@Override
	public Object exec(List list) {
		Object result;
		{
			SimpleScalar valueWrapper = (SimpleScalar) list.get(0);
			//
			if (valueWrapper != null) {
				result = TypeDescription.of(valueWrapper.getAsString()).toLongForm();
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public String getName() {
		return "getLongName";
	}
}
