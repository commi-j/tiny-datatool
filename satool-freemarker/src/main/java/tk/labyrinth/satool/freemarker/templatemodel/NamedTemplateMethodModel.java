package tk.labyrinth.satool.freemarker.templatemodel;

import freemarker.template.TemplateMethodModelEx;

public interface NamedTemplateMethodModel extends NamedTemplateModel, TemplateMethodModelEx {
	// empty
}
