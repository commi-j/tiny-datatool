package tk.labyrinth.satool.freemarker;

import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;

@Value(staticConstructor = "of")
public class TemplateGenericObjectModel extends WrappingTemplateModel implements TemplateHashModel {

	GenericObject genericObject;

	@Nullable
	@Override
	public TemplateModel get(String key) throws TemplateModelException {
		ValueWrapper attributeValue = genericObject.findAttributeValue(key);
		return attributeValue != null ? wrap(attributeValue.unwrap()) : null;
	}

	@Override
	public boolean isEmpty() throws TemplateModelException {
		return genericObject.getAttributes().isEmpty();
	}
}
