package tk.labyrinth.satool.freemarker;

import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.WrappingTemplateModel;
import io.vavr.collection.List;
import lombok.Value;

@Value(staticConstructor = "of")
public class TemplateVavrListModel extends WrappingTemplateModel implements TemplateSequenceModel {

	List<?> list;

	@Override
	public TemplateModel get(int index) throws TemplateModelException {
		return wrap(list.get(index));
	}

	@Override
	public int size() throws TemplateModelException {
		return list.size();
	}
}
