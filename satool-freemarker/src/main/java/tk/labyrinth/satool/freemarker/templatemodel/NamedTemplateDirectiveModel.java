package tk.labyrinth.satool.freemarker.templatemodel;

import freemarker.template.TemplateDirectiveModel;

public interface NamedTemplateDirectiveModel extends NamedTemplateModel, TemplateDirectiveModel {
	// empty
}
