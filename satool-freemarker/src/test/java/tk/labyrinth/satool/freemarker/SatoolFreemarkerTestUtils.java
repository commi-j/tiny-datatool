package tk.labyrinth.satool.freemarker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;

public class SatoolFreemarkerTestUtils {

	public static String render(Configuration configuration, String templateName, Object data) {
		String result;
		{
			try {
				Template template = configuration.getTemplate(templateName);
				StringWriter writer = new StringWriter();
				template.process(data, writer);
				result = writer.toString();
			} catch (IOException | TemplateException ex) {
				throw new RuntimeException(ex);
			}
		}
		return result;
	}
}
