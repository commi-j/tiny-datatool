package tk.labyrinth.satool.freemarker;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.StringTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SatoolMacrosTest {

	@Test
	void testPrettyList() {
		Configuration configuration = FreemarkerConfigurationFactory.createDefault();
		//
		StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
		//
		configuration.setTemplateLoader(new MultiTemplateLoader(new TemplateLoader[]{
				stringTemplateLoader,
				new ClassTemplateLoader(Thread.currentThread().getContextClassLoader(), "freemarker")
		}));
		//
		configuration.addAutoInclude("satool-macros.ftl");
		//
		stringTemplateLoader.putTemplate(
				"test0",
				"""
						<@pretty_list list=["a","b"]/>
						<@pretty_list list=["a","b"] sep="&"/>
						""");
		//
		Assertions.assertEquals(
				"""
						a, b
						a&b
						""",
				SatoolFreemarkerTestUtils
						.render(configuration, "test0", null)
						.replace("\r\n", "\n"));
	}
}
