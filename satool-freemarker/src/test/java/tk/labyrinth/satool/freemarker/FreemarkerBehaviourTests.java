package tk.labyrinth.satool.freemarker;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

class FreemarkerBehaviourTests {

	@Test
	void testMacroWithLocalVariableWithoutDefaultValue() {
		Configuration configuration = FreemarkerConfigurationFactory.createDefault();
		//
		StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
		configuration.setTemplateLoader(stringTemplateLoader);
		//
		stringTemplateLoader.putTemplate(
				"test0",
				"""
						<#macro hello name>
							Hello, ${name}!
						</#macro>
						<@hello/>
						""");
		//
		ContribAssertions.assertThrows(
				() -> SatoolFreemarkerTestUtils.render(configuration, "test0", null),
				fault -> Assertions.assertEquals(
						"""
								java.lang.RuntimeException: freemarker.core._MiscTemplateException: When calling macro "hello", required parameter "name" (parameter #1) was not specified.
								      
								----
								Tip: If the omission was deliberate, you may consider making the parameter optional in the macro by specifying a default value for it, like <#macro macroName paramName=defaultExpr>)
								----
								      
								----
								FTL stack trace ("~" means nesting-related):
									- Failed at: #macro hello name  [in template "test0" in macro "hello" at line 1, column 1]
									- Reached through: @hello  [in template "test0" at line 4, column 1]
								----""",
						fault.toString()));
	}
}
