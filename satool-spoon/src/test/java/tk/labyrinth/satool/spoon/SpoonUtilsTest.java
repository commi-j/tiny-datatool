package tk.labyrinth.satool.spoon;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.geom.Point2D;
import java.util.Map;

class SpoonUtilsTest {

	@Test
	void testGetLongName() {
		Assertions.assertEquals(
				"String",
				SpoonUtils.getLongName(SpoonStaticFactory.createReference(String.class).getTypeDeclaration()));
		Assertions.assertEquals(
				"Point2D.Double",
				SpoonUtils.getLongName(SpoonStaticFactory.createReference(Point2D.Double.class).getTypeDeclaration()));
		Assertions.assertEquals(
				"Map.Entry",
				SpoonUtils.getLongName(SpoonStaticFactory.createReference(Map.Entry.class).getTypeDeclaration()));
	}
}