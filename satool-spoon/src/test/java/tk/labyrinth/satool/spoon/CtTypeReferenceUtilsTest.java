package tk.labyrinth.satool.spoon;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import java.awt.geom.Point2D;
import java.util.List;

class CtTypeReferenceUtilsTest {

	@Test
	void testGetTypeDescription() {
		Assertions
				.assertThat(CtTypeReferenceUtils.getTypeDescription(SpoonStaticFactory.createReference(String.class)))
				.isEqualTo(TypeDescription.ofNonParameterized(String.class));
		Assertions
				.assertThat(CtTypeReferenceUtils.getTypeDescription(SpoonStaticFactory.createReference(int.class)))
				.isEqualTo(TypeDescription.ofNonParameterized(int.class));
		Assertions
				.assertThat(CtTypeReferenceUtils.getTypeDescription(SpoonStaticFactory.createReference(
						Point2D.Double.class)))
				.isEqualTo(TypeDescription.ofNonParameterized(Point2D.Double.class));
		Assertions
				.assertThat(CtTypeReferenceUtils.getTypeDescription(SpoonStaticFactory.createReference(
						TypeUtils.parameterize(List.class, String.class))))
				.isEqualTo(TypeDescription.ofSimplyParameterized(List.class, String.class));
	}
}