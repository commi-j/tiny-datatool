package tk.labyrinth.satool.spoon;

import io.vavr.collection.List;
import spoon.reflect.reference.CtPackageReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.reference.CtWildcardReference;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import java.util.Objects;

public class CtTypeReferenceUtils {

	public static <T> CtTypeReference<T> createFrom(Class<T> javaClass) {
		return SpoonStaticFactory.createReference(javaClass);
	}

	public static <T> CtTypeReference<T> createFrom(ClassSignature classSignature) {
		return SpoonStaticFactory.createReference(classSignature);
	}

	public static boolean equals(CtTypeReference<?> typeReference, Class<?> javaClass) {
		return Objects.equals(typeReference, createFrom(javaClass));
	}

	public static ClassSignature getClassSignature(CtTypeReference<?> typeReference) {
		return ClassSignature.from(getTypeSignature(typeReference).toString());
	}

	public static String getPackageName(CtTypeReference<?> typeReference) {
		return typeReference.getDeclaringType() != null
				? getPackageName(typeReference.getDeclaringType())
				: typeReference.getPackage().getQualifiedName();
	}

	public static TypeDescription getTypeDescription(CtTypeReference<?> typeReference) {
		TypeDescription result;
		{
			if (isParameterized(typeReference)) {
				result = TypeDescription.of("%s<%s>".formatted(
						getTypeSignature(typeReference),
						List.ofAll(typeReference.getActualTypeArguments())
								.map(CtTypeReferenceUtils::getTypeDescription)
								.mkString(",")));
			} else {
				result = TypeDescription.of(getTypeSignature(typeReference).toString());
			}
		}
		return result;
	}

	public static TypeSignature getTypeSignature(CtTypeReference<?> typeReference) {
		TypeSignature result;
		{
			if (isWildcardReference(typeReference)) {
				result = getTypeSignature(requireWildcardReference(typeReference).getBoundingType());
			} else {
				CtTypeReference<?> declaringType = typeReference.getDeclaringType();
				//
				if (declaringType != null) {
					return TypeSignature.ofValid("%s.%s".formatted(
							getTypeSignature(declaringType),
							typeReference.getSimpleName()));
				} else {
					CtPackageReference pckg = typeReference.getPackage();
					//
					result = TypeSignature.ofValid(pckg != null
							? "%s:%s".formatted(pckg, typeReference.getSimpleName())
							: typeReference.getSimpleName());
				}
			}
		}
		return result;
	}

	public static boolean isParameterized(CtTypeReference<?> typeReference) {
		return !typeReference.getActualTypeArguments().isEmpty();
	}

	public static boolean isWildcardReference(CtTypeReference<?> typeReference) {
		return typeReference instanceof CtWildcardReference;
	}

	public static CtWildcardReference requireWildcardReference(CtTypeReference<?> typeReference) {
		return (CtWildcardReference) typeReference;
	}
}
