package tk.labyrinth.satool.spoon;

import spoon.reflect.declaration.CtClass;
import tk.labyrinth.jaap.model.signature.ClassSignature;

public class CtClassUtils {

	public static ClassSignature getClassSignature(CtClass<?> cl) {
		return ClassSignature.guessFromQualifiedName(cl.getQualifiedName());
	}
}
