package tk.labyrinth.satool.spoon;

import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import jakarta.annotation.PostConstruct;

@Route("spoon")
@RequiredArgsConstructor
@Slf4j
public class SpoonDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		//
	}
}
