package tk.labyrinth.satool.spoon;

import io.vavr.collection.List;
import spoon.reflect.code.CtExpression;
import spoon.reflect.code.CtFieldRead;
import spoon.reflect.code.CtLiteral;
import spoon.reflect.code.CtNewArray;
import spoon.reflect.path.CtRole;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

import java.util.Objects;

public class CtAnnotationUtils {

	public static String getAttributeValueAsString(CtExpression<?> expression) {
		String result;
		{
			if (expression instanceof CtFieldRead<?> ctFieldRead) {
				if (Objects.equals(ctFieldRead.getVariable().getSimpleName(), "class")) {
					result = CtTypeReferenceUtils.getTypeSignature(ctFieldRead.getVariable().getDeclaringType())
							.toString();
				} else {
					throw new NotImplementedException();
				}
			} else if (expression instanceof CtLiteral<?>) {
				result = String.valueOf((Object) expression.getValueByRole(CtRole.VALUE));
			} else if (expression instanceof CtNewArray<?> ctNewArray) {
				result = List.ofAll(ctNewArray.getElements())
						.map(CtAnnotationUtils::getAttributeValueAsString)
						.mkString(",");
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
