package tk.labyrinth.satool.spoon;

import io.vavr.collection.List;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtType;
import spoon.reflect.path.CtRole;
import spoon.reflect.path.impl.CtPathElement;
import spoon.reflect.path.impl.CtPathImpl;
import spoon.reflect.path.impl.CtRolePathElement;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;

import javax.annotation.Nullable;
import java.util.Collection;

public class SpoonUtils {

	public static ElementSignature elementToSignature(CtElement element) {
		CtPathImpl pathImpl = (CtPathImpl) element.getPath();
		List<CtPathElement<?, ?>> pathElements = ClassUtils.castInferred(List.ofAll(pathImpl.getElements()));
		String elementSignatureString = pathElements.zipWithIndex()
				.map(tuple -> renderPathElement(
						element,
						tuple._2() != 0 ? pathElements.get(tuple._2() - 1) : null,
						tuple._1()))
				.mkString();
		return ElementSignature.of(elementSignatureString);
	}

	public static String getLongName(CtType<?> type) {
		return type.getDeclaringType() != null
				? "%s.%s".formatted(getLongName(type.getDeclaringType()), type.getSimpleName())
				: type.getSimpleName();
	}

	public static String renderPathElement(
			CtElement element,
			@Nullable CtPathElement<?, ?> previousPathElement,
			CtPathElement<?, ?> pathElement) {
		String result;
		{
			CtRolePathElement rolePathElement = (CtRolePathElement) pathElement;
			if (rolePathElement.getRole() == CtRole.ANNOTATION) {
				CtAnnotation<?> annotation = (CtAnnotation<?>) element;
				result = "@%s".formatted(elementToSignature(annotation.getType().getTypeDeclaration()));
			} else if (rolePathElement.getRole() == CtRole.CONSTRUCTOR) {
				String spoonSignatureString = rolePathElement.getArguments().get("signature");
				result = List.of(spoonSignatureString.substring(1, spoonSignatureString.length() - 1).split(","))
						.map(ClassSignature::guessFromQualifiedName)
						.map(ClassSignature::toString)
						.mkString("(", ",", ")");
			} else if (rolePathElement.getRole() == CtRole.CONTAINED_TYPE) {
				if (previousPathElement == null) {
					result = rolePathElement.getArguments().get("name");
				} else if (previousPathElement instanceof CtRolePathElement previousRolePathElement &&
						previousRolePathElement.getRole() == CtRole.SUB_PACKAGE) {
					result = ":" + rolePathElement.getArguments().get("name");
				} else {
					result = "." + rolePathElement.getArguments().get("name");
				}
			} else if (rolePathElement.getRole() == CtRole.FIELD) {
				result = "#" + rolePathElement.getArguments().get("name");
			} else if (rolePathElement.getRole() == CtRole.NESTED_TYPE) {
				result = ".%s".formatted(rolePathElement.getArguments().get("name"));
			} else if (rolePathElement.getRole() == CtRole.PARAMETER) {
				result = "#" + rolePathElement.getArguments().get("name");
			} else if (rolePathElement.getRole() == CtRole.SUB_PACKAGE) {
				result = previousPathElement != null
						? "." + rolePathElement.getArguments().get("name")
						: rolePathElement.getArguments().get("name");
			} else if (rolePathElement.getRole() == CtRole.METHOD) {
				String spoonSignatureString = rolePathElement.getArguments().get("signature");
				result = "#%s".formatted(MethodSimpleSignature.guessFromQualifiedSignature(spoonSignatureString));
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public static ClassSignature resolveJavaClassSignature(CtType<?> type) {
		return ClassSignature.guessFromQualifiedName(type.getQualifiedName());
	}

	public static CtTypeReference<?> unwrapIfCollection(CtTypeReference<?> typeReference) {
		CtTypeReference<?> result;
		{
			if (typeReference.isSubtypeOf(SpoonStaticFactory.createReference(Collection.class))) {
				result = typeReference.getActualTypeArguments().get(0);
			} else {
				result = typeReference;
			}
		}
		return result;
	}
}
