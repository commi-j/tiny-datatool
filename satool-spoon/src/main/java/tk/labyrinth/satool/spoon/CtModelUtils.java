package tk.labyrinth.satool.spoon;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtType;
import tk.labyrinth.jaap.model.signature.ClassSignature;

import java.util.Objects;
import java.util.function.Function;

public class CtModelUtils {

	@Nullable
	public static CtType<?> findType(CtModel model, ClassSignature classSignature) {
		CtType<?> result;
		{
			ClassSignature topLevelSignature = classSignature.getTopLevelSignature();
			//
			CtType<?> foundType = model.getAllTypes().stream()
					.filter(type -> Objects.equals(CtTypeUtils.getClassSignature(type), topLevelSignature))
					.findAny()
					.orElse(null);
			//
			if (foundType != null) {
				if (classSignature.isTopLevel()) {
					result = foundType;
				} else {
					result = getTypeTree(foundType)
							.find(type -> Objects.equals(CtTypeUtils.getClassSignature(type), classSignature))
							.getOrNull();
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	public static List<CtType<?>> getTypeTree(CtType<?> type) {
		return List
				.of(
						List.of(type),
						List.ofAll(type.getNestedTypes()).flatMap(CtModelUtils::getTypeTree))
				.flatMap(Function.identity());
	}
}
