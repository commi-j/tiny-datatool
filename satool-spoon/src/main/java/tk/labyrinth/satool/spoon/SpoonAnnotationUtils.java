package tk.labyrinth.satool.spoon;

import io.vavr.collection.List;
import spoon.reflect.declaration.CtImport;
import spoon.reflect.declaration.CtType;
import spoon.reflect.declaration.CtTypeMember;

import java.lang.annotation.Annotation;
import java.util.Objects;

// FIXME: This workaround is not used because we found another one with adding library to Spoon classpath.
//  Need to think if we need this class or can safely remove it.
public class SpoonAnnotationUtils {

	public enum Certainty {
		FALSE,
		MAYBE,
		TRUE,
	}

	public static Certainty getHasAnnotationCertainty(
			CtTypeMember element,
			Class<? extends Annotation> annotationClass) {
		Certainty result;
		{
			if (element.hasAnnotation(annotationClass)) {
				result = Certainty.TRUE;
			} else {
				String annotationPackageName = annotationClass.getPackageName();
				//
				CtType<Object> topLevelType = element.getTopLevelType();
				List<CtImport> imports = List.ofAll(topLevelType.getPosition().getCompilationUnit().getImports());
				String packageName = topLevelType.getPackage().getQualifiedName();
				//
				if (imports.map(CtImport::toString).contains(annotationPackageName + ".*") ||
						Objects.equals(annotationPackageName, packageName)) {
					result = Certainty.MAYBE;
				} else {
					result = Certainty.FALSE;
				}
			}
		}
		return result;
	}

	public static boolean mayHaveAnnotation(CtTypeMember element, Class<? extends Annotation> annotationClass) {
		return getHasAnnotationCertainty(element, annotationClass) != Certainty.FALSE;
	}
}
