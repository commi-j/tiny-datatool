package tk.labyrinth.satool.spoon;

import spoon.reflect.declaration.CtType;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

public class CtTypeUtils {

	public static ClassSignature getClassSignature(CtType<?> type) {
		return CtTypeReferenceUtils.getClassSignature(type.getReference());
	}

	public static TypeSignature getTypeSignature(CtType<?> type) {
		return CtTypeReferenceUtils.getTypeSignature(type.getReference());
	}
}
