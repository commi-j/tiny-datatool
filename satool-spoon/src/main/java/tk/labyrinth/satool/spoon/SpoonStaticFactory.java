package tk.labyrinth.satool.spoon;

import io.vavr.collection.List;
import spoon.reflect.factory.TypeFactory;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class SpoonStaticFactory {

	private static final TypeFactory typeFactory = new TypeFactory();

	public static <T> CtTypeReference<T> createReference(Class<T> javaClass) {
		return typeFactory.createReference(javaClass);
	}

	public static <T> CtTypeReference<T> createReference(ClassSignature classSignature) {
		return typeFactory.createReference(classSignature.getQualifiedName());
	}

	@SuppressWarnings("unchecked")
	public static <T> CtTypeReference<T> createReference(Type javaType) {
		CtTypeReference<T> result;
		{
			if (javaType instanceof Class<?> javaClass) {
				result = createReference((Class<T>) javaClass);
			} else if (javaType instanceof ParameterizedType parameterizedType) {
				CtTypeReference<T> reference = createReference(parameterizedType.getRawType());
				//
				List.of(parameterizedType.getActualTypeArguments()).forEach(actualTypeArgument ->
						reference.addActualTypeArgument(createReference(actualTypeArgument)));
				//
				result = reference;
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
