package tk.labyrinth.satool.spoon;

import io.vavr.collection.List;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;

public class CtMethodUtils {

	public static MethodFullSignature getFullSignature(CtMethod<?> method) {
		return getSimpleSignature(method)
				.toFull(CtClassUtils.getClassSignature((CtClass<?>) method.getParent()).toString());
	}

	public static MethodSimpleSignature getSimpleSignature(CtMethod<?> method) {
		return MethodSimpleSignature.of(
				method.getSimpleName(),
				List.ofAll(method.getParameters())
						.map(CtParameter::getType)
						.map(CtTypeReferenceUtils::getTypeSignature)
						.asJava());
	}
}
