General

- Add annotation to specify default sorting for objects (job runs DESC by created).
- Replace @PandoraExtensionPoint with @BeholderExtensionPoint

Manifester

- Properties: declared in lcc in Java (fooBar) but in files they are foo-bar.
  Need to check if it works and if yes, add aliases support.
- Find out why manifest index is duplicated on job run (or what else fails rendering the dashboard).
- PersistentDiagram | Make custom relations to be any of java signature and database name

Plantuml

- Make plant page remember state (diagram);
- Add "open in new tab button" to plant view;

Cosmetics

- Add Copy button to Fault view.
- Support rendering of polymorphic leafs.