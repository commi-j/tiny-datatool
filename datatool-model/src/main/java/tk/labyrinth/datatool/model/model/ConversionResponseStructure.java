package tk.labyrinth.datatool.model.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ConversionResponseStructure {

  JsonNode jsonNode;
}
