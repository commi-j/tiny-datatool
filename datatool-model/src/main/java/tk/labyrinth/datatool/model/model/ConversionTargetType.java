package tk.labyrinth.datatool.model.model;

public enum ConversionTargetType {

  JSON,
  XML

}
