package tk.labyrinth.datatool.model.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class ConversionRequestStructure {

  private final String inputString;
  private final boolean hasDataType;
  private final boolean needNormalizeKeys;
  private final boolean needNormalizeValues;
  private final ConversionTargetType conversionTargetType;
  private List<List<ConversionRequestSubject>> subjects;
}
