package tk.labyrinth.datatool.model.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ConversionRequestSubject {

  String key;
  String value;
  DataType type;

  public ConversionRequestSubject(String key, String value) {
    this(key, value, DataType.STRING);
  }
}
