package tk.labyrinth.datatool.model.model;

@Deprecated
public enum DataType {
	ARRAY,
	BOOLEAN,
	NUMBER,
	STRING
}
