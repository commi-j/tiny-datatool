# S.A.Tool

[![RELEASES](https://img.shields.io/maven-central/v/tk.labyrinth.datatool/datatool-application.svg?label=Releases&style=flat-square)](https://mvnrepository.com/artifact/tk.labyrinth.datatool/datatool-application)
[![DOCKER_HUB](https://img.shields.io/docker/v/tklabyrinth/satool?label=Docker%20Hub)](https://hub.docker.com/r/tklabyrinth/satool)
[![PIPELINE_MASTER](https://gitlab.com/commi-j/tiny-datatool/badges/master/pipeline.svg?key_text=Master&key_width=50&style=flat-square)](https://gitlab.com/commitman/datatool-application/-/pipelines)
[![TEST_COVERAGE_MASTER](https://gitlab.com/commi-j/tiny-datatool/badges/master/coverage.svg?key_text=Coverage&style=flat-square)](https://gitlab.com/commi-j/tiny-datatool/-/jobs)

[![SNAPSHOTS](https://img.shields.io/nexus/s/tk.labyrinth.datatool/datatool-application?label=Snapshots&server=https%3A%2F%2Foss.sonatype.org&style=flat-square)](https://oss.sonatype.org/#nexus-search;gav~tk.labyrinth.datatool~datatool-application~~~)
[![DOCKER_HUB](https://img.shields.io/docker/v/tklabyrinth/satool?label=Docker%20Hub)](https://hub.docker.com/r/tklabyrinth/satool)
[![PIPELINE_DEV](https://gitlab.com/commi-j/tiny-datatool/badges/dev/pipeline.svg?key_text=Dev&key_width=35&style=flat-square)](https://gitlab.com/commitman/datatool-application/-/pipelines)
[![TEST_COVERAGE_DEV](https://gitlab.com/commi-j/tiny-datatool/badges/dev/coverage.svg?key_text=Coverage&style=flat-square)](https://gitlab.com/commi-j/tiny-datatool/-/jobs)

[![LICENSE](https://img.shields.io/gitlab/license/commi-j/tiny-datatool?color=q&label=License&style=flat-square)](LICENSE)

A small but very necessary application, although no one really understands what should be in it, but it will definitely
be very cool.

* * *

## Developing the project

To run project locally use bundled IDEA run configurations:

- clean install (from root module, to build project, including Vaadin frontend resources)
- DatatoolApplication (from *-application module, where Vaadin resources located)

### Links

- Codestyle: https://gitlab.com/commi-idea/idea-codestyle

### Credentials leakages

- Mongo password leaked in run profile (TODO: What this was about?);

### Contacts

- Telegram: [commi-dev](https://t.me/commi_dev)