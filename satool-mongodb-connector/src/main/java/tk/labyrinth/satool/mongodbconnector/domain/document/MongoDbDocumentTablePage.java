package tk.labyrinth.satool.mongodbconnector.domain.document;

import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.satool.mongodbconnector.MongoDbLayout;

@RequiredArgsConstructor
@Route(value = "collection", layout = MongoDbLayout.class)
public class MongoDbDocumentTablePage extends CssVerticalLayout implements HasUrlParameter<String> {

	@Override
	public void setParameter(BeforeEvent event, String parameter) {
	}
}
