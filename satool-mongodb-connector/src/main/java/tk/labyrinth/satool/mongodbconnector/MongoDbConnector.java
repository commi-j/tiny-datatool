package tk.labyrinth.satool.mongodbconnector;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoCollection;
import io.vavr.collection.List;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.mongodbconnector.domain.collection.MongoDbCollection;

import jakarta.annotation.PostConstruct;

@PrototypeScopedComponent
public class MongoDbConnector {

	@SmartAutowired
	private ConnectionString connectionString;

	private MongoTemplate mongoTemplate;

	@PostConstruct
	private void postConstruct() {
		mongoTemplate = new MongoTemplate(new SimpleMongoClientDatabaseFactory(connectionString));
	}

	public List<MongoDbCollection> getCollections() {
		return List.ofAll(mongoTemplate.getCollectionNames())
				.map(collectionName -> {
					MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
					return MongoDbCollection.builder()
							.documentCount(collection.countDocuments())
							.name(collectionName)
							.build();
				});
	}
}
