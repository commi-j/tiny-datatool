package tk.labyrinth.satool.mongodbconnector;

import com.mongodb.ConnectionString;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.Route;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.mongodbconnector.domain.collection.MongoDbCollection;

import javax.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import java.util.List;

@RequiredArgsConstructor
@Route(value = "collections", layout = MongoDbLayout.class)
public class MongoDbCollectionsPage extends CssVerticalLayout implements
		AcceptsConnectionString,
		RenderableView<MongoDbCollectionsPage.Properties> {

	private final BeanContext beanContext;

	private final Grid<MongoDbCollection> collectionGrid = new Grid<>();

	@Nullable
	private MongoDbConnector mongoDbConnector = null;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				collectionGrid
						.addColumn(MongoDbCollection::getName)
						.setHeader("Name");
				collectionGrid
						.addColumn(MongoDbCollection::getDocumentCount)
						.setHeader("Documents");
			}
			CssFlexItem.setFlexGrow(collectionGrid, 1);
			add(collectionGrid);
		}
	}

	@Override
	public void acceptConnectionString(@Nullable String connectionString) {
		{
			if (connectionString != null) {
				mongoDbConnector = beanContext.getBeanFactory()
						.withBean(new ConnectionString(connectionString))
						.getBean(MongoDbConnector.class);
			} else {
				mongoDbConnector = null;
			}
		}
		{
			render();
		}
	}

	@Override
	public Component asVaadinComponent() {
		return this;
	}

	public void render() {
		collectionGrid.setItems(mongoDbConnector != null
				? mongoDbConnector.getCollections().asJava()
				: List.of());
	}

	@Override
	public void render(Properties properties) {
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties {

	}
}
