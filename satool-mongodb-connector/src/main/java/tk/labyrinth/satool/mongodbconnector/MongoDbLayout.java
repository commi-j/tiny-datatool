package tk.labyrinth.satool.mongodbconnector;

import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

@RoutePrefix("mongodb")
public class MongoDbLayout extends CssVerticalLayout implements RouterLayout {
	// FIXME: This is strongly outdated, require a lot of rework.
	//  And actually should be moved to SATool repo.
//	private final ValueBox<String> connectionStringBox = new StringValueBox()
//			.withLabel("Connection String");

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			CssHorizontalLayout header = new CssHorizontalLayout();
			{
				header.addClassNames(PandoraStyles.LAYOUT);
			}
			{
//				CssFlexItem.setFlexGrow(connectionStringBox.asVaadinComponent(), 1);
//				header.add(connectionStringBox.asVaadinComponent());
			}
			add(header);
		}
	}

	private void render(Void properties) {
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		{
			CssFlexItem.setFlexGrow(content, 1);
		}
		{
			if (content instanceof AcceptsConnectionString acceptsConnectionString) {
//				connectionStringBox.setChangeListener(acceptsConnectionString::acceptConnectionString);
//				acceptsConnectionString.acceptConnectionString(connectionStringBox.getValue());
			}
		}
		{
			getElement().appendChild(content.getElement());
		}
	}
}
