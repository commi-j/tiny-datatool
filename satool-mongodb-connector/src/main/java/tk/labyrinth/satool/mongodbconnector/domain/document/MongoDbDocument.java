package tk.labyrinth.satool.mongodbconnector.domain.document;

import io.vavr.collection.LinkedHashMap;
import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class MongoDbDocument {

	LinkedHashMap<String, Object> attributes;
}
