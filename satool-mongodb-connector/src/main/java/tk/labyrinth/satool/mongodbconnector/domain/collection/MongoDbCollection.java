package tk.labyrinth.satool.mongodbconnector.domain.collection;

import lombok.Builder;
import lombok.Value;

import java.time.Instant;

@Builder(toBuilder = true)
@Value
public class MongoDbCollection {

	Long documentCount;

	String name;

	// TODO
	Instant updatedAt;
}
