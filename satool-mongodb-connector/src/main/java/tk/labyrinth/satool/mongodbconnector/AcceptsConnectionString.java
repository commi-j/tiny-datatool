package tk.labyrinth.satool.mongodbconnector;

public interface AcceptsConnectionString {

	void acceptConnectionString(String connectionString);
}
