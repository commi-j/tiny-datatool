package tk.labyrinth.satool.maven.domain;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class MavenRepository {

	@Nullable
	String name;

	@Nullable
	String url;
}
