package tk.labyrinth.satool.maven.domain;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Parent;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;

import javax.annotation.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderPattern("$groupId:$artifactId")
@Value
@With
public class MavenDependency {

	String artifactId;

	String groupId;

	@Nullable
	String scope;

	String type;

	@Nullable
	String version;

	public String getGroupIdArtifactId() {
		return "%s:%s".formatted(groupId, artifactId);
	}

	public static MavenDependency from(Dependency mavenModelDependency) {
		return MavenDependency.builder()
				.artifactId(mavenModelDependency.getArtifactId())
				.groupId(mavenModelDependency.getGroupId())
				.scope(mavenModelDependency.getScope())
				.type(mavenModelDependency.getType())
				.version(mavenModelDependency.getVersion())
				.build();
	}

	public static MavenDependency from(Parent mavenModelParent) {
		return MavenDependency.builder()
				.artifactId(mavenModelParent.getArtifactId())
				.groupId(mavenModelParent.getGroupId())
				.scope("parent")
				.type("parent")
				.version(mavenModelParent.getVersion())
				.build();
	}
}
