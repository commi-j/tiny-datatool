package tk.labyrinth.satool.maven.testing;

import tk.labyrinth.satool.maven.domain.MavenArtifactCoordinates;

public class TestingConstants {

	public static final MavenArtifactCoordinates SPRING_BOOT_DEPENDENCIES_COORDINATES = MavenArtifactCoordinates
			.builder()
			.artifactId("spring-boot-dependencies")
			.groupId("org.springframework.boot")
			.version("3.0.0")
			.build();
}
