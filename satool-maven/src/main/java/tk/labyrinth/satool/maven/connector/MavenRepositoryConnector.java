package tk.labyrinth.satool.maven.connector;

import lombok.NonNull;
import org.apache.commons.io.IOUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import tk.labyrinth.pandora.misc4j.java.net.UrlUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.maven.domain.MavenArtifactCoordinates;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@LazyComponent
public class MavenRepositoryConnector {

	public static final String MAVEN_CENTRAL_URL_STRING = "https://repo.maven.apache.org/maven2";

	public static final URL MAVEN_CENTRAL_URL = UrlUtils.from(MAVEN_CENTRAL_URL_STRING);

	public Model fetchPom(@NonNull URL repositoryUrl, @NonNull MavenArtifactCoordinates artifactCoordinates) {
		try {
			String pomString = IOUtils.toString(
					new URL("%s%s".formatted(repositoryUrl, coordinatesToPomPath(artifactCoordinates))),
					StandardCharsets.UTF_8);
			//
			Model pomModel = new MavenXpp3Reader().read(new StringReader(pomString), true);
			//
			return pomModel;
		} catch (IOException | XmlPullParserException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String coordinatesToPomPath(MavenArtifactCoordinates artifactCoordinates) {
		return "/%1$s/%2$s/%3$s/%2$s-%3$s.pom".formatted(
				artifactCoordinates.getGroupId().replace('.', '/'),
				artifactCoordinates.getArtifactId(),
				artifactCoordinates.getVersion());
	}
}
