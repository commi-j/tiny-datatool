package tk.labyrinth.satool.maven.domain;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class MavenArtifactCoordinates {

	String artifactId;

	String groupId;

	String version;

	public static MavenArtifactCoordinates of(String groupId, String artifactId, String version) {
		return new MavenArtifactCoordinates(artifactId, groupId, version);
	}
}
