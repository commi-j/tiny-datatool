package tk.labyrinth.satool.maven.connector;

import org.apache.maven.model.Model;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.satool.maven.testing.TestingConstants;

class MavenRepositoryConnectorTest {

	@Test
	void testFetchPom() {
		MavenRepositoryConnector mavenRepositoryConnector = new MavenRepositoryConnector();
		//
		Model model = mavenRepositoryConnector.fetchPom(
				MavenRepositoryConnector.MAVEN_CENTRAL_URL,
				TestingConstants.SPRING_BOOT_DEPENDENCIES_COORDINATES);
		//
		Assertions.assertEquals("org.springframework.boot:spring-boot-dependencies:pom:3.0.0", model.getId());
		Assertions.assertEquals(176, model.getProperties().size());
		Assertions.assertEquals(398, model.getDependencyManagement().getDependencies().size());
	}
}