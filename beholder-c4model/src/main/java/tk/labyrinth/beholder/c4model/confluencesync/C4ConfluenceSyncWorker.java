package tk.labyrinth.beholder.c4model.confluencesync;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.CredentialsUtils;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.accessibleresource.WebResource;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClient;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClientFactory;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceGetContentQuery;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceDocument;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceCellElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceRowElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceTableElement;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceBody;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceStorage;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceVersion;
import tk.labyrinth.satool.plantuml.c4model.model.C4Component;
import tk.labyrinth.satool.plantuml.c4model.model.C4Container;
import tk.labyrinth.satool.plantuml.c4model.model.C4Model;
import tk.labyrinth.satool.plantuml.c4model.model.C4Person;
import tk.labyrinth.satool.plantuml.c4model.model.C4Relationship;
import tk.labyrinth.satool.plantuml.c4model.model.C4System;

import java.util.Objects;
import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
public class C4ConfluenceSyncWorker {

	private final ConfluenceFeignClientFactory confluenceFeignClientFactory;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@SmartAutowired
	private TypedObjectSearcher<WebResource> webResourceSearcher;

	public Try<C4ConfluenceSyncScoutResult> scout(C4ConfluenceSyncConfiguration configuration) {
		return Try.ofSupplier(() -> {
			C4ConfluenceSyncScoutResult result;
			{
				if (configuration.getConfluenceReference() != null && configuration.getPageId() != null) {
					WebResource webResource = webResourceSearcher.getSingle(configuration.getConfluenceReference());
					//
					Optional<Credentials> credentialsOptional = Optional.ofNullable(webResource.getCredentialsReference())
							.map(credentialsSearcher::getSingle);
					//
					ConfluenceFeignClient confluenceFeignClient = confluenceFeignClientFactory.create(webResource.getUrl());
					//
					ConfluenceContent content = confluenceFeignClient.getContent(
							credentialsOptional.map(CredentialsUtils::toAuthorizationString).orElse(null),
							configuration.getPageId(),
							ConfluenceGetContentQuery.builder()
									.expand(ConfluenceStorage.EXPAND_KEY)
									.build());
					//
					ConfluenceDocument document = ConfluenceDocument.from(content.getBody().getStorage().getValue());
					//
					val detailses = getSourceAndTargetDetailses(configuration, document);
					ConfluenceElement sourceDetails = detailses.getLeft();
					ConfluenceElement targetDetails = detailses.getRight();
					//
					C4Model model = parseModel(sourceDetails);
					//
					result = C4ConfluenceSyncScoutResult.builder()
							.checks(List.empty())
							.model(model)
							.build();
				} else {
					result = C4ConfluenceSyncScoutResult.builder()
							.checks(List
									.of(
											configuration.getConfluenceReference() == null
													? Pair.of("No ConfluenceReference specified", false)
													: null,
											configuration.getPageId() == null
													? Pair.of("No PageId specified", false)
													: null)
									.filter(Objects::nonNull))
							.build();
				}
			}
			return result;
		});
	}

	public Try<Void> sync(C4ConfluenceSyncConfiguration configuration) {
		// TODO: It is suboptimal to invoke scout and fetch content again.
		//  The proper way would be to reuse method of transforming source to model.
		return scout(configuration)
				.map(scoutResult -> {
					WebResource webResource = webResourceSearcher.getSingle(configuration.getConfluenceReference());
					//
					Optional<Credentials> credentialsOptional = Optional.ofNullable(webResource.getCredentialsReference())
							.map(credentialsSearcher::getSingle);
					String authorization = credentialsOptional.map(CredentialsUtils::toAuthorizationString).orElse(null);
					//
					ConfluenceFeignClient confluenceFeignClient = confluenceFeignClientFactory.create(webResource.getUrl());
					//
					ConfluenceContent content = confluenceFeignClient.getContent(
							authorization,
							configuration.getPageId(),
							ConfluenceGetContentQuery.builder()
									.expand("%s,%s".formatted(ConfluenceStorage.EXPAND_KEY, ConfluenceVersion.EXPAND_KEY))
									.build());
					//
					ConfluenceDocument document = ConfluenceDocument.from(content.getBody().getStorage().getValue());
					//
					val detailses = getSourceAndTargetDetailses(configuration, document);
					ConfluenceElement sourceDetails = detailses.getLeft();
					ConfluenceElement targetDetails = detailses.getRight();
					//
					if (targetDetails != null) {
						String plantumlXml = createPlantumlXml(scoutResult.getModel());
						//
						ConfluenceElement contentRoot = targetDetails.select("/*/rich-text-body").get(0);
						//
						contentRoot.getJsoupElement().children().remove();
						//
						contentRoot.getJsoupElement().append(plantumlXml);
						//
						confluenceFeignClient.putContent(
								authorization,
								configuration.getPageId(),
								ConfluenceContent.builder()
										.body(ConfluenceBody.builder()
												.storage(ConfluenceStorage.builder()
														.representation("storage")
														.value(document.toString())
														.build())
												.build())
										.title(content.getTitle())
										.type("page")
										.version(ConfluenceVersion.builder()
												.number(content.getVersion().getNumber() + 1)
												.build())
										.build());
					} else {
						// TODO: exception?
					}
					//
					return null;
				});
	}

	public static String createPlantumlXml(C4Model model) {
		return """
				<ac:structured-macro ac:name="details">
					<ac:parameter ac:name="hidden">true</ac:parameter>
					<ac:rich-text-body>
						Note: All the content (diagrams) within the parent macro is generated and may be overriden in case of changes in the source tables.
					</ac:rich-text-body>
				</ac:structured-macro>
				<ac:structured-macro ac:name="auitabs">
					<ac:rich-text-body>
						<ac:structured-macro ac:name="auitabspage">
							<ac:parameter ac:name="title">Context</ac:parameter>
							<ac:rich-text-body>
								%s
							</ac:rich-text-body>
						</ac:structured-macro>
						<ac:structured-macro ac:name="auitabspage">
							<ac:parameter ac:name="title">Containers</ac:parameter>
							<ac:rich-text-body>
								<ac:structured-macro ac:name="auitabs">
									<ac:rich-text-body>
										%s
									</ac:rich-text-body>
								</ac:structured-macro>
							</ac:rich-text-body>
						</ac:structured-macro>
						<ac:structured-macro ac:name="auitabspage">
							<ac:parameter ac:name="title">Components</ac:parameter>
							<ac:rich-text-body>
								<ac:structured-macro ac:name="auitabs">
									<ac:rich-text-body>
										%s
									</ac:rich-text-body>
								</ac:structured-macro>
							</ac:rich-text-body>
						</ac:structured-macro>
					</ac:rich-text-body>
				</ac:structured-macro>
				<sub>Generated by Beholder</sub>""".formatted(
				renderPlantumlMacro(model.renderSystemsDiagram()),
				model.getExpandableSystems()
						.map(system -> Pair.of(system.getAlias(), system.getName()))
						.prepend(Pair.of(null, "All"))
						.map(pair -> Pair.of(pair.getRight(), model.renderContainersDiagram(pair.getLeft())))
						.map(pair -> renderPlantumlTab(pair.getLeft(), pair.getRight()))
						.mkString("\n"),
				model.getExpandableContainers()
						.map(container -> Pair.of(container.getAlias(), container.getName()))
						.prepend(Pair.of(null, "All"))
						.map(pair -> Pair.of(pair.getRight(), model.renderComponentsDiagram(pair.getLeft())))
						.map(pair -> renderPlantumlTab(pair.getLeft(), pair.getRight()))
						.mkString("\n"));
	}

	public static Pair<@Nullable ConfluenceElement, @Nullable ConfluenceElement> getSourceAndTargetDetailses(
			C4ConfluenceSyncConfiguration configuration,
			ConfluenceDocument document) {
		val detailsElementAndIdPairs = List
				.ofAll(document.select("//structured-macro[@name='details']"))
				.map(detailsElement -> Pair.of(
						detailsElement,
						Optional.ofNullable(detailsElement.select("//parameter[@name='id']").singleOption().getOrNull())
								.map(ConfluenceElement::text)
								.orElse(null)));
		//
		List<ConfluenceElement> sourceElements = configuration.getSourceDetailsId() != null
				? detailsElementAndIdPairs
				.filter(pair -> Objects.equals(pair.getRight(), configuration.getSourceDetailsId()))
				.map(Pair::getLeft)
				: List.empty();
		List<ConfluenceElement> targetElements = configuration.getTargetDetailsId() != null
				? detailsElementAndIdPairs
				.filter(pair -> Objects.equals(pair.getRight(), configuration.getTargetDetailsId()))
				.map(Pair::getLeft)
				: List.empty();
		//
		// TODO: Support check messages.
		//
		return Pair.of(
				sourceElements.size() == 1 ? sourceElements.get(0) : null,
				targetElements.size() == 1 ? targetElements.get(0) : null);
	}

	public static boolean hasDatabaseStyle(String style) {
		return Objects.equals(style, "DATABASE");
	}

	public static boolean hasExternalStyle(String style) {
		return Objects.equals(style, "EXTERNAL");
	}

	public static C4Model parseModel(ConfluenceElement element) {
		Map<String, ConfluenceElement> headerAndTableElementPairs = List.ofAll(element.select("//h2"))
				.toMap(ConfluenceElement::text, ConfluenceElement::nextElementSibling);
		//
		Map<String, List<C4Component>> components = Optional
				.ofNullable(headerAndTableElementPairs.get("Components").getOrNull())
				.filter(innerElement -> Objects.equals(innerElement.tagName(), "table"))
				.map(ConfluenceTableElement::from)
				.map(table -> {
					Map<String, List<C4Component>> result;
					{
						List<ConfluenceRowElement> rows = table.getRows();
						//
						if (!rows.isEmpty() && Objects.equals(
								rows.get(0).getCells().filter(ConfluenceCellElement::isHeader).map(ConfluenceCellElement::text),
								List.of("Alias", "Name", "Description", "Container Alias", "Style"))) {
							result = rows
									.tail()
									.map(row -> row.getCells().map(ConfluenceCellElement::text))
									.filter(cellValues -> !cellValues.get(0).isEmpty() && !cellValues.get(3).isEmpty())
									.map(cellValues -> Pair.of(
											cellValues.get(3),
											C4Component.builder()
													.alias(cellValues.get(0))
													.database(hasDatabaseStyle(cellValues.get(4)))
													.description(Optional.ofNullable(cellValues.get(2))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.external(hasExternalStyle(cellValues.get(4)))
													.name(Optional.ofNullable(cellValues.get(1))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.build()))
									.groupBy(Pair::getLeft)
									.mapValues(list -> list.map(Pair::getRight));
						} else {
							result = HashMap.empty();
						}
					}
					return result;
				})
				.orElse(HashMap.empty());
		//
		Map<String, List<C4Container>> containers = Optional
				.ofNullable(headerAndTableElementPairs.get("Containers").getOrNull())
				.filter(innerElement -> Objects.equals(innerElement.tagName(), "table"))
				.map(ConfluenceTableElement::from)
				.map(table -> {
					Map<String, List<C4Container>> result;
					{
						List<ConfluenceRowElement> rows = table.getRows();
						//
						if (!rows.isEmpty() && Objects.equals(
								rows.get(0).getCells().filter(ConfluenceCellElement::isHeader).map(ConfluenceCellElement::text),
								List.of("Alias", "Name", "Description", "System Alias", "Style"))) {
							result = rows
									.tail()
									.map(row -> row.getCells().map(ConfluenceCellElement::text))
									.filter(cellValues -> !cellValues.get(0).isEmpty() && !cellValues.get(3).isEmpty())
									.map(cellValues -> Pair.of(
											cellValues.get(3),
											C4Container.builder()
													.alias(cellValues.get(0))
													.components(components.get(cellValues.get(0)).getOrNull())
													.database(hasDatabaseStyle(cellValues.get(4)))
													.description(Optional.ofNullable(cellValues.get(2))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.external(hasExternalStyle(cellValues.get(4)))
													.name(Optional.ofNullable(cellValues.get(1))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.build()))
									.groupBy(Pair::getLeft)
									.mapValues(list -> list.map(Pair::getRight));
						} else {
							result = HashMap.empty();
						}
					}
					return result;
				})
				.orElse(HashMap.empty());
		//
		return C4Model.builder()
				.persons(Optional.ofNullable(headerAndTableElementPairs.get("Persons").getOrNull())
						.filter(innerElement -> Objects.equals(innerElement.tagName(), "table"))
						.map(ConfluenceTableElement::from)
						.map(table -> {
							List<C4Person> result;
							{
								List<ConfluenceRowElement> rows = table.getRows();
								//
								if (!rows.isEmpty() && Objects.equals(
										rows.get(0).getCells().filter(ConfluenceCellElement::isHeader).map(ConfluenceCellElement::text),
										List.of("Alias", "Name", "Description"))) {
									result = rows
											.tail()
											.map(row -> row.getCells().map(ConfluenceCellElement::text))
											.filter(cellValues -> !cellValues.get(0).isEmpty())
											.map(cellValues -> C4Person.builder()
													.alias(cellValues.get(0))
													.description(Optional.ofNullable(cellValues.get(2))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.name(Optional.ofNullable(cellValues.get(1))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.build());
								} else {
									result = List.empty();
								}
							}
							return result;
						})
						.orElse(null))
				.relationships(Optional.ofNullable(headerAndTableElementPairs.get("Relationships").getOrNull())
						.filter(innerElement -> Objects.equals(innerElement.tagName(), "table"))
						.map(ConfluenceTableElement::from)
						.map(table -> {
							List<C4Relationship> result;
							{
								List<ConfluenceRowElement> rows = table.getRows();
								//
								if (!rows.isEmpty() && Objects.equals(
										rows.get(0).getCells().filter(ConfluenceCellElement::isHeader).map(ConfluenceCellElement::text),
										List.of("From Alias", "To Alias", "Text", "Description"))) {
									result = rows
											.tail()
											.map(row -> row.getCells().map(ConfluenceCellElement::text))
											.filter(cellValues -> !cellValues.get(0).isEmpty() && !cellValues.get(1).isEmpty())
											.map(cellValues -> C4Relationship.builder()
													.description(Optional.ofNullable(cellValues.get(3))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.fromAlias(cellValues.get(0))
													.text(Optional.ofNullable(cellValues.get(2))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.toAlias(cellValues.get(1))
													.build());
								} else {
									result = List.empty();
								}
							}
							return result;
						})
						.orElse(null))
				.systems(Optional.ofNullable(headerAndTableElementPairs.get("Systems").getOrNull())
						.filter(innerElement -> Objects.equals(innerElement.tagName(), "table"))
						.map(ConfluenceTableElement::from)
						.map(table -> {
							List<C4System> result;
							{
								List<ConfluenceRowElement> rows = table.getRows();
								//
								if (!rows.isEmpty() && Objects.equals(
										rows.get(0).getCells().filter(ConfluenceCellElement::isHeader).map(ConfluenceCellElement::text),
										List.of("Alias", "Name", "Description", "Style"))) {
									result = rows
											.tail()
											.map(row -> row.getCells().map(ConfluenceCellElement::text))
											.filter(cellValues -> !cellValues.get(0).isEmpty())
											.map(cellValues -> C4System.builder()
													.alias(cellValues.get(0))
													.containers(containers.get(cellValues.get(0)).getOrNull())
													.database(hasDatabaseStyle(cellValues.get(3)))
													.description(Optional.ofNullable(cellValues.get(2))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.external(hasExternalStyle(cellValues.get(3)))
													.name(Optional.ofNullable(cellValues.get(1))
															.filter(value -> !value.isEmpty())
															.orElse(null))
													.build());
								} else {
									result = List.empty();
								}
							}
							return result;
						})
						.orElse(null))
				.build();
	}

	public static String renderPlantumlMacro(String data) {
		return """
				<ac:structured-macro ac:name="plantuml">
					<ac:plain-text-body><![CDATA[%s]]></ac:plain-text-body>
				</ac:structured-macro>""".formatted(data);
	}

	public static String renderPlantumlTab(String tabName, String plantumlData) {
		return """
				<ac:structured-macro ac:name="auitabspage">
					<ac:parameter ac:name="title">%s</ac:parameter>
					<ac:rich-text-body>
						%s
					</ac:rich-text-body>
				</ac:structured-macro>""".formatted(tabName, renderPlantumlMacro(plantumlData));
	}
}
