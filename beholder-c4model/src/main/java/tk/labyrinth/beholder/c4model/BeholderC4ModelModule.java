package tk.labyrinth.beholder.c4model;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.satool.plantuml.SatoolPlantumlModule;

@Import({
		SatoolPlantumlModule.class,
})
@Slf4j
@SpringBootApplication
public class BeholderC4ModelModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
