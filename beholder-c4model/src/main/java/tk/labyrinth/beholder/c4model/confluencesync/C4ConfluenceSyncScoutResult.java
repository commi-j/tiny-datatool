package tk.labyrinth.beholder.c4model.confluencesync;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.satool.plantuml.c4model.model.C4Model;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class C4ConfluenceSyncScoutResult {

	@NonNull
	List<Pair<String, Boolean>> checks;

	@Nullable
	C4Model model;
}
