package tk.labyrinth.beholder.c4model.confluencesync;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.domain.accessibleresource.WebResource;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model
@ModelTag("confluence")
@Value
@With
public class C4ConfluenceSyncConfiguration {

	UidReference<WebResource> confluenceReference;

	String pageId;

	String sourceDetailsId;

	String targetDetailsId;
}
