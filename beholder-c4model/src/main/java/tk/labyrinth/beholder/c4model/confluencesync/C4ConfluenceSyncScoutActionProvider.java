package tk.labyrinth.beholder.c4model.confluencesync;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.core.FaultViews;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.component.objectaction.TypedObjectActionProvider;
import tk.labyrinth.satool.plantuml.c4model.view.PlantumlC4DiagramViewRenderer;

@LazyComponent
@RequiredArgsConstructor
public class C4ConfluenceSyncScoutActionProvider extends TypedObjectActionProvider<C4ConfluenceSyncConfiguration> {

	private final C4ConfluenceSyncWorker c4confluenceSyncWorker;

	private final PlantumlC4DiagramViewRenderer c4diagramViewRenderer;

	@Nullable
	@Override
	protected Component provideActionForTypedObject(Context<C4ConfluenceSyncConfiguration> context) {
		return ButtonRenderer.render(builder -> builder
				.onClick(event -> {
					Try<C4ConfluenceSyncScoutResult> resultTry = c4confluenceSyncWorker.scout(context.getObject());
					//
					Component component = resultTry
							.map(scoutResult -> {
								Component result;
								{
									if (scoutResult.getModel() != null) {
										result = c4diagramViewRenderer.render(innerBuilder -> innerBuilder
												.model(scoutResult.getModel())
												.build());
									} else {
										result = new Span("No Model");
									}
								}
								return result;
							})
							.recover(FaultViews::createTextArea)
							.get();
					//
					Dialogs.show(component);
				})
				.text("Scout")
				.themeVariants(ButtonVariant.LUMO_SMALL)
				.build());
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}
}
