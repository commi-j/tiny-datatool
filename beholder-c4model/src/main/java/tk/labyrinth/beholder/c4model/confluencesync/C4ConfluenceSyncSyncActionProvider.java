package tk.labyrinth.beholder.c4model.confluencesync;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.core.FaultViews;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.component.objectaction.TypedObjectActionProvider;

@LazyComponent
@RequiredArgsConstructor
public class C4ConfluenceSyncSyncActionProvider extends TypedObjectActionProvider<C4ConfluenceSyncConfiguration> {

	private final C4ConfluenceSyncWorker c4confluenceSyncWorker;

	@Nullable
	@Override
	protected Component provideActionForTypedObject(Context<C4ConfluenceSyncConfiguration> context) {
		return ButtonRenderer.render(builder -> builder
				.onClick(event -> {
					Dialogs.show(c4confluenceSyncWorker.sync(context.getObject())
							.<Component>map(v -> new Span("Done"))
							.recover(FaultViews::createTextArea)
							.get());
				})
				.text("Sync")
				.themeVariants(ButtonVariant.LUMO_SMALL)
				.build());
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}
}
