package tk.labyrinth.beholder.c4model.confluencesync;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class C4ConfluenceSyncJobRunner implements JobRunner<C4ConfluenceSyncJobRunner.Parameters> {

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder()
				.build();
	}

	@Override
	public void run(Context<Parameters> context) {
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		String pageId;

		String sourceExcerptName;

		String targetExcerptName;
	}
}
