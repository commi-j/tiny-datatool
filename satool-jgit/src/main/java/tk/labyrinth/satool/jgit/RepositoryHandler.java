package tk.labyrinth.satool.jgit;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;

import javax.annotation.CheckForNull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class RepositoryHandler {

	public static Path loadRevision(String repositoryUrl, String revisionId) {
		return loadRevision(repositoryUrl, revisionId, null, (String) null);
	}

	public static Path loadRevision(
			String repositoryUrl,
			String revisionId,
			@CheckForNull CredentialsProvider credentialsProvider) {
		return loadRevision(repositoryUrl, revisionId, credentialsProvider, (String) null);
	}

	public static void loadRevision(
			String repositoryUrl,
			String revisionId,
			@CheckForNull CredentialsProvider credentialsProvider,
			Path directory) {
		try (Git git = Git.cloneRepository()
				.setBranch(revisionId)
				.setCredentialsProvider(credentialsProvider)
				.setDirectory(directory.toFile())
				.setURI(repositoryUrl)
				.call()) {
//			git.getRepository();
		} catch (GitAPIException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static Path loadRevision(
			String repositoryUrl,
			String revisionId,
			@CheckForNull CredentialsProvider credentialsProvider,
			@CheckForNull String tempDirectoryPrefix) {
		Path result;
		{
			try {
				result = Files.createTempDirectory(tempDirectoryPrefix);
				//
				loadRevision(repositoryUrl, revisionId, credentialsProvider, result);
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		}
		return result;
	}

	public static Path loadRevision(String repositoryUrl, String revisionId, @CheckForNull String tempDirectoryPrefix) {
		return loadRevision(repositoryUrl, revisionId, null, tempDirectoryPrefix);
	}
}
