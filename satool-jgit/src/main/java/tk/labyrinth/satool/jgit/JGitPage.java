package tk.labyrinth.satool.jgit;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Data;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import javax.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import java.io.File;
import java.util.Collection;
import java.util.Comparator;
import java.util.UUID;
import java.util.stream.Collectors;

@Route("jgit")
public class JGitPage extends CssVerticalLayout {

	private final ComboBox<Ref> branchField = new ComboBox<>("Branch");

	private final TreeGrid<File> directoryGrid = new TreeGrid<>();

	private final TextField repositoryField = new TextField("Repository");

	@Nullable
	private File directoryRoot = null;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			CssHorizontalLayout toolbar = new CssHorizontalLayout();
			{
				toolbar.addClassNames(PandoraStyles.LAYOUT);
				//
				toolbar.setAlignItems(AlignItems.BASELINE);
			}
			{
				repositoryField.setValue("https://gitlab.com/commi-j/tiny-datatool");
				toolbar.add(repositoryField);
			}
			{
				toolbar.add(new Button(
						"Fetch",
						event -> {
							try {
								Collection<Ref> refs = Git.lsRemoteRepository()
										.setHeads(true)
										.setRemote(repositoryField.getValue())
										.call();
								branchField.setItems(refs.stream()
										.sorted(Comparator.comparing(Ref::getName))
										.collect(Collectors.toList()));
							} catch (GitAPIException ex) {
								throw new RuntimeException(ex);
							}
						}));
			}
			{
				branchField.setItemLabelGenerator(item -> item.getName().substring("refs/heads/".length()));
				branchField.addValueChangeListener(event -> {
				});
				toolbar.add(branchField);
			}
			{
				toolbar.add(new Button("Clone", event -> {
					directoryRoot = RepositoryHandler.loadRevision(
							repositoryField.getValue(),
							branchField.getValue().getName()).toFile();
					refreshGrid();
				}));
			}
			add(toolbar);
		}
		{
			directoryGrid.addHierarchyColumn(File::getName);
			add(directoryGrid);
		}
		{
			add(new Button(
					"WAT",
					event -> {
						;
					}));
		}
	}

	private void refreshGrid() {
		if (directoryRoot != null) {
			directoryGrid.setItems(
					java.util.List.of(directoryRoot.listFiles()),
					item -> item.isDirectory()
							? java.util.List.of(item.listFiles())
							: java.util.List.of());
		} else {
			directoryGrid.setItems(java.util.List.of(), item -> java.util.List.of());
		}
	}

	private static void qwe() {
//		try {
//			Git git = Git.cloneRepository()
//					.setBranch(event.getValue().getName())
//					.setDirectory(Files.createTempDirectory(null).toFile())
//					.setURI(repositoryField.getValue())
//					.call();
//			Repository repository = git.getRepository();
//			//
//			RevWalk revWalk = new RevWalk(repository);
//			RevCommit revCommit = revWalk.parseCommit(event.getValue().getObjectId());
//			RevTree revTree = revCommit.getTree();
//			//
//			TreeWalk treeWalk = new TreeWalk(repository);
//			treeWalk.addTree(revTree);
//			treeWalk.setRecursive(true);
//			//
//			Node rootNode = Node.builder().build();
//			while (treeWalk.next()) {
//				String path = treeWalk.getPathString();
//				String[] segments = path.split("/");
//				//
//				Node parent = rootNode;
//				for (String segment : segments) {
//					Node child = parent.getChildren()
//							.find(innerChild -> Objects.equals(innerChild.getName(), segment))
//							.getOrNull();
//					if (child == null) {
//						child = Node.builder().name(segment).build();
//						parent.setChildren(parent.getChildren().append(child).sortBy(Node::getName));
//					}
//					parent = child;
//				}
//			}
//		} catch (GitAPIException | IOException ex) {
//			throw new RuntimeException(ex);
//		}
	}

	@Builder
	@Data
	public static class Node {

		private final UUID uid = UUID.randomUUID();

		@Builder.Default
		private List<Node> children = List.empty();

		/**
		 * Null for root.
		 */
		@Nullable
		private String name;
	}
}
