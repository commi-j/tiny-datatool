package tk.labyrinth.satool.jgit;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.core.task.TaskExecutor;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.simple.Checkbox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SecretBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.StringBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SuggestBox;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import javax.annotation.CheckForNull;
import jakarta.annotation.PostConstruct;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;

@RequiredArgsConstructor
@Route("explore/jgit")
@Slf4j
public class ExploreJGitPage extends CssVerticalLayout {

	private final TreeGrid<File> contentGrid = new TreeGrid<>();

	private final TextArea faultArea = new TextArea();

	private final Label fetchStartedAtlabel = new Label();

	private final Observable<State> stateObservable = Observable.notInitialized();

	private final TaskExecutor taskExecutor;

	private void onPropertiesChange(Properties nextProperties) {
		logger.debug("#onPropertiesChange: nextProperties = {}", nextProperties);
		//
		taskExecutor.execute(() -> {
			logger.debug("Trying to fetch references");
			//
			Try<List<Ref>> availableReferencesTry = Try.of(() -> GitHandler.lsRemote(
					nextProperties.getRemote(),
					nextProperties.getHeads(),
					nextProperties.getTags(),
					nextProperties.getUsername() != null && nextProperties.getPassword() != null
							? new UsernamePasswordCredentialsProvider(
							nextProperties.getUsername(),
							nextProperties.getPassword().getValue())
							: null));
			//
			logger.debug("Updating state, try outcome is {}", availableReferencesTry.isSuccess());
			//
			stateObservable.update(currentState -> {
				State result;
				{
					if (currentState.getProperties().getRemote() == nextProperties.getRemote()) {
						result = currentState.toBuilder()
								.availableReferences(availableReferencesTry.getOrElse(List.empty()))
								.fault((availableReferencesTry.isSuccess() ? null : availableReferencesTry.getCause()))
								.build();
					} else {
						logger.debug("Update is outdated: currentState.properties = {}, nextProperties = {}",
								currentState.getProperties(),
								nextProperties);
						//
						result = currentState;
					}
				}
				return result;
			});
		});
	}

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setHeightFull();
		}
		{
			CssHorizontalLayout propertiesLayout = new CssHorizontalLayout();
			{
				propertiesLayout.addClassNames(PandoraStyles.LAYOUT);
				//
				propertiesLayout.setAlignItems(AlignItems.BASELINE);
			}
			{
				StringBox remoteBox = new StringBox();
				CssFlexItem.setFlexGrow(remoteBox.asVaadinComponent(), 3);
				propertiesLayout.add(remoteBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> remoteBox.render(SimpleValueBox.Properties.<String>builder()
						.label("Remote")
						.onValueChange(nextRemote -> render(nextState.getProperties().withRemote(nextRemote)))
						.value(nextState.getProperties().getRemote())
						.build()));
			}
			{
				Checkbox headsBox = new Checkbox();
				propertiesLayout.add(headsBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> headsBox.render(Checkbox.Properties.builder()
						.label("Heads")
						.onValueChange(nextHeads -> render(nextState.getProperties().withHeads(nextHeads)))
						.value(nextState.getProperties().getHeads())
						.build()));
			}
			{
				Checkbox tagsBox = new Checkbox();
				propertiesLayout.add(tagsBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> tagsBox.render(Checkbox.Properties.builder()
						.label("Tags")
						.onValueChange(nextTags -> render(nextState.getProperties().withTags(nextTags)))
						.value(nextState.getProperties().getTags())
						.build()));
			}
			{
				SuggestBox<Ref> referenceBox = new SuggestBox<>();
				CssFlexItem.setFlexGrow(referenceBox.asVaadinComponent(), 1);
				propertiesLayout.add(referenceBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> referenceBox.render(SuggestBox.Properties.<Ref>builder()
						.label("Reference")
						.toStringRenderFunction(Ref::getName)
						.onValueChange(nextReference -> render(nextState.getProperties().withReference(nextReference)))
						.suggestFunction(text -> nextState.getAvailableReferences()
								.filter(availableReference -> StringUtils.containsIgnoreCase(
										availableReference.getName(),
										text))
								.take(49) // TODO: Limit by Vaadin, we want to also have limit, but smarter.
								.prepend(null))
						.value(nextState.getProperties().getReference())
						.build()));
			}
			{
				StringBox usernameBox = new StringBox();
				CssFlexItem.setFlexGrow(usernameBox.asVaadinComponent(), 1);
				propertiesLayout.add(usernameBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> usernameBox.render(SimpleValueBox.Properties.<String>builder()
						.label("Username")
						.onValueChange(nextUsername -> render(nextState.getProperties().withUsername(nextUsername)))
						.value(nextState.getProperties().getUsername())
						.build()));
			}
			{
				SecretBox passwordBox = new SecretBox();
				CssFlexItem.setFlexGrow(passwordBox.asVaadinComponent(), 1);
				propertiesLayout.add(passwordBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> passwordBox.render(SimpleValueBox.Properties.<Secret>builder()
						.label("Password")
						.onValueChange(nextPassword -> render(nextState.getProperties().withPassword(nextPassword)))
						.value(nextState.getProperties().getPassword())
						.build()));
			}
			add(propertiesLayout);
		}
		{
			CssVerticalLayout contentLayout = new CssVerticalLayout();
			{
				contentLayout.add(fetchStartedAtlabel);
			}
			{
				contentGrid.addHierarchyColumn(File::getName);
				add(contentGrid);
			}
			{
				add(faultArea);
			}
			add(contentLayout);
		}
		{
			stateObservable.subscribe(nextState -> {
				{
					fetchStartedAtlabel.setText("Fetch started at %s".formatted(nextState.getFetchStartedAt()));
					fetchStartedAtlabel.setVisible(nextState.getContent() == null && nextState.getFault() == null);
				}
				{
//					contentGrid.setItems();
					contentGrid.setVisible(nextState.getContent() != null);
				}
				{
					if (nextState.getFault() != null) {
						StringWriter stringWriter = new StringWriter();
						nextState.getFault().printStackTrace(new PrintWriter(stringWriter));
						faultArea.setValue(stringWriter.toString());
					} else {
						faultArea.setValue("");
					}
					faultArea.setVisible(nextState.getFault() != null);
				}
			});
		}
		{
			// Subscribe.
			//
			stateObservable.getFlux()
					.map(State::getProperties)
					.distinctUntilChanged()
					.subscribe(this::onPropertiesChange);
		}
		{
			// Initialize.
			//
			stateObservable.set(State.builder()
					.availableReferences(List.empty())
					.content(null)
					.fault(null)
					.properties(Properties.builder()
							.heads(true)
							.password(null)
							.reference(null)
							.remote("https://gitlab.com/commi-j/tiny-datatool")
							.tags(false)
							.username(null)
							.build())
					.build());
		}
	}

	public void render(Properties properties) {
		stateObservable.update(currentState -> currentState.withProperties(properties));
	}

	@Builder(toBuilder = true)
	@Value
	@With
	private static class Properties {

		@NonNull
		Boolean heads;

		@CheckForNull
		Secret password;

		@CheckForNull
		Ref reference;

		@CheckForNull
		String remote;

		@NonNull
		Boolean tags;

		@CheckForNull
		String username;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State {

		List<Ref> availableReferences;

		@CheckForNull
		File content;

		@CheckForNull
		Throwable fault;

		@CheckForNull
		Instant fetchStartedAt;

		Properties properties;
	}
}
