package tk.labyrinth.satool.jgit;

import io.vavr.collection.List;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.CredentialsProvider;

import javax.annotation.CheckForNull;
import java.util.Objects;

public class GitHandler {

	public static Ref getHead(String remote, @CheckForNull CredentialsProvider credentialsProvider) {
		return lsRemote(remote, false, false, credentialsProvider)
				.find(ref -> Objects.equals(ref.getName(), "HEAD"))
				.get();
	}

	public static List<Ref> lsRemote(
			String remote,
			boolean heads,
			boolean tags,
			@CheckForNull CredentialsProvider credentialsProvider) {
		try {
			return List.ofAll(Git.lsRemoteRepository()
					.setCredentialsProvider(credentialsProvider)
					.setHeads(heads)
					.setRemote(remote)
					.setTags(tags)
					.call());
		} catch (GitAPIException ex) {
			throw new RuntimeException(ex);
		}
	}
}
