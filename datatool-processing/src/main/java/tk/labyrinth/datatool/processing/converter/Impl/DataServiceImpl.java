package tk.labyrinth.datatool.processing.converter.Impl;

import static tk.labyrinth.datatool.processing.data.Constants.TABULATION;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;
import tk.labyrinth.datatool.model.model.ConversionRequestSubject;
import tk.labyrinth.datatool.model.model.DataType;
import tk.labyrinth.datatool.processing.converter.DataService;
import tk.labyrinth.datatool.processing.utils.NormalizationUtils;
import tk.labyrinth.datatool.processing.utils.StringUtils;

@Service
public class DataServiceImpl implements DataService {

	@Override
	public List<ConversionRequestSubject> getConversionRequestSubjects(
			List<String> headlines,
			List<String> dataTypes,
			boolean needNormalizeValues,
			String dataString) {

		List<ConversionRequestSubject> subjects = new ArrayList<>();
		List<String> stringList = new ArrayList<>(Arrays.asList(dataString.split(TABULATION)));
		for (int i = 0; i < stringList.size(); i++) {
			String key = headlines.get(i);

			String value;
			if (needNormalizeValues) {
				value = NormalizationUtils.normalizeValue(stringList.get(i));
			} else {
				value = stringList.get(i);
			}

			DataType type = StringUtils.getDataType(dataTypes.get(i));
			ConversionRequestSubject subject = new ConversionRequestSubject(key, value, type);
			subjects.add(subject);
		}
		return subjects;
	}
}
