package tk.labyrinth.datatool.processing.converter;

import java.util.List;
import tk.labyrinth.datatool.model.model.ConversionRequestSubject;

public interface DataService {

	List<ConversionRequestSubject> getConversionRequestSubjects(List<String> headlines,
			List<String> dataTypes,
			boolean needNormalizeValues, String dataString);
}
