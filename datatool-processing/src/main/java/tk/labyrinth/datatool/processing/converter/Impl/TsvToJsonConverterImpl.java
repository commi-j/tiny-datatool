package tk.labyrinth.datatool.processing.converter.Impl;

import static tk.labyrinth.datatool.processing.data.Constants.TABULATION;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import tk.labyrinth.datatool.model.model.ConversionRequestStructure;
import tk.labyrinth.datatool.model.model.ConversionRequestSubject;
import tk.labyrinth.datatool.model.model.ConversionResponseStructure;
import tk.labyrinth.datatool.processing.converter.DataService;
import tk.labyrinth.datatool.processing.converter.TsvToJsonConverter;
import tk.labyrinth.datatool.processing.utils.NormalizationUtils;
import tk.labyrinth.datatool.processing.utils.StringUtils;

@Service
@RequiredArgsConstructor
public class TsvToJsonConverterImpl implements TsvToJsonConverter {

	private final DataService dataService;

	@Override
	public ConversionResponseStructure convert(ConversionRequestStructure request) {
		List<List<ConversionRequestSubject>> subjects = getSubjects(
				request.getInputString(),
				request.isHasDataType(),
				request.isNeedNormalizeKeys(),
				request.isNeedNormalizeValues());

		request.setSubjects(subjects);
		String jsonString = StringUtils.getJsonString(request.getSubjects());

		JsonNode jsonNode;
		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonNode = mapper.readTree(jsonString);
		} catch (JsonProcessingException e) {
			//FIXME: логирование
			e.printStackTrace();
			jsonNode = null;
		}

		return ConversionResponseStructure
				.builder()
				.jsonNode(jsonNode)
				.build();
	}

	private List<List<ConversionRequestSubject>> getSubjects(String inputString, boolean hasDataType,
			boolean needNormalizeKeys, boolean needNormalizeValues) {
		List<String> data = StringUtils.splitIntoLines(inputString);
		List<String> headlines = getMetaInfoAndRemoveItFromData(data, needNormalizeKeys);
		List<String> dataTypes = hasDataType ? getMetaInfoAndRemoveItFromData(data, null) : null;
		List<List<ConversionRequestSubject>> result = getJsonTargetSubject(headlines, dataTypes, data, needNormalizeValues);
		return result;
	}

	public static List<String> getMetaInfoAndRemoveItFromData(
			List<String> source,
			Boolean needNormalizeKeys) {
		List<String> result;

		String headlinesString = source.remove(0);
		List<String> metaInfo = Arrays.asList(headlinesString.split(TABULATION));
		if (BooleanUtils.isTrue(needNormalizeKeys)) {
			result = metaInfo.stream()
					.map(NormalizationUtils::normalizeKeys)
					.collect(Collectors.toList());
		} else {
			result = metaInfo.stream()
					.map(String::trim)
					.collect(Collectors.toList());
		}

		return result;
	}

	private List<List<ConversionRequestSubject>> getJsonTargetSubject(
			List<String> headlines,
			List<String> dataTypes,
			List<String> data,
			boolean needNormalizeValues) {

		List<List<ConversionRequestSubject>> result = data.stream()
				.map(dataString ->
						dataService.getConversionRequestSubjects(headlines, dataTypes, needNormalizeValues, dataString))
				.collect(Collectors.toList());

		return result;
	}

}