package tk.labyrinth.datatool.processing.data;

import java.util.HashSet;
import java.util.Set;
import lombok.Value;

@Value
public class AdditionalSpacesDictionary {

	public static final Set<String> HAIR_SPACES;
	public static final Set<Integer> SPACES_CHAR_CODES;

	static {
		HAIR_SPACES = new HashSet<>();
		SPACES_CHAR_CODES = new HashSet<>();
		//
		final String HAIR_SPACE = " ";
		final int HAIR_SPACE_CHAR_CODE = 8202;
		HAIR_SPACES.add(HAIR_SPACE);
		SPACES_CHAR_CODES.add(HAIR_SPACE_CHAR_CODE);
		//
		final String SIX_PER_EM_SPACE = " ";
		final int SIX_PER_EM_SPACE_CHAR_CODE = 8198;
		HAIR_SPACES.add(SIX_PER_EM_SPACE);
		SPACES_CHAR_CODES.add(SIX_PER_EM_SPACE_CHAR_CODE);
		//
		final String THIN_SPACE = " ";
		final int THIN_SPACE_CHAR_CODE = 8201;
		HAIR_SPACES.add(THIN_SPACE);
		SPACES_CHAR_CODES.add(THIN_SPACE_CHAR_CODE);
		//
		final String FOUR_PER_EM_SPACE = " ";
		final int FOUR_PER_EM_SPACE_CHAR_CODE = 8197;
		HAIR_SPACES.add(FOUR_PER_EM_SPACE);
		SPACES_CHAR_CODES.add(FOUR_PER_EM_SPACE_CHAR_CODE);
		//
		final String MATHEMATICAL_SPACE = " ";
		final int MATHEMATICAL_SPACE_CHAR_CODE = 8287;
		HAIR_SPACES.add(MATHEMATICAL_SPACE);
		SPACES_CHAR_CODES.add(MATHEMATICAL_SPACE_CHAR_CODE);
		//
		final String PUNCTUATION_SPACE = " ";
		final int PUNCTUATION_SPACE_CHAR_CODE = 8200;
		HAIR_SPACES.add(PUNCTUATION_SPACE);
		SPACES_CHAR_CODES.add(PUNCTUATION_SPACE_CHAR_CODE);
		//
		final String THREE_PER_EM_SPACE = " ";
		final int THREE_PER_EM_SPACE_CHAR_CODE = 8196;
		HAIR_SPACES.add(THREE_PER_EM_SPACE);
		SPACES_CHAR_CODES.add(THREE_PER_EM_SPACE_CHAR_CODE);
		//
		final String EN_SPACE = " ";
		final int EN_SPACE_CHAR_CODE = 8194;
		HAIR_SPACES.add(EN_SPACE);
		SPACES_CHAR_CODES.add(EN_SPACE_CHAR_CODE);
		//
		final String IDEOGRAPHIC_SPACE = "　";
		final int IDEOGRAPHIC_SPACE_CHAR_CODE = 12288;
		HAIR_SPACES.add(IDEOGRAPHIC_SPACE);
		SPACES_CHAR_CODES.add(IDEOGRAPHIC_SPACE_CHAR_CODE);
		//
		final String EM_SPACE = " ";
		final int EM_SPACE_CHAR_CODE = 8195;
		HAIR_SPACES.add(EM_SPACE);
		SPACES_CHAR_CODES.add(EM_SPACE_CHAR_CODE);
		//
		final String NARROW_NO_BREAK_SPACE = " ";
		final int NARROW_NO_BREAK_SPACE_CHAR_CODE = 8239;
		HAIR_SPACES.add(NARROW_NO_BREAK_SPACE);
		SPACES_CHAR_CODES.add(NARROW_NO_BREAK_SPACE_CHAR_CODE);
		//
		final String NO_BREAK_SPACE = " ";
		final int NO_BREAK_SPACE_CHAR_CODE = 160;
		HAIR_SPACES.add(NO_BREAK_SPACE);
		SPACES_CHAR_CODES.add(NO_BREAK_SPACE_CHAR_CODE);
		//
		final String FIGURE_SPACE = " ";
		final int FIGURE_SPACE_CHAR_CODE = 8199;
		HAIR_SPACES.add(FIGURE_SPACE);
		SPACES_CHAR_CODES.add(FIGURE_SPACE_CHAR_CODE);
		//
		final String ZERO_WIDTH_SPACE = "\u200B";
		final int ZERO_WIDTH_SPACE_CHAR_CODE = 8203;
		HAIR_SPACES.add(ZERO_WIDTH_SPACE);
		SPACES_CHAR_CODES.add(ZERO_WIDTH_SPACE_CHAR_CODE);
	}
}
