package tk.labyrinth.datatool.processing.converter;

import tk.labyrinth.datatool.model.model.ConversionRequestStructure;
import tk.labyrinth.datatool.model.model.ConversionResponseStructure;

public interface TsvToJsonConverter {

	/**
	 * Метод конвертирует простой набор данных в JSON.
	 * Формат входных данных - табличный, tsv.
	 * Не обрабатываются сложные объекты (напр. коллекции).
	 */
	ConversionResponseStructure convert(ConversionRequestStructure source);
}
