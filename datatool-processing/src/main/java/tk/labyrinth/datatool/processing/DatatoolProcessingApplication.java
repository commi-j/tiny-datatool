package tk.labyrinth.datatool.processing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatatoolProcessingApplication {

  public static void main(String[] args) {
    SpringApplication.run(DatatoolProcessingApplication.class, args);
  }

}
