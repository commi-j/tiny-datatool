package tk.labyrinth.datatool.processing.utils;

import static tk.labyrinth.datatool.processing.data.Constants.CLOSE_CURLY_BRACE;
import static tk.labyrinth.datatool.processing.data.Constants.CLOSE_SQUARE_BRACKET;
import static tk.labyrinth.datatool.processing.data.Constants.COLON;
import static tk.labyrinth.datatool.processing.data.Constants.COMMA;
import static tk.labyrinth.datatool.processing.data.Constants.OPEN_CURLY_BRACE;
import static tk.labyrinth.datatool.processing.data.Constants.OPEN_SQUARE_BRACKET;
import static tk.labyrinth.datatool.processing.data.Constants.PARAGRAPH;
import static tk.labyrinth.datatool.processing.data.Constants.QUOTATION_MARK;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import tk.labyrinth.datatool.model.model.ConversionRequestSubject;
import tk.labyrinth.datatool.model.model.DataType;

public class StringUtils {

	public static List<String> splitIntoLines(String source) {
		//FIXME: предусмотреть переносы строки по \r\n, \r, \n
		return new ArrayList<>(Arrays.asList(source.split(PARAGRAPH)));
	}

	public static DataType getDataType(String sourceDataType) {
		DataType result = DataType.STRING;
		DataType[] values = DataType.values();
		for (DataType value : values) {
			if (value.name().equalsIgnoreCase(sourceDataType)) {
				result = value;
				break;
			}
		}
		return result;
	}

	public static String getJsonString(List<List<ConversionRequestSubject>> subjects) {
		AtomicInteger count = new AtomicInteger(0);
		StringBuilder result = new StringBuilder();
		subjects.forEach(subject -> {
			result.append(OPEN_CURLY_BRACE);
			for (int i = 0; i < subject.size(); i++) {
				result.append(QUOTATION_MARK);
				result.append(subject.get(i).getKey());
				result.append(QUOTATION_MARK);
				result.append(COLON);

				String value = subject.get(i).getValue();
				result.append(getValueJsonString(subject.get(i).getType(), subject.get(i).getValue()));

				if (i < (subject.size() - 1)) {
					result.append(COMMA);
				}
			}
			result.append(CLOSE_CURLY_BRACE);
			if (count.get() < (subjects.size() - 1)) {
				result.append(COMMA);
			}
			count.getAndIncrement();
		});
		if (subjects.size() > 1) {
			result.insert(0, OPEN_SQUARE_BRACKET);
			result.append(CLOSE_SQUARE_BRACKET);
		}
		return result.toString();
	}

	private static String getValueJsonString(DataType type, String value) {
		StringBuilder result = new StringBuilder();
		switch (type) {
			//FIXME: реализовать обработку массива/коллекций
			case ARRAY -> {
				result.append("array test case");
				result.append("array test case");
			}
			case BOOLEAN, NUMBER -> result.append(value.trim());
			default -> {
				result.append(QUOTATION_MARK);
				result.append(value);
				result.append(QUOTATION_MARK);
			}
		}
		return result.toString();
	}
}
