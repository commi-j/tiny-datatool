package tk.labyrinth.datatool.processing.data;

public class Constants {

	public static final String NORMAL_SPACE = "\s";
	public static final int NORMAL_SPACE_CHAR_CODE = 32;
	public static final String PARAGRAPH = "\n";
	public static final String TABULATION = "\t";
	public static final String OPEN_CURLY_BRACE = "{";
	public static final String CLOSE_CURLY_BRACE = "}";
	public static final String QUOTATION_MARK = "\"";
	public static final String COLON = ":";
	public static final String COMMA = ",";
	public static final String OPEN_SQUARE_BRACKET = "[";
	public static final String CLOSE_SQUARE_BRACKET = "]";
}
