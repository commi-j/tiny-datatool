package tk.labyrinth.datatool.processing.utils;

import static tk.labyrinth.datatool.processing.data.AdditionalSpacesDictionary.SPACES_CHAR_CODES;
import static tk.labyrinth.datatool.processing.data.Constants.NORMAL_SPACE;

import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class NormalizationUtils {

	private static final ReentrantLock normalizeKeysLock = new ReentrantLock();
	private static final ReentrantLock normalizeValueLock = new ReentrantLock();

	public static String normalizeKeys(String key) {
		String resultString;

		synchronized (normalizeKeysLock) {
			StringBuilder result = new StringBuilder(key.trim());
			result.replace(0, 1, result.substring(0, 1).toUpperCase());
			result.replace(1, result.length(), result.substring(1).toLowerCase());
			resultString = removeExtraSpaces(result);
			normalizeKeysLock.notifyAll();
		}

		return resultString;
	}

	public static String normalizeValue(String value) {
		return removeExtraSpaces(new StringBuilder(value));
	}

	public static String removeExtraSpaces(StringBuilder source) {
		String result = source.chars()
				.mapToObj(it -> SPACES_CHAR_CODES.contains(it) ? NORMAL_SPACE : Character.toString(it))
				.collect(Collectors.joining())
				.trim();

		return result;
	}
}
