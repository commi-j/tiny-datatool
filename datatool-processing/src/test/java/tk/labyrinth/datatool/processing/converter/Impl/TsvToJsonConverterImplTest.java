package tk.labyrinth.datatool.processing.converter.Impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tk.labyrinth.datatool.model.model.ConversionRequestStructure;
import tk.labyrinth.datatool.model.model.ConversionResponseStructure;
import tk.labyrinth.datatool.model.model.ConversionTargetType;
import tk.labyrinth.datatool.processing.builders.JsonStringBuilder;

@Disabled("FIXME: Fix line separator issue.")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TsvToJsonConverterImpl.class, DataServiceImpl.class})
class TsvToJsonConverterImplTest {

	@Autowired
	private TsvToJsonConverterImpl tsvToJsonConverter;

	@Test
	void testConvert() {
		String inputString = JsonStringBuilder.getRequestString();
		ConversionRequestStructure structure = ConversionRequestStructure.builder()
				.inputString(inputString)
				.hasDataType(true)
				.conversionTargetType(ConversionTargetType.JSON)
				.build();
		ConversionResponseStructure responseStructure = tsvToJsonConverter.convert(structure);
		assertEquals(getTestResult(), responseStructure.getJsonNode().toPrettyString());
	}

	@Test
	void testConvertNeedKeyChangeCaseAndRemoveGaps() {
		String inputString = JsonStringBuilder.getRequestStringNeedKeyUppercaseAndRemoveGaps();
		ConversionRequestStructure structure = ConversionRequestStructure.builder()
				.inputString(inputString)
				.hasDataType(true)
				.needNormalizeKeys(true)
				.conversionTargetType(ConversionTargetType.JSON)
				.build();
		ConversionResponseStructure responseStructure = tsvToJsonConverter.convert(structure);
		assertEquals(getTestResult(), responseStructure.getJsonNode().toPrettyString());
	}

	@Test
	void testConvertNoGaps() {
		String inputString = JsonStringBuilder.getRequestStringUnnecessarySpaces();
		ConversionRequestStructure structure = ConversionRequestStructure.builder()
				.inputString(inputString)
				.hasDataType(true)
				.conversionTargetType(ConversionTargetType.JSON)
				.build();
		ConversionResponseStructure responseStructure = tsvToJsonConverter.convert(structure);
		assertEquals(getTestResult(), responseStructure.getJsonNode().toPrettyString());
	}

	private static String getTestResult() {
		return
				"""
						[ {
						  "Patronymic" : "Михайлова",
						  "Firstname" : "Алёна",
						  "Lastname" : "Георгиевна",
						  "Age" : 34
						}, {
						  "Patronymic" : "Иванов",
						  "Firstname" : "Семён",
						  "Lastname" : "Антонович",
						  "Age" : 12
						}, {
						  "Patronymic" : "Иванова",
						  "Firstname" : "Полина",
						  "Lastname" : "Егоровна",
						  "Age" : 45
						}, {
						  "Patronymic" : "Петрова",
						  "Firstname" : "Виктория",
						  "Lastname" : "Алексеевна",
						  "Age" : 29
						}, {
						  "Patronymic" : "Анисимова",
						  "Firstname" : "Алиса",
						  "Lastname" : "Ибрагимовна",
						  "Age" : 71
						}, {
						  "Patronymic" : "Покровский",
						  "Firstname" : "Алексей",
						  "Lastname" : "Юрьевич",
						  "Age" : 90
						}, {
						  "Patronymic" : "Иванова",
						  "Firstname" : "Анастасия",
						  "Lastname" : "Владимировна",
						  "Age" : 25
						}, {
						  "Patronymic" : "Жданов",
						  "Firstname" : "Мирон",
						  "Lastname" : "Михайлович",
						  "Age" : 63
						}, {
						  "Patronymic" : "Артемов",
						  "Firstname" : "Александр",
						  "Lastname" : "Леонидович",
						  "Age" : 55
						}, {
						  "Patronymic" : "Воронов",
						  "Firstname" : "Даниил",
						  "Lastname" : "Львович",
						  "Age" : 11
						} ]""";
	}
}