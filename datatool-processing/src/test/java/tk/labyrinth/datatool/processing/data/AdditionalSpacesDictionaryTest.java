package tk.labyrinth.datatool.processing.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class AdditionalSpacesDictionaryTest {

	@Test
	public void testData() {
		assertEquals(14, AdditionalSpacesDictionary.HAIR_SPACES.size());
		assertEquals(14, AdditionalSpacesDictionary.SPACES_CHAR_CODES.size());
		assertEquals(AdditionalSpacesDictionary.HAIR_SPACES.size(), AdditionalSpacesDictionary.SPACES_CHAR_CODES.size());
	}
}