package tk.labyrinth.datatool.processing.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static tk.labyrinth.datatool.processing.data.AdditionalSpacesDictionary.HAIR_SPACES;

import org.junit.jupiter.api.Test;

class NormalizationUtilsTest {

	private static final String TEST_KEY_UPPERCASE_SHORT_STRING = "a";
	private static final String TEST_KEY_UPPERCASE_STRING = "abc";
	private static final String TEST_ADDITIONAL_SPACE_STRING = getDummyStringWithAdditionalSpaces();

	private static final String EXPECTED_KEY_UPPERCASE_SHORT_STRING = "A";
	private static final String EXPECTED_KEY_UPPERCASE_STRING = "Abc";
	private static final String EXPECTED_KEY_ADDITIONAL_SPACE_STRING = "Dummy";
	private static final String EXPECTED_VALUE_ADDITIONAL_SPACE_STRING = "dummy";

	@Test
	void testNormalizeKeysShortString() {
		String actual = NormalizationUtils.normalizeKeys(TEST_KEY_UPPERCASE_SHORT_STRING);
		assertEquals(EXPECTED_KEY_UPPERCASE_SHORT_STRING, actual);
	}

	@Test
	void testNormalizeKeys() {
		String actual = NormalizationUtils.normalizeKeys(TEST_KEY_UPPERCASE_STRING);
		assertEquals(EXPECTED_KEY_UPPERCASE_STRING, actual);
	}

	@Test
	void testNormalizeKeysRemoveAdditionalSpaces() {
		String actual = NormalizationUtils.normalizeKeys(TEST_ADDITIONAL_SPACE_STRING);
		assertEquals(EXPECTED_KEY_ADDITIONAL_SPACE_STRING, actual);
	}

	@Test
	void testNormalizeValue() {
		String actual = NormalizationUtils.normalizeValue(TEST_ADDITIONAL_SPACE_STRING);
		assertEquals(EXPECTED_VALUE_ADDITIONAL_SPACE_STRING, actual);
	}

	@Test
	void testRemoveExtraSpaces() {
		StringBuilder source = new StringBuilder(TEST_ADDITIONAL_SPACE_STRING);
		String actual = NormalizationUtils.removeExtraSpaces(source);
		assertEquals(EXPECTED_VALUE_ADDITIONAL_SPACE_STRING, actual);
	}

	private static String getDummyStringWithAdditionalSpaces() {
		StringBuilder builder = new StringBuilder("dummy");
		HAIR_SPACES.forEach(builder::append);
		return builder.toString();
	}
}