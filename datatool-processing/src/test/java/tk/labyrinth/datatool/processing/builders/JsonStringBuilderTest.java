package tk.labyrinth.datatool.processing.builders;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JsonStringBuilderTest {

	@Test
	void testStringAlternative() {
		Assertions.assertEquals(
				"""
						Patronymic	Firstname	Lastname	Age
						string	string	string	number
						Михайлова	Алёна	Георгиевна	34
						Иванов	Семён	Антонович	12
						Иванова	Полина	Егоровна	45
						Петрова	Виктория	Алексеевна	29
						Анисимова	Алиса	Ибрагимовна	71
						Покровский	Алексей	Юрьевич	90
						Иванова	Анастасия	Владимировна	25
						Жданов	Мирон	Михайлович	63
						Артемов	Александр	Леонидович	55
						Воронов	Даниил	Львович	11""",
				JsonStringBuilder.getRequestString());
	}
}
