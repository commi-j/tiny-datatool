package tk.labyrinth.datatool.processing.builders;

public class JsonStringBuilder {

	private static final String COMMA = ",";

	private static final String PARAGRAPH = "\n";

	private static final String TABULATION = "\t";

	public static String getRequestString() {
		return
				"""
						Patronymic	Firstname	Lastname	Age
						string	string	string	number
						Михайлова	Алёна	Георгиевна	34
						Иванов	Семён	Антонович	12
						Иванова	Полина	Егоровна	45
						Петрова	Виктория	Алексеевна	29
						Анисимова	Алиса	Ибрагимовна	71
						Покровский	Алексей	Юрьевич	90
						Иванова	Анастасия	Владимировна	25
						Жданов	Мирон	Михайлович	63
						Артемов	Александр	Леонидович	55
						Воронов	Даниил	Львович	11""";
	}

	public static String getRequestStringUnnecessarySpaces() {
		return
				"""
						 Patronymic      	 Firstname	  Lastname 	   Age
						string	string	string	number
						Михайлова	Алёна	Георгиевна	34
						Иванов	Семён	Антонович	12
						Иванова	Полина	Егоровна	45
						Петрова	Виктория	Алексеевна	29
						Анисимова	Алиса	Ибрагимовна	71
						Покровский	Алексей	Юрьевич	90
						Иванова	Анастасия	Владимировна	25
						Жданов	Мирон	Михайлович	63
						Артемов	Александр	Леонидович	55
						Воронов	Даниил	Львович	11""";
	}

	public static String getRequestStringNeedKeyUppercaseAndRemoveGaps() {
		return
				"""
						pATRONYMIC   	 fiRStname	 lAstnaMe      	        Age 
						string	string	string	number
						Михайлова	Алёна	Георгиевна	34
						Иванов	Семён	Антонович	12
						Иванова	Полина	Егоровна	45
						Петрова	Виктория	Алексеевна	29
						Анисимова	Алиса	Ибрагимовна	71
						Покровский	Алексей	Юрьевич	90
						Иванова	Анастасия	Владимировна	25
						Жданов	Мирон	Михайлович	63
						Артемов	Александр	Леонидович	55
						Воронов	Даниил	Львович	11""";
	}
}
