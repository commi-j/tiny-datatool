package tk.labyrinth.satool.gerritconnector.apiexplorer;

import com.google.gerrit.extensions.api.GerritApi;
import com.google.gerrit.extensions.common.ProjectInfo;
import com.urswolfer.gerrit.client.rest.GerritAuthData;
import com.urswolfer.gerrit.client.rest.GerritRestApiFactory;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.TaskExecutor;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.simple.SecretBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.StringBox;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import javax.annotation.CheckForNull;
import jakarta.annotation.PostConstruct;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.util.List;

@Route("gerrit/apiexplorer")
@RequiredArgsConstructor
@Slf4j
public class GerritApiExplorerPage extends CssVerticalLayout {

	private final Grid<ProjectInfo> contentGrid = new Grid<>();

	private final TextArea faultArea = new TextArea();

	private final Label fetchStartedAtlabel = new Label();

	private final Observable<State> stateObservable = Observable.notInitialized();

	private final TaskExecutor taskExecutor;

	private void onPropertiesChange(Properties nextProperties) {
		logger.debug("#onPropertiesChange: nextProperties = {}", nextProperties);
		//
		taskExecutor.execute(() -> {
			logger.debug("Trying to fetch data");
			//
			stateObservable.update(currentState -> {
				State result;
				{
					if (currentState.getProperties() == nextProperties) {
						result = currentState.toBuilder()
								.fetchStartedAt(Instant.now())
								.build();
					} else {
						logger.debug("Fetch is outdated: currentState.properties = {}, nextProperties = {}",
								currentState.getProperties(),
								nextProperties);
						//
						result = currentState;
					}
				}
				return result;
			});
			//
			Try<List<ProjectInfo>> contentTry = Try.of(() -> {
				GerritRestApiFactory gerritRestApiFactory = new GerritRestApiFactory();
				GerritAuthData.Basic authData = new GerritAuthData.Basic(
						nextProperties.getUrl(),
						nextProperties.getUsername(),
						nextProperties.getPassword() != null ? nextProperties.getPassword().getValue() : null);
				//
				GerritApi gerritApi = gerritRestApiFactory.create(authData);
				//
				return gerritApi.projects().list().get();
			});
			//
			logger.debug("Updating state, try outcome is {}", contentTry.isSuccess());
			//
			stateObservable.update(currentState -> {
				State result;
				{
					if (currentState.getProperties() == nextProperties) {
						result = currentState.toBuilder()
								.content(contentTry.getOrNull())
								.fault((contentTry.isSuccess() ? null : contentTry.getCause()))
								.build();
					} else {
						logger.debug("Update is outdated: currentState.properties = {}, nextProperties = {}",
								currentState.getProperties(),
								nextProperties);
						//
						result = currentState;
					}
				}
				return result;
			});
		});
	}

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setHeightFull();
		}
		{
			CssHorizontalLayout buttonLayout = new CssHorizontalLayout();
			{
				buttonLayout.addClassNames(PandoraStyles.LAYOUT);
			}
			{
				StringBox urlBox = new StringBox();
				CssFlexItem.setFlexGrow(urlBox.asVaadinComponent(), 3);
				buttonLayout.add(urlBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> urlBox.render(SimpleValueBox.Properties.<String>builder()
						.label("Url")
						.onValueChange(nextUrl -> render(nextState.getProperties().withUrl(nextUrl)))
						.value(nextState.getProperties().getUrl())
						.build()));
			}
			{
				StringBox usernameBox = new StringBox();
				CssFlexItem.setFlexGrow(usernameBox.asVaadinComponent(), 1);
				buttonLayout.add(usernameBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> usernameBox.render(SimpleValueBox.Properties.<String>builder()
						.label("Username")
						.onValueChange(nextUsername -> render(nextState.getProperties().withUsername(nextUsername)))
						.value(nextState.getProperties().getUsername())
						.build()));
			}
			{
				SecretBox passwordBox = new SecretBox();
				CssFlexItem.setFlexGrow(passwordBox.asVaadinComponent(), 1);
				buttonLayout.add(passwordBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> passwordBox.render(SimpleValueBox.Properties.<Secret>builder()
						.label("Password")
						.onValueChange(nextPassword -> render(nextState.getProperties().withPassword(nextPassword)))
						.value(nextState.getProperties().getPassword())
						.build()));
			}
			{
				// TODO: Refresh
			}
			add(buttonLayout);
		}
		{
			add(fetchStartedAtlabel);
		}
		{
			contentGrid
					.addColumn(item -> item.id)
					.setHeader("Id")
					.setResizable(true)
					.setSortable(true);
			contentGrid
					.addColumn(item -> item.name)
					.setHeader("Name")
					.setResizable(true)
					.setSortable(true);
			contentGrid
					.addColumn(item -> item.description)
					.setHeader("Description")
					.setResizable(true)
					.setSortable(true);
			contentGrid
					.addColumn(item -> item.webLinks)
					.setHeader("Web Links")
					.setResizable(true)
					.setSortable(true);
			add(contentGrid);
		}
		{
			add(faultArea);
		}
		{
			stateObservable.subscribe(nextState -> {
				{
					fetchStartedAtlabel.setText("Fetch started at %s".formatted(nextState.getFetchStartedAt()));
					fetchStartedAtlabel.setVisible(nextState.getContent() == null && nextState.getFault() == null);
				}
				{
					contentGrid.setItems(nextState.getContent() != null
							? nextState.getContent()
							: List.of());
					contentGrid.setVisible(nextState.getContent() != null);
				}
				{
					if (nextState.getFault() != null) {
						StringWriter stringWriter = new StringWriter();
						nextState.getFault().printStackTrace(new PrintWriter(stringWriter));
						faultArea.setValue(stringWriter.toString());
					} else {
						faultArea.setValue("");
					}
					faultArea.setVisible(nextState.getFault() != null);
				}
			});
		}
		{
			stateObservable.getFlux()
					.map(State::getProperties)
					.distinctUntilChanged()
					.subscribe(this::onPropertiesChange);
		}
		{
			stateObservable.set(State.builder()
					.content(null)
					.fault(null)
					.properties(Properties.builder()
							.password(null)
							.url("https://gerrit-review.googlesource.com")
							.username(null)
							.build())
					.build());
		}
	}

	public void render(Properties properties) {
		stateObservable.update(currentState -> currentState.withProperties(properties));
	}

	@Builder(toBuilder = true)
	@Value
	@With
	private static class Properties {

		@CheckForNull
		Secret password;

		@CheckForNull
		String url;

		@CheckForNull
		String username;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	private static class State {

		@CheckForNull
		List<ProjectInfo> content;

		@CheckForNull
		Throwable fault;

		@CheckForNull
		Instant fetchStartedAt;

		Properties properties;
	}
}
