package tk.labyrinth.satool.gerritconnector.api;

import lombok.Builder;
import lombok.Value;

import javax.annotation.CheckForNull;

@Builder(toBuilder = true)
@Value
public class GerritConnectionParameters {

	String host;

	@CheckForNull
	String password;

	@CheckForNull
	String username;
}
