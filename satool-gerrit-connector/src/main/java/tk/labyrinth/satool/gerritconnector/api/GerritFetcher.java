package tk.labyrinth.satool.gerritconnector.api;

import com.google.gerrit.extensions.api.GerritApi;
import com.google.gerrit.extensions.common.ProjectInfo;
import com.google.gerrit.extensions.restapi.RestApiException;
import com.urswolfer.gerrit.client.rest.GerritAuthData;
import com.urswolfer.gerrit.client.rest.GerritRestApiFactory;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import javax.annotation.CheckForNull;
import java.time.Duration;
import java.time.Instant;

@Component
@Lazy
@Slf4j
public class GerritFetcher {

	public Flux<List<ProjectInfo>> searchProjectInfosFlux(
			GerritConnectionParameters connectionParameters,
			@CheckForNull Integer limit) {
		if (limit != null && limit == 0) {
			throw new IllegalArgumentException("Require positive limit: %s".formatted(limit));
		}
		//
		GerritApi gerritApi = new GerritRestApiFactory().create(new GerritAuthData.Basic(
				connectionParameters.getHost(),
				connectionParameters.getUsername(),
				connectionParameters.getPassword()));
		//
		return Flux.create(sink -> {
			try {
				//
				int limitToUse = limit != null ? limit : 50;
				int start = 0;
				boolean done = false;
				//
				do {
					logger.info("Fetching projects: start = {}", start);
					//
					Instant beforeCall = Instant.now();
					//
					List<ProjectInfo> projectInfos = List.ofAll(gerritApi.projects().list()
							.withDescription(true)
							.withLimit(limitToUse)
							.withStart(start)
							.get());
					//
					Instant afterCall = Instant.now();
					//
					logger.info("Fetched: projectInfos.size = {}, callDuration = {}ms",
							projectInfos.size(),
							Duration.between(beforeCall, afterCall).toMillis());
					//
					sink.next(projectInfos);
					//
					if (projectInfos.size() == limitToUse) {
						start += limitToUse;
					} else {
						done = true;
					}
				} while (!done);
				//
				logger.info("Done");
				//
				sink.complete();
			} catch (RestApiException | RuntimeException ex) {
				sink.error(ex);
			}
		});
	}
}
