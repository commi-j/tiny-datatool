package tk.labyrinth.satool.gerritconnector;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import jakarta.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class GerritConnectorExtension {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
