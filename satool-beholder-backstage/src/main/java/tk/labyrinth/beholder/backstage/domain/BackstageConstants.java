package tk.labyrinth.beholder.backstage.domain;

public class BackstageConstants {

	public static final String BACKSTAGE = "backstage";

	public static final String SPRING_PROFILE = "beholder-backstage";
}
