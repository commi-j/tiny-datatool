package tk.labyrinth.beholder.backstage.mapping;

import io.vavr.collection.List;
import tk.labyrinth.beholder.backstage.domain.BackstageDomain;
import tk.labyrinth.satool.beholder.domain.store.directory.DirectoryAttributeMapping;
import tk.labyrinth.satool.beholder.domain.store.directory.DirectoryModelMapping;
import tk.labyrinth.satool.beholder.domain.store.directory.DirectoryModelMappingKind;

public class DefaultBackstageDirectoryModelMappings {

	public static final DirectoryModelMapping DOMAIN = DirectoryModelMapping.builder()
			.attributeMappings(List.of(
					DirectoryAttributeMapping.of("description", "metadata.description"),
					DirectoryAttributeMapping.of("links", "metadata.links"),
					DirectoryAttributeMapping.of("name", "metadata.name"),
					DirectoryAttributeMapping.of("owner", "spec.owner")))
			.kind(DirectoryModelMappingKind.OBJECT_PER_FILE)
			.location("./domains/$name-domain.yaml")
			.modelCode(BackstageDomain.MODEL_CODE)
			.build();
}
