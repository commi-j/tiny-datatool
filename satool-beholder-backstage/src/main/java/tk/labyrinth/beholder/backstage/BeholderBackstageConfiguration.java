package tk.labyrinth.beholder.backstage;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan
@Configuration
public class BeholderBackstageConfiguration {
	// empty
}
