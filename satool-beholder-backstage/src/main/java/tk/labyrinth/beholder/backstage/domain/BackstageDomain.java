package tk.labyrinth.beholder.backstage.domain;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;

@Builder(toBuilder = true)
@ModelTag(BackstageConstants.BACKSTAGE)
@Value
public class BackstageDomain {

	public static final String MODEL_CODE = "backstagedomain";

	String description;

	List<BackstageLink> links;

	String name;

	String owner;
}
