package tk.labyrinth.beholder.backstage;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import tk.labyrinth.beholder.backstage.domain.BackstageConstants;
import tk.labyrinth.satool.beholder.domain.space.initializer.SpaceInitializerBase;
import tk.labyrinth.satool.beholder.domain.uiconfiguration.BeholderUiConfiguration;

import javax.annotation.CheckForNull;

//@Component
@ConditionalOnProperty("beholder.enableBackstageSpace")
//@Order(DefaultStoresInitializer.PRIORITY + 1)
public class BackstageSpaceInitializer extends SpaceInitializerBase {

	@Override
	protected String getSpaceKey() {
		return BackstageConstants.BACKSTAGE;
	}

	@CheckForNull
	@Override
	protected BeholderUiConfiguration getUiConfiguration() {
//			uiConfigurationRepository.save(BeholderUiConfiguration.builder()
//					.key(BackstageConstants.BACKSTAGE)
//					.menuConfigurations(List.of(
//							BeholderMenuConfiguration.builder()
//									.filterTag(BackstageConstants.BACKSTAGE)
//									.build(),
//							BeholderMenuConfiguration.builder()
//									.filterTag("beholder")
//									.build()))
//					.build());
		return null;
	}
}
