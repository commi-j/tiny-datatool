package tk.labyrinth.beholder.backstage.extension;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import tk.labyrinth.beholder.backstage.BeholderBackstageConfiguration;
import tk.labyrinth.beholder.backstage.domain.BackstageConstants;

import jakarta.annotation.PostConstruct;

@Import(BeholderBackstageConfiguration.class)
@Profile(BackstageConstants.SPRING_PROFILE)
@Slf4j
@SpringBootApplication
public class BeholderBackstageExtension {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
