package tk.labyrinth.beholder.backstage.domain;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class BackstageLink {

	String icon;

	String title;

	String url;
}
