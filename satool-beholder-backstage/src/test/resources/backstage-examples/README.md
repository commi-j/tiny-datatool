# Backstage example structure. We use it as test case for .yaml directory store.

Source: https://github.com/backstage/backstage/tree/master/packages/catalog-model/examples

Below is the original README:

# Example Entities

This is a set of example entities that you can make use of to demonstrate basic Backstage features.