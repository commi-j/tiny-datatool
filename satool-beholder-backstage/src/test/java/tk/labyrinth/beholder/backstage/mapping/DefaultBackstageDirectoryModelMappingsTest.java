package tk.labyrinth.beholder.backstage.mapping;

import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.satool.beholder.domain.store.directory.DirectoryModelMappingProcessor;

import java.io.File;
import java.nio.file.Path;

class DefaultBackstageDirectoryModelMappingsTest {

	@Test
	void testGetMatchingFilesWithComponents() {
		Assertions.assertEquals(
				13,
				DirectoryModelMappingProcessor
						.getMatchingFiles(
								Path.of("src/test/resources/backstage-examples"),
								"./components/$name-component.yaml")
						.size());
	}

	@Test
	void testGetMatchingFilesWithDomains() {
		Assertions.assertEquals(
				List.of(
						new File("src/test/resources/backstage-examples/domains/artists-domain.yaml"),
						new File("src/test/resources/backstage-examples/domains/playback-domain.yaml")),
				DirectoryModelMappingProcessor.getMatchingFiles(
						Path.of("src/test/resources/backstage-examples"),
						"./domains/$name-domain.yaml"));
	}

	@Test
	void testGetMatchingFilesWithSystems() {
		Assertions.assertEquals(
				List.of(
						new File("src/test/resources/backstage-examples/systems/artist-engagement-portal-system.yaml"),
						new File("src/test/resources/backstage-examples/systems/audio-playback-system.yaml"),
						new File("src/test/resources/backstage-examples/systems/podcast-system.yaml")),
				DirectoryModelMappingProcessor.getMatchingFiles(
						Path.of("src/test/resources/backstage-examples"),
						"./systems/$name-system.yaml"));
	}

	@Test
	void testReadAllDomains() {
		DirectoryModelMappingProcessor mappingProcessor = new DirectoryModelMappingProcessor();
		//
		Assertions.assertEquals(
				List.of(
						GenericObject.of(
								GenericObjectAttribute.ofSimple("description", "Everything related to artists"),
								GenericObjectAttribute.ofObjectList("links", List.of(
										GenericObject.of(
												GenericObjectAttribute.ofSimple("title", "Domain Readme"),
												GenericObjectAttribute.ofSimple("url", "http://example.com/domain/artists/")),
										GenericObject.of(
												GenericObjectAttribute.ofSimple("icon", "dashboard"),
												GenericObjectAttribute.ofSimple("title", "Domain Metrics Dashboard"),
												GenericObjectAttribute.ofSimple("url", "http://example.com/domains/artists/dashboard")))),
								GenericObjectAttribute.ofSimple("name", "artists"),
								GenericObjectAttribute.ofSimple("owner", "team-a")),
						GenericObject.of(
								GenericObjectAttribute.ofSimple("description", "Everything related to audio playback"),
								GenericObjectAttribute.ofSimple("name", "playback"),
								GenericObjectAttribute.ofSimple("owner", "user:frank.tiernan"))),
				mappingProcessor.readAll(
						DefaultBackstageDirectoryModelMappings.DOMAIN,
						Path.of("src/test/resources/backstage-examples")));
	}
}