package tk.labyrinth.satool.gradle.model;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RepositoriesNode {

	List<RepositoryNode> repositories;

	List<String> unprocessed;
}
