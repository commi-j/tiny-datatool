package tk.labyrinth.satool.gradle.model;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Try;
import org.apache.commons.io.IOUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.GStringExpression;
import org.codehaus.groovy.ast.expr.MapEntryExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.NamedArgumentListExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.ExpressionStatement;
import org.codehaus.groovy.ast.stmt.Statement;
import org.codehaus.groovy.control.SourceUnit;
import org.gradle.internal.Pair;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.misc4j2.java.io.ResourceUtils;

import java.io.File;
import java.io.FileReader;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GradleModelFactory {

	private BuildGradleModel processBuildGradle(ModuleNode moduleNode) {
		List<Statement> statements = List.ofAll(moduleNode.getStatementBlock().getStatements());
		//
		List<Expression> expressions = statements
				.map(ExpressionStatement.class::cast)
				.map(ExpressionStatement::getExpression);
		//
		List<BinaryExpression> binaryExpressions = expressions
				.filter(BinaryExpression.class::isInstance)
				.map(BinaryExpression.class::cast);
		List<MethodCallExpression> methodCallExpressions = expressions
				.filter(MethodCallExpression.class::isInstance)
				.map(MethodCallExpression.class::cast);
		//
		MethodCallExpression dependenciesExpression = methodCallExpressions
				.find(methodCallExpression -> Objects.equals(
						extractConstantValue(methodCallExpression.getMethod()),
						"dependencies"))
				.getOrNull();
		MethodCallExpression pluginsExpression = methodCallExpressions
				.find(methodCallExpression -> Objects.equals(
						extractConstantValue(methodCallExpression.getMethod()),
						"plugins"))
				.getOrNull();
		MethodCallExpression repositoriesExpression = methodCallExpressions
				.find(methodCallExpression -> Objects.equals(
						extractConstantValue(methodCallExpression.getMethod()),
						"repositories"))
				.getOrNull();
		//
		return BuildGradleModel.builder()
				.dependencies(dependenciesExpression != null ? processDependencies(dependenciesExpression) : null)
				.plugins(pluginsExpression != null ? processPlugins(pluginsExpression) : null)
				.repositories(repositoriesExpression != null ? processRepositories(repositoriesExpression) : null)
				.unprocessed(List.empty())
				.build();
	}

	private DependenciesNode processDependencies(MethodCallExpression methodCallExpression) {
		List<Statement> statements = extractClosureStatements(methodCallExpression);
		//
		List<Try<DependencyNode>> dependencyNodeTries = statements
				.map(statement -> Try.ofSupplier(() -> {
					MethodCallExpression expression = (MethodCallExpression) ((ExpressionStatement) statement).getExpression();
					//
					return processDependencyScope(expression);
				}))
				.flatMap(dependencyNodeTriesTry -> dependencyNodeTriesTry.isSuccess()
						? dependencyNodeTriesTry.get()
						: List.of(Try.failure(dependencyNodeTriesTry.getCause())));
		//
		return DependenciesNode.builder()
				.dependencies(dependencyNodeTries.filter(Try::isSuccess).map(Try::get))
				//
				// TODO: Need to add line number to easier detect problems.
				.unprocessed(dependencyNodeTries.filter(Try::isFailure).map(Try::getCause).map(Throwable::toString))
				.build();
	}

	@Nullable
	private DependencyNode processDependency(String scope, Expression expression) {
		DependencyNode result;
		{
			if (expression instanceof ClosureExpression) {
				result = null;
			} else if (expression instanceof ConstantExpression) {
				String value = extractConstantValue(expression);
				List<String> segments = List.of(value.split(":"));
				//
				result = DependencyNode.builder()
						.group(segments.get(0))
						.name(segments.get(1))
						.scope(scope)
						.version(segments.size() == 3 ? segments.get(2) : null)
						.build();
			} else if (expression instanceof GStringExpression gStringExpression) {
				String value = gStringExpression.getText();
				List<String> segments = List.of(value.split(":"));
				//
				result = DependencyNode.builder()
						.group(segments.get(0))
						.name(segments.get(1))
						.scope(scope)
						.version(segments.size() == 3 ? segments.get(2) : null)
						.build();
			} else if (expression instanceof MethodCallExpression methodCallExpression) {
				List<Expression> arguments = List.ofAll(((ArgumentListExpression) methodCallExpression.getArguments())
						.getExpressions());
				//
				String value = extractConstantValue(arguments.single());
				List<String> segments = List.of(value.split(":"));
				//
				result = DependencyNode.builder()
						.group(segments.get(0))
						.name(segments.get(1))
						.scope(scope)
						.type(methodCallExpression.getMethodAsString())
						.version(segments.size() == 3 ? segments.get(2) : null)
						.build();
			} else if (expression instanceof NamedArgumentListExpression namedArgumentListExpression) {
				Map<String, MapEntryExpression> mapEntryExpressions = namedArgumentListExpression
						.getMapEntryExpressions()
						.stream()
						.collect(Collectors.toMap(
								mapEntryExpression -> ((ConstantExpression) mapEntryExpression.getKeyExpression())
										.getValue().toString(),
								Function.identity()));
				//
				result = DependencyNode.builder()
						.group(extractConstantValue(mapEntryExpressions.get("group").getValueExpression()))
						.name(extractConstantValue(mapEntryExpressions.get("name").getValueExpression()))
						.scope(scope)
						.version(extractConstantValue(mapEntryExpressions.get("version").getValueExpression()))
						.build();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	private List<Try<DependencyNode>> processDependencyScope(MethodCallExpression methodCallExpression) {
		String scope = extractConstantValue(methodCallExpression.getMethod());
		//
		TupleExpression arguments = (TupleExpression) methodCallExpression.getArguments();
		List<Expression> expressions = List.ofAll(arguments.getExpressions());
		//
		return expressions
				.map(expression -> Try.ofSupplier(() -> processDependency(scope, expression)))
				.filter(dependencyNodeTry -> dependencyNodeTry.isFailure() || dependencyNodeTry.getOrNull() != null);
	}

	private PluginNode processPlugin(MethodCallExpression methodCallExpression) {
		PluginNode result;
		{
			String value = extractConstantValue(methodCallExpression.getMethod());
			//
			result = switch (value) {
				case "id" -> PluginNode.builder()
						.id(extractSingleConstantArgument(methodCallExpression.getArguments()))
						.build();
				case "version" -> processPlugin(((MethodCallExpression) methodCallExpression.getObjectExpression()))
						.toBuilder()
						.version(extractSingleConstantArgument(methodCallExpression.getArguments()))
						.build();
				default -> throw new NotImplementedException();
			};
		}
		return result;
	}

	private PluginsNode processPlugins(MethodCallExpression methodCallExpression) {
		List<Statement> statements = extractClosureStatements(methodCallExpression);
		//
		List<MethodCallExpression> methodCallExpressions = statements
				.map(ExpressionStatement.class::cast)
				.map(ExpressionStatement::getExpression)
				.map(MethodCallExpression.class::cast);
		//
		return PluginsNode.builder()
				.plugins(methodCallExpressions.map(this::processPlugin))
				.unprocessed(List.empty())
				.build();
	}

	private RepositoriesNode processRepositories(MethodCallExpression methodCallExpression) {
		List<Pair<Statement, RepositoryNode>> processedRepositories = extractClosureStatements(methodCallExpression)
				.map(statement -> Pair.of(statement, processRepository(statement)));
		//
		return RepositoriesNode.builder()
				.repositories(processedRepositories.map(Pair::getRight).filter(Objects::nonNull))
				.unprocessed(processedRepositories
						.filter(pair -> pair.getRight() == null)
						.map(Pair::getLeft)
						.map(Statement::getText))
				.build();
	}

	@Nullable
	private RepositoryNode processRepository(Statement statement) {
		RepositoryNode result;
		{
			MethodCallExpression methodCallExpression = (MethodCallExpression) ((ExpressionStatement) statement)
					.getExpression();
			String methodName = methodCallExpression.getMethodAsString();
			List<Expression> arguments = List.ofAll(((ArgumentListExpression) methodCallExpression.getArguments())
					.getExpressions());
			//
			if (arguments.isEmpty()) {
				Set<String> hardcodedRepositoryNames = HashSet.of(
						"google",
						"gradlePluginPortal",
						"jcenter",
						"mavenCentral",
						"mavenLocal");
				//
				result = hardcodedRepositoryNames.contains(methodName)
						? RepositoryNode.builder()
						.kind("maven")
						.name(methodName)
						.build()
						: null;
			} else {
				if (Objects.equals(methodName, "maven")) {
					result = RepositoryNode.builder()
							.kind("maven")
							.url(arguments.single().getText())
							.build();
				} else {
					// TODO: ivy
					//
					result = null;
				}
			}
		}
		return result;
	}

	public BuildGradleModel createBuildGradleModel(File file) {
		return ResourceUtils.tryReturnWithResourceSupplier(
				() -> new FileReader(file),
				reader -> {
					String source = String.join("\n", IOUtils.readLines(reader));
					//
					SourceUnit unit = SourceUnit.create("build.gradle", source);
					unit.parse();
					ModuleNode moduleNode = unit.buildAST();
					//
					// TODO: Handle settings.gradle.
					return processBuildGradle(moduleNode);
				});
	}

	private static List<Statement> extractClosureStatements(MethodCallExpression methodCallExpression) {
		Expression expression = List
				.ofAll(((ArgumentListExpression) methodCallExpression.getArguments()).getExpressions())
				.single();
		//
		List<Statement> statements = List
				.ofAll(((BlockStatement) ((ClosureExpression) expression).getCode()).getStatements());
		//
		return statements;
	}

	private static String extractConstantValue(Expression expression) {
		return (String) ((ConstantExpression) expression).getValue();
	}

	private static String extractSingleConstantArgument(Expression argumentsExpression) {
		return extractConstantValue(List.ofAll(((ArgumentListExpression) argumentsExpression).getExpressions())
				.single());
	}
}
