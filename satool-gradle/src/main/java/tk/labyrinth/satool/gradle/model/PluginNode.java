package tk.labyrinth.satool.gradle.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class PluginNode {

	String id;

	@Nullable
	String version;
}
