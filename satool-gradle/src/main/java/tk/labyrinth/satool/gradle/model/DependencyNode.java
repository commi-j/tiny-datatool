package tk.labyrinth.satool.gradle.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class DependencyNode {

	String group;

	String name;

	/**
	 * annotationProcessor;<br>
	 * api;<br>
	 * compileOnly;<br>
	 * developmentOnly;<br>
	 * implementation;<br>
	 * runtimeOnly;<br>
	 * testImplementation;<br>
	 */
	String scope;

	/**
	 * platform
	 */
	@Nullable
	String type;

	@Nullable
	String version;
}
