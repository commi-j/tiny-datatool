package tk.labyrinth.satool.gradle.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RepositoryNode {

	/**
	 * - maven;<br>
	 */
	@Nullable
	String kind;

	/**
	 * May be null where only url is specified.
	 */
	@Nullable
	String name;

	/**
	 * May be null for hardcoded named repositories which are well-known.
	 */
	@Nullable
	String url;
}
