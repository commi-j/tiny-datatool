package tk.labyrinth.satool.gradle.model;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class DependenciesNode {

	List<DependencyNode> dependencies;

	List<String> unprocessed;
}
