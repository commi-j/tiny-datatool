package tk.labyrinth.satool.gradle.model;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class BuildGradleModel {

	@Nullable
	DependenciesNode dependencies;

	@Nullable
	PluginsNode plugins;

	@Nullable
	RepositoriesNode repositories;

	List<String> unprocessed;
}
