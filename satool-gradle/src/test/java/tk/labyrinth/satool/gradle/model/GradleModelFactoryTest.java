package tk.labyrinth.satool.gradle.model;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;

class GradleModelFactoryTest {

	@Test
	void testCreateBuildGradleModel() {
		GradleModelFactory gradleModelFactory = new GradleModelFactory();
		//
		BuildGradleModel buildGradleModel = gradleModelFactory.createBuildGradleModel(
				new File("src/test/resources/build-dependencies.gradle"));
		//
		Assertions
				.assertThat(buildGradleModel)
				.isEqualTo(BuildGradleModel.builder()
						.dependencies(DependenciesNode.builder()
								.dependencies(List.of(
										DependencyNode.builder()
												.group("foo")
												.name("bar.simple")
												.scope("implementation")
												.build(),
										DependencyNode.builder()
												.group("foo")
												.name("bar.parentheses")
												.scope("implementation")
												.build(),
										DependencyNode.builder()
												.group("foo")
												.name("bar.closure")
												.scope("implementation")
												.build(),
										DependencyNode.builder()
												.group("foo")
												.name("bar.platform")
												.scope("implementation")
												.type("platform")
												.build()
								))
								.unprocessed(List.of(
										"java.lang.ClassCastException: class org.codehaus.groovy.ast.expr.VariableExpression cannot be cast to class org.codehaus.groovy.ast.expr.MethodCallExpression (org.codehaus.groovy.ast.expr.VariableExpression and org.codehaus.groovy.ast.expr.MethodCallExpression are in unnamed module of loader 'app')"))
								.build())
						.unprocessed(List.empty())
						.build());
	}
}