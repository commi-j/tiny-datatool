package tk.labyrinth.satool.kafman.modelling;

import io.vavr.collection.List;

public interface DatatypeQueryResolver {

	List<String> getAttributeOperators(Class<?> rootType, String attributeNameChainString);

	List<String> getAttributes(Class<?> rootType, String attributeNameChainEntry);

	List<String> getValues(Class<?> rootType, String attributeNameChainString, String valueEntry);
}
