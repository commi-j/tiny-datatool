package tk.labyrinth.satool.kafman.query;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.satool.kafman.datatype.Attribute;
import tk.labyrinth.satool.kafman.datatype.AttributeMap;

@RequiredArgsConstructor
@Service
public class QueryParser {

	private final AttributeMap attributeMap;

	public CompleteAttributeChain parseCompleteAttributeChain(Class<?> rootType, String attributeEntryChain) {
		return parseIncompleteAttributeChain(rootType, attributeEntryChain + ".").getCompleteChain();
	}

	public IncompleteAttributeChain parseIncompleteAttributeChain(Class<?> rootType, String attributeEntryChain) {
		IncompleteAttributeChain result;
		{
			Class<?> currentType = rootType;
			List<String> path = List.of(attributeEntryChain.split("\\.", -1));
			List<Attribute> attributes = List.of();
			if (!path.isEmpty()) {
				while (currentType != null && path.size() > 1) {
					Attribute currentAttribute = attributeMap.findAttribute(currentType, path.get());
					if (currentAttribute != null) {
						attributes = attributes.append(currentAttribute);
						currentType = currentAttribute.getType();
						path = path.tail();
					} else {
						throw new IllegalArgumentException("Attribute not found: " + attributeEntryChain);
					}
				}
				result = IncompleteAttributeChain.builder()
						.completeChain(CompleteAttributeChain.builder()
								.attributes(attributes)
								.rootType(rootType)
								.build())
						.incompleteText(path.single())
						.build();
			} else {
				throw new UnreachableStateException();
			}
		}
		return result;
	}
}
