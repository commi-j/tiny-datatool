package tk.labyrinth.satool.kafman.query;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.stream.Collectors;

@Builder(toBuilder = true)
@Value
@With
public class QueryObject {

	List<QueryElement> elements;

	public QueryObject append(QueryElement element) {
		return withElements(elements.append(element));
	}

	public QueryElement getLastElement() {
		return elements.last();
	}

	public QueryObject getPreviousObject() {
		return withElements(elements.init());
	}

	public String render() {
		return elements.toStream()
				.map(QueryElement::getValue)
				.collect(Collectors.joining(" "));
	}

	public static QueryObject empty() {
		return new QueryObject(List.empty());
	}
}
