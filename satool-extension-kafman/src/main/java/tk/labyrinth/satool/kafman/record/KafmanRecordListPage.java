package tk.labyrinth.satool.kafman.record;

import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.satool.kafman.base.ObjectListPageBase;
import tk.labyrinth.satool.kafman.root.KafmanRootParentLayout;

@RequiredArgsConstructor
@Route(value = "records", layout = KafmanRootParentLayout.class)
public class KafmanRecordListPage extends ObjectListPageBase<KafmanRecord> {
	// empty
}
