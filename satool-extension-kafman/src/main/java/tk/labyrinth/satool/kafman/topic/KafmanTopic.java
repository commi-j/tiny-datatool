package tk.labyrinth.satool.kafman.topic;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;

import java.util.UUID;

@Builder(toBuilder = true)
@Value
public class KafmanTopic {

	KafmanBrokerReference brokerReference;

	UUID id;

	String name;

	Integer partitionCount;
}
