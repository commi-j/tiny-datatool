package tk.labyrinth.satool.kafman.query;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.satool.kafman.datatype.Attribute;

@Builder(toBuilder = true)
@Value
public class CompleteAttributeChain {

	/**
	 * 0..n
	 */
	List<Attribute> attributes;

	Class<?> rootType;

	boolean hasAttributes() {
		return !attributes.isEmpty();
	}

	public CompleteAttributeChain dropFirst() {
		if (attributes.isEmpty()) {
			throw new IllegalArgumentException("Require attributes.size > 0: " + this);
		}
		return CompleteAttributeChain.builder()
				.attributes(attributes.tail())
				.rootType(attributes.get().getType())
				.build();
	}

	public Class<?> getEndType() {
		return hasAttributes() ? attributes.last().getType() : rootType;
	}

	public List<String> getNameChain() {
		return attributes.map(Attribute::getName);
	}

	public String getNameChainString() {
		return String.join(".", getNameChain());
	}

	public int length() {
		return attributes.size();
	}
}
