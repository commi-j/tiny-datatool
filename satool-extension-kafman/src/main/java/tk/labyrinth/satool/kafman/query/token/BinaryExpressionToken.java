package tk.labyrinth.satool.kafman.query.token;

import lombok.Value;

@Value(staticConstructor = "of")
public class BinaryExpressionToken implements Token {

	Token attribute;

	Token operator;

	Token value;

	@Override
	public boolean isSimple() {
		return false;
	}
}
