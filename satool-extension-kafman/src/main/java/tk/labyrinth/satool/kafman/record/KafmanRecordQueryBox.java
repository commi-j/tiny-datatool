package tk.labyrinth.satool.kafman.record;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.QueryBoxBase;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanRecordQueryBox extends QueryBoxBase<KafmanRecord> {
	// empty
}
