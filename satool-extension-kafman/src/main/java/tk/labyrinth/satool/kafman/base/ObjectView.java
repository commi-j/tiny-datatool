package tk.labyrinth.satool.kafman.base;

import com.vaadin.flow.component.Component;

public interface ObjectView<T> {

	Component asComponent();

	void setValue(T value);
}
