package tk.labyrinth.satool.kafman.base;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import io.vavr.control.Option;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

public abstract class ObjectViewBase<T> extends Composite<CssGridLayout> implements ObjectView<T> {

	@PostConstruct
	private void postConstruct() {
		preConfigure(getContent());
		configure(getContent());
	}

	protected abstract void configure(CssGridLayout layout);

	protected void preConfigure(CssGridLayout layout) {
		layout.setGridTemplateColumns(PandoraStyles.repeatDefaultCellWidth(4));
	}

	protected abstract void setValue(Option<T> valueOption);

	@Override
	public Component asComponent() {
		return getContent();
	}

	@Override
	public void setValue(T value) {
		setValue(Option.of(value));
	}
}
