package tk.labyrinth.satool.kafman.datatype;

import io.vavr.collection.List;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

@Component
public class AttributeMap {

	private final ConcurrentMap<Class<?>, List<Attribute>> relations = new ConcurrentHashMap<>();

	private <T> void registerRelation(Class<T> ownerType, String attributeName, Class<?> attributeType) {
		Attribute attribute = new SimpleAttribute<>(attributeName, attributeType);
		relations.compute(ownerType, (key, value) -> value != null ? value.append(attribute) : List.of(attribute));
	}

	@Nullable
	public Attribute findAttribute(Class<?> ownerType, String attributeName) {
		return relations.get(ownerType).find(attribute ->
				Objects.equals(attribute.getName(), attributeName)).getOrNull();
	}

	public List<Attribute> getAttributes(Class<?> ownerType) {
		return relations.get(ownerType);
	}

	public <T, R> void registerPropertyRelation(
			Class<T> ownerType,
			String attributeName,
			Class<R> attributeType,
			Function<T, R> function) {
		registerRelation(ownerType, attributeName, attributeType);
	}

	public <T, R> void registerReferenceRelation(
			Class<T> ownerType,
			String attributeName,
			Class<R> attributeType,
			Function<T, Reference<R>> function) {
		registerRelation(ownerType, attributeName, attributeType);
	}
}
