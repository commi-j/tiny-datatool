package tk.labyrinth.satool.kafman.record;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import tk.labyrinth.expresso.lang.predicate.BinaryPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.satool.kafman.base.ChildObjectSearcherBase;
import tk.labyrinth.satool.kafman.base.ObjectItem;
import tk.labyrinth.satool.kafman.connector.KafmanConnector;
import tk.labyrinth.satool.kafman.connector.KafmanConnectorRegistry;
import tk.labyrinth.satool.kafman.partition.KafmanPartition;
import tk.labyrinth.satool.kafman.partition.KafmanPartitionReference;

import java.util.Objects;

@Lazy
@RequiredArgsConstructor
@Service
public class KafmanRecordSearcher extends ChildObjectSearcherBase<KafmanRecord, KafmanPartition> {

	private final KafmanConnectorRegistry connectorRegistry;

	@Override
	@SuppressWarnings("unchecked")
	protected ParameterizedQuery<Class<KafmanRecord>> adjustSearchQuery(
			ParameterizedQuery<Class<KafmanRecord>> searchQuery) {
		ParameterizedQuery<Class<KafmanRecord>> result;
		{
			Predicate predicate = searchQuery.getPredicate();
			if (predicate != null) {
				if (predicate instanceof BinaryPredicate) {
					BinaryPredicate<String, Object> binaryPredicate = (BinaryPredicate<String, Object>) predicate;
					String attributeName = binaryPredicate.first();
					if (Objects.equals(attributeName, "partition")) {
						result = ParameterizedQuery.builderWithClassParameter(searchQuery.getParameter())
								.predicate(binaryPredicate
										.withFirst("partitionReference")
										.withSecond(KafmanPartitionReference.from(binaryPredicate.second().toString())))
								.build();
					} else {
						result = searchQuery;
					}
				} else {
					result = searchQuery;
				}
			} else {
				result = searchQuery;
			}
		}
		return result;
	}

	@Override
	protected Stream<KafmanRecord> fromParentObjects(List<KafmanPartition> partitions) {
		return partitions
				.groupBy(partition -> partition.getTopicReference().getBrokerReference())
				.toStream()
				.flatMap(pair -> {
					KafmanConnector connector = connectorRegistry.get(pair._1());
					return connector.searchRecords(pair._2());
				});
	}

	@Override
	protected Class<KafmanPartition> getParentType() {
		return KafmanPartition.class;
	}

	@Override
	protected ObjectItem<KafmanRecord> render(KafmanRecord object) {
		// FIXME
		return ObjectItem.of(null, "TEXT");
	}
}
