package tk.labyrinth.satool.kafman.query;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class IncompleteAttributeChain {

	CompleteAttributeChain completeChain;

	String incompleteText;

	public String getNameChainStream() {
		return getPrefix() + incompleteText;
	}

	public Class<?> getOwnerType() {
		return completeChain.getEndType();
	}

	public String getPrefix() {
		return completeChain.hasAttributes()
				? completeChain.getNameChainString() + "."
				: "";
	}
}
