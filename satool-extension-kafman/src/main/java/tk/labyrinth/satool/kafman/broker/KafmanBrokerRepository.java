package tk.labyrinth.satool.kafman.broker;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface KafmanBrokerRepository extends CrudRepository<KafmanBroker, String> {
	// empty
}
