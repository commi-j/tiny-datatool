package tk.labyrinth.satool.kafman.broker;

import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.ObjectViewCardBase;

@PrototypeScopedComponent
public class KafmanBrokerCard extends ObjectViewCardBase<KafmanBroker> {
	// empty
}
