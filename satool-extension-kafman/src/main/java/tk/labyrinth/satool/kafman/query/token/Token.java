package tk.labyrinth.satool.kafman.query.token;

public interface Token {

	boolean isSimple();
}
