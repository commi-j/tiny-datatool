package tk.labyrinth.satool.kafman.datatype;

public interface Attribute {

	String getName();

	Class<?> getType();
}
