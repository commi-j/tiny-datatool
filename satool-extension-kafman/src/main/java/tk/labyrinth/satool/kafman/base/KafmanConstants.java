package tk.labyrinth.satool.kafman.base;

public class KafmanConstants {

	public static final String REFERENCE_ATTRIBUTE_NAME = "reference()";

	public static final String THIS_ATTRIBUTE_NAME = "this()";
}
