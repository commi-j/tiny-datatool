package tk.labyrinth.satool.kafman.broker;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.control.Option;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.base.ObjectViewBase;

@PrototypeScopedComponent
public class KafmanBrokerView extends ObjectViewBase<KafmanBroker> {

	private final TextField bootstrapServersField = new TextField("Bootstrap Servers");

	private final TextField keyField = new TextField("Key");

	private final TextField nameField = new TextField("Name");

	@Override
	protected void configure(CssGridLayout layout) {
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				layout.add(nameField);
			}
			{
				layout.add(keyField);
			}
			{
				layout.add(bootstrapServersField);
			}
		}
		{
			{
				layout.getChildren()
						.filter(HasValue.class::isInstance)
						.map(HasValue.class::cast)
						.forEach(child -> child.setReadOnly(true));
			}
		}
	}

	@Override
	protected void setValue(Option<KafmanBroker> valueOption) {
		bootstrapServersField.setValue(valueOption.map(KafmanBroker::getBootstrapServers).getOrElse(""));
		keyField.setValue(valueOption.map(KafmanBroker::getKey).getOrElse(""));
		nameField.setValue(valueOption.map(KafmanBroker::getName).getOrElse(""));
	}
}
