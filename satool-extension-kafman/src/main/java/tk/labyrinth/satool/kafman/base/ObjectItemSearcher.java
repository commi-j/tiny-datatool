package tk.labyrinth.satool.kafman.base;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

public interface ObjectItemSearcher<Q, T> extends TypeAware<T> {

	List<ObjectItem<T>> searchForObjectItems(Q query);
}
