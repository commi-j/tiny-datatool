package tk.labyrinth.satool.kafman.topic;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.control.Option;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.base.ObjectViewBase;

@PrototypeScopedComponent
public class KafmanTopicView extends ObjectViewBase<KafmanTopic> {

	private final TextField brokerReferenceField = new TextField("Broker Reference");

	private final TextField idField = new TextField("Id");

	private final TextField nameField = new TextField("Name");

	private final TextField partitionCountField = new TextField("Partition Count");

	@Override
	protected void configure(CssGridLayout layout) {
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				layout.add(brokerReferenceField);
			}
			{
				layout.add(nameField);
			}
			{
				layout.add(partitionCountField);
			}
			{
				layout.add(idField);
			}
		}
		{
			{
				layout.getChildren()
						.filter(HasValue.class::isInstance)
						.map(HasValue.class::cast)
						.forEach(child -> child.setReadOnly(true));
			}
		}
	}

	@Override
	protected void setValue(Option<KafmanTopic> valueOption) {
		brokerReferenceField.setValue(valueOption.map(KafmanTopic::getBrokerReference).map(String::valueOf).getOrElse(""));
		idField.setValue(valueOption.map(KafmanTopic::getId).map(String::valueOf).getOrElse(""));
		nameField.setValue(valueOption.map(KafmanTopic::getName).getOrElse(""));
		partitionCountField.setValue(valueOption.map(KafmanTopic::getPartitionCount).map(String::valueOf).getOrElse(""));
	}
}
