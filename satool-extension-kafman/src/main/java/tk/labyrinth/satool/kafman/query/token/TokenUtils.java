package tk.labyrinth.satool.kafman.query.token;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;

import java.util.Objects;

/**
 * a == b AND (c == d OR e == f)
 * (a == b AND c == d) OR e == f
 */
public class TokenUtils {

	public static final String AND_CLAUSE = "AND";

	public static final String CLOSE_BRACE = ")";

	public static final String OPEN_BRACE = "(";

	public static final String OR_CLAUSE = "OR";

	public static Token resolve(List<String> tokenStrings) {
		List<Token> tokensWithSeparatedBraces = separateBraces(tokenStrings).map(SimpleToken::of);
		List<Token> tokensWithResolvedBraces = resolveBraces(tokensWithSeparatedBraces);
		Token tokenWithResolvedJunctions = resolveJunctions(tokensWithResolvedBraces);
		return tokenWithResolvedJunctions;
	}

	public static List<Token> resolveBraces(List<Token> tokens) {
		List<Token> result;
		{
			int indexOfFirstBraceToken = tokens.indexWhere(token ->
					token.isSimple() && (Objects.equals(token.toString(), OPEN_BRACE) ||
							Objects.equals(token.toString(), CLOSE_BRACE)));
			if (indexOfFirstBraceToken != -1) {
				boolean isFirstOpen = Objects.equals(tokens.get(indexOfFirstBraceToken).toString(), OPEN_BRACE);
				if (isFirstOpen) {
					// Open
					//
					int indexOfSecondBraceToken = tokens.indexWhere(
							innerToken -> innerToken.isSimple() &&
									(Objects.equals(innerToken.toString(), OPEN_BRACE) ||
											Objects.equals(innerToken.toString(), CLOSE_BRACE)),
							indexOfFirstBraceToken + 1);
					if (indexOfSecondBraceToken != -1) {
						boolean isSecondOpen = Objects.equals(
								tokens.get(indexOfSecondBraceToken).toString(),
								OPEN_BRACE);
						if (isSecondOpen) {
							result = resolveBraces(tokens.subSequence(0, indexOfSecondBraceToken)
									.appendAll(resolveBraces(tokens.subSequence(indexOfSecondBraceToken + 1))));
						} else {
							result = tokens.subSequence(0, indexOfFirstBraceToken)
									.append(BracesToken.of(tokens.subSequence(
											indexOfFirstBraceToken + 1,
											indexOfSecondBraceToken)))
									.appendAll(resolveBraces(tokens.subSequence(indexOfSecondBraceToken + 1)));
						}
					} else {
						// Open without following close -> malformed.
						// TODO: Treat all as unclosed part;
						//
						result = tokens;
					}
				} else {
					// Close without preceding open -> malformed.
					// TODO: Mark token as invalid;
					//
					result = tokens.subSequence(0, indexOfFirstBraceToken + 1)
							.appendAll(resolveBraces(tokens.subSequence(indexOfFirstBraceToken + 1)));
				}
			} else {
				// No braces -> nothing to resolve.
				result = tokens;
			}
		}
		return result;
	}

	public static Token resolveJunctions(List<Token> tokens) {
		Token result;
		{
			List<Token> currentAndTokenChunk = List.of();
			List<Token> currentOrTokenChunk = List.of();
			List<List<Token>> andTokenChunks = List.of();
			List<List<Token>> orTokenChunks = List.of();
			for (Token token : tokens) {
				if (token.isSimple()) {
					if (Objects.equals(token.toString().toLowerCase(), "or")) {
						orTokenChunks = orTokenChunks.append(currentOrTokenChunk);
						currentOrTokenChunk = List.of();
					} else if (Objects.equals(token.toString().toLowerCase(), "and")) {
						andTokenChunks = andTokenChunks.append(currentAndTokenChunk);
						currentAndTokenChunk = List.of();
					} else {
						currentAndTokenChunk = currentAndTokenChunk.append(token);
						currentOrTokenChunk = currentOrTokenChunk.append(token);
					}
				} else {
					currentAndTokenChunk = currentAndTokenChunk.append(token);
					currentOrTokenChunk = currentOrTokenChunk.append(token);
				}
			}
			if (!orTokenChunks.isEmpty()) {
				result = JunctionToken.of(
						"OR",
						orTokenChunks.append(currentOrTokenChunk).map(TokenUtils::resolveJunctions));
			} else if (!andTokenChunks.isEmpty()) {
				result = JunctionToken.of(
						"AND",
						andTokenChunks.append(currentAndTokenChunk).map(TokenUtils::resolveJunctions));
			} else {
				if (currentAndTokenChunk.size() == 0) {
					result = new EmptyToken();
				} else if (currentAndTokenChunk.size() == 1) {
					Token token = currentAndTokenChunk.get();
					if (token instanceof BracesToken) {
						BracesToken bracesToken = (BracesToken) token;
						result = BracesToken.of(resolveJunctions(bracesToken.getTokens()));
					} else {
						result = new InvalidToken(currentAndTokenChunk);
					}
				} else if (currentAndTokenChunk.size() == 3) {
					result = BinaryExpressionToken.of(
							currentAndTokenChunk.get(0),
							currentAndTokenChunk.get(1),
							currentAndTokenChunk.get(2));
				} else {
					result = new InvalidToken(currentAndTokenChunk);
				}
			}
		}
		return result;
	}

	public static List<String> separateBraces(List<String> tokenStrings) {
		List<String> result;
		{
			int indexOfTokenWithBrace = tokenStrings.indexWhere(tokenString ->
					(tokenString.startsWith(OPEN_BRACE) || tokenString.endsWith(CLOSE_BRACE)) &&
							tokenString.length() > 1);
			if (indexOfTokenWithBrace != -1) {
				List<String> newTokens;
				{
					String tokenWithBrace = tokenStrings.get(indexOfTokenWithBrace);
					if (tokenWithBrace.startsWith(OPEN_BRACE)) {
						newTokens = List.of(tokenWithBrace.substring(0, 1), tokenWithBrace.substring(1));
					} else if (tokenWithBrace.endsWith(CLOSE_BRACE)) {
						newTokens = List.of(
								tokenWithBrace.substring(0, tokenWithBrace.length() - 1),
								tokenWithBrace.substring(tokenWithBrace.length() - 1));
					} else {
						throw new UnreachableStateException();
					}
				}
				result = tokenStrings.subSequence(0, indexOfTokenWithBrace)
						.appendAll(newTokens)
						.appendAll(separateBraces(tokenStrings.subSequence(indexOfTokenWithBrace + 1)));
			} else {
				result = tokenStrings;
			}
		}
		return result;
	}
}
