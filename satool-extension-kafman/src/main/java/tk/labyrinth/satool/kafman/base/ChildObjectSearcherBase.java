package tk.labyrinth.satool.kafman.base;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.lang.predicate.BinaryPredicate;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.domain.java.JavaContext;
import tk.labyrinth.expresso.query.domain.java.JavaQueryExecutor;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcher;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcherBase;
import tk.labyrinth.satool.kafman.query.CompleteAttributeChain;
import tk.labyrinth.satool.kafman.query.QueryParser;

import java.util.Objects;

public abstract class ChildObjectSearcherBase<T, P> extends ObjectSearcherBase<ParameterizedQuery<Class<T>>, T> implements
		ObjectItemSearcher<ParameterizedQuery<Class<T>>, T> {

	@Autowired
	private ObjectSearcher<ParameterizedQuery<Class<P>>, P> parentObjectSearcher;

	@Autowired
	private JavaQueryExecutor queryExecutor;

	@Autowired
	private QueryParser queryParser;

	@SuppressWarnings("unchecked")
	private Pair<ParameterizedQuery<Class<T>>, ParameterizedQuery<Class<P>>> destructureSearchQuery(
			ParameterizedQuery<Class<T>> searchQuery) {
		Predicate thisPredicate;
		Predicate parentPredicate;
		{
			Predicate inputPredicate = searchQuery.getPredicate();
			if (inputPredicate != null) {
				if (inputPredicate instanceof BinaryPredicate) {
					BinaryPredicate<String, ?> propertyPredicate = (BinaryPredicate<String, ?>) inputPredicate;
					CompleteAttributeChain attributeChain = queryParser.parseCompleteAttributeChain(
							searchQuery.getParameter(),
							propertyPredicate.first());
					if (attributeChain.length() > 1) {
						thisPredicate = null;
						parentPredicate = propertyPredicate.withFirst(attributeChain.dropFirst().getNameChainString());
					} else {
						thisPredicate = inputPredicate;
						//
						// FIXME: This is a workaround, we transform any attribute of matching type into reference.
						// Goal solution would be to check if attribute matches "parent reference".
						parentPredicate = Objects.equals(attributeChain.getEndType(), getParentType())
								? propertyPredicate.withFirst(KafmanConstants.REFERENCE_ATTRIBUTE_NAME)
								: null;
					}
				} else if (inputPredicate instanceof JunctionPredicate) {
					throw new NotImplementedException(searchQuery.toString());
				} else {
					throw new NotImplementedException(searchQuery.toString());
				}
			} else {
				thisPredicate = null;
				parentPredicate = null;
			}
		}
		return Pair.of(
				ParameterizedQuery.builderWithClassParameter(searchQuery.getParameter())
						.predicate(thisPredicate)
						.build(),
				ParameterizedQuery.builderWithClassParameter(getParentType())
						.predicate(parentPredicate)
						.build());
	}

	protected ParameterizedQuery<Class<T>> adjustSearchQuery(ParameterizedQuery<Class<T>> searchQuery) {
		return searchQuery;
	}

	protected abstract Stream<T> fromParentObjects(List<P> parentObjects);

	protected abstract Class<P> getParentType();

	protected abstract ObjectItem<T> render(T object);

	@Override
	public long count(ParameterizedQuery<Class<T>> query) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public <R extends Reference<T>> ResolvedReference<T, R> resolve(R reference) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public List<T> search(ParameterizedQuery<Class<T>> searchQuery) {
		Pair<ParameterizedQuery<Class<T>>, ParameterizedQuery<Class<P>>> destructured =
				destructureSearchQuery(searchQuery);
		//
		List<P> parentObjects = parentObjectSearcher.search(destructured.getRight());
		//
		KafmanJavaContext javaContext = new KafmanJavaContext(fromParentObjects(parentObjects));
		//
		return queryExecutor.search(javaContext, adjustSearchQuery(destructured.getLeft())).stream()
				.collect(List.collector());
	}

	@Override
	public List<ObjectItem<T>> searchForObjectItems(ParameterizedQuery<Class<T>> query) {
		return search(query).map(this::render);
	}

	@Value
	public static class KafmanJavaContext implements JavaContext {

		Stream<?> stream;

		@Override
		@SuppressWarnings("unchecked")
		public <T> java.util.stream.Stream<T> getStream(Class<T> type) {
			return (java.util.stream.Stream<T>) stream.toJavaStream();
		}
	}
}
