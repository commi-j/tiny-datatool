package tk.labyrinth.satool.kafman.root;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerListPage;
import tk.labyrinth.satool.kafman.partition.KafmanPartitionListPage;
import tk.labyrinth.satool.kafman.record.KafmanRecordListPage;
import tk.labyrinth.satool.kafman.topic.KafmanTopicListPage;

import jakarta.annotation.PostConstruct;

@RoutePrefix("kafman")
public class KafmanRootParentLayout extends CssHorizontalLayout implements RouterLayout {

	@PostConstruct
	private void postConstruct() {
		{
			setHeightFull();
		}
		{
			CssVerticalLayout sidebar = new CssVerticalLayout();
			{
				sidebar.addClassName(PandoraStyles.LAYOUT);
			}
			{
				sidebar.add(new RouterLink("Brokers", KafmanBrokerListPage.class));
				sidebar.add(new RouterLink("Topics", KafmanTopicListPage.class));
				sidebar.add(new RouterLink("Partitions", KafmanPartitionListPage.class));
				sidebar.add(new RouterLink("Records", KafmanRecordListPage.class));
			}
			add(sidebar);
		}
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		CssFlexItem.setFlexGrow(content, 1);
		add((Component) content);
	}
}
