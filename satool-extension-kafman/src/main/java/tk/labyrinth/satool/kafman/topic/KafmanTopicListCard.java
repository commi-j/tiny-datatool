package tk.labyrinth.satool.kafman.topic;

import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.ListViewCardBase;

@PrototypeScopedComponent
public class KafmanTopicListCard extends ListViewCardBase<KafmanTopic> {
	// empty
}
