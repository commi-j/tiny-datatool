package tk.labyrinth.satool.kafman.base;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.TextRenderer;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.satool.kafman.model.KafmanDatatypeQueryResolver;
import tk.labyrinth.satool.kafman.query.QueryDataProvider;
import tk.labyrinth.satool.kafman.query.QueryObject;

import javax.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class QueryBoxBase<T> extends Composite<HorizontalLayout> implements TypeAware<T> {

	private final ComboBox<QueryObject> comboBox = new ComboBox<>();

	@Autowired
	private KafmanDatatypeQueryResolver datatypeQueryResolver;

	@Getter
	@Nullable
	@Setter
	private Consumer<ParameterizedQuery<Class<T>>> searchCallback = null;

	@PostConstruct
	private void postConstruct() {
		{
			comboBox.setAllowCustomValue(true);
			comboBox.setItems(new QueryDataProvider<>(datatypeQueryResolver, getParameterClass()));
			comboBox.setItemLabelGenerator(queryObject -> queryObject.getElements().toStream()
					.map(element -> element.getValue() + "")
					.collect(Collectors.joining(" ", "", "")));
			comboBox.setRenderer(new TextRenderer<>(queryElement -> queryElement.getElements().last().getValue()));
			//
			comboBox.addCustomValueSetListener(event ->
					event.getSource().setValue(QueryDataProvider.resolveQuery(event.getDetail())));
			//
			CssFlexItem.setFlexGrow(comboBox, 1);
			getContent().add(comboBox);
		}
		{
			Button searchButton = new Button(VaadinIcon.SEARCH.create(), event -> triggerSearch());
			getContent().add(searchButton);
		}
		{
			comboBox.setValue(QueryObject.empty());
		}
	}

	private void triggerSearch() {
		if (searchCallback != null) {
			searchCallback.accept(getValue());
		}
	}

	public ParameterizedQuery<Class<T>> getValue() {
		return datatypeQueryResolver.resolve(
				getParameterClass(),
				// FIXME: Find out why value could be empty (when erasing text), it should never be null.
				comboBox.getOptionalValue().orElse(QueryObject.empty()));
	}
}
