package tk.labyrinth.satool.kafman.broker;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.Reference;

@Value
public class KafmanBrokerReference implements Reference<KafmanBroker> {

	String brokerKey;

	@Override
	public String toString() {
		return render(brokerKey);
	}

	public static KafmanBrokerReference from(KafmanBroker broker) {
		return of(broker.getKey());
	}

	public static KafmanBrokerReference from(String value) {
		return of(value);
	}

	public static KafmanBrokerReference of(String brokerKey) {
		return new KafmanBrokerReference(brokerKey);
	}

	public static String render(String brokerKey) {
		return brokerKey;
	}
}
