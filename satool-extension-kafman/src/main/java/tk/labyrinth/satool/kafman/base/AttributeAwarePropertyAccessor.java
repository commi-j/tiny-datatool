package tk.labyrinth.satool.kafman.base;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.domain.java.JavaPropertyAccessor;
import tk.labyrinth.expresso.query.domain.java.SpringPropertyAccessor;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.satool.kafman.datatype.DatatypeRegistry;

import java.util.Objects;
import java.util.function.Function;

@RequiredArgsConstructor
//@Service
public class AttributeAwarePropertyAccessor implements JavaPropertyAccessor {

	private final DatatypeRegistry datatypeRegistry;

	private final SpringPropertyAccessor springPropertyAccessor = new SpringPropertyAccessor();

	@SuppressWarnings("unchecked")
	private <I, O> O resolveReference(I target) {
		O result;
		{
			Class<I> targetType = (Class<I>) target.getClass();
			Function<I, Reference<I>> referenceBuilder = datatypeRegistry.findReferenceBuilder(targetType);
			if (referenceBuilder != null) {
				result = (O) referenceBuilder.apply(target);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Override
	public <T> T get(Object target, String property) {
		T result;
		if (Objects.equals(property, KafmanConstants.REFERENCE_ATTRIBUTE_NAME)) {
			result = resolveReference(target);
		} else {
			result = springPropertyAccessor.get(target, property);
		}
		return result;
	}
}
