package tk.labyrinth.satool.kafman.broker;

import com.vaadin.flow.component.combobox.ComboBox;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;

import jakarta.annotation.PostConstruct;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanBrokerBox extends ComboBox<KafmanBrokerReference> {

	private final KafmanBrokerRegistry brokerRegistry;

	@PostConstruct
	private void postConstruct() {
		setAllowCustomValue(true);
		setItems();
	}
}
