package tk.labyrinth.satool.kafman.topic;

import com.vaadin.flow.component.combobox.ComboBox;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;

import jakarta.annotation.PostConstruct;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanTopicBox extends ComboBox<KafmanTopicReference> {

	@PostConstruct
	private void postConstruct() {
		setAllowCustomValue(true);
		setItems();
	}
}
