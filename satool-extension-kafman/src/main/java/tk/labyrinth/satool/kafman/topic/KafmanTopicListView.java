package tk.labyrinth.satool.kafman.topic;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.RouterLink;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.ListViewBase;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerPage;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanTopicListView extends ListViewBase<KafmanTopic> {

	@Override
	protected void configureGrid(Grid<KafmanTopic> grid) {
		grid
				.addComponentColumn(item -> new RouterLink(
						item.getBrokerReference().getBrokerKey(),
						KafmanBrokerPage.class,
						item.getBrokerReference().toString()))
				.setHeader("Broker");
		grid
				.addComponentColumn(item -> new RouterLink(
						item.getName(),
						KafmanTopicPage.class,
						KafmanTopicReference.render(item)))
				.setHeader("Name");
		grid
				.addColumn(KafmanTopic::getPartitionCount)
				.setHeader("Partition Count");
		grid
				.addColumn(KafmanTopic::getId)
				.setHeader("Id");
	}
}
