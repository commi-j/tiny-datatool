package tk.labyrinth.satool.kafman.base;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.Reference;

@Value(staticConstructor = "of")
public class ObjectItem<T> {

	Reference<T> reference;

	String text;
}
