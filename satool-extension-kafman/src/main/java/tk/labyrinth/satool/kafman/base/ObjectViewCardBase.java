package tk.labyrinth.satool.kafman.base;

import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import jakarta.annotation.PostConstruct;

@PrototypeScopedComponent
public abstract class ObjectViewCardBase<T> extends Details {

	@Autowired
	private ObjectView<T> view;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.CARD);
			addThemeVariants(DetailsVariant.FILLED);
			setOpened(true);
		}
		{
			addContent(view.asComponent());
		}
	}

	public void setValue(T value) {
		view.setValue(value);
	}
}
