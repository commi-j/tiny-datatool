package tk.labyrinth.satool.kafman.base;

import com.vaadin.flow.component.grid.Grid;
import io.vavr.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.datatool.frontend.base.TableBase;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcher;

import jakarta.annotation.PostConstruct;
import java.util.function.Supplier;

/**
 * @deprecated {@link TableBase}
 */
@Deprecated
public abstract class ListViewBase<T> extends CssVerticalLayout {

	private final Grid<T> grid = new Grid<>();

	@Autowired
	private ObjectSearcher<ParameterizedQuery<Class<T>>, T> objectSearcher;

	private Supplier<ParameterizedQuery<Class<T>>> searchQuerySupplier = null;

	@PostConstruct
	private void postConstruct() {
		{
			configureGrid(grid);
			add(grid);
		}
	}

	protected abstract void configureGrid(Grid<T> grid);

	public void initialize(Supplier<ParameterizedQuery<Class<T>>> querySupplier) {
		this.searchQuerySupplier = querySupplier;
		refresh();
	}

	public void refresh() {
		List<T> items = objectSearcher.search(searchQuerySupplier.get());
		grid.setItems(items.asJava());
	}
}
