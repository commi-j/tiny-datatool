package tk.labyrinth.satool.kafman.datatype;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class ComplexAttribute<T> implements Attribute {

	@Getter(AccessLevel.NONE)
	AttributeMap attributeMap;

	String name;

	Class<T> type;
}
