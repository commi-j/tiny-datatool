package tk.labyrinth.satool.kafman.broker;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.RouterLink;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.ListViewBase;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanBrokerListView extends ListViewBase<KafmanBroker> {

	@Override
	protected void configureGrid(Grid<KafmanBroker> grid) {
		grid
				.addComponentColumn(item ->
						new RouterLink(item.getName(), KafmanBrokerPage.class, item.getKey()))
				.setHeader("Broker");
		grid.addColumn(KafmanBroker::getBootstrapServers)
				.setHeader("Bootstrap Servers");
		add(grid);
	}
}
