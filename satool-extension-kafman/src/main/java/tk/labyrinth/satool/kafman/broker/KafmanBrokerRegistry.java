package tk.labyrinth.satool.kafman.broker;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@RequiredArgsConstructor
public class KafmanBrokerRegistry {

	private final List<KafmanBroker> brokers = new CopyOnWriteArrayList<>();
//	private final KafmanBrokerRepository repository;

	public KafmanBroker add(KafmanBroker broker) {
		KafmanBroker result = broker.toBuilder()
				.build();
		brokers.add(result);
		return result;
	}

	@Nullable
	public KafmanBroker find(KafmanBrokerReference brokerReference) {
		return findByKey(brokerReference.getBrokerKey());
	}

	@Nullable
	public KafmanBroker findByKey(String brokerKey) {
		return brokers.stream()
				.filter(broker -> Objects.equals(broker.getKey(), brokerKey))
				.findAny()
				.orElse(null);
	}

	public List<KafmanBroker> get() {
		return new ArrayList<>(brokers);
	}
}
//@RequiredArgsConstructor
//@Service
//public class KafmanBrokerRegistry {
//
//	private final GenericObjectSearcher objectSearcher;
//
//	private final KafmanBrokerRepository repository;
//
//	public KafmanBroker add(KafmanBroker broker) {
//		return repository.save(broker);
//	}
//
//	@Nullable
//	public KafmanBroker find(KafmanBrokerReference brokerReference) {
//		return findByKey(brokerReference.getBrokerKey());
//	}
//
//	@Nullable
//	public KafmanBroker findByKey(String brokerKey) {
//		return objectSearcher.search(SearchQuery.builder(KafmanBroker.class)
//				.predicate(Predicates.equalTo("brokerKey", brokerKey))
//				.build()).getOrNull();
//	}
//
//	public io.vavr.collection.List<KafmanBroker> get() {
//		return objectSearcher.search(SearchQuery.all(KafmanBroker.class));
//	}
//}