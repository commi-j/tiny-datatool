package tk.labyrinth.satool.kafman.query;

public enum QueryElementType {
	JUNCTION_OPERATOR,
	PROPERTY_OPERATOR,
	PROPERTY,
	VALUE,
}
