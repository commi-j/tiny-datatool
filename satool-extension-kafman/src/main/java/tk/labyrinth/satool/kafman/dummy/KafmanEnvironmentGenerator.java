package tk.labyrinth.satool.kafman.dummy;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import jakarta.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;
import tk.labyrinth.satool.kafman.broker.KafmanBroker;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerRegistry;
import tk.labyrinth.satool.kafman.connector.KafmanConnector;
import tk.labyrinth.satool.kafman.connector.KafmanConnectorRegistry;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.IntStream;

@RequiredArgsConstructor
@Service
@Slf4j
public class KafmanEnvironmentGenerator implements ApplicationRunner {

	private final KafmanConnectorRegistry brokerConnectorRegistry;

	private final KafmanBrokerRegistry brokerRegistry;

	private List<KafkaContainer> containers = List.empty();

	private void addPredefined() {
		KafmanBroker broker = KafmanBroker.builder()
				.bootstrapServers("localhost:9092")
				.key("localhost_9092")
				.name("localhost:9092")
				.build();
		brokerRegistry.add(broker);
	}

	@PreDestroy
	private void preDestroy() {
		containers.forEach(KafkaContainer::stop);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
//		addPredefined();
		//
		int brokers = 0;
		int topicsPerBroker = 10;
		int maxPartitionsPerTopic = 5;
		int recordsPerTopic = 10;
		//
		for (int i = 0; i < brokers; i++) {
			KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:latest"));
			containers = containers.append(kafkaContainer);
			//
			kafkaContainer.start();
			//
			KafmanBroker broker = KafmanBroker.builder()
					.bootstrapServers(kafkaContainer.getBootstrapServers())
					.key("broker" + Math.round(Math.random() * 1000))
					.name("Broker | " + kafkaContainer.getBootstrapServers())
					.build();
			brokerRegistry.add(broker);
			//
			KafmanConnector connector = brokerConnectorRegistry.get(KafmanBrokerReference.from(broker));
			//
			CreateTopicsResult createTopicsResult = connector.getAdminClient().createTopics(Stream.range(0, topicsPerBroker)
					.map(index -> new NewTopic(
							UUID.randomUUID().toString().substring(0, 8),
							Optional.of(((int) (Math.random() * maxPartitionsPerTopic)) + 1),
							Optional.empty()))
					.toJavaList());
			createTopicsResult.all().get();
			Set<String> topicNames = createTopicsResult.values().keySet();
			{
				//
				Producer<String, String> producer = connector.getProducer();
				topicNames.forEach(topicName ->
						IntStream.range(0, recordsPerTopic).forEach(index ->
								producer.send(new ProducerRecord<>(topicName, "value"))));
			}
		}
	}
}
