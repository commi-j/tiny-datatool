package tk.labyrinth.satool.kafman.configuration;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Value
@ConfigurationProperties(prefix = "kafman")
public class KafmanProperties {
	//
}
