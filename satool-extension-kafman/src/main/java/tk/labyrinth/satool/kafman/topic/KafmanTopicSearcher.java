package tk.labyrinth.satool.kafman.topic;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import tk.labyrinth.expresso.lang.predicate.BinaryPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.satool.kafman.base.ChildObjectSearcherBase;
import tk.labyrinth.satool.kafman.base.ObjectItem;
import tk.labyrinth.satool.kafman.broker.KafmanBroker;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;
import tk.labyrinth.satool.kafman.connector.KafmanConnectorRegistry;

import java.util.Objects;

@Lazy
@RequiredArgsConstructor
@Service
public class KafmanTopicSearcher extends ChildObjectSearcherBase<KafmanTopic, KafmanBroker> {

	private final KafmanConnectorRegistry connectorRegistry;

	@Override
	@SuppressWarnings("unchecked")
	protected ParameterizedQuery<Class<KafmanTopic>> adjustSearchQuery(
			ParameterizedQuery<Class<KafmanTopic>> searchQuery) {
		ParameterizedQuery<Class<KafmanTopic>> result;
		{
			Predicate predicate = searchQuery.getPredicate();
			if (predicate != null) {
				if (predicate instanceof BinaryPredicate) {
					BinaryPredicate<String, Object> binaryPredicate = (BinaryPredicate<String, Object>) predicate;
					String attributeName = binaryPredicate.first();
					//
//					if (Objects.equals(attributeName, KafmanConstants.THIS_ATTRIBUTE_NAME)) {
//						// FIXME: Support proper resolving this as reference to self.
//						result = SearchQuery.builder(searchQuery.getType())
//								.predicate(binaryPredicate.withFirst("name"))
//								.build();
//					} else
					if (Objects.equals(attributeName, "broker")) {
						result = ParameterizedQuery.builderWithClassParameter(searchQuery.getParameter())
								.predicate(binaryPredicate
										.withFirst("brokerReference")
										.withSecond(KafmanBrokerReference.of(binaryPredicate.second().toString())))
								.build();
					} else {
						result = searchQuery;
					}
				} else {
					result = searchQuery;
				}
			} else {
				result = searchQuery;
			}
		}
		return result;
	}

	@Override
	protected Stream<KafmanTopic> fromParentObjects(List<KafmanBroker> brokers) {
		return brokers.toStream().flatMap(broker ->
				connectorRegistry.get(KafmanBrokerReference.from(broker)).getAllTopics());
	}

	@Override
	protected Class<KafmanBroker> getParentType() {
		return KafmanBroker.class;
	}

	@Override
	protected ObjectItem<KafmanTopic> render(KafmanTopic object) {
		return ObjectItem.of(KafmanTopicReference.from(object), KafmanTopicReference.render(object));
	}
}
