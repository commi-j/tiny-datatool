package tk.labyrinth.satool.kafman.model;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.satool.kafman.base.KafmanConstants;
import tk.labyrinth.satool.kafman.base.ObjectItem;
import tk.labyrinth.satool.kafman.base.ObjectItemSearcher;
import tk.labyrinth.satool.kafman.datatype.AttributeMap;
import tk.labyrinth.satool.kafman.datatype.DatatypeRegistry;
import tk.labyrinth.satool.kafman.modelling.DatatypeQueryResolver;
import tk.labyrinth.satool.kafman.query.CompleteAttributeChain;
import tk.labyrinth.satool.kafman.query.IncompleteAttributeChain;
import tk.labyrinth.satool.kafman.query.QueryElement;
import tk.labyrinth.satool.kafman.query.QueryObject;
import tk.labyrinth.satool.kafman.query.QueryParser;
import tk.labyrinth.satool.kafman.query.token.BinaryExpressionToken;
import tk.labyrinth.satool.kafman.query.token.EmptyToken;
import tk.labyrinth.satool.kafman.query.token.JunctionToken;
import tk.labyrinth.satool.kafman.query.token.Token;
import tk.labyrinth.satool.kafman.query.token.TokenUtils;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class KafmanDatatypeQueryResolver implements DatatypeQueryResolver {

	private final AttributeMap attributeMap;

	private final DatatypeRegistry datatypeRegistry;
//	private final ObjectSearcherRegistry objectSearcherRegistry; // FIXME

	private final QueryParser queryParser;

	private <T> List<String> searchValues(Class<T> type, String valueEntry) {
		List<String> result;
		{
			ObjectItemSearcher<ParameterizedQuery<Class<T>>, T> objectSearcher = null;
//					objectSearcherRegistry.getObjectItemSearcher(type); // FIXME
			if (objectSearcher != null) {
				List<ObjectItem<T>> values = objectSearcher.searchForObjectItems(ParameterizedQuery
						.builderWithClassParameter(type)
						.predicate(Predicates.contains(
								KafmanConstants.REFERENCE_ATTRIBUTE_NAME,
								valueEntry,
								false))
						.build());
				result = values.map(ObjectItem::getText);
			} else {
				result = List.of();
			}
		}
		return result;
	}

	@Override
	public List<String> getAttributeOperators(Class<?> rootType, String attributeNameChainString) {
		List<String> result;
		{
			CompleteAttributeChain completeChain = queryParser.parseCompleteAttributeChain(
					rootType,
					attributeNameChainString);
			Class<?> type = completeChain.getEndType();
			if (type == String.class) {
				result = List.of("==", "!=", "IN", "NOT_IN", "CONTAINS", "STARTS_WITH", "ENDS_WITH");
			} else {
				result = List.of("==", "!=", "IN", "NOT_IN");
			}
		}
		return result;
	}

	@Override
	public List<String> getAttributes(Class<?> rootOwnerType, String attributeNameChainEntry) {
		IncompleteAttributeChain incompleteChain = queryParser.parseIncompleteAttributeChain(
				rootOwnerType,
				attributeNameChainEntry);
		return attributeMap.getAttributes(incompleteChain.getOwnerType()).toStream()
				.filter(attribute -> attribute.getName().toLowerCase().contains(incompleteChain.getIncompleteText()))
				.map(attribute -> incompleteChain.getPrefix() + attribute.getName())
				.toList();
	}

	@Override
	public List<String> getValues(Class<?> rootType, String attributeNameChainString, String valueEntry) {
		List<String> result;
		{
			CompleteAttributeChain completeChain = queryParser.parseCompleteAttributeChain(
					rootType,
					attributeNameChainString);
			Class<?> type = completeChain.getEndType();
			if (datatypeRegistry.isSimpleType(type)) {
				result = List.of();
			} else {
				result = searchValues(type, valueEntry);
			}
		}
		return result;
	}

	public <T> ParameterizedQuery<Class<T>> resolve(Class<T> rootType, QueryObject queryObject) {
		Token token = TokenUtils.resolve(queryObject.getElements().map(QueryElement::getValue));
		return ParameterizedQuery.builderWithClassParameter(rootType)
				.predicate(resolvePredicate(rootType, token))
				.build();
	}

	@Nullable
	public <T> Predicate resolvePredicate(Class<T> type, Token token) {
		Predicate result;
		if (token instanceof BinaryExpressionToken) {
			BinaryExpressionToken binaryExpressionToken = (BinaryExpressionToken) token;
			BiFunction<String, Object, Predicate> predicate = switch (binaryExpressionToken.getOperator().toString()) {
				case "==" -> Predicates::equalTo;
				case "!=" -> Predicates::notEqualTo;
				case "CONTAINS" -> (property, value) -> Predicates.contains(property, (String) value, false);
				default -> throw new NotImplementedException(token.toString());
			};
			String attributeName = binaryExpressionToken.getAttribute().toString();
			CompleteAttributeChain completeAttributeChain = queryParser.parseCompleteAttributeChain(
					type,
					attributeName);
			Class<?> attributeType = completeAttributeChain.getEndType();
			String valueString = binaryExpressionToken.getValue().toString();
			Object value;
			if (datatypeRegistry.isSimpleType(attributeType)) {
				value = datatypeRegistry.getSimpleParser(attributeType).apply(valueString);
			} else {
				// TODO: Check if referenceable type.
				//
				Function<String, ? extends Reference<?>> referenceParser = datatypeRegistry.findReferenceParser(
						attributeType);
				if (referenceParser != null) {
					value = referenceParser.apply(valueString);
				} else {
					throw new NotImplementedException(token.toString());
				}
			}
			result = predicate.apply(attributeName, value);
		} else if (token instanceof EmptyToken) {
			result = null;
		} else if (token instanceof JunctionToken) {
			JunctionToken junctionToken = (JunctionToken) token;
			if (Objects.equals(junctionToken.getOperator(), "AND")) {
				result = Predicates.and(junctionToken.getTokens().toJavaStream()
						.map(innerToken -> resolvePredicate(type, innerToken)));
			} else if (Objects.equals(junctionToken.getOperator(), "OR")) {
				result = Predicates.or(junctionToken.getTokens().toJavaStream()
						.map(innerToken -> resolvePredicate(type, innerToken)));
			} else {
				throw new NotImplementedException(token.toString());
			}
		} else {
			throw new NotImplementedException(token.toString());
		}
		return result;
	}
}
