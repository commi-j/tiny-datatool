package tk.labyrinth.satool.kafman.partition;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.base.KafmanConstants;
import tk.labyrinth.satool.kafman.base.ListViewBase;
import tk.labyrinth.satool.kafman.base.ObjectView;
import tk.labyrinth.satool.kafman.record.KafmanRecord;
import tk.labyrinth.satool.kafman.root.KafmanRootParentLayout;

import jakarta.annotation.PostConstruct;

@RequiredArgsConstructor
@Route(value = "partition", layout = KafmanRootParentLayout.class)
public class KafmanPartitionPage extends CssVerticalLayout implements HasUrlParameter<String> {

	private final KafmanPartitionSearcher partitionSearcher;

	private final ObjectView<KafmanPartition> partitionView;

	private final ListViewBase<KafmanRecord> recordListView;

	private void initializeFound(KafmanPartition partition) {
		{
			// Configuring UI
			//
			{
				setAlignItems(AlignItems.STRETCH);
			}
			{
				add(partitionView.asComponent());
			}
			{
				add(recordListView);
			}
		}
		{
			// Configuring logic
			//
			partitionView.setValue(partition);
			recordListView.initialize(() -> ParameterizedQuery.builderWithClassParameter(KafmanRecord.class)
					.predicate(Predicates.equalTo("partition", KafmanPartitionReference.from(partition)))
					.build());
		}
	}

	private void initializeNotFound(String referenceString) {
		add(new Label("Not found: " + referenceString));
	}

	@PostConstruct
	private void postConstruct() {
		addClassName(PandoraStyles.LAYOUT);
	}

	@Override
	public void setParameter(BeforeEvent event, String parameter) {
		KafmanPartition partition = partitionSearcher.search(ParameterizedQuery
						.builderWithClassParameter(KafmanPartition.class)
						.predicate(Predicates.equalTo(
								KafmanConstants.REFERENCE_ATTRIBUTE_NAME,
								KafmanPartitionReference.from(parameter)))
						.build())
				.getOrNull();
		if (partition != null) {
			initializeFound(partition);
		} else {
			initializeNotFound(parameter);
		}
	}
}
