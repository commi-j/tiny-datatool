package tk.labyrinth.satool.kafman.broker;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.base.ListViewCardBase;
import tk.labyrinth.satool.kafman.base.ObjectViewCardBase;
import tk.labyrinth.satool.kafman.root.KafmanRootParentLayout;
import tk.labyrinth.satool.kafman.topic.KafmanTopic;

import jakarta.annotation.PostConstruct;

@RequiredArgsConstructor
@Route(value = "broker", layout = KafmanRootParentLayout.class)
public class KafmanBrokerPage extends CssVerticalLayout implements HasUrlParameter<String> {

	private final ObjectViewCardBase<KafmanBroker> brokerCard;

	private final KafmanBrokerRegistry brokerRegistry;

	private final KafmanBrokerSearcher brokerSearcher;

	private final ListViewCardBase<KafmanTopic> topicListCard;

	private void initializeFound(KafmanBroker broker) {
		{
			// Configuring UI
			//
			{
				setAlignItems(AlignItems.STRETCH);
			}
			{
				add(brokerCard);
			}
			{
				add(topicListCard);
			}
		}
		{
			// Configuring logic
			//
			{
				brokerCard.setSummaryText("Broker");
				brokerCard.setValue(broker);
			}
			{
				topicListCard.setSummaryText("Topics");
				topicListCard.getView().initialize(() -> ParameterizedQuery.builderWithClassParameter(KafmanTopic.class)
						.predicate(Predicates.equalTo("broker", KafmanBrokerReference.from(broker)))
						.build());
			}
		}
	}

	private void initializeNotFound(String referenceString) {
		add(new Label("Not found: " + referenceString));
	}

	@PostConstruct
	private void postConstruct() {
		addClassName(PandoraStyles.LAYOUT);
	}

	@Override
	public void setParameter(BeforeEvent event, String parameter) {
		KafmanBroker broker = brokerRegistry.findByKey(parameter);
//		KafmanBroker broker = brokerSearcher.searchForObjects(SearchQuery.builder(KafmanBroker.class)
//				.predicate(Predicates.equalTo(
//						KafmanConstants.REFERENCE_ATTRIBUTE_NAME,
//						KafmanBrokerReference.from(parameter)))
//				.build())
//				.getOrNull();
		if (broker != null) {
			initializeFound(broker);
		} else {
			initializeNotFound(parameter);
		}
	}
}
