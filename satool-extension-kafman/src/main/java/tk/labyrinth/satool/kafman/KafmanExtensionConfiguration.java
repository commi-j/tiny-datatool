package tk.labyrinth.satool.kafman;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.labyrinth.pandora.storage.mongodb.EnablePandoraStorageMongoDb;

import jakarta.annotation.PostConstruct;

@EnablePandoraStorageMongoDb
@Slf4j
@SpringBootApplication
public class KafmanExtensionConfiguration {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
