package tk.labyrinth.satool.kafman.topic;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.base.KafmanConstants;
import tk.labyrinth.satool.kafman.base.ListViewBase;
import tk.labyrinth.satool.kafman.base.ObjectView;
import tk.labyrinth.satool.kafman.partition.KafmanPartition;
import tk.labyrinth.satool.kafman.root.KafmanRootParentLayout;

import jakarta.annotation.PostConstruct;

@RequiredArgsConstructor
@Route(value = "topic", layout = KafmanRootParentLayout.class)
public class KafmanTopicPage extends CssVerticalLayout implements HasUrlParameter<String> {

	private final ListViewBase<KafmanPartition> partitionListView;

	private final KafmanTopicSearcher topicSearcher;

	private final ObjectView<KafmanTopic> topicView;

	private void initializeFound(KafmanTopic topic) {
		{
			// Configuring UI
			//
			{
				setAlignItems(AlignItems.STRETCH);
			}
			{
				add(topicView.asComponent());
			}
			{
				add(partitionListView);
			}
		}
		{
			// Configuring logic
			//
			topicView.setValue(topic);
			partitionListView.initialize(() -> ParameterizedQuery.builderWithClassParameter(KafmanPartition.class)
					.predicate(Predicates.equalTo("topic", KafmanTopicReference.from(topic)))
					.build());
		}
	}

	private void initializeNotFound(String referenceString) {
		add(new Label("Not found: " + referenceString));
	}

	@PostConstruct
	private void postConstruct() {
		addClassName(PandoraStyles.LAYOUT);
	}

	@Override
	public void setParameter(BeforeEvent event, String parameter) {
		KafmanTopic topic = topicSearcher.search(ParameterizedQuery.builderWithClassParameter(KafmanTopic.class)
						.predicate(Predicates.equalTo(
								KafmanConstants.REFERENCE_ATTRIBUTE_NAME,
								KafmanTopicReference.from(parameter)))
						.build())
				.getOrNull();
		if (topic != null) {
			initializeFound(topic);
		} else {
			initializeNotFound(parameter);
		}
	}
}
