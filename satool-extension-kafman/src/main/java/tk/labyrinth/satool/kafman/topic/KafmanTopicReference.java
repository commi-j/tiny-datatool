package tk.labyrinth.satool.kafman.topic;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;

import javax.annotation.Nullable;

@Value
public class KafmanTopicReference implements Reference<KafmanTopic> {

	KafmanBrokerReference brokerReference;

	String topicName;

	@Override
	public String toString() {
		return render(brokerReference, topicName);
	}

	public static KafmanTopicReference from(KafmanTopic topic) {
		return of(topic.getBrokerReference(), topic.getName());
	}

	public static KafmanTopicReference from(String value) {
		KafmanTopicReference result = parse(value);
		if (result == null) {
			throw new IllegalArgumentException("Require well-formed: " + value);
		}
		return result;
	}

	public static KafmanTopicReference of(KafmanBrokerReference brokerReference, String topicName) {
		return new KafmanTopicReference(brokerReference, topicName);
	}

	public static KafmanTopicReference of(String brokerKey, String topicName) {
		return of(KafmanBrokerReference.of(brokerKey), topicName);
	}

	@Nullable
	public static KafmanTopicReference parse(String value) {
		KafmanTopicReference result;
		{
			int indexOfColon = value.indexOf(':');
			if (indexOfColon != -1) {
				String brokerKey = value.substring(0, indexOfColon);
				String topicName = value.substring(indexOfColon + 1);
				result = of(brokerKey, topicName);
			} else {
				result = null;
			}
		}
		return result;
	}

	public static String render(KafmanBrokerReference brokerReference, String topicName) {
		return brokerReference.toString() + ":" + topicName;
	}

	public static String render(KafmanTopic topic) {
		return render(topic.getBrokerReference(), topic.getName());
	}

	public static String render(String brokerKey, String topicName) {
		return brokerKey + ":" + topicName;
	}
}
