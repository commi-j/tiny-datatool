package tk.labyrinth.satool.kafman.record;

import lombok.Builder;
import lombok.Value;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import tk.labyrinth.satool.kafman.partition.KafmanPartitionReference;

import java.time.Instant;

@Builder(toBuilder = true)
@Value
public class KafmanRecord {

	Long offset;

	KafmanPartitionReference partitionReference;

	Instant timestamp;

	/**
	 * {@link ConsumerRecord#timestampType()}
	 */
	String timestampType;
}
