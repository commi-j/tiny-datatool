package tk.labyrinth.satool.kafman.record;

import com.vaadin.flow.component.grid.Grid;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.ListViewBase;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanRecordListView extends ListViewBase<KafmanRecord> {

	@Override
	protected void configureGrid(Grid<KafmanRecord> grid) {
		grid
				.addColumn(KafmanRecord::getPartitionReference)
				.setHeader("Partition");
		grid
				.addColumn(KafmanRecord::getOffset)
				.setHeader("Offset");
		grid
				.addColumn(KafmanRecord::getTimestamp)
				.setHeader("Timestamp");
		grid
				.addColumn(KafmanRecord::getTimestampType)
				.setHeader("Timestamp Type");
	}
}
