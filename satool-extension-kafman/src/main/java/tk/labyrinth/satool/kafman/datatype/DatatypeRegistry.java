package tk.labyrinth.satool.kafman.datatype;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

@Component
public class DatatypeRegistry {

	private final AtomicReference<Map<Class<?>, Function<?, ? extends Reference<?>>>> referenceBuildersReference =
			new AtomicReference<>(HashMap.empty());

	private final AtomicReference<Map<Class<?>, Function<String, ? extends Reference<?>>>> referenceParsersReference =
			new AtomicReference<>(HashMap.empty());

	@Nullable
	@SuppressWarnings("unchecked")
	public <T, R extends Reference<T>> Function<T, R> findReferenceBuilder(Class<T> type) {
		return (Function<T, R>) referenceBuildersReference.get().get(type).get();
	}

	@Nullable
	@SuppressWarnings("unchecked")
	public <T> Function<String, ? extends Reference<T>> findReferenceParser(Class<T> type) {
		return (Function<String, ? extends Reference<T>>) referenceParsersReference.get().get(type).get();
	}

	@SuppressWarnings("unchecked")
	public <T> Function<String, T> getSimpleParser(Class<T> type) {
		Function<String, ?> result = switch (type.getName()) {
			case "boolean", "java.lang.Boolean" -> Boolean::parseBoolean;
			case "int", "java.lang.Integer" -> Integer::parseInt;
			case "long", "java.lang.Long" -> Long::parseLong;
			case "java.lang.String" -> Function.identity();
			default -> throw new NotImplementedException();
		};
		return (Function<String, T>) result;
	}

	public boolean isSimpleType(Class<?> type) {
		return List
				.of(
						(Type) Boolean.class,
						Integer.class,
						Long.class,
						String.class)
				.contains(type);
	}

	public <T, R extends Reference<T>> void registerReference(
			Class<R> referenceType,
			Function<T, R> referenceBuilder,
			Function<String, R> referenceParser) {
		Class<?> objectType = TypeAware.getRawParameterClass(referenceType);
		referenceBuildersReference.updateAndGet(referenceBuilders ->
				referenceBuilders.put(objectType, referenceBuilder));
		referenceParsersReference.updateAndGet(referenceParsers -> referenceParsers.put(objectType, referenceParser));
	}
}
