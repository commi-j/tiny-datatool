package tk.labyrinth.satool.kafman.partition;

import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.satool.kafman.base.ObjectListPageBase;
import tk.labyrinth.satool.kafman.root.KafmanRootParentLayout;

@RequiredArgsConstructor
@Route(value = "partitions", layout = KafmanRootParentLayout.class)
public class KafmanPartitionListPage extends ObjectListPageBase<KafmanPartition> {
	// empty
}
