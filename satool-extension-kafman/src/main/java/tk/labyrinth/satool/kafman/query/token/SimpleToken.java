package tk.labyrinth.satool.kafman.query.token;

import lombok.Value;

@Value(staticConstructor = "of")
public class SimpleToken implements Token {

	String value;

	@Override
	public boolean isSimple() {
		return true;
	}

	@Override
	public String toString() {
		return value;
	}
}
