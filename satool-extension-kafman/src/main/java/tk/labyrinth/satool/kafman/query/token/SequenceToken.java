package tk.labyrinth.satool.kafman.query.token;

import io.vavr.collection.List;
import lombok.Value;

@Value
public class SequenceToken implements Token {

	List<Token> tokens;

	/**
	 * If this Token contains any {@link SequenceToken}s, they are destructured to be part of current token.
	 * Flattening is applied recursively.
	 *
	 * @return non-null
	 */
	public SequenceToken flatten() {
		return new SequenceToken(tokens.flatMap(token -> {
			List<Token> result;
			if (token instanceof SequenceToken) {
				result = ((SequenceToken) token).flatten().getTokens();
			} else {
				result = List.of(token);
			}
			return result;
		}));
	}

	@Override
	public boolean isSimple() {
		return false;
	}

	@Override
	public String toString() {
		return tokens.mkString(" ");
	}
}
