package tk.labyrinth.satool.kafman.broker;

import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.ListViewCardBase;

@PrototypeScopedComponent
public class KafmanBrokerListCard extends ListViewCardBase<KafmanBroker> {
	// empty
}
