package tk.labyrinth.satool.kafman.connector;

import io.vavr.Tuple;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.KafmanKafkaConsumerAccessor;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.broker.KafmanBroker;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;
import tk.labyrinth.satool.kafman.partition.KafmanPartition;
import tk.labyrinth.satool.kafman.partition.KafmanPartitionReference;
import tk.labyrinth.satool.kafman.record.KafmanRecord;
import tk.labyrinth.satool.kafman.topic.KafmanTopic;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;

@PrototypeScopedComponent
public class KafmanConnector {

	@Getter
	private final AdminClient adminClient;

	@Getter
	private final KafmanBroker broker;

	@Getter
	private final KafkaConsumer<?, ?> consumer;

	@Getter
	private final KafkaProducer<String, String> producer;

	public KafmanConnector(KafmanBroker broker) {
		this.broker = broker;
		adminClient = AdminClient.create(Map.of("bootstrap.servers", broker.getBootstrapServers()));
		consumer = new KafkaConsumer<>(Map.of(
				"bootstrap.servers", broker.getBootstrapServers(),
				"key.deserializer", StringDeserializer.class.getName(),
				"value.deserializer", StringDeserializer.class.getName()));
		producer = new KafkaProducer<>(Map.of(
				"bootstrap.servers", broker.getBootstrapServers(),
				"key.serializer", StringSerializer.class.getName(),
				"value.serializer", StringSerializer.class.getName()));
	}

	private <T> T doWithNewConsumer(Function<KafkaConsumer<?, ?>, T> consumerFunction) {
		KafkaConsumer<?, ?> consumer = KafmanKafkaConsumerAccessor.createLoglessConsumer(Map.of(
				"bootstrap.servers", broker.getBootstrapServers(),
				"key.deserializer", StringDeserializer.class.getName(),
				"value.deserializer", StringDeserializer.class.getName()));
		try {
			return consumerFunction.apply(consumer);
		} finally {
			consumer.close();
		}
	}

	private List<Pair<KafmanPartitionReference, Long>> queryBeginningOffsets(
			List<KafmanPartitionReference> partitionReferences) {
		Map<KafmanPartitionReference, Long> beginningOffsets = consumer
				.beginningOffsets(partitionReferences.toStream()
						.map(partitionReference -> new TopicPartition(
								partitionReference.getTopicReference().getTopicName(),
								partitionReference.getPartitionIndex()))
						.toJavaList())
				.entrySet()
				.stream()
				.collect(Collectors.toMap(
						entry -> KafmanPartitionReference.of(
								broker.getKey(),
								entry.getKey().topic(),
								entry.getKey().partition()),
						Map.Entry::getValue));
		return partitionReferences.map(partitionReference ->
				Pair.of(partitionReference, beginningOffsets.get(partitionReference)));
	}

	private List<Pair<KafmanPartitionReference, Long>> queryEndOffsets(
			List<KafmanPartitionReference> partitionReferences) {
		Map<KafmanPartitionReference, Long> beginningOffsets = consumer
				.endOffsets(partitionReferences.map(KafmanConnector::topicPartition).asJava())
				.entrySet()
				.stream()
				.collect(Collectors.toMap(
						entry -> KafmanPartitionReference.of(
								broker.getKey(),
								entry.getKey().topic(),
								entry.getKey().partition()),
						Map.Entry::getValue));
		return partitionReferences.map(partitionReference ->
				Pair.of(partitionReference, beginningOffsets.get(partitionReference)));
	}

	public List<KafmanTopic> getAllTopics() {
		try {
			return getTopics(List.ofAll(adminClient
					.listTopics(new ListTopicsOptions().listInternal(true))
					.names()
					.get()));
		} catch (ExecutionException | InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}

	public List<KafmanPartition> getPartitions(List<KafmanTopic> topics) {
		List<KafmanPartitionReference> partitionReferences = topics
				.flatMap(topic -> Stream.range(0, topic.getPartitionCount())
						.map(index -> KafmanPartitionReference.of(broker.getKey(), topic.getName(), index))
						.toList());
		//
		Map<KafmanPartitionReference, Long> beginningOffsets = queryBeginningOffsets(partitionReferences)
				.toJavaMap(pair -> Tuple.of(pair.getLeft(), pair.getRight()));
		Map<KafmanPartitionReference, Long> endOffsets = queryEndOffsets(partitionReferences)
				.toJavaMap(pair -> Tuple.of(pair.getLeft(), pair.getRight()));
		//
		return partitionReferences.map(partitionReference -> KafmanPartition.builder()
				.index(partitionReference.getPartitionIndex())
				.latestOffset(beginningOffsets.get(partitionReference))
				.newestOffset(endOffsets.get(partitionReference))
				.topicReference(partitionReference.getTopicReference())
				.build());
	}

	;

	public KafmanTopic getTopic(String topicName) {
		return getTopics(List.of(topicName)).single();
	}

	public List<KafmanTopic> getTopics(List<String> topicNames) {
		try {
			return adminClient.describeTopics(topicNames.asJava())
					.all()
					.get()
					.values()
					.stream()
					.map(topicDescription -> KafmanTopic.builder()
							.brokerReference(KafmanBrokerReference.from(broker))
							.id(new UUID(
									topicDescription.topicId().getMostSignificantBits(),
									topicDescription.topicId().getLeastSignificantBits()))
							.name(topicDescription.name())
							.partitionCount(topicDescription.partitions().size())
							.build())
					.collect(List.collector());
		} catch (ExecutionException | InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}

	public List<KafmanRecord> searchRecords(KafmanPartition partition) {
		return searchRecords(List.of(partition));
	}

	public List<KafmanRecord> searchRecords(List<KafmanPartition> partitions) {
		return doWithNewConsumer(consumer -> {
			// TODO: Index partitions.
			//  RecordTimestampCache.ensureCached(partitions,consumer);
			//  ^
			//  USES binarySearch
			//
			List<Pair<KafmanPartition, TopicPartition>> partitionPairs = partitions.map(partition ->
					Pair.of(partition, topicPartition(partition)));
			consumer.assign(partitionPairs.map(Pair::getRight).asJava());
			partitionPairs.forEach(pair -> consumer.seek(pair.getRight(), pair.getLeft().getLatestOffset()));
			//
			// TODO: Make initial state object.
			//
			// TODO: Make loop
			ConsumerRecords<?, ?> consumerRecords = consumer.poll(Duration.ofSeconds(1));
			// TODO: enrich state object.
			// TODO: send enriched object to flux.
			//
			return StreamUtils.from(consumerRecords.iterator())
					.map(consumerRecord -> KafmanRecord.builder()
							.offset(consumerRecord.offset())
							.partitionReference(KafmanPartitionReference.of(
									broker.getKey(),
									consumerRecord.topic(),
									consumerRecord.partition()))
							.timestamp(Instant.ofEpochMilli(consumerRecord.timestamp()))
							.timestampType(consumerRecord.timestampType().name())
							.build())
					.collect(List.collector());
		});
	}

	public static TopicPartition topicPartition(KafmanPartition partition) {
		return new TopicPartition(partition.getTopicReference().getTopicName(), partition.getIndex());
	}

	public static TopicPartition topicPartition(KafmanPartitionReference partitionReference) {
		return new TopicPartition(
				partitionReference.getTopicReference().getTopicName(),
				partitionReference.getPartitionIndex());
	}
}
