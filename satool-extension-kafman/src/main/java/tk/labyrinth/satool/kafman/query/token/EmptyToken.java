package tk.labyrinth.satool.kafman.query.token;

public class EmptyToken implements Token {

	@Override
	public boolean isSimple() {
		return false;
	}
}
