package tk.labyrinth.satool.kafman.model;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;
import tk.labyrinth.satool.kafman.base.KafmanConstants;
import tk.labyrinth.satool.kafman.broker.KafmanBroker;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;
import tk.labyrinth.satool.kafman.datatype.AttributeMap;
import tk.labyrinth.satool.kafman.datatype.DatatypeRegistry;
import tk.labyrinth.satool.kafman.partition.KafmanPartition;
import tk.labyrinth.satool.kafman.partition.KafmanPartitionReference;
import tk.labyrinth.satool.kafman.record.KafmanRecord;
import tk.labyrinth.satool.kafman.topic.KafmanTopic;
import tk.labyrinth.satool.kafman.topic.KafmanTopicReference;

@RequiredArgsConstructor
@Service
public class KafmanModelBuilder implements ApplicationRunner {

	private final AttributeMap attributeMap;

	private final DatatypeRegistry datatypeRegistry;

	@Override
	public void run(ApplicationArguments args) {
		{
			attributeMap.registerPropertyRelation(
					KafmanBroker.class,
					"key",
					String.class,
					KafmanBroker::getKey);
			attributeMap.registerPropertyRelation(
					KafmanBroker.class,
					"name",
					String.class,
					KafmanBroker::getName);
			datatypeRegistry.registerReference(
					KafmanBrokerReference.class,
					KafmanBrokerReference::from,
					KafmanBrokerReference::from);
		}
		{
			attributeMap.registerReferenceRelation(
					KafmanTopic.class,
					"broker",
					KafmanBroker.class,
					KafmanTopic::getBrokerReference);
			attributeMap.registerPropertyRelation(
					KafmanTopic.class,
					"name",
					String.class,
					KafmanTopic::getName);
			attributeMap.registerReferenceRelation(
					KafmanTopic.class,
					KafmanConstants.REFERENCE_ATTRIBUTE_NAME,
					KafmanTopic.class,
					null);
			datatypeRegistry.registerReference(
					KafmanTopicReference.class,
					KafmanTopicReference::from,
					KafmanTopicReference::from);
		}
		{
			attributeMap.registerReferenceRelation(
					KafmanPartition.class,
					"topic",
					KafmanTopic.class,
					KafmanPartition::getTopicReference);
			attributeMap.registerPropertyRelation(
					KafmanPartition.class,
					"index",
					Integer.class,
					KafmanPartition::getIndex);
			datatypeRegistry.registerReference(
					KafmanPartitionReference.class,
					KafmanPartitionReference::from,
					KafmanPartitionReference::from);
		}
		{
			attributeMap.registerPropertyRelation(
					KafmanRecord.class,
					"body",
					String.class,
					null);
//			attributeMap.registerRelation(KafmanRecord.class, "headers", List.class);
			attributeMap.registerPropertyRelation(
					KafmanRecord.class,
					"key",
					String.class,
					null);
			attributeMap.registerPropertyRelation(
					KafmanRecord.class,
					"offset",
					Long.class,
					null);
			attributeMap.registerReferenceRelation(
					KafmanRecord.class,
					"partition",
					KafmanPartition.class,
					null);
			attributeMap.registerReferenceRelation(
					KafmanPartition.class,
					KafmanConstants.REFERENCE_ATTRIBUTE_NAME,
					KafmanPartition.class,
					null);
		}
	}
}
