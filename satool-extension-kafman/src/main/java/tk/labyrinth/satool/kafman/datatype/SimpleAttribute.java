package tk.labyrinth.satool.kafman.datatype;

import lombok.Value;

@Value
public class SimpleAttribute<T> implements Attribute {

	String name;

	Class<T> type;
}
