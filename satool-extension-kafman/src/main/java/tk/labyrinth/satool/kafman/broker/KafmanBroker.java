package tk.labyrinth.satool.kafman.broker;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;

@Builder(toBuilder = true)
@Value
public class KafmanBroker {

	String bootstrapServers;

	/**
	 * Technical identifier with no whitespaced and colons. Should never change.
	 */
	@Id
	String key;

	/**
	 * User-friendly name.
	 */
	String name;
}
