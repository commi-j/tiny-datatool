package tk.labyrinth.satool.kafman.partition;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;
import tk.labyrinth.satool.kafman.topic.KafmanTopicReference;

@Value
public class KafmanPartitionReference implements Reference<KafmanPartition> {

	Integer partitionIndex;

	KafmanTopicReference topicReference;

	@Override
	public String toString() {
		return render(topicReference, partitionIndex);
	}

	public static KafmanPartitionReference from(KafmanPartition partition) {
		return of(partition.getTopicReference(), partition.getIndex());
	}

	public static KafmanPartitionReference from(String value) {
		String[] split = value.split(":");
		return of(split[0], split[1], Integer.parseInt(split[2]));
	}

	public static KafmanPartitionReference of(KafmanTopicReference topicReference, Integer partitionIndex) {
		return new KafmanPartitionReference(partitionIndex, topicReference);
	}

	public static KafmanPartitionReference of(String brokerKey, String topicName, Integer partitionIndex) {
		return new KafmanPartitionReference(partitionIndex, KafmanTopicReference.of(brokerKey, topicName));
	}

	public static String render(KafmanPartition partition) {
		return partition.getTopicReference() + ":" + partition.getIndex();
	}

	public static String render(KafmanBrokerReference brokerReference, String topicName, Integer partitionIndex) {
		return brokerReference + ":" + topicName + ":" + partitionIndex;
	}

	public static String render(KafmanTopicReference topicReference, Integer partitionIndex) {
		return topicReference + ":" + partitionIndex;
	}

	public static String render(String brokerKey, String topicName, Integer partitionIndex) {
		return brokerKey + ":" + topicName + ":" + partitionIndex;
	}
}
