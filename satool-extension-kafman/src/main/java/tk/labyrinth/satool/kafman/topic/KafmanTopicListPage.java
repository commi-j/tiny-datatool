package tk.labyrinth.satool.kafman.topic;

import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.satool.kafman.base.ObjectListPageBase;
import tk.labyrinth.satool.kafman.root.KafmanRootParentLayout;

@RequiredArgsConstructor
@Route(value = "topics", layout = KafmanRootParentLayout.class)
public class KafmanTopicListPage extends ObjectListPageBase<KafmanTopic> {
	// empty
}
