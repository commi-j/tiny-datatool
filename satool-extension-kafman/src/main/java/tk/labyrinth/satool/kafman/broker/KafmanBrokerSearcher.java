package tk.labyrinth.satool.kafman.broker;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcherBase;

@RequiredArgsConstructor
@Service
public class KafmanBrokerSearcher extends ObjectSearcherBase<ParameterizedQuery<Class<KafmanBroker>>, KafmanBroker> {

	private final KafmanBrokerRegistry brokerRegistry;

	@Override
	public long count(ParameterizedQuery<Class<KafmanBroker>> query) {
		return search(query).size();
	}

	@Override
	public <R extends Reference<KafmanBroker>> ResolvedReference<KafmanBroker, R> resolve(R reference) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public List<KafmanBroker> search(ParameterizedQuery<Class<KafmanBroker>> query) {
		return List.ofAll(brokerRegistry.get());
//		// TODO: Apply query filtering.
//		return brokerRegistry.get();
//		throw new NotImplementedException();
	}
}
