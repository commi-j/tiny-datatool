package tk.labyrinth.satool.kafman.partition;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.satool.kafman.topic.KafmanTopicReference;

@Builder(toBuilder = true)
@Value
public class KafmanPartition {

	Integer index;

	Long latestOffset;

	Long newestOffset;

	KafmanTopicReference topicReference;
}
