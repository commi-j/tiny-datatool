package tk.labyrinth.satool.kafman.connector;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Component;
import tk.labyrinth.satool.kafman.broker.KafmanBroker;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerReference;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerRegistry;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
@RequiredArgsConstructor
public class KafmanConnectorRegistry {

	private final KafmanBrokerRegistry brokerRegistry;

	private final ObjectProvider<KafmanConnector> connectorProvider;

	private final ConcurrentMap<KafmanBroker, KafmanConnector> connectors = new ConcurrentHashMap<>();

	@Deprecated
	private KafmanConnector get(KafmanBroker broker) {
		return connectors.computeIfAbsent(broker, connectorProvider::getObject);
	}

	public KafmanConnector get(KafmanBrokerReference brokerReference) {
		return get(brokerRegistry.find(brokerReference));
	}
}
