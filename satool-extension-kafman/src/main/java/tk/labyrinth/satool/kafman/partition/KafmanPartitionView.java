package tk.labyrinth.satool.kafman.partition;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.control.Option;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.base.ObjectViewBase;

@PrototypeScopedComponent
public class KafmanPartitionView extends ObjectViewBase<KafmanPartition> {

	private final TextField topicReferenceField = new TextField("Topic Reference");

	private final TextField indexField = new TextField("Index");

	@Override
	protected void configure(CssGridLayout layout) {
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				layout.add(topicReferenceField);
			}
			{
				layout.add(indexField);
			}
		}
		{
			{
				layout.getChildren()
						.filter(HasValue.class::isInstance)
						.map(HasValue.class::cast)
						.forEach(child -> child.setReadOnly(true));
			}
		}
	}

	@Override
	protected void setValue(Option<KafmanPartition> valueOption) {
		indexField.setValue(valueOption.map(KafmanPartition::getIndex).map(String::valueOf).getOrElse(""));
		topicReferenceField.setValue(valueOption.map(KafmanPartition::getTopicReference).map(String::valueOf).getOrElse(""));
	}
}
