package tk.labyrinth.satool.kafman.partition;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import tk.labyrinth.expresso.lang.predicate.BinaryPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.satool.kafman.base.ChildObjectSearcherBase;
import tk.labyrinth.satool.kafman.base.ObjectItem;
import tk.labyrinth.satool.kafman.connector.KafmanConnectorRegistry;
import tk.labyrinth.satool.kafman.topic.KafmanTopic;
import tk.labyrinth.satool.kafman.topic.KafmanTopicReference;

import java.util.Objects;

@Lazy
@RequiredArgsConstructor
@Service
public class KafmanPartitionSearcher extends ChildObjectSearcherBase<KafmanPartition, KafmanTopic> {

	private final KafmanConnectorRegistry connectorRegistry;

	@Override
	@SuppressWarnings("unchecked")
	protected ParameterizedQuery<Class<KafmanPartition>> adjustSearchQuery(
			ParameterizedQuery<Class<KafmanPartition>> searchQuery) {
		ParameterizedQuery<Class<KafmanPartition>> result;
		{
			Predicate predicate = searchQuery.getPredicate();
			if (predicate != null) {
				if (predicate instanceof BinaryPredicate) {
					BinaryPredicate<String, Object> binaryPredicate = (BinaryPredicate<String, Object>) predicate;
					String attributeName = binaryPredicate.first();
					if (Objects.equals(attributeName, "topic")) {
						result = ParameterizedQuery.builderWithClassParameter(searchQuery.getParameter())
								.predicate(binaryPredicate
										.withFirst("topicReference")
										.withSecond(KafmanTopicReference.from(binaryPredicate.second().toString())))
								.build();
					} else {
						result = searchQuery;
					}
				} else {
					result = searchQuery;
				}
			} else {
				result = searchQuery;
			}
		}
		return result;
	}

	@Override
	protected Stream<KafmanPartition> fromParentObjects(List<KafmanTopic> topics) {
		return topics
				.groupBy(KafmanTopic::getBrokerReference)
				.toStream()
				.flatMap(pair -> connectorRegistry.get(pair._1()).getPartitions(pair._2()));
	}

	@Override
	protected Class<KafmanTopic> getParentType() {
		return KafmanTopic.class;
	}

	@Override
	protected ObjectItem<KafmanPartition> render(KafmanPartition object) {
		return ObjectItem.of(
				KafmanPartitionReference.from(object),
				object.getTopicReference().getTopicName() + ":" + object.getIndex());
	}
}
