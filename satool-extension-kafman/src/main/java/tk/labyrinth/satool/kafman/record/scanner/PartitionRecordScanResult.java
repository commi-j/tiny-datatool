package tk.labyrinth.satool.kafman.record.scanner;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.satool.kafman.partition.KafmanPartitionReference;

@Builder(toBuilder = true)
@Value
public class PartitionRecordScanResult {

	Integer currentOffset;

	Integer endOffset;

	KafmanPartitionReference partitionReference;

	Integer startOffset;
}
