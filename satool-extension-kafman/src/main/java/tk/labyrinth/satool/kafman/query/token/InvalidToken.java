package tk.labyrinth.satool.kafman.query.token;

import io.vavr.collection.List;
import lombok.Value;

@Value
public class InvalidToken implements Token {

	List<Token> tokens;

	@Override
	public boolean isSimple() {
		return false;
	}
}
