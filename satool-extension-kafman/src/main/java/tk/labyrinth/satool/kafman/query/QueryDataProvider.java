package tk.labyrinth.satool.kafman.query;

import com.vaadin.flow.data.provider.AbstractDataProvider;
import com.vaadin.flow.data.provider.Query;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.satool.kafman.modelling.DatatypeQueryResolver;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class QueryDataProvider<T> extends AbstractDataProvider<QueryObject, String> {

	private final DatatypeQueryResolver datatypeQueryResolver;

	private final Class<T> type;

	@Override
	public Stream<QueryObject> fetch(Query<QueryObject, String> query) {
		{
			query.getOffset();
			query.getLimit();
		}
		String filter = query.getFilter().orElseThrow();
		//
		QueryObject currentObject = resolveQuery(filter);
		QueryElement lastElement = currentObject.getLastElement();
		QueryObject previousObject = currentObject.getPreviousObject();
		//
		List<String> suggestedValues = switch (lastElement.getType()) {
			case JUNCTION_OPERATOR -> List.of("AND", "OR");
			case PROPERTY -> datatypeQueryResolver.getAttributes(type, lastElement.getValue());
			case PROPERTY_OPERATOR -> datatypeQueryResolver.getAttributeOperators(
					type,
					previousObject.getLastElement().getValue());
			case VALUE -> datatypeQueryResolver.getValues(
					type,
					previousObject.getPreviousObject().getLastElement().getValue(),
					lastElement.getValue());
		};
		return suggestedValues
				.map(suggestedValue -> previousObject.append(lastElement.withValue(suggestedValue)))
				.toJavaStream();
	}

	@Override
	public boolean isInMemory() {
		return false;
	}

	@Override
	public int size(Query<QueryObject, String> query) {
		return (int) fetch(query).count();
	}

	public static QueryObject resolveQuery(String value) {
		List<QueryElement> elements = StreamUtils.mapWithPreviousOutput(
				Stream.of(value.split("\\s+", -1)),
				(QueryElement previousOutput, String currentInput) -> {
					QueryElement result;
					if (previousOutput == null) {
						result = new QueryElement(QueryElementType.PROPERTY, currentInput);
					} else {
						result = switch (previousOutput.getType()) {
							case JUNCTION_OPERATOR -> new QueryElement(QueryElementType.PROPERTY, currentInput);
							case PROPERTY -> new QueryElement(QueryElementType.PROPERTY_OPERATOR, currentInput);
							case PROPERTY_OPERATOR -> new QueryElement(QueryElementType.VALUE, currentInput);
							case VALUE -> new QueryElement(QueryElementType.JUNCTION_OPERATOR, currentInput);
						};
					}
					return result;
				})
				.collect(List.collector());
		//
		return QueryObject.builder()
				.elements(elements)
				.build();
	}
}
