package tk.labyrinth.satool.kafman.query;

import lombok.Value;
import lombok.With;

@Value
@With
public class QueryElement {

	QueryElementType type;

	String value;
}
