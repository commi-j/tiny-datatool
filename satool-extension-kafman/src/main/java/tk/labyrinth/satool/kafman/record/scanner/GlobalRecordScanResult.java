package tk.labyrinth.satool.kafman.record.scanner;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.satool.kafman.record.KafmanRecord;

@Builder(toBuilder = true)
@Value
public class GlobalRecordScanResult {

	List<PartitionRecordScanResult> partitionScans;

	List<KafmanRecord> records;

	String query;
}
