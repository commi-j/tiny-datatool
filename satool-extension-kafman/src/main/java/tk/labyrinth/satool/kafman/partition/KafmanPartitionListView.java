package tk.labyrinth.satool.kafman.partition;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.RouterLink;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.ListViewBase;
import tk.labyrinth.satool.kafman.broker.KafmanBrokerPage;
import tk.labyrinth.satool.kafman.topic.KafmanTopicPage;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanPartitionListView extends ListViewBase<KafmanPartition> {

	@Override
	protected void configureGrid(Grid<KafmanPartition> grid) {
		grid
				.addComponentColumn(item -> new RouterLink(
						item.getTopicReference().getBrokerReference().toString(),
						KafmanBrokerPage.class,
						item.getTopicReference().getBrokerReference().toString()))
				.setHeader("Broker");
		grid
				.addComponentColumn(item -> new RouterLink(
						item.getTopicReference().getTopicName(),
						KafmanTopicPage.class,
						item.getTopicReference().toString()))
				.setHeader("Topic");
		grid
				.addComponentColumn(item -> new RouterLink(
						item.getIndex().toString(),
						KafmanPartitionPage.class,
						KafmanPartitionReference.render(item)))
				.setHeader("Index");
		grid
				.addColumn(KafmanPartition::getLatestOffset)
				.setHeader("First Offset");
		grid
				.addColumn(KafmanPartition::getNewestOffset)
				.setHeader("Last Offset");
	}
}
