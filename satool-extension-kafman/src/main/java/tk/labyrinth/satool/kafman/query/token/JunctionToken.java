package tk.labyrinth.satool.kafman.query.token;

import io.vavr.collection.List;
import lombok.Value;

@Value(staticConstructor = "of")
public class JunctionToken implements Token {

	String operator;

	List<Token> tokens;

	@Override
	public boolean isSimple() {
		return false;
	}

	public static JunctionToken of(String operator, Token... tokens) {
		return of(operator, List.of(tokens));
	}
}
