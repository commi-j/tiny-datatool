package tk.labyrinth.satool.kafman.broker;

import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.satool.kafman.base.ObjectListPageBase;
import tk.labyrinth.satool.kafman.root.KafmanRootParentLayout;

@RequiredArgsConstructor
@Route(value = "brokers", layout = KafmanRootParentLayout.class)
public class KafmanBrokerListPage extends ObjectListPageBase<KafmanBroker> {
	// empty
}
