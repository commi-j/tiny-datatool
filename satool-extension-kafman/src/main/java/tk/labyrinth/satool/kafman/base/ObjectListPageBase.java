package tk.labyrinth.satool.kafman.base;

import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.QueryParameters;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.kafman.partition.KafmanPartition;
import tk.labyrinth.satool.kafman.topic.KafmanTopicReference;

import jakarta.annotation.PostConstruct;

public abstract class ObjectListPageBase<T> extends CssVerticalLayout implements BeforeEnterObserver {

	@Autowired
	private ListViewBase<T> listView;

	@Autowired
	private QueryBoxBase<T> queryBox;

	@PostConstruct
	private void postConstruct() {
		{
			// Configuring UI
			//
			{
				addClassName(PandoraStyles.LAYOUT);
			}
			{
				{
					add(queryBox);
				}
				{
					//
					CssFlexItem.setFlexGrow(listView, 1);
					add(listView);
				}
			}
		}
		{
			// Configuring logic
			//
			listView.initialize(queryBox::getValue);
			queryBox.setSearchCallback(searchQuery -> listView.refresh());
		}
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		QueryParameters queryParameters = event.getLocation().getQueryParameters();
		{
			List<String> topicStrings = Option.of(queryParameters.getParameters().get("topic"))
					.map(List::ofAll)
					.getOrNull();
			if (topicStrings != null) {
				String topicString = topicStrings.single();
				KafmanTopicReference topicReference = KafmanTopicReference.from(topicString);
				//
//				brokerBox.setValue(topicReference.getBrokerReference());
//				topicBox.setValue(topicReference);
			}
		}
		//
//		KafmanBroker broker = brokerRegistry.findByKey(topicReference.getBrokerKey());
//		KafmanConnector connector = connectorRegistry.get(broker);
//		//
//		KafmanTopic topic = connector.getTopic(topicReference.getTopicName());
//		listView.initialize(connector, List.of(topic));
	}
}
