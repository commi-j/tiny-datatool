package tk.labyrinth.satool.kafman.partition;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.satool.kafman.base.QueryBoxBase;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class KafmanPartitionQueryBox extends QueryBoxBase<KafmanPartition> {
	// empty
}
