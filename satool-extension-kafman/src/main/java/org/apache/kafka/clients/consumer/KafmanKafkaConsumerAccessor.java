package org.apache.kafka.clients.consumer;

import java.util.Map;

public class KafmanKafkaConsumerAccessor {

	public static <K, V> KafkaConsumer<K, V> createLoglessConsumer(Map<String, Object> configs) {
		return new KafkaConsumer<>(new KafmanConsumerConfig(configs), null, null);
	}
}
