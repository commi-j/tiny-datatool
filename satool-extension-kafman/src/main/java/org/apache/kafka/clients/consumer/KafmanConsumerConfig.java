package org.apache.kafka.clients.consumer;

import java.util.Map;

public class KafmanConsumerConfig extends ConsumerConfig {

	protected KafmanConsumerConfig(Map<?, ?> props) {
		super(props, false);
	}
}
