package tk.labyrinth.satool.kafman.query.token;

import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TokenUtilsTest {

	@Test
	void testResolve() {
		Assertions.assertEquals(
				JunctionToken.of(
						"AND",
						BinaryExpressionToken.of(
								SimpleToken.of("a"),
								SimpleToken.of("=="),
								SimpleToken.of("b")),
						BracesToken.of(
								JunctionToken.of(
										"OR",
										BinaryExpressionToken.of(
												SimpleToken.of("c"),
												SimpleToken.of("=="),
												SimpleToken.of("d")),
										BinaryExpressionToken.of(
												SimpleToken.of("e"),
												SimpleToken.of("=="),
												SimpleToken.of("f"))))),
				TokenUtils.resolve(List.of("a == b AND (c == d OR e == f)".split("\s"))));
	}

	@Test
	void testSeparateBraces() {
		Assertions.assertEquals(
				List.of("a", "==", "b", "AND", "(", "c", "==", "d", "OR", "e", "==", "f", ")"),
				TokenUtils.separateBraces(List.of("a == b AND (c == d OR e == f)".split("\s"))));
		Assertions.assertEquals(
				List.of("(", "a", "==", "b", "AND", "c", "==", "d", ")", "OR", "e", "==", "f"),
				TokenUtils.separateBraces(List.of("(a == b AND c == d) OR e == f".split("\s"))));
	}
}