package tk.labyrinth.satool.confluenceconnector.api;

import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(toBuilder = true)
@Value
public class ConfluenceConnectionParameters {

	@Nullable
	String authorization;

	String baseUrl;
}
