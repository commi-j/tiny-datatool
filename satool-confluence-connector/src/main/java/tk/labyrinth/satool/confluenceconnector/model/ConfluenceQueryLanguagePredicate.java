package tk.labyrinth.satool.confluenceconnector.model;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;

/**
 * <a href="https://developer.atlassian.com/server/confluence/cql-field-reference/">https://developer.atlassian.com/server/confluence/cql-field-reference/</a>
 */
@Builder(toBuilder = true)
@Value
public class ConfluenceQueryLanguagePredicate {

	@Nullable
	List<String> ancestors;

	@Nullable
	List<String> ids;

	@Nullable
	List<String> labels;

	/**
	 * Value is contentId.<br>
	 * Does not support IN.<br>
	 * <a href="https://developer.atlassian.com/server/confluence/cql-field-reference/#parent">https://developer.atlassian.com/server/confluence/cql-field-reference/#parent</a><br>
	 */
	@Nullable
	String parent;

	@Nullable
	List<String> spaceKeys;

	@Nullable
	List<String> titles;

	@Override
	public String toString() {
		return Stream
				.of(
						ancestors != null && !ancestors.isEmpty()
								? "ancestor" + (ancestors.size() > 1
								? " IN (%s)".formatted(ancestors.map("'%s'"::formatted).mkString(","))
								: " = '%s'".formatted(ancestors.get(0)))
								: null,
						ids != null && !ids.isEmpty()
								? "id" + (ids.size() > 1
								? " IN (%s)".formatted(ids.map("'%s'"::formatted).mkString(","))
								: " = '%s'".formatted(ids.get(0)))
								: null,
						labels != null && !labels.isEmpty()
								? "label" + (labels.size() > 1
								? " IN (%s)".formatted(labels.map("'%s'"::formatted).mkString(","))
								: " = '%s'".formatted(labels.get(0)))
								: null,
						parent != null
								? "parent = %s".formatted(parent)
								: null,
						spaceKeys != null && !spaceKeys.isEmpty()
								? "space" + (spaceKeys.size() > 1
								? " IN (%s)".formatted(spaceKeys.map("'%s'"::formatted).mkString(","))
								: " = '%s'".formatted(spaceKeys.get(0)))
								: null,
						titles != null && !titles.isEmpty()
								? "title" + (titles.size() > 1
								? " IN (%s)".formatted(titles.map("'%s'"::formatted).mkString(","))
								: " = '%s'".formatted(titles.get(0)))
								: null)
				.filter(Objects::nonNull)
				.mkString(" AND ");
	}
}
