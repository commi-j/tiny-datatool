package tk.labyrinth.satool.confluenceconnector.api.contentchildattachment;

import lombok.Value;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceVersion;

@Value
public class ConfluenceContentChildAttachment {

	ConfluenceContent container;

	String id;

	/**
	 * "current".
	 */
	String status;

	String title;

	/**
	 * "attachment".
	 */
	String type;

	ConfluenceVersion version;
}
