package tk.labyrinth.satool.confluenceconnector.api.contentchildattachment;

import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceLinks;

@Value
public class ConfluencePostContentChildAttachmentResult {

	ConfluenceLinks links;

	List<ConfluenceContentChildAttachment> results;

	Integer size;
}
