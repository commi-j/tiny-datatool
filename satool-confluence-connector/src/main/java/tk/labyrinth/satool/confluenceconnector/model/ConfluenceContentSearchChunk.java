package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vavr.collection.List;
import lombok.Value;

@Value
public class ConfluenceContentSearchChunk<T> {

	ObjectNode _links;

	String cqlQuery;

	int limit;

	List<T> results;

	/**
	 * Ms.
	 */
	int searchDuration;

	int size;

	int start;

	int totalSize;
}
