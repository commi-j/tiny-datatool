package tk.labyrinth.satool.confluenceconnector;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.dependencies.jackson.ObjectMapperConfiguration;

@Deprecated // Moved to SATool.
@EnableFeignClients
@Import({
		ObjectMapperConfiguration.class,
})
@SpringBootApplication
public class ConfluenceConnectorConfiguration {
	// empty
}
