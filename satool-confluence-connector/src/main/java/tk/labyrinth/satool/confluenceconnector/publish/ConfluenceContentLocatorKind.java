package tk.labyrinth.satool.confluenceconnector.publish;

import io.vavr.collection.Stream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Getter
@RequiredArgsConstructor
public enum ConfluenceContentLocatorKind {
	CHILD_TITLE(true, "childTitle"),
	CONTENT_ID(false, "contentId"),
	PAGE_ID(false, "pageId");

	@Accessors(fluent = true)
	private final boolean dependsOnParent;

	private final String prefix;

	public String getPayload(String locator) {
		if (detect(locator) != this) {
			throw new IllegalArgumentException("Incompatible");
		}
		//
		return locator.substring(prefix.length() + 1);
	}

	public static ConfluenceContentLocatorKind detect(String locator) {
		return Stream.of(values())
				.filter(value -> locator.startsWith("%s:".formatted(value.getPrefix())))
				.get();
	}
}
