package tk.labyrinth.satool.confluenceconnector.apiexplorer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import io.vavr.Tuple4;
import io.vavr.collection.List;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.TaskExecutor;
import reactor.core.publisher.Sinks;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;
import tk.labyrinth.pandora.functionalcomponents.box.IntegerBoxRenderer;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.TextAreaRenderer;
import tk.labyrinth.pandora.misc4j.integration.auth.AuthorizationUtils;
import tk.labyrinth.pandora.misc4j.lib.reactor.ReactorUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.simple.SecretBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.StringBox;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.ui.domain.state.model.TypedUiStateBinder;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClient;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClientFactory;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceGetContentQuery;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceBody;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceStorage;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceVersion;
import tk.labyrinth.satool.confluenceconnector.view.JsonGrid;

import javax.annotation.CheckForNull;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Route("confluence/apiexplorer")
@RequiredArgsConstructor
@Slf4j
public class ConfluenceApiExplorerPage extends CssVerticalLayout {

	private final ConfluenceFeignClientFactory clientFactory;

	private final JsonGrid contentGrid = new JsonGrid();

	private final ConverterRegistry converterRegistry;

	private final TextArea faultArea = new TextArea();

	private final Label fetchStartedAtlabel = new Label();

	private final Observable<State> stateObservable = Observable.notInitialized();

	private final TaskExecutor taskExecutor;

	private final TypedUiStateBinder<Properties> typedUiStateBinder;

	private void onPropertiesChange(Properties nextProperties) {
		logger.debug("#onPropertiesChange: nextProperties = {}", nextProperties);
		//
		taskExecutor.execute(() -> {
			logger.debug("Trying to fetch data");
			//
			stateObservable.update(currentState -> {
				State result;
				{
					if (currentState.getProperties() == nextProperties) {
						result = currentState.toBuilder()
								.fetchStartedAt(Instant.now())
								.build();
					} else {
						logger.debug("Fetch is outdated: currentState.properties = {}, nextProperties = {}",
								currentState.getProperties(),
								nextProperties);
						//
						result = currentState;
					}
				}
				return result;
			});
			//
			Try<ObjectNode> contentTry = Try.of(() -> {
				ConfluenceFeignClient client = clientFactory.create(nextProperties.getUrl());
				//
				return client.getContentAsObjectNode(
						nextProperties.computeCredentials(),
						nextProperties.getContentId(),
						ConfluenceGetContentQuery.builder()
								.expand(!nextProperties.getExpandKeys().isEmpty()
										? nextProperties.getExpandKeys().mkString(",")
										: null)
								.build());
			});
			//
			logger.debug("Updating state, try outcome is {}", contentTry.isSuccess());
			//
			stateObservable.update(currentState -> {
				State result;
				{
					if (currentState.getProperties() == nextProperties) {
						result = currentState.toBuilder()
								.content(contentTry.getOrNull())
								.fault((contentTry.isSuccess() ? null : contentTry.getCause()))
								.build();
					} else {
						logger.debug("Update is outdated: currentState.properties = {}, nextProperties = {}",
								currentState.getProperties(),
								nextProperties);
						//
						result = currentState;
					}
				}
				return result;
			});
		});
	}

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setHeightFull();
		}
		{
			CssHorizontalLayout navigationLayout = new CssHorizontalLayout();
			{
				navigationLayout.addClassNames(PandoraStyles.LAYOUT);
			}
			{
				StringBox urlBox = new StringBox();
				CssFlexItem.setFlexGrow(urlBox.asVaadinComponent(), 3);
				navigationLayout.add(urlBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> urlBox.render(SimpleValueBox.Properties.<String>builder()
						.label("Url")
						.onValueChange(nextUrl -> render(nextState.getProperties().withUrl(nextUrl)))
						.value(nextState.getProperties().getUrl())
						.build()));
			}
			{
				StringBox contentIdBox = new StringBox();
				CssFlexItem.setFlexGrow(contentIdBox.asVaadinComponent(), 1);
				navigationLayout.add(contentIdBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> contentIdBox.render(SimpleValueBox.Properties.<String>builder()
						.label("Content Id")
						.onValueChange(nextContentId -> render(nextState.getProperties().withContentId(nextContentId)))
						.value(nextState.getProperties().getContentId())
						.build()));
			}
			{
				StringBox usernameBox = new StringBox();
				CssFlexItem.setFlexGrow(usernameBox.asVaadinComponent(), 1);
				navigationLayout.add(usernameBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> usernameBox.render(SimpleValueBox.Properties.<String>builder()
						.label("Username")
						.onValueChange(nextUsername -> render(nextState.getProperties().withUsername(nextUsername)))
						.value(nextState.getProperties().getUsername())
						.build()));
			}
			{
				SecretBox passwordBox = new SecretBox();
				CssFlexItem.setFlexGrow(passwordBox.asVaadinComponent(), 1);
				navigationLayout.add(passwordBox.asVaadinComponent());
				stateObservable.subscribe(nextState -> passwordBox.render(SimpleValueBox.Properties.<Secret>builder()
						.label("Password")
						.onValueChange(nextPassword -> render(nextState.getProperties().withPassword(nextPassword)))
						.value(nextState.getProperties().getPassword())
						.build()));
			}
			{
				// TODO: Refresh
			}
			add(navigationLayout);
		}
		{
			CssHorizontalLayout expandLayout = new CssHorizontalLayout();
			{
				expandLayout.addClassNames(PandoraStyles.LAYOUT);
				//
				expandLayout.setAlignItems(AlignItems.BASELINE);
			}
			add(expandLayout);
			//
			stateObservable.subscribe(nextState -> {
				expandLayout.removeAll();
				//
				expandLayout.add(new Label("Expand: "));
				nextState.getProperties().getExpandKeys().forEach(expandElement ->
						expandLayout.add(new Label(expandElement)));
				//
				expandLayout.addStretch();
				expandLayout.add(ButtonRenderer.render(builder -> builder
						// TODO
						.enabled(nextState.getContent() != null)
						.onClick(event -> ConfirmationViews
								.showFunctionDialog(
										new Tuple4<>(
												nextState.getProperties().getContentId(),
												nextState.getContent().get("title").asText(),
												Optional.ofNullable(nextState.getContent())
														.map(node -> node.get("version"))
														.map(node -> node.get("number"))
														.map(JsonNode::intValue)
														.map(versionNumber -> versionNumber + 1)
														.orElse(null),
												Optional.ofNullable(nextState.getContent())
														.map(node -> node.get("body"))
														.map(node -> node.get("storage"))
														.map(node -> node.get("value"))
														.map(JsonNode::asText)
														.orElse(null)),
										(state, sink) -> {
											CssGridLayout layout = new CssGridLayout();
											{
												layout.addClassNames(PandoraStyles.LAYOUT);
												//
												layout.setGridTemplateColumns("1fr 2fr 1fr");
												layout.setMaxHeight("80vh");
												layout.setWidth("80vw");
											}
											{
												layout.add(StringBoxRenderer.render(innerBuilder -> innerBuilder
														.label("Content Id")
														.onValueChange(nextValue -> sink.accept(
																state.update1(nextValue)))
														.value(state._1())
														.build()));
												layout.add(StringBoxRenderer.render(innerBuilder -> innerBuilder
														.label("Title")
														.onValueChange(nextValue -> sink.accept(
																state.update2(nextValue)))
														.value(state._2())
														.build()));
												layout.add(IntegerBoxRenderer.render(innerBuilder -> innerBuilder
														.label("Version / Number")
														.onValueChange(nextValue -> sink.accept(
																state.update3(nextValue)))
														.value(state._3())
														.build()));
												{
													TextArea contentArea = TextAreaRenderer.render(
															innerBuilder -> innerBuilder
																	.label("Body / Storage / Value")
																	.onValueChange(nextValue -> sink.accept(
																			state.update4(nextValue)))
																	.value(state._4())
																	.build());
													CssGridItem.setGridColumnSpan(contentArea, 3);
													layout.add(contentArea);
												}
											}
											return layout;
										})
								.subscribeAlwaysAccepted(result -> {
									ConfluenceFeignClient client = clientFactory.create(
											nextState.getProperties().getUrl());
									//
									client.putContent(
											nextState.getProperties().computeCredentials(),
											result._1(),
											ConfluenceContent.builder()
													.body(ConfluenceBody.builder()
															.storage(ConfluenceStorage.builder()
																	.representation("storage")
																	.value(result._4())
																	.build())
															.build())
													.title(result._2())
													.type(nextState.getContent().get("type").asText())
													.version(ConfluenceVersion.builder()
															.number(result._3())
															.build())
													.build());
								}))
						.text("PUT")
						.build()));
			});
		}
		{
			add(fetchStartedAtlabel);
		}
		{
			contentGrid.configure(grid -> {
				grid
						.addHierarchyColumn(item -> item.getId() != null ? item.getId() : "")
						.setResizable(true)
						.setHeader("Id");
				grid
						.addColumn(item -> item.getNode().isValueNode() ? item.getNode().asText() : null)
						.setResizable(true)
						.setHeader("Value");
				grid
						.addComponentColumn(item -> {
							Component component;
							{
								String itemPath = item.calcPath();
								String expandKey = itemPath != null
										? itemPath.replace("_expandable.", "")
										: null;
								//
								State currentState = stateObservable.get();
								List<String> expandKeys = currentState.getProperties().getExpandKeys();
								//
								boolean checked = expandKeys.contains(expandKey);
								boolean expandableChild = item.getParent() != null &&
										Objects.equals(item.getParent().getId(), "_expandable");
								//
								if (checked || expandableChild) {
									Checkbox checkbox = new Checkbox(checked);
									checkbox.addValueChangeListener(event ->
											render(currentState.getProperties().withExpandKeys(event.getValue()
													? expandKeys.append(expandKey)
													: expandKeys.remove(expandKey))));
									component = checkbox;
								} else {
									component = new Div();
								}
							}
							return component;
						})
						.setHeader("Expand");
			});
			add(contentGrid.asVaadinComponent());
		}
		{
			add(faultArea);
		}
		{
			stateObservable.subscribe(nextState -> {
				{
					fetchStartedAtlabel.setText("Fetch started at %s".formatted(nextState.getFetchStartedAt()));
					fetchStartedAtlabel.setVisible(nextState.getContent() == null && nextState.getFault() == null);
				}
				{
					contentGrid.setValue(nextState.getContent());
					contentGrid.setExpandDepth(1);
					contentGrid.asVaadinComponent().setVisible(nextState.getContent() != null);
				}
				{
					if (nextState.getFault() != null) {
						StringWriter stringWriter = new StringWriter();
						nextState.getFault().printStackTrace(new PrintWriter(stringWriter));
						faultArea.setValue(stringWriter.toString());
					} else {
						faultArea.setValue("");
					}
					faultArea.setVisible(nextState.getFault() != null);
				}
			});
		}
		{
			stateObservable.getFlux()
					.map(State::getProperties)
					.distinctUntilChanged()
					.subscribe(this::onPropertiesChange);
		}
		{
			stateObservable.set(State.builder()
					.content(null)
					.fault(null)
					.properties(Properties.builder()
							.contentId("12525567")
							.expandKeys(List.empty())
							.password(null)
							.url("https://blackdogs.atlassian.net/wiki")
							.username(null)
							.build())
					.build());
		}
		{
			Sinks.Many<Properties> propertiesSink = ReactorUtils.wrap(stateObservable.getFlux()
					.map(State::getProperties));
			//
			typedUiStateBinder.bind(
					Properties.class,
					() -> stateObservable.get().getProperties(),
					nextUiState -> stateObservable.update(currentState -> currentState.withProperties(
							converterRegistry.convert(nextUiState, Properties.class))),
					propertiesSink.asFlux());
			addDetachListener(event -> propertiesSink.tryEmitComplete());
		}
	}

	public void render(Properties properties) {
		stateObservable.update(currentState -> currentState.withProperties(properties));
	}

	@Builder(toBuilder = true)
	@Value
	@With
	private static class Properties {

		@CheckForNull
		@UiStateAttribute
		String contentId;

		List<String> expandKeys;

		@CheckForNull
		Secret password;

		@CheckForNull
		@UiStateAttribute
		String url;

		@CheckForNull
		@UiStateAttribute
		String username;

		public String computeCredentials() {
			return username != null && password != null
					? AuthorizationUtils.encodeBasic(username, password.getValue())
					: null;
		}
	}

	@Builder(toBuilder = true)
	@Value
	@With
	private static class State {

		@CheckForNull
		ObjectNode content;

		@CheckForNull
		Throwable fault;

		@CheckForNull
		Instant fetchStartedAt;

		Properties properties;
	}
}
