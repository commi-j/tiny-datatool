package tk.labyrinth.satool.confluenceconnector.element.table;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.jsoup.nodes.Element;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceElement;

import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class ConfluenceCellElement {

	public static final String TD_TAG = "td";

	public static final String TH_TAG = "th";

	@NonNull
	Element jsoupElement;

	public boolean isHeader() {
		return Objects.equals(tagName(), TH_TAG);
	}

	public List<ConfluenceElement> select(String xpathSelector) {
		return List.ofAll(jsoupElement.selectXpath(xpathSelector))
				.map(element -> ConfluenceElement.builder()
						.jsoupElement(element)
						.build());
	}

	public String tagName() {
		return jsoupElement.tagName();
	}

	public String text() {
		return jsoupElement.text();
	}

	@Override
	public String toString() {
		return jsoupElement.toString();
	}

	public static ConfluenceCellElement from(ConfluenceElement element) {
		return from(element.getJsoupElement());
	}

	public static ConfluenceCellElement from(Element jsoupElement) {
		if (!Objects.equals(jsoupElement.tagName(), TD_TAG) && !Objects.equals(jsoupElement.tagName(), TH_TAG)) {
			throw new IllegalArgumentException("Require tagName IN (%s, %s): %s".formatted(TD_TAG, TH_TAG, jsoupElement));
		}
		return new ConfluenceCellElement(jsoupElement);
	}
}
