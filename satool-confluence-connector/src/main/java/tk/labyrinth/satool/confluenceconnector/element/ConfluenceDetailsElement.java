package tk.labyrinth.satool.confluenceconnector.element;

import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;

@NonFinal
@SuperBuilder(toBuilder = true)
@Value
public class ConfluenceDetailsElement extends ConfluenceStructuredMacroElement {

	public static final String NAME = "details";

	@Override
	public String toString() {
		return getJsoupElement().toString();
	}

	public static String selector() {
		return "//%s[@name='%s']".formatted(TAG, NAME);
	}
}
