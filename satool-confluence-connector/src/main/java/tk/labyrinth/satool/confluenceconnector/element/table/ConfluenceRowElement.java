package tk.labyrinth.satool.confluenceconnector.element.table;

import io.vavr.collection.List;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;
import org.jsoup.nodes.Element;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceElement;

import java.util.Objects;

@NonFinal
@SuperBuilder(toBuilder = true)
@Value
public class ConfluenceRowElement extends ConfluenceElement {

	public static final String TAG = "tr";

	public List<ConfluenceCellElement> getCells() {
		return select("%1$s/%2$s | %1$s/%3$s".formatted(TAG, ConfluenceCellElement.TD_TAG, ConfluenceCellElement.TH_TAG))
				.map(ConfluenceElement::getJsoupElement)
				.map(ConfluenceCellElement::from);
	}

	@Override
	public String toString() {
		return getJsoupElement().toString();
	}

	public static ConfluenceRowElement from(Element jsoupElement) {
		if (!Objects.equals(jsoupElement.tagName(), TAG)) {
			throw new IllegalArgumentException("Require tagName = %s: %s".formatted(TAG, jsoupElement));
		}
		return ConfluenceRowElement.builder()
				.jsoupElement(jsoupElement)
				.build();
	}
}
