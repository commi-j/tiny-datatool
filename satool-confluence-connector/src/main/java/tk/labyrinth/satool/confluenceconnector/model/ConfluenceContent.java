package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.Map;

@Builder(builderClassName = "Builder", toBuilder = true)
@Jacksonized
@Value
public class ConfluenceContent {

	public static final String ANCESTORS_EXPAND_KEY = "ancestors";

	public static final String METADATA_EXPAND_KEY = "metadata";

	public static final String VERSION_EXPAND_KEY = "version";

	@JsonAnyGetter
	@JsonAnySetter
	Map<String, JsonNode> __unmapped;

	ObjectNode _expandable;

	ObjectNode _links;

	List<ConfluenceContent> ancestors;

	ConfluenceBody body;

	ConfluenceHistory history;

	String id;

	ConfluenceMetadata metadata;

	ConfluenceSpace space;

	/**
	 * "current".
	 */
	String status;

	String title;

	/**
	 * "attachment";<br>
	 * "page";<br>
	 */
	String type;

	ConfluenceVersion version;
}
