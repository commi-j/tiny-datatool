package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Builder;
import lombok.Value;

import java.time.Instant;
import java.util.Map;

@Builder
@Value
public class ConfluenceLastUpdated {

	public static final String EXPAND_KEY = "history.lastUpdated";

	@JsonAnyGetter
	@JsonAnySetter
	Map<String, JsonNode> __unmapped;

	Instant when;
}
