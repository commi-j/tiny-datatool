package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.List;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceChunk<T> {

	ObjectNode _links;

	int limit;

	List<T> results;

	/**
	 * From what is known, it's 25 for pages and 50 for attachments.
	 */
	int size;

	int start;
}
