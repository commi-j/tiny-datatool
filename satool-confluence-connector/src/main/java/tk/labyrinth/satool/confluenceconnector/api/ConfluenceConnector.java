package tk.labyrinth.satool.confluenceconnector.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.confluenceconnector.api.contentchildattachment.ConfluencePostContentChildAttachmentResult;

import java.util.List;

@LazyComponent
public class ConfluenceConnector {

	private final RestTemplate restTemplate;

	public ConfluenceConnector(ObjectMapper objectMapper) {
		restTemplate = new RestTemplate(List.of(
				new AllEncompassingFormHttpMessageConverter(),
				new MappingJackson2HttpMessageConverter(objectMapper)));
	}

	/**
	 * <a href="https://confluence.atlassian.com/confkb/using-the-confluence-rest-api-to-upload-an-attachment-to-one-or-more-pages-1014274390.html">https://confluence.atlassian.com/confkb/using-the-confluence-rest-api-to-upload-an-attachment-to-one-or-more-pages-1014274390.html</a>
	 *
	 * @param confluenceBaseUrl   non-null
	 * @param authorization       non-null
	 * @param confluenceContentId non-null
	 * @param file                non-null, content and filename are relevant
	 * @param comment             nullable
	 *
	 * @return non-null
	 */
	public ConfluencePostContentChildAttachmentResult postContentChildAttachment(
			String confluenceBaseUrl,
			String authorization,
			String confluenceContentId,
			Resource file,
			@Nullable String comment) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.AUTHORIZATION, authorization);
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA_VALUE);
		headers.add("X-Atlassian-Token", "nocheck");
		//
		MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
		bodyBuilder
				.part(
						"file",
						IoUtils.unchecked(file::getContentAsByteArray),
						MediaType.IMAGE_JPEG)
				.filename(file.getFilename());
		if (comment != null) {
			bodyBuilder
					.part("comment", comment);
		}
		//
		HttpEntity<?> httpEntity = new HttpEntity<>(bodyBuilder.build(), headers);
		//
		ConfluencePostContentChildAttachmentResult result = restTemplate.postForObject(
				"%s/rest/api/content/%s/child/attachment".formatted(confluenceBaseUrl, confluenceContentId),
				httpEntity,
				ConfluencePostContentChildAttachmentResult.class);
		//
		return result;
	}
}
