package tk.labyrinth.satool.confluenceconnector.api;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceQueryLanguagePredicate;

@RequiredArgsConstructor
public class ConnectedConfluenceFetcher {

	private final ConfluenceFetcher confluenceFetcher;

	private final ConfluenceConnectionParameters connectionParameters;

	@Nullable
	public ConfluenceContent findOne(ConfluenceQueryLanguagePredicate criteria, @Nullable String expand) {
		return confluenceFetcher.findOne(connectionParameters, criteria, expand);
	}
}
