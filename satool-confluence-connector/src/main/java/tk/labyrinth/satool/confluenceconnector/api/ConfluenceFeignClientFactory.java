package tk.labyrinth.satool.confluenceconnector.api;

import feign.Request;
import org.springframework.cloud.openfeign.FeignClientBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class ConfluenceFeignClientFactory {

	private final FeignClientBuilder feignClientBuilder;

	public ConfluenceFeignClientFactory(ApplicationContext applicationContext) {
		feignClientBuilder = new FeignClientBuilder(applicationContext);
	}

	public ConfluenceFeignClient create(String baseUrl) {
		return feignClientBuilder.forType(ConfluenceFeignClient.class, "satool-confluence")
				.customize(feignBuilder -> feignBuilder.options(new Request.Options(
						// Raising connectTimeout 10 -> 30, others are unchanged.
						30, TimeUnit.SECONDS,
						60, TimeUnit.SECONDS,
						true)))
				.url(baseUrl + "/rest/api")
				.build();
	}
}
