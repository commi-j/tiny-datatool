package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Value;

import java.util.Map;

@Value
public class ConfluenceHistory {

	public static final String EXPAND_KEY = "history";

	@JsonAnyGetter
	@JsonAnySetter
	Map<String, JsonNode> __unmapped;

	ConfluenceLastUpdated lastUpdated;
}
