package tk.labyrinth.satool.confluenceconnector.element;

import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;

@NonFinal
@SuperBuilder(toBuilder = true)
@Value
public class ConfluenceTocWidget {

}
