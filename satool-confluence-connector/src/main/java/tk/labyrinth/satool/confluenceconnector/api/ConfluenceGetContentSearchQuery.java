package tk.labyrinth.satool.confluenceconnector.api;

import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;

@NonFinal
@SuperBuilder(toBuilder = true)
@Value
public class ConfluenceGetContentSearchQuery extends ConfluenceGetContentQuery {

	@Nullable
	String cql;
}
