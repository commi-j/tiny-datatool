package tk.labyrinth.satool.confluenceconnector.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContentSearchChunk;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceQueryLanguagePredicate;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class ConfluenceFetcher {

	private final ConfluenceFeignClientFactory clientFactory;

	private ConfluenceContentSearchChunk<ConfluenceContent> doSearchContents(
			ConfluenceFeignClient client,
			@Nullable String authorization,
			@Nullable String cql,
			@Nullable String expand,
			int start,
			int limit) {
		logger.info("Fetching: cql = {} start = {}", cql, start);
		//
		ConfluenceContentSearchChunk<ConfluenceContent> chunk = client.getContentSearch(
				authorization,
				ConfluenceGetContentSearchQuery.builder()
						.cql(cql)
						.expand(expand)
						.limit(limit)
						.start(start)
						.build());
		//
		logger.info("Fetched: size = {}, searchDuration = {}", chunk.getSize(), chunk.getSearchDuration());
		//
		return chunk;
	}

	@Nullable
	public ConfluenceContent findOne(
			ConfluenceConnectionParameters connectionParameters,
			ConfluenceQueryLanguagePredicate criteria,
			@Nullable String expand) {
		ConfluenceFeignClient client = clientFactory.create(connectionParameters.getBaseUrl());
		//
		String cql = criteria.toString();
		//
		ConfluenceContentSearchChunk<ConfluenceContent> chunk = client.getContentSearch(
				connectionParameters.getAuthorization(),
				ConfluenceGetContentSearchQuery.builder()
						.cql(cql)
						.expand(expand)
						.limit(2)
						.build());
		//
		if (chunk.getSize() > 1) {
			throw new IllegalArgumentException("Require <= 1: cql = %s, chunk.results.ids = %s".formatted(
					cql,
					chunk.getResults().map(ConfluenceContent::getId)));
		}
		//
		return !chunk.getResults().isEmpty() ? chunk.getResults().get(0) : null;
	}

	public ConnectedConfluenceFetcher getConnectedVariant(ConfluenceConnectionParameters connectionParameters) {
		return new ConnectedConfluenceFetcher(this, connectionParameters);
	}

	public ConfluenceContentSearchChunk<ConfluenceContent> searchContents(
			ConfluenceConnectionParameters connectionParameters,
			@Nullable ConfluenceQueryLanguagePredicate criteria,
			@Nullable String expand,
			@Nullable Integer limit) {
		return doSearchContents(
				clientFactory.create(connectionParameters.getBaseUrl()),
				connectionParameters.getAuthorization(),
				criteria != null ? criteria.toString() : null,
				expand,
				0,
				limit != null ? limit : 25);
	}

	public Flux<ConfluenceContentSearchChunk<ConfluenceContent>> searchContentsFlux(
			ConfluenceConnectionParameters connectionParameters,
			@Nullable ConfluenceQueryLanguagePredicate criteria,
			@Nullable String expand,
			@Nullable Integer limit) {
		if (limit != null && limit == 0) {
			throw new IllegalArgumentException("Require positive limit: %s".formatted(limit));
		}
		//
		return Flux.create(sink -> {
			try {
				ConfluenceFeignClient client = clientFactory.create(connectionParameters.getBaseUrl());
				//
				String cql = criteria != null ? criteria.toString() : null;
				int limitToUse = limit != null ? limit : 25;
				int start = 0;
				boolean done = false;
				//
				do {
					ConfluenceContentSearchChunk<ConfluenceContent> chunk = doSearchContents(
							client,
							connectionParameters.getAuthorization(),
							cql,
							expand,
							start,
							limitToUse);
					//
					sink.next(chunk);
					//
					if (chunk.getSize() == chunk.getLimit()) {
						start += chunk.getLimit();
					} else {
						done = true;
					}
				} while (!done);
				//
				logger.info("Done");
				//
				sink.complete();
			} catch (RuntimeException ex) {
				sink.error(ex);
			}
		});
	}
}
