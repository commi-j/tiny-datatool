package tk.labyrinth.satool.confluenceconnector.element;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jsoup.nodes.Element;

@NonFinal
@SuperBuilder(toBuilder = true)
@Value
public class ConfluenceElement {

	@NonNull
	Element jsoupElement;

	@Nullable
	public ConfluenceElement nextElementSibling() {
		Element nextElementSibling = jsoupElement.nextElementSibling();
		//
		return nextElementSibling != null
				? ConfluenceElement.builder()
				.jsoupElement(nextElementSibling)
				.build()
				: null;
	}

	public List<ConfluenceElement> select(String xpathSelector) {
		return List.ofAll(jsoupElement.selectXpath(xpathSelector))
				.map(element -> ConfluenceElement.builder()
						.jsoupElement(element)
						.build());
	}

	public String tagName() {
		return jsoupElement.tagName();
	}

	public String text() {
		return jsoupElement.text();
	}

	@Override
	public String toString() {
		return jsoupElement.toString();
	}
}
