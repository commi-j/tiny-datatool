package tk.labyrinth.satool.confluenceconnector.element.table;

import io.vavr.collection.List;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;
import org.jsoup.nodes.Element;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceElement;

import java.util.Objects;

@NonFinal
@SuperBuilder(toBuilder = true)
@Value
public class ConfluenceTableElement extends ConfluenceElement {

	public static final String TAG = "table";

	public List<ConfluenceRowElement> getRows() {
		return select("%s/tbody/%s".formatted(TAG, ConfluenceRowElement.TAG))
				.map(ConfluenceElement::getJsoupElement)
				.map(ConfluenceRowElement::from);
	}

	@Override
	public String toString() {
		return getJsoupElement().toString();
	}

	public static ConfluenceTableElement from(ConfluenceElement element) {
		return from(element.getJsoupElement());
	}

	public static ConfluenceTableElement from(Element jsoupElement) {
		if (!Objects.equals(jsoupElement.tagName(), TAG)) {
			throw new IllegalArgumentException("Require tagName = %s: %s".formatted(TAG, jsoupElement));
		}
		return ConfluenceTableElement.builder()
				.jsoupElement(jsoupElement)
				.build();
	}

	public static List<ConfluenceTableElement> select(ConfluenceElement element) {
		return element.select(xpathSelector())
				.map(ConfluenceElement::getJsoupElement)
				.map(ConfluenceTableElement::from);
	}

	public static String xpathSelector() {
		return "//%s".formatted(TAG);
	}
}
