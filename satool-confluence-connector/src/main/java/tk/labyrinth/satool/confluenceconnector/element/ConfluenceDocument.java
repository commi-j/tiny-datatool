package tk.labyrinth.satool.confluenceconnector.element;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

@Builder(toBuilder = true)
@Value
public class ConfluenceDocument {

	@NonNull
	Document jsoupDocument;

	public List<ConfluenceElement> select(String xpathSelector) {
		// We iterate over children for selection because default JSoup select on document works incorrect
		// with multiple root children.
		// See 'select-on-multiple-root' tagged test of this class.
		//
		return List.ofAll(jsoupDocument.children()).flatMap(child -> child.selectXpath(xpathSelector))
				.map(element -> ConfluenceElement.builder()
						.jsoupElement(element)
						.build());
	}

	@Override
	public String toString() {
		return jsoupDocument.toString();
	}

	public static ConfluenceDocument from(String value) {
		return new ConfluenceDocument(Jsoup.parse(value, Parser.xmlParser()));
	}
}
