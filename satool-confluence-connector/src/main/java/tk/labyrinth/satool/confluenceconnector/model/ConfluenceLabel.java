package tk.labyrinth.satool.confluenceconnector.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceLabel {

	String id;

	String name;

	String prefix;
}
