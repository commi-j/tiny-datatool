package tk.labyrinth.satool.confluenceconnector.api;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceChunk;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContentSearchChunk;

import javax.annotation.Nullable;

/**
 * - /content/{id}<br>
 * - /content/search<br>
 * - /search<br>
 *
 * @see ConfluenceFeignClientFactory
 */
public interface ConfluenceFeignClient {

	/**
	 * This can be used to delete attachments.
	 *
	 * @param authorization nullable
	 * @param id            valid
	 *
	 * @return non-null
	 */
	@DeleteMapping("content/{id}")
	ObjectNode deleteContent(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@PathVariable("id") String id);

	@GetMapping("content/{id}")
	default ConfluenceContent getContent(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@PathVariable("id") String id) {
		return getContent(authorization, id, ConfluenceGetContentQuery.builder().build());
	}

	@GetMapping("content/{id}")
	ConfluenceContent getContent(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@PathVariable("id") String id,
			@SpringQueryMap ConfluenceGetContentQuery query);

	@GetMapping("content/{id}")
	ObjectNode getContentAsObjectNode(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@PathVariable("id") String id,
			@SpringQueryMap ConfluenceGetContentQuery query);

	@GetMapping("content/{id}/child/attachment")
	ConfluenceChunk<ConfluenceContent> getContentChildAttachment(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@PathVariable("id") String id,
			@SpringQueryMap ConfluenceGetContentChildAttachmentQuery query);

	@GetMapping("content/search")
	ConfluenceContentSearchChunk<ConfluenceContent> getContentSearch(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@SpringQueryMap ConfluenceGetContentSearchQuery query);

	@GetMapping("content/search")
	ObjectNode getContentSearchAsObjectNode(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@SpringQueryMap ConfluenceGetContentSearchQuery query);

	/**
	 * Required content attributes are:<br>
	 * Payload:<br>
	 * - ancestors.[0].id = parent id;<br>
	 * - title = new title;<br>
	 * - body.storage.value = new body;<br>
	 * Technical:<br>
	 * - body.storage.representation = "storage";<br>
	 * - space.key - key of parent's space (which is redundant as we already refer to parent);<br>
	 * - type = "page";<br>
	 *
	 * @param authorization non-null
	 * @param content       non-null
	 *
	 * @return created content
	 */
	@PostMapping("content")
	ConfluenceContent postContent(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@RequestBody ConfluenceContent content);

	/**
	 * Required content attributes are:<br>
	 * Payload:<br>
	 * - title = new title;<br>
	 * - body.storage.value = new body;<br>
	 * Technical:<br>
	 * - body.storage.representation = "storage";<br>
	 * - type = "page";<br>
	 * - version.number = +1 of current;<br>
	 * Also:<br>
	 * * id is not required as we provide it in path;<br>
	 *
	 * @param authorization nullable
	 * @param id            non-null
	 * @param content       non-null
	 *
	 * @return updated content
	 */
	@PutMapping("content/{id}")
	ConfluenceContent putContent(
			@RequestHeader(HttpHeaders.AUTHORIZATION) @Nullable String authorization,
			@PathVariable("id") String id,
			@RequestBody ConfluenceContent content);
}
