package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
public class ConfluenceStorage {

	public static final String EXPAND_KEY = "body.storage";

	@JsonAnyGetter
	@JsonAnySetter
	Map<String, JsonNode> __unmapped;

	/**
	 * "storage".
	 */
	String representation;

	/**
	 * XML.
	 */
	String value;
}
