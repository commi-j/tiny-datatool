package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceVersion {

	public static final String EXPAND_KEY = "version";

	ObjectNode by;

	String message;

	Integer number;

	Instant when;
}
