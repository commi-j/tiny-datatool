package tk.labyrinth.satool.confluenceconnector.element.tool;

public class ConfluenceXmlUtils {

	public static String simplify(String xmlString) {
		return xmlString
				.replace("<p>", "")
				.replace("</p>", "\n")
				.replace("<p>", "\n")
				.trim();
	}
}
