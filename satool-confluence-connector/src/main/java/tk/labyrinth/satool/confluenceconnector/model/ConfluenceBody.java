package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
public class ConfluenceBody {

	public static final String EXPAND_KEY = "body";

	@JsonAnyGetter
	@JsonAnySetter
	Map<String, JsonNode> __unmapped;

	ConfluenceStorage storage;
}
