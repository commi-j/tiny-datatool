package tk.labyrinth.satool.confluenceconnector.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Builder(builderClassName = "Builder", toBuilder = true)
@Jacksonized
@Value
public class ConfluenceMetadata {

	public static final String LABELS_EXPAND_KEY = "metadata.labels";

	ConfluenceChunk<ConfluenceLabel> labels;
}
