package tk.labyrinth.satool.confluenceconnector.model;

import lombok.Value;

import javax.annotation.Nullable;

@Value
public class ConfluenceLinks {

	@Nullable
	String base;

	@Nullable
	String context;

	@Nullable
	String self;
}
