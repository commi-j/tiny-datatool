package tk.labyrinth.satool.confluenceconnector.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
public class ConfluenceSpace {

	public static final String EXPAND_KEY = "space";

	@JsonAnyGetter
	@JsonAnySetter
	Map<String, JsonNode> __unmapped;

	ObjectNode _expandable;

	String id;

	String key;

	String name;
}
