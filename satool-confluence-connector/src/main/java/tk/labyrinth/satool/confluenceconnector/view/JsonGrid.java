package tk.labyrinth.satool.confluenceconnector.view;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.treegrid.TreeGrid;
import io.vavr.collection.Stream;
import lombok.Getter;
import lombok.Value;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.pandora.ui.component.view.View;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Consumer;

public class JsonGrid implements View {

	@Getter
	private final TreeGrid<Item> grid = new TreeGrid<>();

	@Nullable
	private JsonNode value = null;

	private Item createRootItem(JsonNode rootNode) {
		return Item.of(null, rootNode, null);
	}

	@Override
	public Component asVaadinComponent() {
		return grid;
	}

	public void configure(Consumer<TreeGrid<Item>> gridConfigurer) {
		gridConfigurer.accept(grid);
	}

	public void setExpandDepth(@Nullable Integer expandDepth) {
		if (expandDepth != null) {
			grid.expandRecursively(List.of(createRootItem(value)), expandDepth - 1);
		} else {
			grid.collapseRecursively(List.of(createRootItem(value)), Integer.MAX_VALUE);
		}
	}

	public void setValue(@Nullable JsonNode value) {
		this.value = value;
		//
		if (value != null) {
			grid.setItems(
					java.util.stream.Stream.of(createRootItem(value)),
					item -> {
						Stream<Item> children;
						{
							JsonNode node = item.getNode();
							if (node instanceof ArrayNode) {
								children = Stream.ofAll(StreamUtils.from(node.elements()))
										.zipWithIndex()
										.map(tuple -> Item.of(tuple._2() + "", tuple._1(), item));
							} else if (node instanceof ObjectNode) {
								children = Stream.ofAll(StreamUtils.from(node.fields()))
										.map(entry -> Item.of(entry.getKey(), entry.getValue(), item));
							} else {
								children = Stream.empty();
							}
						}
						return children.toJavaStream();
					});
		} else {
			grid.setItems(Stream.<Item>empty().toJavaStream(), item -> Stream.<Item>empty().toJavaStream());
		}
	}

	@Value(staticConstructor = "of")
	public static class Item {

		/**
		 * String for object parent;<br>
		 * Integer for array parent;<br>
		 * Null for root.<br>
		 */
		@Nullable
		String id;

		JsonNode node;

		/**
		 * Null for root.<br>
		 */
		@Nullable
		Item parent;

		public String calcPath() {
			return parent != null && parent.id != null ? parent.calcPath() + "." + id : id;
		}
	}
}
