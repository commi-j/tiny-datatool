
<#macro layout>
	<ac:layout>
		<#nested>
	</ac:layout>
</#macro>

<#macro cell>
	<ac:layout-cell>
		<#nested>
	</ac:layout-cell>
</#macro>

<#macro section type>
	<ac:layout-section ac:type="${type}">
		<#nested>
	</ac:layout-section>
</#macro>

<#macro section_single>
	<@section type="single">
		<@cell>
			<#nested>
		</@cell>
	</@section>
</#macro>

<#macro section_two_equal>
	<@section type="two_equal">
		<#nested>
	</@section>
</#macro>

<#macro section_two_left_sidebar>
	<@section type="two_left_sidebar">
		<#nested>
	</@section>
</#macro>
