
<#macro anchor name>
	<ac:structured-macro ac:name="anchor">
		<ac:parameter ac:name="">${name}</ac:parameter>
	</ac:structured-macro>
</#macro>

<#macro children all=false>
	<ac:structured-macro ac:name="children">
		<#if all><ac:parameter ac:name="all">true</ac:parameter></#if>
	</ac:structured-macro>
</#macro>

<#macro details hidden=false id="">
	<ac:structured-macro ac:name="details">
		<#if hidden><ac:parameter ac:name="hidden">true</ac:parameter></#if>
		<#if id?has_content><ac:parameter ac:name="id">${id}</ac:parameter></#if>
		<ac:rich-text-body>
			<#nested>
		</ac:rich-text-body>
	</ac:structured-macro>
</#macro>

<#-- size = S, M, L corresponding to 150, 250 and 400 px -->
<#macro image filename border=false size="">
<#switch size>
	<#case "S">
		<#assign height=150>
		<#break>
	<#case "M">
		<#assign height=250>
		<#break>
	<#case "L">
		<#assign height=400>
		<#break>
	<#default>
		<#assign height=0>
</#switch>
<ac:image<#if border> ac:border="true"</#if><#if height!=0> ac:height="${height}"</#if>>
	<ri:attachment ri:filename="${filename}"/>
</ac:image>
</#macro>

<#macro expand title="">
	<ac:structured-macro ac:name="expand">
		<#if title?has_content><ac:parameter ac:name="title">${title}</ac:parameter></#if>
		<ac:rich-text-body>
			<#nested>
		</ac:rich-text-body>
	</ac:structured-macro>
</#macro>

<#macro line_separator>
	<br/><hr/><br/>
</#macro>

<#-- format = PNG, SVG -->
<#macro plantuml content format="" title="">
	<ac:structured-macro ac:name="expand">
		<#if format?has_content><ac:parameter ac:name="format">${format}</ac:parameter></#if>
		<#if title?has_content><ac:parameter ac:name="title">${title}</ac:parameter></#if>
		<ac:plain-text-body><![CDATA[${content}]]></ac:plain-text-body>
	</ac:structured-macro>
</#macro>

<#macro toc maxLevel=-1 minLevel=-1>
	<ac:structured-macro ac:name="toc">
		<#if maxLevel!=-1><ac:parameter ac:name="maxLevel">${maxLevel}</ac:parameter></#if>
		<#if minLevel!=-1><ac:parameter ac:name="minLevel">${minLevel}</ac:parameter></#if>
	</ac:structured-macro>
</#macro>