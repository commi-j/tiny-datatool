package tk.labyrinth.satool.confluenceconnector.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContentSearchChunk;

@SpringBootTest
@Tag("web")
class ConfluenceFeignClientTest {

	@Autowired
	private ConfluenceFeignClientFactory confluenceFeignClientFactory;

	@Test
	void testGetContentFromDifferentSites() {
		{
			ConfluenceFeignClient blackdogsClient = confluenceFeignClientFactory.create(
					"https://blackdogs.atlassian.net/wiki");
			//
			ConfluenceContent content = blackdogsClient.getContent(null, "12525567");
			//
			Assertions.assertEquals("12525567", content.getId());
			Assertions.assertEquals("page", content.getType());
		}
		{
			ConfluenceFeignClient templatesClient = confluenceFeignClientFactory.create(
					"https://templates.atlassian.net/wiki");
			//
			ConfluenceContent content = templatesClient.getContent(null, "33111");
			//
			Assertions.assertEquals("33111", content.getId());
			Assertions.assertEquals("page", content.getType());
		}
	}

	@Test
	void testGetContentSearch() {
		ConfluenceFeignClient blackdogsClient = confluenceFeignClientFactory.create(
				"https://blackdogs.atlassian.net/wiki");
		//
		ConfluenceContentSearchChunk<ConfluenceContent> result = blackdogsClient.getContentSearch(
				null,
				ConfluenceGetContentSearchQuery.builder()
						.cql("(label='shared-links')")
						.expand("body")
						.build());
		//
		Assertions.assertEquals(25, result.getLimit());
		Assertions.assertEquals(10, result.getSize());
		Assertions.assertEquals(0, result.getStart());
	}
}