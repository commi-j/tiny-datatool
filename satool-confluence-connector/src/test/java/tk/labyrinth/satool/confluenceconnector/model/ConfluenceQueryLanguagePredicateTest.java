package tk.labyrinth.satool.confluenceconnector.model;

import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ConfluenceQueryLanguagePredicateTest {

	@Test
	void testLabels() {
		Assertions.assertEquals(
				"",
				ConfluenceQueryLanguagePredicate.builder()
						.labels(null)
						.build().toString());
		Assertions.assertEquals(
				"",
				ConfluenceQueryLanguagePredicate.builder()
						.labels(List.empty())
						.build().toString());
		Assertions.assertEquals(
				"label = 'a'",
				ConfluenceQueryLanguagePredicate.builder()
						.labels(List.of("a"))
						.build().toString());
		Assertions.assertEquals(
				"label IN ('a','b')",
				ConfluenceQueryLanguagePredicate.builder()
						.labels(List.of("a", "b"))
						.build().toString());
	}
}