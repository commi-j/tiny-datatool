package tk.labyrinth.satool.confluenceconnector.freemarker;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.StringTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.core.XHTMLOutputFormat;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;
import tk.labyrinth.satool.freemarker.FreemarkerConfigurationFactory;

import java.io.IOException;
import java.io.StringWriter;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ConfluenceWidgetsTest {

	private Configuration configuration;

	private StringTemplateLoader stringTemplateLoader;

	private String render(String template) {
		String result;
		{
			stringTemplateLoader.putTemplate("tmp", template);
			Template freemarkerTemplate = IoUtils.unchecked(() -> configuration.getTemplate("tmp"));
			stringTemplateLoader.removeTemplate("tmp");
			configuration.clearTemplateCache();
			//
			try (StringWriter writer = new StringWriter()) {
				freemarkerTemplate.process(null, writer);
				result = writer.toString();
			} catch (IOException | TemplateException ex) {
				throw new RuntimeException(ex);
			}
		}
		return result;
	}

	@BeforeAll
	void beforeAll() {
		{
			stringTemplateLoader = new StringTemplateLoader();
		}
		{
			configuration = FreemarkerConfigurationFactory.createDefault();
			configuration.setOutputFormat(XHTMLOutputFormat.INSTANCE);
			configuration.setTemplateLoader(new MultiTemplateLoader(new TemplateLoader[]{
					stringTemplateLoader,
					new ClassTemplateLoader(Thread.currentThread().getContextClassLoader(), "freemarker")
			}));
			//
			configuration.addAutoInclude("confluence-widgets.ftl");
		}
	}

	@Test
	void testImage() {
		Assertions
				.assertThat(render("<@image filename='foo.bar'/>").replace("\r\n", "\n"))
				.isEqualTo("""
						<ac:image>
							<ri:attachment ri:filename="foo.bar"/>
						</ac:image>
						""");
		Assertions
				.assertThat(render("<@image filename='foo.bar' border=true/>").replace("\r\n", "\n"))
				.isEqualTo("""
						<ac:image ac:border="true">
							<ri:attachment ri:filename="foo.bar"/>
						</ac:image>
						""");
		Assertions
				.assertThat(render("<@image filename='foo.bar' size='S'/>").replace("\r\n", "\n"))
				.isEqualTo("""
						<ac:image ac:height="150">
							<ri:attachment ri:filename="foo.bar"/>
						</ac:image>
						""");
		Assertions
				.assertThat(render("<@image filename='foo.bar' border=true size='L'/>").replace("\r\n", "\n"))
				.isEqualTo("""
						<ac:image ac:border="true" ac:height="400">
							<ri:attachment ri:filename="foo.bar"/>
						</ac:image>
						""");
		ContribAssertions.assertThrows(
				() -> render("<@image/>"),
				fault -> Assertions
						.assertThat(fault.toString())
						.startsWith("java.lang.RuntimeException: freemarker.core._MiscTemplateException: " +
								"When calling macro \"image\", required parameter \"filename\" (parameter #1) was not specified."));
	}
}
