package tk.labyrinth.satool.confluenceconnector.element;

import io.vavr.collection.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClient;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClientFactory;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceGetContentQuery;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;

@SpringBootTest
@Tag("web")
class ConfluenceDocumentTest {

	@Autowired
	private ConfluenceFeignClientFactory confluenceFeignClientFactory;

	@Test
	void testSelect() {
		ConfluenceFeignClient blackdogsClient = confluenceFeignClientFactory.create(
				"https://blackdogs.atlassian.net/wiki");
		//
		ConfluenceContent content = blackdogsClient.getContent(
				null,
				"12525567",
				ConfluenceGetContentQuery.builder()
						.expand("body.storage")
						.build());
		//
		ConfluenceDocument document = ConfluenceDocument.from(content.getBody().getStorage().getValue());
		List<ConfluenceElement> elements = document.select("//structured-macro[@name='tip']");
		//
		Assertions.assertEquals(1, elements.size());
	}

	@Tag("select-on-multiple-root")
	@Test
	void testSelectOnMultipleElementsAtRootLevel() {
		{
			Document jsoupDocument = Jsoup.parse("<tag/><tag/>", Parser.xmlParser());
			Assertions.assertEquals(1, jsoupDocument.selectXpath("//tag").size());
			Assertions.assertEquals(1, jsoupDocument.selectXpath("/tag").size());
		}
		{
			ConfluenceDocument document = ConfluenceDocument.from("<tag/><tag/>");
			Assertions.assertEquals(2, document.select("//tag").size());
			Assertions.assertEquals(2, document.select("/tag").size());
		}
	}
}