package tk.labyrinth.satool.confluenceconnector.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.TestPropertySource;
import tk.labyrinth.satool.confluenceconnector.api.contentchildattachment.ConfluenceContentChildAttachment;
import tk.labyrinth.satool.confluenceconnector.api.contentchildattachment.ConfluencePostContentChildAttachmentResult;

@Disabled("Local/mutating test")
@SpringBootTest
@TestPropertySource("classpath:nocommit.properties")
class ConfluenceConnectorTest {

	@Autowired
	private ConfluenceConnector confluenceConnector;

	@Value("${test.confluencePostAuthorization:#{null}}")
	private String testConfluencePostAuthorization;

	@Value("${test.confluencePostBaseUrl:#{null}}")
	private String testConfluencePostBaseUrl;

	@Value("${test.confluencePostPageId:#{null}}")
	private String testConfluencePostPageId;

	@Test
	void testPostContentChildAttachment() {
		ConfluencePostContentChildAttachmentResult result = confluenceConnector.postContentChildAttachment(
				testConfluencePostBaseUrl,
				testConfluencePostAuthorization,
				testConfluencePostPageId,
				new ClassPathResource("mane-3373385886.jpg"),
				null);
		//
		ConfluenceContentChildAttachment attachment = result.getResults().get(0);
		//
		Assertions.assertEquals("mane-3373385886.jpg", attachment.getTitle());
		Assertions.assertEquals(null, attachment.getVersion().getMessage());
	}
}