# SA Tool Confluence Connector

- /confluence/apiexplorer

## Public Confluence Spaces

- https://templates.atlassian.net/wiki/spaces/RD/overview
- https://confluence.atlassian.com/confeval/confluence-evaluator-resources/confluence-theming-examples-powered-by-confluence

## Useful Links

- https://try.jsoup.org/