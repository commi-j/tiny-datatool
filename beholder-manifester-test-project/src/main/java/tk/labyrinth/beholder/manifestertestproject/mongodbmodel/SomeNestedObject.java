package tk.labyrinth.beholder.manifestertestproject.mongodbmodel;

/**
 * Should not be referred to by root classes.
 */
public class SomeNestedObject {

	String text;
}
