package tk.labyrinth.beholder.manifestertestproject;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class BeholderManifesterTestProjectApplication {
	// empty
}
