package tk.labyrinth.beholder.manifestertestproject.springproperties;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("some-properties")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Lazy
public class SomeProperties {

	String someStringValue;
}
