package tk.labyrinth.beholder.manifestertestproject.mongodbmodel;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class RootDocument {

	SomeEnumeration enumeration;

	Integer id;

	List<SomeItem> items;

	SomeObject object;
}
