package tk.labyrinth.beholder.manifestertestproject.springproperties;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SomePropertiesTest {

	@Autowired
	private SomeProperties someProperties;

	@Test
	void testPropertiesFileHasPrecedenceOverYamlFiles() {
		Assertions.assertThat(someProperties.getSomeStringValue()).isEqualTo("FROM-PROPERTIES");
	}
}