package tk.labyrinth.datatool.pipelines.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class StageAttributeDescription {

	String name;

	String type;
}
