package tk.labyrinth.datatool.pipelines.ui;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import tk.labyrinth.datatool.pipelines.model.PipelineConfiguration;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;

import jakarta.annotation.PostConstruct;
import java.util.Optional;

@PrototypeScopedComponent
public class PipelineConfigurationView extends VerticalLayout {

	private final Tabs tabs = new Tabs();

	private final TextField idField = new TextField("Id");

	@PostConstruct
	private void postConstruct() {
		{
			add(idField);
		}
		{
			add(tabs);
		}
	}

	public void setValue(PipelineConfiguration value) {
		{
			idField.setValue(Optional.ofNullable(value.getId()).orElse(""));
		}
		{
			tabs.removeAll();
			value.getStages().forEach(stage -> tabs.add(new Tab(stage.getDescriptionId())));
		}
	}
}
