package tk.labyrinth.datatool.pipelines.dummy;

import org.springframework.stereotype.Service;
import tk.labyrinth.datatool.pipelines.model.StageAttributeDescription;
import tk.labyrinth.datatool.pipelines.model.StageDescription;
import tk.labyrinth.datatool.pipelines.model.StageHandler;

import java.util.List;

@Service
public class TsvToJsonDummyStageHandler implements StageHandler {

	@Override
	public StageDescription getDescription() {
		return StageDescription.builder()
				.id("tsv-to-json-dummy")
				.inboundAttributes(List.of(
						StageAttributeDescription.builder()
								.name("input")
								.type("tsv")
								.build()))
				.outboundAttributes(List.of(
						StageAttributeDescription.builder()
								.name("result")
								.type("json-array")
								.build()))
				.build();
	}
}
