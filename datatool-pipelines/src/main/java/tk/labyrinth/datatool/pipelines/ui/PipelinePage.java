package tk.labyrinth.datatool.pipelines.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectFactory;
import tk.labyrinth.datatool.pipelines.model.PipelineConfiguration;
import tk.labyrinth.datatool.pipelines.model.PipelinesConstants;
import tk.labyrinth.datatool.pipelines.tools.PipelineConfigurationRegistry;

import javax.annotation.Nullable;
import java.util.Objects;

@RequiredArgsConstructor
@Route(value = "pipelines", layout = PipelinesLayout.class)
public class PipelinePage extends VerticalLayout implements HasUrlParameter<String> {

	private final PipelineConfigurationRegistry pipelineConfigurationRegistry;

	private final ObjectFactory<PipelineConfigurationView> pipelineConfigurationViewFactory;

	private Component resolveComponent(PipelineConfiguration value, String id) {
		Component result;
		if (value != null) {
			PipelineConfigurationView component = pipelineConfigurationViewFactory.getObject();
			component.setValue(value);
			result = component;
		} else {
			result = new Label("Not found: " + id);
		}
		return result;
	}

	@Nullable
	private PipelineConfiguration resolveValue(String id) {
		PipelineConfiguration result;
		if (Objects.equals(id, PipelinesConstants.NEW)) {
			result = PipelineConfiguration.empty();
		} else {
			result = pipelineConfigurationRegistry.find(id);
		}
		return result;
	}

	@Override
	public void setParameter(BeforeEvent event, String parameter) {
		add(resolveComponent(resolveValue(parameter), parameter));
	}
}
