package tk.labyrinth.datatool.pipelines.model;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Builder
@Value
public class PipelineConfiguration {

	String id;

	List<StageConfiguration> stages;

	public static PipelineConfiguration empty() {
		return PipelineConfiguration.builder()
				.stages(List.of())
				.build();
	}
}
