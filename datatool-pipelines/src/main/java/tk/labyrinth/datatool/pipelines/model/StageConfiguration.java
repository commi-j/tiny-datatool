package tk.labyrinth.datatool.pipelines.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class StageConfiguration {

	String descriptionId;
}
