package tk.labyrinth.datatool.pipelines.model;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Builder
@Value
public class StageDescription {

	String id;

	List<StageAttributeDescription> inboundAttributes;

	List<StageAttributeDescription> outboundAttributes;
}
