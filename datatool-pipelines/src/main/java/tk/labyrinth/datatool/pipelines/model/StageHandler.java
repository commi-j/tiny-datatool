package tk.labyrinth.datatool.pipelines.model;

public interface StageHandler {

	StageDescription getDescription();
}
