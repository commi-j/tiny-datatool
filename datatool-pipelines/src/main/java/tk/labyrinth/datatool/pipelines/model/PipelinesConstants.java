package tk.labyrinth.datatool.pipelines.model;

public class PipelinesConstants {

	/**
	 * Reserved identifier to refer to pipeline under creation.
	 */
	public static final String NEW = "new";
}
