package tk.labyrinth.datatool.pipelines.tools;

import org.springframework.stereotype.Component;
import tk.labyrinth.datatool.pipelines.model.PipelineConfiguration;

import javax.annotation.Nullable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

@Component
public class PipelineConfigurationRegistry {

	private final ConcurrentMap<String, PipelineConfiguration> pipelineConfigurations = new ConcurrentHashMap<>();

	@Nullable
	public PipelineConfiguration find(String pipelineConfigurationId) {
		return pipelineConfigurations.get(pipelineConfigurationId);
	}

	public Stream<PipelineConfiguration> getAll() {
		return pipelineConfigurations.values().stream();
	}

	public void store(PipelineConfiguration pipelineConfiguration) {
		pipelineConfigurations.put(pipelineConfiguration.getId(), pipelineConfiguration);
	}
}
