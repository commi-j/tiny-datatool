package tk.labyrinth.datatool.pipelines.dummy;

import org.springframework.stereotype.Service;
import tk.labyrinth.datatool.pipelines.model.StageAttributeDescription;
import tk.labyrinth.datatool.pipelines.model.StageDescription;
import tk.labyrinth.datatool.pipelines.model.StageHandler;

import java.util.List;

@Service
public class RgbDummyStageHandler implements StageHandler {

	@Override
	public StageDescription getDescription() {
		return StageDescription.builder()
				.id("rgb-dummy")
				.inboundAttributes(List.of(
						StageAttributeDescription.builder()
								.name("red")
								.type("colour-component")
								.build(),
						StageAttributeDescription.builder()
								.name("green")
								.type("colour-component")
								.build(),
						StageAttributeDescription.builder()
								.name("blue")
								.type("colour-component")
								.build()))
				.outboundAttributes(List.of(
						StageAttributeDescription.builder()
								.name("result")
								.type("colour")
								.build()))
				.build();
	}
}
