package tk.labyrinth.datatool.pipelines.ui;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.datatool.pipelines.model.PipelineConfiguration;
import tk.labyrinth.datatool.pipelines.model.PipelinesConstants;
import tk.labyrinth.datatool.pipelines.tools.PipelineConfigurationRegistry;

import jakarta.annotation.PostConstruct;
import java.util.Comparator;

@RequiredArgsConstructor
@Route(value = "pipelines", layout = PipelinesLayout.class)
public class PipelinesPage extends HorizontalLayout {

	private final PipelineConfigurationRegistry pipelineConfigurationRegistry;

	@PostConstruct
	private void postConstruct() {
		{
			pipelineConfigurationRegistry.getAll()
					.sorted(Comparator.comparing(PipelineConfiguration::getId))
					.forEach(pipelineConfiguration -> add(new RouterLink(
							pipelineConfiguration.getId(),
							PipelinePage.class,
							pipelineConfiguration.getId())));
		}
		{
			add(new RouterLink("+", PipelinePage.class, PipelinesConstants.NEW));
		}
	}
}
