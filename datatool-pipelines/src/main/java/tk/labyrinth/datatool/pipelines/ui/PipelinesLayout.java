package tk.labyrinth.datatool.pipelines.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import jakarta.annotation.PostConstruct;

public class PipelinesLayout extends CssVerticalLayout implements RouterLayout {

	@PostConstruct
	private void postConstruct() {
		add(new RouterLink("Pipelines", PipelinesPage.class));
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		add((Component) content);
	}
}
