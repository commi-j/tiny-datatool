package tk.labyrinth.datatool.application.perftest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import tk.labyrinth.pandora.telemetry.milestone.MilestoneHandler;
import tk.labyrinth.pandora.telemetry.milestone.SimpleMilestoneHandler;

public class BeholderAspect {

	private static MilestoneHandler milestoneHandler = new SimpleMilestoneHandler();

	@Around("execution(* tk.labyrinth.pandora.datatypes.object.GenericObjectObjectMapperConfigurer.Deserializer.convert(..))")
	public Object aroundWithSpan(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}
}
