package tk.labyrinth.datatool.application;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import tk.labyrinth.beholder.autodocs.confluence.BeholderAutodocsConfluenceModule;
import tk.labyrinth.beholder.backstage.extension.BeholderBackstageExtension;
import tk.labyrinth.datatool.frontend.DatatoolFrontendApplication;
import tk.labyrinth.datatool.pipelines.DatatoolPipelinesConfiguration;
import tk.labyrinth.satool.beholder.entry.BeholderExtensionEntry;
import tk.labyrinth.satool.gerritconnector.GerritConnectorExtension;
import tk.labyrinth.satool.jgit.JGitConfiguration;
import tk.labyrinth.satool.kafman.KafmanExtensionConfiguration;
import tk.labyrinth.satool.mongodbconnector.MongoDbConnectorConfiguration;

@ComponentScan(basePackageClasses = {
		BeholderBackstageExtension.class,
		BeholderExtensionEntry.class,
})
@Import({
		BeholderAutodocsConfluenceModule.class,
		DatatoolFrontendApplication.class,
		DatatoolPipelinesConfiguration.class,
		GerritConnectorExtension.class,
		JGitConfiguration.class,
		KafmanExtensionConfiguration.class,
		MongoDbConnectorConfiguration.class,
})
@RequiredArgsConstructor
@Slf4j
@SpringBootApplication
public class BeholderApplication {

	@Value("${application.build.timestamp}")
	private String applicationBuildTimestamp;

	@PostConstruct
	private void postConstruct() {
		logger.info("application.build.timestamp = {}", applicationBuildTimestamp);
	}

	public static void main(String... args) {
		{
			// There was a problem accessing KafkaConsumer package private constructor from
			// org.springframework.boot.devtools.restart.classloader.RestartClassLoader.
			// https://stackoverflow.com/questions/57750294/class-loader-error-unnamed-module-of-loader-org-springframework-boot-devtools
			// https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using.devtools.restart.disable
			//
			System.setProperty("spring.devtools.restart.enabled", "false");
		}
		{
			SpringApplication.run(BeholderApplication.class, args);
		}
	}
}
