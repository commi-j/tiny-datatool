package tk.labyrinth.datatool.application.perftest;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.telemetry.milestone.attribute.MilestoneAttributeContributor;

import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;

@LazyComponent
public class ObjectMapperAttContributor implements MilestoneAttributeContributor {

	public static final AttributeKey<String> FROM_ATTRIBUTE_KEY = AttributeKey.stringKey("from");

	public static final AttributeKey<String> TO_ATTRIBUTE_KEY = AttributeKey.stringKey("to");

	@Override
	public Attributes contributeAttributes(Method method, @Nullable Object target, List<Object> arguments) {
		Attributes result;
		{
			if (Objects.equals(method.getName(), "convertValue") &&
					method.getDeclaringClass() == ObjectMapper.class) {
				result = Attributes.of(
						FROM_ATTRIBUTE_KEY, Optional.ofNullable(arguments.get(0))
								.map(argument -> argument.getClass().toString())
								.orElse("null"),
						TO_ATTRIBUTE_KEY, String.valueOf(arguments.get(1)));
			} else {
				result = Attributes.empty();
			}
		}
		return result;
	}
}
