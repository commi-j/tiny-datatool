package tk.labyrinth.datatool.application;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import tk.labyrinth.datatool.pipelines.model.PipelineConfiguration;
import tk.labyrinth.datatool.pipelines.model.StageConfiguration;
import tk.labyrinth.datatool.pipelines.tools.PipelineConfigurationRegistry;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DummyPipelineRunner implements ApplicationRunner {

	private final PipelineConfigurationRegistry pipelineConfigurationRegistry;

	@Override
	public void run(ApplicationArguments args) {
		pipelineConfigurationRegistry.store(PipelineConfiguration.builder()
				.id("dummy-dict")
				.stages(List.of(
						StageConfiguration.builder()
								.descriptionId("tsv-to-json-dummy")
								.build(),
						StageConfiguration.builder()
								.descriptionId("json-jq")
								.build()))
				.build());
	}
}
