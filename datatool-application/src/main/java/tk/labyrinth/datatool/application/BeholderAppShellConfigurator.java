package tk.labyrinth.datatool.application;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.theme.Theme;
import org.springframework.stereotype.Component;

@Component
@CssImport("./style/pandora.css")
@CssImport("./style/pandora-lumo.css")
@CssImport("./style/pandora-colours.css")
@CssImport("./style/satool.css")
@Push
@Theme("pandora-theme")
public class BeholderAppShellConfigurator implements AppShellConfigurator {

	@Override
	public void configurePage(AppShellSettings settings) {
		// no-op
	}
}
