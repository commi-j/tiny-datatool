package tk.labyrinth.datatool.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JavaBaseTypeObjectMapperTest {

	/**
	 * Value with which they are not failed within Gitlab CI.
	 */
	public static final long LOW_CONVERSION_TIME = 30;

	private List<GenericObject> genericObjects;

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	private List<JavaBaseType> javaBaseTypes;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeAll
	void beforeAll() {
		javaBaseTypes = javaBaseTypeRegistry.getJavaBaseTypeEntries();
		//
		Assertions.assertThat(javaBaseTypes.size()).isEqualTo(204);
		//
		genericObjects = objectMapper.convertValue(
				javaBaseTypes,
				objectMapper.getTypeFactory().constructParametricType(List.class, GenericObject.class));
	}

	@Test
	void testConvertValueDurationForGenericObjectListToJavaBaseTypeList() {
		long startedAt = System.currentTimeMillis();
		objectMapper.convertValue(
				genericObjects,
				objectMapper.getTypeFactory().constructParametricType(List.class, JavaBaseType.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		Assertions.assertThat(duration).isBetween(0L, LOW_CONVERSION_TIME);
	}

	@Test
	void testConvertValueDurationForGenericObjectToJavaBaseType() {
		long startedAt = System.currentTimeMillis();
		genericObjects.forEach(genericObject -> objectMapper.convertValue(genericObject, JavaBaseType.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		Assertions.assertThat(duration).isBetween(0L, LOW_CONVERSION_TIME);
	}

	@Test
	void testConvertValueDurationForJavaBaseTypeListToGenericObjectList() {
		long startedAt = System.currentTimeMillis();
		objectMapper.convertValue(
				javaBaseTypes,
				objectMapper.getTypeFactory().constructParametricType(List.class, GenericObject.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		Assertions.assertThat(duration).isBetween(0L, LOW_CONVERSION_TIME);
	}

	@Test
	void testConvertValueDurationForJavaBaseTypeToGenericObject() {
		long startedAt = System.currentTimeMillis();
		javaBaseTypes.forEach(javaBaseType -> objectMapper.convertValue(javaBaseType, GenericObject.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		Assertions.assertThat(duration).isBetween(0L, LOW_CONVERSION_TIME);
	}
}