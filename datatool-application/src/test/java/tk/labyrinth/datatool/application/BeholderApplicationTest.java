package tk.labyrinth.datatool.application;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;

import java.time.Duration;

class BeholderApplicationTest {

	@Test
	void testRunApplication() {
		long beforeRun = System.currentTimeMillis();
		//
		SpringApplication.run(BeholderApplication.class);
		//
		long afterRun = System.currentTimeMillis();
		//
		Duration runDuration = Duration.ofMillis(afterRun - beforeRun);
		//
		Assertions.assertThat(runDuration).isLessThan(Duration.ofSeconds(30));
	}
}