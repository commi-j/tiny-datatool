
Use env.properties file to specify properties that could be discovered during local run.
You can include keys similar to the following to include other locations:

- spring.config.import = optional:file:env/dev.properties