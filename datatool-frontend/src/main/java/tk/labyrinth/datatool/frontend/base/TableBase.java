package tk.labyrinth.datatool.frontend.base;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

public abstract class TableBase<T, P> extends CssVerticalLayout {

	private final Grid<T> grid = new Grid<>();

	private P parameters = null;

	protected abstract void configureGrid(Grid<T> grid, P parameters);

	protected abstract List<T> doSearch(P parameters);

	public Component asVaadinComponent() {
		return this;
	}

	public void initialize(P parameters) {
		this.parameters = parameters;
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT);
			grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
			//
			configureGrid(grid, parameters);
			add(grid);
		}
		refresh();
	}

	public void refresh() {
		grid.setItems(doSearch(parameters).asJava());
	}
}
