package tk.labyrinth.datatool.frontend.views.index;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.component.dependency.CssImport;

@Route(value = "index")
@RouteAlias(value = "")
@PageTitle("Index")
public class IndexView extends Div {

    public IndexView() {
        addClassName("index-view");
        add(new Text("Hello World!"));
    }

}
