package tk.labyrinth.datatool.frontend;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import tk.labyrinth.datatool.processing.DatatoolProcessingApplication;
import tk.labyrinth.pandora.ui.PandoraUiModule;

@Import({
		DatatoolProcessingApplication.class,
		PandoraUiModule.class,
})
@SpringBootApplication
public class DatatoolFrontendApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
//    LaunchUtil.launchBrowserInDevelopmentMode(SpringApplication.run(DatatoolFrontendApplication.class, args));
	}
}
