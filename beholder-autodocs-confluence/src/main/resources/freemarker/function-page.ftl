<@layout>
	<@page_header tocLevel=2/>
	<@section_single>
		<h1>Contract</h1>
		<@details>
			<#assign firstCoordinates = function.coordinateses[0]/>
			<table>
				<tbody>
					<tr>
						<th>
							<p>01.01</p>
							<p>Code</p>
						</th>
						<td>-</td>
					</tr>
					<tr>
						<th>
							<p>01.02</p>
							<p>Name</p>
						</th>
						<td>${function.name!"NULL"}</td>
					</tr>
					<tr>
						<th>
							<p>01.03</p>
							<p>Component</p>
						</th>
						<td>TODO</td>
					</tr>
					<tr>
						<th>
							<p>02.01</p>
							<p>Method</p>
						</th>
						<td>
							<#list function.coordinateses as coordinates>
								<p>${coordinates.method!"NULL"}</p>
							</#list>
						</td>
					</tr>
					<tr>
						<th>
							<p>02.02</p>
							<p>Full Path</p>
						</th>
						<td>
							<#list function.coordinateses as coordinates>
								<p>${coordinates.path!"NULL"}</p>
							</#list>
						</td>
					</tr>
					<tr>
						<th>
							<p>02.03</p>
							<p>Own Path</p>
						</th>
						<td>
							<#list function.coordinateses as coordinates>
								<p>${coordinates.path!"NULL"}</p>
							</#list>
						</td>
					</tr>
					<tr>
						<th>
							<p>02.03</p>
							<p>Access Control</p>
						</th>
						<td>${function.accessControl!"NULL"}</td>
					</tr>
					<tr>
						<th>
							<p>05.01</p>
							<p>Interface</p>
						</th>
						<td>TODO</td>
					</tr>
					<tr>
						<th>
							<p>05.02</p>
							<p>Implementation</p>
						</th>
						<td>${function.javaSignature}</td>
					</tr>
					<tr>
						<th>
							<p>09.01</p>
							<p>Template Version</p>
						</th>
						<td>v0.8.0</td>
					</tr>
				</tbody>
			</table>
		</@details>
		<@line_separator/>
	</@section_single>
	<@section_single>
		<h1>Parameters</h1>
		<h3>Parameters</h3>
		<@object_table_with_kind function.parameters/>
		<#list relevantParametersModels as model>
			<h2>${getLongName(model.javaSignature)}</h2>
			<h3>Contract</h3>
			<ul>
				<li>Signature: ${model.javaSignature}</li>
			</ul>
			<h3>Model</h3>
			<@object_table model.attributes/>
		<#sep>
			<@line_separator/>
		</#list>
		<@line_separator/>
	</@section_single>
	<@section_single>
		<h1>Result</h1>
		<h3>Result</h3>
		<@object_table_with_kind function.result/>
		<#list relevantResultModels as model>
			<h2>${getLongName(model.javaSignature)}</h2>
			<h3>Contract</h3>
			<ul>
				<li>Signature: ${model.javaSignature}</li>
			</ul>
			<h3>Model</h3>
			<@object_table model.attributes/>
			<@line_separator/>
		<#sep>
			<@line_separator/>
		</#list>
		<@line_separator/>
	</@section_single>
	<@page_footer now/>
</@layout>
