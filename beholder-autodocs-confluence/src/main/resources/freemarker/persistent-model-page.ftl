<@layout>
	<@page_header tocLevel=2/>
	<@section_single>
		<h1>Diagram</h1>
		<@diagram kind="PERSISTENT_MODEL_MAP" strategy="PLANT_UML"/>
		<@line_separator/>
		<h1>Model</h1>
		<table>
			<tbody>
				<tr>
					<th>#</th>
					<th>Field Name</th>
					<th>Column Names</th>
					<th>Type</th>
					<th>Comments</th>
				</tr>
				<#list persistentModel.attributes![] as attribute>
					<tr>
						<td>${attribute?counter}</td>
						<td>${attribute.javaFieldName!"NULL"}</td>
						<td><@pretty_list list=attribute.databaseNames!["NULL"]/></td>
						<td>${attribute.javaFieldType!"NULL"}</td>
						<td>
							<#if attribute.primaryKey??>
								<#if attribute.primaryKey = "true">
									Primary Key
								</#if>
							<#else>
								NULL_PK
							</#if>
						</td>
					</tr>
				<#else>
					<tr>
						<td>1</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</#list>
			</tbody>
		</table>
		<hr/>
	</@section_single>
	<@page_footer now/>
</@layout>
