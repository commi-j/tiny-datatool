<@layout>
	<@page_header tocLevel=2/>
	<@section_single>
		<h1>Contract</h1>
		<ul>
			<li>Normalized path: ${restEndpointGroup.normalizedPath}</li>
			<li>Raw path: ${restEndpointGroup.rawPath}</li>
			<li>Signature: ${restEndpointGroup.javaSignature!"NULL"}</li>
		</ul>
		<@line_separator/>
	</@section_single>
	<@section_single>
		<h1>Endpoints</h1>
		<@line_separator/>
	</@section_single>
	<#list restEndpointGroup.endpoints as endpoint>
		<@section_single>
			<#assign firstCoordinates = endpoint.coordinateses[0]/>
			<h2>${firstCoordinates.method!"NULL"} ${firstCoordinates.path!"NULL"}</h2>
			<h3>Contract</h3>
			<ul>
				<li>Full path: ${firstCoordinates.method!"NULL"} ${restEndpointGroup.normalizedPath}${firstCoordinates.path!"NULL"}</li>
				<#if (endpoint.coordinateses?size > 1)>
					<#list endpoint.coordinateses as coordinates>
						<#if (coordinates?index > 0)>
							<li>Alias: ${coordinates.method!"NULL"} ${coordinates.path!"NULL"}</li>
						</#if>
					</#list>
				</#if>
				<li>Signature: ${endpoint.javaSignature!"NULL"}</li>
			</ul>
			<h3>Parameters</h3>
			<@object_table_with_kind endpoint.parameters/>
			<h3>Result</h3>
			<@object_table_with_kind endpoint.result/>
			<@line_separator/>
		</@section_single>
	</#list>
	<@section_single>
		<h1>Models</h1>
		<@line_separator/>
	</@section_single>
	<#list relevantModels as model>
		<@section_single>
			<h2>${getLongName(model.javaSignature)}</h2>
			<h3>Contract</h3>
			<ul>
				<li>Signature: ${model.javaSignature}</li>
			</ul>
			<@object_table model.attributes/>
			<@line_separator/>
		</@section_single>
	</#list>
	<@page_footer now/>
</@layout>
