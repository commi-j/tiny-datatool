<#assign persistentModels = manifest.persistentModels![]>
<@layout>
	<@page_header tocLevel=2/>
	<@section_single>
		<h1>Persistent Models</h1>
		<@line_separator/>
	</@section_single>
	<#list persistentModels as persistentModel>
		<@section_single>
			<h2>${persistentModel.javaClassSignature!"NULL"} (${persistentModel.databaseTableName!"NULL"})</h2>
			<h3>${persistentModel.javaClassSignature!"NULL"}</h3>
			<table>
				<tbody>
					<tr>
						<th>#</th>
						<th>Field Name</th>
						<th>Column Names</th>
						<th>Type</th>
						<th>Comments</th>
					</tr>
					<#list persistentModel.attributes![] as attribute>
						<tr>
							<td>${attribute?counter}</td>
							<td>${attribute.javaFieldName!"NULL"}</td>
							<td><@pretty_list list=attribute.databaseColumnNames!["NULL"]/></td>
							<td>${attribute.javaFieldType!"NULL"}</td>
							<td>
								<#if attribute.primaryKey??>
									<#if attribute.primaryKey = "true">
										Primary Key
									</#if>
								<#else>
									NULL_PK
								</#if>
							</td>
						</tr>
					<#else>
						<tr>
							<td>1</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</#list>
				</tbody>
			</table>
			<hr/>
		</@section_single>
	</#list>
	<@page_footer now/>
</@layout>
