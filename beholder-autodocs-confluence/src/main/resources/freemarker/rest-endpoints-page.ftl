<#assign restControllers = manifest.restControllers![]>
<@layout>
	<@page_header tocLevel=3/>
	<@section_single>
		<h1>REST Endpoints</h1>
	</@section_single>
	<#list restControllers as restController>
		<@section_single>
			<h2>${restController.path!"NULL"}</h2>
			<ul>
				<li>${restController.signature!"NULL"}</li>
			</ul>
		</@section_single>
		<#list restController.methods as requestMappingMethod>
			<@section_single>
				<h3>${requestMappingMethod.method!"NULL"} <@pretty_list list=requestMappingMethod.paths!["NULL"]/></h3>
				<ul>
					<li>${requestMappingMethod.signature!"NULL"}</li>
				</ul>
				<h4>Parameters</h4>
				<#if requestMappingMethod.parameters??>
					<#if requestMappingMethod.parameters?size>0>
						<table>
							<tbody>
								<tr>
									<th>#</th>
									<th>Kind</th>
									<th>Name</th>
									<th>Type</th>
									<th>Comments</th>
								</tr>
								<#list requestMappingMethod.parameters as parameter>
									<tr>
										<td>${parameter?counter}</td>
										<td>${parameter.kind!"NULL"}</td>
										<td>${parameter.name!"NULL"}</td>
										<td>${parameter.datatype!"NULL"}</td>
										<td>
											<#if parameter.required??>
												<#if parameter.required == "true">
													Required
												</#if>
											</#if>
										</td>
									</tr>
								</#list>
							</tbody>
						</table>
					<#else>
						None
					</#if>
				<#else>
					NULL
				</#if>
				<h4>Result</h4>
				TODO
				<hr/>
			</@section_single>
		</#list>
	</#list>
	<@page_footer now/>
</@layout>
