<@layout>
	<@page_header tocLevel=2/>
	<@section_single>
		<h1>Non-declared Properties</h1>
		TODO
		<@line_separator/>
	</@section_single>
	<@section_single>
		<h1>All Properties</h1>
	</@section_single>
	<#list manifest.properties as property>
		<@section_single>
			<h2>${property.name}</h2>
			<h3>Declarations</h3>
			<ul>
				<#list property.declarationEntries as declarationEntry>
					<li>Location: ${declarationEntry.location}</li>
				<#else>
					<li>None</li>
				</#list>
			</ul>
			<h3>Usages</h3>
			<ul>
				<#list property.usageEntries as usageEntry>
					<li>Location: ${usageEntry.location}</li>
				<#else>
					<li>None</li>
				</#list>
			</ul>
			<@diagram kind="PROPERTY_MAP" strategy="PLANT_UML"/>
			<@line_separator/>
		</@section_single>
	</#list>
	<@page_footer now/>
</@layout>
