<@layout>
	<@page_header tocLevel=2/>
	<@section_single>
		<h1>Diagram</h1>
		<@diagram kind="PERSISTENT_MODEL_MAP_ALL" strategy="PNG" filenamePattern="diagram"/>
		<@line_separator/>
	</@section_single>
	<@page_footer now/>
</@layout>
