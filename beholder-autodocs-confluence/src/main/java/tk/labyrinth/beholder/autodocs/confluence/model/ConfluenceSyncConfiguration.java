package tk.labyrinth.beholder.autodocs.confluence.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.DefinitionArtifactManifestReference;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceSyncConfiguration {

	DefinitionArtifactManifestReference artifactManifestReference;

	ConfluenceSyncParty confluenceParty;

	// Per-endpoint page - multi-selector & endpoint-id;
	// Per-controller page - multi-selector & controller-id;
	// Per-manifest page - single-selector;
	//
	// Label to determine endpoint page (update);
	// Content attribute to determine endpoint (update);
	// Rule to place new endpoint (create);
	Object endpointsConfiguration;
}
