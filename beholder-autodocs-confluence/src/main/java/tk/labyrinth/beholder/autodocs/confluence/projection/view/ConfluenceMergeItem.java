package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeNode;

import java.util.Objects;
import java.util.UUID;
import java.util.function.UnaryOperator;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceMergeItem {

	@Nullable
	ConfluenceMergeItemCheckResult checkResult;

	@NonNull
	List<ConfluenceMergeItem> children;

	@Nullable
	String computedPageLocator;

	@Nullable
	String computedPageTitle;

	@Deprecated // replace with exact values.
	ConfluenceMergeNode node;

	@Nullable
	UUID ongoingCheckUid;

	@NonNull
	List<String> path;

	GenericObject variableObject;

	@Nullable
	public ConfluenceMergeItem find(List<String> path) {
		return Objects.equals(this.path, path)
				? this
				: matches(this.path, path)
				? children.
				map(child -> child.find(path))
				.filter(Objects::nonNull)
				.getOrNull()
				: null;
	}

	public List<ConfluenceMergeItem> flatten() {
		return List.of(this).appendAll(children.flatMap(ConfluenceMergeItem::flatten));
	}

	public ConfluenceMergeItem replace(ConfluenceMergeItem item) {
		return update(item.getPath(), currentItem -> item);
	}

	public ConfluenceMergeItem update(List<String> path, UnaryOperator<ConfluenceMergeItem> updateOperator) {
		return Objects.equals(path, this.path)
				? updateOperator.apply(this)
				: withChildren(children.map(child -> matches(child.getPath(), path)
				? child.update(path, updateOperator)
				: child));
	}

	public ConfluenceMergeItem updateRecursively(UnaryOperator<ConfluenceMergeItem> updateOperator) {
		return updateOperator.apply(withChildren(children.map(child -> child.updateRecursively(updateOperator))));
	}

	public static boolean matches(List<String> first, List<String> second) {
		return first.size() >= second.size()
				? Objects.equals(first.subSequence(0, second.size()), second)
				: Objects.equals(first, second.subSequence(0, first.size()));
	}
}
