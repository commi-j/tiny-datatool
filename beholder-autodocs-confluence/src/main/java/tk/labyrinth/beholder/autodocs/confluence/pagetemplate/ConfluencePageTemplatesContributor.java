package tk.labyrinth.beholder.autodocs.confluence.pagetemplate;

import io.vavr.collection.List;
import org.apache.commons.io.IOUtils;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectcontributor.ObjectContributor;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.ConfluencePageTemplate;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@LazyComponent
public class ConfluencePageTemplatesContributor implements ObjectContributor<ConfluencePageTemplate> {

	@Override
	public UUID computeUid(ConfluencePageTemplate object) {
		return RootObjectUtils.computeUidFromValueHashCode(object.getKey());
	}

	@Override
	public List<ConfluencePageTemplate> contributeObjects() {
		return List
				.of(
						"empty-page",
						"function-page",
						"persistent-model-page",
						"persistent-models-diagram-page",
						"persistent-models-diagram-page-png",
						"persistent-models-page",
						"properties-page",
						"properties-page-png",
						"properties-page-svg",
						"rest-endpoint-group-page",
						"rest-endpoint-page",
						"rest-endpoints-page")
				.map(key -> ConfluencePageTemplate.builder()
						.bodyTemplate(IoUtils.unchecked(() -> IOUtils.resourceToString(
								"freemarker/%s.ftl".formatted(key),
								StandardCharsets.UTF_8,
								ClassLoader.getSystemClassLoader())))
						.key(key)
						.build());
	}
}
