package tk.labyrinth.beholder.autodocs.confluence;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.satool.beholder.BeholderManifesterExtension;

@Import({
		BeholderManifesterExtension.class,
})
@Slf4j
@SpringBootApplication
public class BeholderAutodocsConfluenceModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
