package tk.labyrinth.beholder.autodocs.confluence.projection.endpoint;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.expresso.lang.predicate.Predicate;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluencePageSignatureResolver {

	Predicate pageSelector;

	String uidResolver;
}
