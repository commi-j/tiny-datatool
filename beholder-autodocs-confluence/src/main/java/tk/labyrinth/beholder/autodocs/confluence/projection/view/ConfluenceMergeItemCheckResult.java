package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceMergeItemCheckResult {

	@Nullable
	String pageId;

	@Nullable
	String pageTitle;

	@Nullable
	String parentPageId;

	@NonNull
	ConfluenceMergeItemStatus status;
}
