package tk.labyrinth.beholder.autodocs.confluence.projection.view;

public enum ConfluenceMergeItemStatus {
	ERROR,
	NOT_CHECKED,
	NOT_EXISTS,
	EXISTS,
	OUTDATED,
	UP_TO_DATE,
}
