package tk.labyrinth.beholder.autodocs.confluence.projection.endpoint;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicQualifier;

@Builder(builderClassName = "Builder", toBuilder = true)
@PolymorphicQualifier(rootClass = EndpointProjectionConfiguration.class, qualifierAttributeValue = "single-page")
@Value
@With
public class SinglePageEndpointProjectionConfiguration implements EndpointProjectionConfiguration {

	Predicate pageSelector;
}