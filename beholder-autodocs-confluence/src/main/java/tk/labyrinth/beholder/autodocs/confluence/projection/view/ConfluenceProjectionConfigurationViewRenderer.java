package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.val;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConfiguration;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConnectConfiguration;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.component.typedobject.TypedObjectEditorDialogHandler;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.DefinitionArtifactManifestReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.CodeConfluenceMergeStructureReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeNode;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeStructure;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class ConfluenceProjectionConfigurationViewRenderer {

	private final TypedObjectEditorDialogHandler<ConfluenceMergeNode> confluenceMergeNodeEditorDialogHandler;

	private final ConfluenceMergeItemsViewRenderer mergeItemsViewRenderer;

	private final ValueBoxRegistry valueBoxRegistry;

	private final ConfluenceProjectionConfigurationWorker worker;

	@SmartAutowired
	private TypedObjectSearcher<ConfluenceMergeStructure> confluenceMergeStructureSearcher;

	private Component renderActionsLayout(Parameters parameters, ConfluenceMergeNode item) {
		// TODO
//		return ObjectUtils.consumeAndReturn(
//				new CssHorizontalLayout(),
//				actionLayout -> actionLayout.add(
//						ButtonRenderer.render(builder -> builder
//								.icon(VaadinIcon.PLUS.create())
//								.onClick(event -> parameters.onValueChange().accept(
//										parameters.currentValue().updateNested(
//												item,
//												item.withChildren(item.getChildrenOrEmpty()
//														.append(ConfluenceMergeNode.builder().build())))))
//								.themeVariants(ButtonVariant.LUMO_SMALL)
//								.build()),
//						ButtonRenderer.render(builder -> builder
//								.icon(VaadinIcon.EDIT.create())
//								.onClick(event -> confluenceMergeNodeEditorDialogHandler
//										.showDialog(TypedObjectEditorDialogHandler.Parameters
//												.<ConfluenceMergeNode>builder()
//												.currentValue(item)
//												.initialValue(item)
//												.javaType(ConfluenceMergeNode.class)
//												.build())
//										.subscribeAlwaysAccepted(value -> parameters.onValueChange()
//												.accept(parameters.currentValue().updateNested(item, value))))
//								.themeVariants(ButtonVariant.LUMO_SMALL)
//								.build()),
//						ButtonRenderer.render(builder -> builder
//								.icon(VaadinIcon.MINUS.create())
//								.onClick(event -> parameters.onValueChange()
//										.accept(parameters.currentValue().updateNested(item, null)))
//								.themeVariants(ButtonVariant.LUMO_SMALL)
//								.build())));
		return new Div();
	}

	public Component render(Function<Parameters.Builder, Parameters> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Parameters.builder()));
	}

	@SuppressWarnings("unchecked")
	public Component render(Parameters parameters) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setHeightFull();
		}
		{
			ConfluenceMergeStructure mergeStructure = Optional
					.ofNullable(parameters.currentValue().getMergeStructureReference())
					.map(confluenceMergeStructureSearcher::findSingle)
					.orElse(null);
			//
			Observable<Boolean> selectionIsEmptyObservable = Observable.withInitialValue(true);
			Observable<Instant> checkObservable = Observable.notInitialized();
			Observable<Instant> publishObservable = Observable.notInitialized();
			//
			{
				CssHorizontalLayout header = new CssHorizontalLayout();
				{
					header.addClassNames(PandoraStyles.LAYOUT);
					//
					header.setAlignItems(AlignItems.BASELINE);
				}
				{
					{
						val artifactManifestReferenceBox = (MutableValueBox<DefinitionArtifactManifestReference>)
								valueBoxRegistry.getValueBox(ClassUtils.getDeclaredFieldType(
										ConfluenceProjectionConfiguration.class,
										ConfluenceProjectionConfiguration.ARTIFACT_MANIFEST_REFERENCE_ATTRIBUTE_NAME));
						//
						artifactManifestReferenceBox.render(builder -> builder
								.currentValue(parameters.currentValue().getArtifactManifestReference())
								.initialValue(parameters.initialValue().getArtifactManifestReference())
								.label("Artifact Manifest")
								.onValueChange(nextValue -> parameters.onValueChange()
										.accept(parameters.currentValue().withArtifactManifestReference(nextValue)))
								.build());
						//
						header.add(artifactManifestReferenceBox.asVaadinComponent());
					}
					{
						val artifactManifestReferenceBox = (MutableValueBox<ConfluenceProjectionConnectConfiguration>)
								valueBoxRegistry.getValueBox(ClassUtils.getDeclaredFieldType(
										ConfluenceProjectionConfiguration.class,
										ConfluenceProjectionConfiguration.CONNECT_CONFIGURATION_ATTRIBUTE_NAME));
						//
						artifactManifestReferenceBox.render(builder -> builder
								.currentValue(parameters.currentValue().getConnectConfiguration())
								.initialValue(parameters.initialValue().getConnectConfiguration())
								.label("Connect Configuration")
								.onValueChange(nextValue -> parameters.onValueChange()
										.accept(parameters.currentValue().withConnectConfiguration(nextValue)))
								.build());
						//
						header.add(artifactManifestReferenceBox.asVaadinComponent());
					}
					{
						val variablesBox = (MutableValueBox<List<GenericObjectAttribute>>)
								valueBoxRegistry.getValueBox(ClassUtils.getDeclaredFieldType(
										ConfluenceProjectionConfiguration.class,
										ConfluenceProjectionConfiguration.VARIABLES_ATTRIBUTE_NAME));
						//
						variablesBox.render(builder -> builder
								.currentValue(parameters.currentValue().getVariables())
								.initialValue(parameters.initialValue().getVariables())
								.label("Variables")
								.onValueChange(nextValue -> parameters.onValueChange()
										.accept(parameters.currentValue().withVariables(nextValue)))
								.build());
						//
						header.add(variablesBox.asVaadinComponent());
					}
					{
						val mergeStructureReferenceBox = (MutableValueBox<CodeConfluenceMergeStructureReference>)
								valueBoxRegistry.getValueBox(ClassUtils.getDeclaredFieldType(
										ConfluenceProjectionConfiguration.class,
										ConfluenceProjectionConfiguration.MERGE_STRUCTURE_REFERENCE_ATTRIBUTE_NAME));
						//
						mergeStructureReferenceBox.render(builder -> builder
								.currentValue(parameters.currentValue().getMergeStructureReference())
								.initialValue(parameters.initialValue().getMergeStructureReference())
								.label("Merge Structure")
								.onValueChange(nextValue -> parameters.onValueChange()
										.accept(parameters.currentValue().withMergeStructureReference(nextValue)))
								.build());
						//
						header.add(mergeStructureReferenceBox.asVaadinComponent());
					}
					header.add(ButtonRenderer.render(builder -> builder
							.enabled(!Objects.equals(parameters.currentValue(), parameters.initialValue()))
//							.enabled(false)
							.onClick(event -> parameters.onSave().run())
							.text("Save")
							.themeVariants(ButtonVariant.LUMO_PRIMARY)
							.build()));
					//
					header.addStretch();
					//
					{
						Button checkButton = ButtonRenderer.render(builder -> builder
								.onClick(event -> checkObservable.set(Instant.now()))
								.text("Check")
								.themeVariants(ButtonVariant.LUMO_PRIMARY)
								.build());
						//
						selectionIsEmptyObservable.subscribe(next -> checkButton.setEnabled(!next));
						//
						header.add(checkButton);
					}
					{
						Button publishButton = ButtonRenderer.render(builder -> builder
								.onClick(event -> publishObservable.set(Instant.now()))
								.text("Publish")
								.themeVariants(ButtonVariant.LUMO_PRIMARY)
								.build());
						//
						checkObservable.subscribe(next -> publishButton.setEnabled(!selectionIsEmptyObservable.get()));
						selectionIsEmptyObservable.subscribe(next -> publishButton.setEnabled(
								checkObservable.initialized() && !next));
						//
						header.add(publishButton);
					}
				}
				layout.add(header);
			}
			{
				if (mergeStructure != null) {
					layout.add(mergeItemsViewRenderer.render(builder -> builder
							.checkTreeFlux(checkObservable.getFlux())
							.projectionConfiguration(parameters.currentValue())
							.publishTreeFlux(publishObservable.getFlux())
							.onSelectionIsEmptyChange(selectionIsEmptyObservable::set)
							.build()));
				} else {
					layout.add("NO MERGE STRUCTURE");
				}
			}
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		Boolean checked;

		@NonNull
		ConfluenceProjectionConfiguration currentValue;

		@NonNull
		ConfluenceProjectionConfiguration initialValue;

		Runnable onSave;

		@NonNull
		Consumer<ConfluenceProjectionConfiguration> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}