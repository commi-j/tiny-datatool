package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.core.task.TaskExecutor;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConfiguration;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Height;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateQueryAttribute;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFetcher;

import java.util.UUID;
import java.util.function.Consumer;

@RequiredArgsConstructor
@Route("confluence-projection-configuration")
@Slf4j
public class ConfluenceProjectionConfigurationPage extends
		FunctionalPage<ConfluenceProjectionConfigurationPage.Parameters> {

	private final ConfluenceFetcher confluenceFetcher;

	private final ConfluenceProjectionConfigurationViewRenderer confluenceProjectionConfigurationViewRenderer;

	private final TaskExecutor taskExecutor;

	@SmartAutowired
	private TypedObjectManipulator<ConfluenceProjectionConfiguration> confluenceProjectionConfigurationManipulator;

	@SmartAutowired
	private TypedObjectSearcher<ConfluenceProjectionConfiguration> confluenceProjectionConfigurationSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@PostConstruct
	private void postConstruct() {
		getContent().setHeightFull();
	}

	private Component renderObject(UidReference<ConfluenceProjectionConfiguration> reference) {
		Component result;
		{
			ConfluenceProjectionConfiguration value = confluenceProjectionConfigurationSearcher.findSingle(reference);
			//
			if (value != null) {
				result = FunctionalComponent.create(
						Pair.of(value, value),
						(parameters, sink) -> confluenceProjectionConfigurationViewRenderer.render(builder -> builder
								.currentValue(parameters.getRight())
								.initialValue(parameters.getLeft())
								.onSave(() -> {
									confluenceProjectionConfigurationManipulator.update(parameters.getRight());
									//
									sink.accept(Pair.of(parameters.getRight(), parameters.getRight()));
								})
								.onValueChange(nextValue -> sink.accept(Pair.of(parameters.getLeft(), nextValue)))
								.build()));
				//
				StyleUtils.setCssProperty(result, Height.HUNDRED_PERCENT);
			} else {
				result = new Span("Object not found: %s".formatted(reference));
			}
		}
		return result;
	}

	@Override
	protected Parameters getInitialProperties() {
		return Parameters.builder()
				.uid(null)
				.build();
	}

	@Override
	protected Component render(Parameters parameters, Consumer<Parameters> sink) {
		Component result;
		{
			UUID uid = parameters.uid();
			//
			if (uid != null) {
				UidReference<ConfluenceProjectionConfiguration> reference = UidReference.of(
						ConfluenceProjectionConfiguration.MODEL_CODE,
						uid);
				//
				result = renderObject(reference);
			} else {
				result = new Span("No uid specified.");
			}
		}
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@UiStateQueryAttribute
		UUID uid;
	}
}
