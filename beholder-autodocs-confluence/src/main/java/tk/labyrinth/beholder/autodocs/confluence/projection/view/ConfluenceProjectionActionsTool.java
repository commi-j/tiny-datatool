package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.functionalcomponents.vaadin.TextAreaRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.ConfluencePublisher;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.ConfluenceRenderedPage;

import java.time.Instant;

@LazyComponent
@RequiredArgsConstructor
public class ConfluenceProjectionActionsTool {

	private final ConfluencePublisher confluencePublisher;

	public void showRenderedBodyDialog(ConfluenceMergeItem mergeItem) {
		ConfluenceRenderedPage renderedPage = confluencePublisher.renderPage(
				mergeItem.getNode().getBodyTemplateReference(),
				mergeItem.getVariableObject()
						//
						// FIXME: Unify with publishing mechanism.
						.withAddedAttribute(GenericObjectAttribute.ofSimple("now", Instant.now().toString()))
						.unwrap());
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setWidth("50em");
		}
		{
			layout.add(TextAreaRenderer.render(builder -> builder
					.label("Content")
					.readOnly(true)
					.value(renderedPage.getContent())
					.build()));
			layout.add("Attachments: %s".formatted(renderedPage.getAttachments().size()));
		}
		Dialogs.show(layout);
	}
}
