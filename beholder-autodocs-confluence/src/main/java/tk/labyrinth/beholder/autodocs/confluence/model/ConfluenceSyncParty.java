package tk.labyrinth.beholder.autodocs.confluence.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceSyncParty {

	KeyCredentialsReference credentialsReference;

	Object layoutTemplate;

	String rootPageId;
}
