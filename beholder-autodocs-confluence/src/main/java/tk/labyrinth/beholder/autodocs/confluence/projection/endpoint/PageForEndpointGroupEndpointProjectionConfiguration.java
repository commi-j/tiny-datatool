package tk.labyrinth.beholder.autodocs.confluence.projection.endpoint;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicQualifier;

@Builder(builderClassName = "Builder", toBuilder = true)
@PolymorphicQualifier(
		rootClass = EndpointProjectionConfiguration.class,
		qualifierAttributeValue = "page-for-endpoint-group")
@Value
@With
public class PageForEndpointGroupEndpointProjectionConfiguration implements EndpointProjectionConfiguration {

	String endpointSignatureResolver;

	ConfluencePageSignatureResolver pageSignatureResolver;
}