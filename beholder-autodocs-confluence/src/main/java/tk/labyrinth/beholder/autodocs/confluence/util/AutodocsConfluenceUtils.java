package tk.labyrinth.beholder.autodocs.confluence.util;

import io.vavr.collection.List;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceQueryLanguagePredicate;

public class AutodocsConfluenceUtils {

	public static ConfluenceQueryLanguagePredicate predicateToConfluenceQueryLanguagePredicate(Predicate predicate) {
		ConfluenceQueryLanguagePredicate result;
		{
			if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
				result = switch (objectPropertyPredicate.property()) {
					case "pageId" -> {
						ConfluenceQueryLanguagePredicate localResult;
						{
							if (objectPropertyPredicate.operator() == ObjectOperator.EQUAL_TO) {
								localResult = ConfluenceQueryLanguagePredicate.builder()
										.ids(List.of(objectPropertyPredicate.value().toString()))
										.build();
							} else {
								throw new NotImplementedException();
							}
						}
						yield localResult;
					}
					case "title" -> {
						ConfluenceQueryLanguagePredicate localResult;
						{
							if (objectPropertyPredicate.operator() == ObjectOperator.EQUAL_TO) {
								localResult = ConfluenceQueryLanguagePredicate.builder()
										.titles(List.of(objectPropertyPredicate.value().toString()))
										.build();
							} else {
								throw new NotImplementedException();
							}
						}
						yield localResult;
					}
					default -> throw new NotImplementedException();
				};
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
