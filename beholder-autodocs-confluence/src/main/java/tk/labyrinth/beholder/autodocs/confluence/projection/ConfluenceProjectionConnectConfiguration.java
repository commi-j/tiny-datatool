package tk.labyrinth.beholder.autodocs.confluence.projection;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderPattern("$baseUrl : $credentialsReference")
@Value
@With
public class ConfluenceProjectionConnectConfiguration {

	String baseUrl;

	KeyCredentialsReference credentialsReference;
}
