package tk.labyrinth.beholder.autodocs.confluence.projection;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.stores.rootobject.HasUid;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.DefinitionArtifactManifestReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.CodeConfluenceMergeStructureReference;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(ConfluenceProjectionConfiguration.MODEL_CODE)
@ModelTag("confluence")
@Value
@With
public class ConfluenceProjectionConfiguration implements HasUid {

	public static final String ARTIFACT_MANIFEST_REFERENCE_ATTRIBUTE_NAME = "artifactManifestReference";

	public static final String CONNECT_CONFIGURATION_ATTRIBUTE_NAME = "connectConfiguration";

	public static final String MERGE_STRUCTURE_REFERENCE_ATTRIBUTE_NAME = "mergeStructureReference";

	public static final String MODEL_CODE = "confluenceprojectionconfiguration";

	public static final String VARIABLES_ATTRIBUTE_NAME = "variables";

	DefinitionArtifactManifestReference artifactManifestReference;

	ConfluenceProjectionConnectConfiguration connectConfiguration;

	CodeConfluenceMergeStructureReference mergeStructureReference;

	Boolean temporal;

	UUID uid;

	// TODO: List<Pair<String,String>> when we support Pair in Jackson.
	List<GenericObjectAttribute> variables;

	public List<GenericObjectAttribute> getVariablesOrEmpty() {
		return variables != null ? variables : List.empty();
	}
}
