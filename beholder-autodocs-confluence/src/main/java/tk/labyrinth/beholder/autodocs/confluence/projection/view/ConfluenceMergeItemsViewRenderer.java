package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import reactor.core.publisher.Flux;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConfiguration;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConnectConfiguration;
import tk.labyrinth.pandora.functionalcomponents.advanced.TreeGridWrapper;
import tk.labyrinth.pandora.functionalcomponents.html.AnchorRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.EnumUtils;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class ConfluenceMergeItemsViewRenderer {

	private final ConfluenceProjectionActionsTool actionsTool;

	private final ConfluenceProjectionConfigurationWorker worker;
//	private Component renderCheckAction(
//			ConfluenceProjectionConnectConfiguration connectConfiguration,
//			ConfluenceMergeItem item,
//			Runnable callback) {
//		String tooltipText;
//		{
//			if (item.getCheckResult() == null) {
//				tooltipText = "Check required";
//			} else if (!EnumUtils.in(
//					item.getCheckResult().getStatus(),
//					ConfluenceMergeItemStatus.EXISTS, ConfluenceMergeItemStatus.NOT_EXISTS)) {
//				tooltipText = "Parent pages required";
//			} else {
//				tooltipText = null;
//			}
//		}
//		//
//		return ButtonRenderer.render(builder -> builder
//				.configurer(button -> {
//				})
//				.disableOnClick(true)
//				.enabled(item.getOngoingCheckUid() == null)
//				.onClick(event -> {
//					// TODO
////					worker.publish(connectConfiguration, item, item);
//					//
//					callback.run();
//				})
//				.text("Check All")
//				.themeVariants(ButtonVariant.LUMO_SMALL)
//				.tooltipText(tooltipText)
//				.build());
//	}

	private Component renderPublishAction(
			ConfluenceProjectionConnectConfiguration connectConfiguration,
			ConfluenceMergeItem item,
			Runnable callback) {
		String tooltipText;
		{
			if (item.getCheckResult() == null) {
				tooltipText = "Check required";
			} else if (!EnumUtils.in(
					item.getCheckResult().getStatus(),
					ConfluenceMergeItemStatus.EXISTS, ConfluenceMergeItemStatus.NOT_EXISTS)) {
				tooltipText = "Parent pages required";
			} else {
				tooltipText = null;
			}
		}
		//
		return ButtonRenderer.render(builder -> builder
				.configurer(button -> {
				})
				.disableOnClick(true)
				.enabled((item.getNode().getTitleTemplate() != null ||
						item.getNode().getBodyTemplateReference() != null) &&
						item.getCheckResult() != null &&
						EnumUtils.in(
								item.getCheckResult().getStatus(),
								ConfluenceMergeItemStatus.EXISTS, ConfluenceMergeItemStatus.NOT_EXISTS))
				.onClick(event -> {
					worker.publish(connectConfiguration, item);
					//
					callback.run();
				})
				.text("Publish")
				.themeVariants(ButtonVariant.LUMO_SMALL)
				.tooltipText(tooltipText)
				.build());
	}

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		ConfluenceProjectionConfiguration projectionConfiguration = parameters.projectionConfiguration();
		//
		Observable<List<ConfluenceMergeItem>> itemsObservable =
				Observable.withInitialValue(worker.init(projectionConfiguration));
		//
		parameters.checkTreeFlux().subscribe(next -> {
			List<ConfluenceMergeItem> updatingRootItem = itemsObservable.get()
					.map(rootItem -> rootItem.updateRecursively(item -> item.withOngoingCheckUid(UUID.randomUUID())));
			//
			itemsObservable.set(updatingRootItem);
			//
			worker
					.initCheck(
							parameters.projectionConfiguration().getConnectConfiguration(),
							updatingRootItem.flatMap(ConfluenceMergeItem::flatten),
							FunctionUtils::throwUnreachableStateException)
					.subscribe(updatedItem -> itemsObservable.update(current -> current.map(currentItem ->
							currentItem.update(
									updatedItem.getPath(),
									item -> item.toBuilder()
											.checkResult(updatedItem.getCheckResult())
											.ongoingCheckUid(null)
											.build()))));
		});
		//
		TreeGridWrapper<ConfluenceMergeItem> grid = new TreeGridWrapper<>(
				ConfluenceMergeItem::getChildren,
				item -> item.withChildren(List.empty()),
				ConfluenceMergeItem::getPath);
		{
			grid.getContent().setHeightFull();
			//
			grid.setSelectionMode(Grid.SelectionMode.MULTI);
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
		}
		Grid.Column<TreeGridWrapper.ItemWrapper<ConfluenceMergeItem>> statusColumn;
		{
			grid
					.addHierarchyColumn(ConfluenceMergeItem::getComputedPageLocator)
					.setFlexGrow(5)
					.setHeader("Computed Locator")
					.setResizable(true);
			grid
					.addColumn(ConfluenceMergeItem::getComputedPageTitle)
					.setFlexGrow(3)
					.setHeader("Computed Title")
					.setResizable(true);
			grid
					.addColumn(item -> item.getChildren().size())
					.setHeader("Children")
					.setResizable(true);
//			grid
//					.addColumn(item -> item.getPath().mkString("/"))
//					.setHeader("Path")
//					.setResizable(true);
			grid
					.addColumn(item -> item.getNode().getScope())
					.setHeader("Scope")
					.setResizable(true);
			statusColumn = grid
					.addComponentColumn(ConfluenceMergeItemsViewRenderer::renderStatus)
					.setFlexGrow(0)
					.setResizable(true);
			grid
					.addComponentColumn(item -> renderPageId(projectionConfiguration.getConnectConfiguration(), item))
					.setHeader("Page Id")
					.setResizable(true);
			grid
					.addComponentColumn(item -> {
						CssHorizontalLayout layout = new CssHorizontalLayout();
						//
						layout.add(renderPublishAction(
								projectionConfiguration.getConnectConfiguration(),
								item,
								() -> {
									ConfluenceMergeItem itemToCheck = item.toBuilder()
											.checkResult(null)
											.ongoingCheckUid(UUID.randomUUID())
											.build();
									//
									itemsObservable.update(current -> current.map(currentRootItem ->
											currentRootItem.replace(itemToCheck)));
									//
									worker
											.initCheck(
													projectionConfiguration.getConnectConfiguration(),
													List.of(itemToCheck),
													path -> Optional
															.ofNullable(itemsObservable.get()
																	.map(currentItem -> currentItem.find(path))
																	.filter(Objects::nonNull)
																	.getOrNull())
															.map(ConfluenceMergeItem::getCheckResult)
															.map(ConfluenceMergeItemCheckResult::getPageId)
															.orElse(null))
											.subscribe(updatedItem -> itemsObservable.update(current ->
													current.map(currentItem -> currentItem.update(
															updatedItem.getPath(),
															item2 -> item2.toBuilder() // FIXME: Need better naming or move to function.
																	.checkResult(updatedItem.getCheckResult())
																	.ongoingCheckUid(null)
																	.build()))));
								}));
						//
						layout.add(ButtonRenderer.render(builder -> builder
								.enabled(item.getNode().getBodyTemplateReference() != null)
								.icon(VaadinIcon.EYE.create())
								.onClick(event -> actionsTool.showRenderedBodyDialog(item))
								.themeVariants(ButtonVariant.LUMO_SMALL)
								.build()));
						//
						return layout;
					})
					.setHeader("Actions")
					.setResizable(true);
		}
		{
			grid.getContent().addSelectionListener(event -> parameters.onSelectionIsEmptyChange().accept(
					event.getAllSelectedItems().isEmpty()));
		}
		{
			parameters.publishTreeFlux().subscribe(next -> {
				Set<List<String>> selectedTreePaths = List.ofAll(grid.getContent().getSelectedItems())
						.map(TreeGridWrapper.ItemWrapper::getItem)
						.flatMap(ConfluenceMergeItem::flatten)
						.map(ConfluenceMergeItem::getPath)
						.toSet();
				//
				List<ConfluenceMergeItem> itemsWithUpdatedOngoingUids = itemsObservable.get()
						.map(rootItem -> rootItem.updateRecursively(item -> selectedTreePaths.contains(item.getPath())
								? item.withOngoingCheckUid(UUID.randomUUID())
								: item));
				//
				itemsObservable.set(itemsWithUpdatedOngoingUids);
				//
				List<ConfluenceMergeItem> itemsToPublish = itemsWithUpdatedOngoingUids
						.flatMap(ConfluenceMergeItem::flatten)
						.filter(item -> item.getOngoingCheckUid() != null);
				//
				worker
						.initPublish(parameters.projectionConfiguration().getConnectConfiguration(), itemsToPublish)
						.subscribe(updatedItem -> itemsObservable.update(current ->
								current.map(currentItem -> currentItem.update(
										updatedItem.getPath(),
										item2 -> item2.toBuilder() // FIXME: Need better naming or move to function.
												.checkResult(updatedItem.getCheckResult())
												.ongoingCheckUid(null)
												.build()))));
			});
		}
		{
			itemsObservable.subscribe(items -> {
				grid.setItems(items);
				//
				int ongoingCount = items.flatMap(ConfluenceMergeItem::flatten)
						.filter(item -> item.getOngoingCheckUid() != null)
						.size();
				statusColumn.setHeader("Status%s".formatted(ongoingCount > 0
						? " (%s left)".formatted(ongoingCount)
						: ""));
			});
			//
			grid.expandRootItems();
		}
		//
		return grid;
	}

	public static Component renderPageId(
			@Nullable ConfluenceProjectionConnectConfiguration connectConfiguration,
			ConfluenceMergeItem item) {
		Component result;
		{
			if (connectConfiguration != null) {
				ConfluenceMergeItemCheckResult checkResult = item.getCheckResult();
				//
				if (checkResult != null) {
					String pageId = checkResult.getPageId();
					//
					if (pageId != null) {
						result = AnchorRenderer.render(builder -> builder
								.href("%s/pages/viewpage.action?pageId=%s".formatted(connectConfiguration.getBaseUrl(), pageId))
								.target(AnchorTarget.BLANK)
								.text(pageId)
								.build());
					} else {
						result = new Div();
					}
				} else {
					result = new Div();
				}
			} else {
				result = new Div();
			}
		}
		return result;
	}

	public static Component renderStatus(ConfluenceMergeItem item) {
		Component result;
		{
			if (item.getCheckResult() != null) {
				ConfluenceMergeItemStatus status = item.getCheckResult().getStatus();
				//
				Span span = new Span(status.toString());
				//
				span.getElement().getThemeList().add(switch (status) {
					case ERROR -> "badge error pill";
					case EXISTS -> "badge pill";
					case NOT_CHECKED -> "badge contrast pill";
					case NOT_EXISTS -> "badge success pill";
					default -> "";
				});
				//
				result = span;
			} else {
				if (item.getOngoingCheckUid() != null) {
					result = new Span("ONG");
				} else {
					result = new Div();
				}
			}
		}
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Flux<Instant> checkTreeFlux;

		@NonNull
		Consumer<Boolean> onSelectionIsEmptyChange;

		@NonNull
		ConfluenceProjectionConfiguration projectionConfiguration;

		@NonNull
		Flux<Instant> publishTreeFlux;

		public static class Builder {
			// Lomboked
		}
	}
}