package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.UI;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import reactor.core.publisher.Flux;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConfiguration;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConnectConfiguration;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.concurrent.WorkProcessingUtils;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.value.wrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.ReactiveVaadin;
import tk.labyrinth.pandora.misc4j.tool.RecursiveProcessor;
import tk.labyrinth.pandora.pattern.PandoraPattern;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.CredentialsUtils;
import tk.labyrinth.pandora.stores.objectsearcher.ReferenceResolver;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.ArtifactManifestIndex;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModelAttribute;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestEndpoint;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestParametersEntry;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestResultEntry;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeNode;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeStructure;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemergecontext.ConfluenceMergeContext;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode.ConfluenceMergeNodeScopeHandler;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode.ConfluenceMergeNodeScopeHandlerRegistry;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.ConfluencePublisher;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.ConfluencePublishingUtils;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceConnectorData;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluencePublishTarget;
import tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment.ManifestEnrichment;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClientFactory;
import tk.labyrinth.satool.confluenceconnector.publish.ConfluenceContentLocatorKind;

import java.time.Instant;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class ConfluenceProjectionConfigurationWorker {

	private final ConfluenceFeignClientFactory confluenceFeignClientFactory;

	private final ConfluenceMergeNodeScopeHandlerRegistry confluenceMergeNodeScopeHandlerRegistry;

	private final ConfluencePublisher confluencePublisher;

	private final ObjectMapper objectMapper;

	private final ReferenceResolver referenceResolver;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifestIndex> artifactManifestIndexSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ManifestEnrichment> manifestEnrichmentSearcher;

	private ConfluenceConnectorData createConfluenceConnectorData(
			ConfluenceProjectionConnectConfiguration connectConfiguration) {
		Credentials credentials = referenceResolver.getSingle(connectConfiguration.getCredentialsReference());
		//
		return ConfluenceConnectorData.builder()
				.authorization(CredentialsUtils.toAuthorizationString(credentials))
				.baseUrl(connectConfiguration.getBaseUrl())
				.feignClient(confluenceFeignClientFactory.create(connectConfiguration.getBaseUrl()))
				.build();
	}

	private List<ConfluenceMergeItem> createMergeItemTree(
			ConfluenceMergeContext parentContext,
			ConfluenceMergeNode mergeNode,
			Integer nodeIndex) {
		List<ConfluenceMergeItem> result;
		{
			String scopeName = mergeNode.getScope();
			//
			if (scopeName != null) {
				ConfluenceMergeNodeScopeHandler scopeHandler = confluenceMergeNodeScopeHandlerRegistry.findHandler(scopeName);
				//
				if (scopeHandler != null) {
					List<GenericObject> variablesObjects = scopeHandler.getVariablesObjects(
							parentContext.getScopeName(),
							parentContext.getVariablesObject());
					//
					result = variablesObjects
							.zipWithIndex()
							.map(tuple -> {
								GenericObject currentVariablesObject = tuple._1();
								List<String> currentPath = parentContext.getPath().append("%s-%s".formatted(nodeIndex, tuple._2()));
								//
								return createMergeItem(
										currentVariablesObject,
										mergeNode,
										currentPath,
										mergeNode.getChildrenOrEmpty()
												.zipWithIndex()
												.flatMap(childTuple -> createMergeItemTree(
														ConfluenceMergeContext.builder()
																.variablesObject(currentVariablesObject)
																.scopeName(scopeName)
																.path(currentPath)
																.build(),
														childTuple._1(),
														childTuple._2())));
							});
				} else {
					result = List.empty();
				}
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	private GenericObject createRootContextObject(
			Map<String, String> variables,
			ArtifactManifestIndex manifestIndex,
			@Nullable ManifestEnrichment manifestEnrichment) {
		return GenericObject.of(
				List
						.of(
								variables
										.map(tuple -> GenericObjectAttribute.ofSimple(tuple._1(), tuple._2()))
										.toList(),
								List
										.of(
												GenericObjectAttribute.of(
														"manifest",
														objectMapper.convertValue(manifestIndex, ValueWrapper.class)),
												manifestEnrichment != null
														? GenericObjectAttribute.of(
														"manifestEnrichment",
														objectMapper.convertValue(manifestEnrichment, ValueWrapper.class))
														: null)
										.filter(Objects::nonNull))
						.flatMap(Function.identity()));
	}

	private GenericObject enrichVariablesObjectWithRestEndpoint(
			GenericObject variablesObject,
			ArtifactManifestIndex manifestIndex,
			RestEndpoint restEndpoint) {
		return variablesObject.withAddedAttributes(
				List
						.of(
								Pair.of(
										"relevantParametersModels",
										selectModels(
												manifestIndex,
												restEndpoint.getParameters().map(RestParametersEntry::getDatatype))),
								Pair.of(
										"relevantResultModels",
										selectModels(
												manifestIndex,
												restEndpoint.getResult().map(RestResultEntry::getDatatype))),
								Pair.of("restEndpoint", restEndpoint))
						.filter(pair -> pair.getRight() != null)
						.map(pair -> GenericObjectAttribute.of(
								pair.getLeft(),
								objectMapper.convertValue(pair.getRight(), ValueWrapper.class))));
	}

	private String publishItem(
			ConfluenceConnectorData connectorData,
			@Nullable String parentPageId,
			ConfluenceMergeItem item) {
		return confluencePublisher.publishPage(
				connectorData,
				item.getComputedPageLocator(),
				parentPageId,
				item.getComputedPageTitle(),
				item.getNode().getBodyTemplateReference(),
				item.getVariableObject()
						//
						// TODO: Replace with global Freemarker function now() (probably with format).
						.withAddedAttribute(GenericObjectAttribute.ofSimple("now", Instant.now().toString())));
	}

	public List<ConfluenceMergeItem> init(ConfluenceProjectionConfiguration projectionConfiguration) {
		ArtifactManifest manifest = referenceResolver.getSingle(projectionConfiguration.getArtifactManifestReference());
		ArtifactManifestIndex manifestIndex = artifactManifestIndexSearcher
				.search(Predicates.equalTo(
						ArtifactManifestIndex.TARGET_REFERENCE_ATTRIBUTE_NAME,
						UidReference.of(ArtifactManifest.MODEL_CODE, manifest.getUid())))
				.single();
		ManifestEnrichment manifestEnrichment = manifestEnrichmentSearcher
				.search(Predicates.equalTo(
						ManifestEnrichment.ARTIFACT_DEFINITION_REFERENCE_ATTRIBUTE_NAME,
						manifest.getDefinitionReference()))
				.getOrNull();
		//
		ConfluenceMergeStructure confluenceMergeStructure = referenceResolver.getSingle(
				projectionConfiguration.getMergeStructureReference());
		//
		ConfluenceMergeContext rootContext = ConfluenceMergeContext.builder()
				.path(List.empty())
				.scopeName("ROOT")
				.variablesObject(createRootContextObject(
						projectionConfiguration.getVariablesOrEmpty().toMap(
								GenericObjectAttribute::getName,
								variable -> variable.getValue().asSimple().getValue()),
						manifestIndex,
						manifestEnrichment))
				.build();
		//
		List<ConfluenceMergeItem> mergeItems = confluenceMergeStructure.getRootNodesOrEmpty()
				.zipWithIndex()
				.flatMap(tuple -> createMergeItemTree(rootContext, tuple._1(), tuple._2()));
		//
		return mergeItems;
	}

	public Flux<ConfluenceMergeItem> initCheck(
			ConfluenceProjectionConnectConfiguration connectConfiguration,
			List<ConfluenceMergeItem> itemsToCheck,
			Function<List<String>, String> knownParentPageIdFunction) {
		ConfluenceConnectorData connectorData = createConfluenceConnectorData(connectConfiguration);
		//
		Map<List<String>, List<ConfluenceMergeItem>> mergeItems = itemsToCheck
				.groupBy(item -> item.getPath().init());
		//
		List<Pair<@Nullable String, ConfluenceMergeItem>> initialItems = mergeItems
				.map(tuple -> tuple.append(tuple._1() != null
						? itemsToCheck.find(item -> Objects.equals(item.getPath(), tuple._1())).getOrNull()
						: null))
				.filter(tuple -> tuple._3() == null)
				.flatMap(tuple -> {
					String parentPageId = tuple._1() != List.<String>empty()
							? knownParentPageIdFunction.apply(tuple._1())
							: null;
					//
					return tuple._2().map(item -> Pair.of(parentPageId, item));
				})
				.toList();
		//
		Flux<ConfluenceMergeItem> updateFlux = WorkProcessingUtils.processConcurrentGrowingWork(
				initialItems,
				(pair, sink) -> {
					ConfluenceMergeItem resultItem = checkItem(connectorData, pair.getLeft(), pair.getRight());
					//
					sink.accept(mergeItems.get(resultItem.getPath()).getOrElse(List.empty())
							.map(item -> Pair.of(resultItem.getCheckResult().getPageId(), item)));
					//
					return resultItem;
				},
				Comparator.comparing(pair -> pair.getRight().getPath().toString()),
				Math.min(itemsToCheck.size(), 4));
		//
		return ReactiveVaadin.uiFlux(UI.getCurrent(), updateFlux);
	}

	public Flux<ConfluenceMergeItem> initPublish(
			ConfluenceProjectionConnectConfiguration connectConfiguration,
			List<ConfluenceMergeItem> itemsToPublish) {
		ConfluenceConnectorData connectorData = createConfluenceConnectorData(connectConfiguration);
		//
		Map<List<String>, List<ConfluenceMergeItem>> mergeItems = itemsToPublish
				.groupBy(item -> item.getPath().init());
		//
		List<Pair<@Nullable String, ConfluenceMergeItem>> initialItems = mergeItems
				.map(tuple -> tuple.append(tuple._1() != null
						? itemsToPublish.find(item -> Objects.equals(item.getPath(), tuple._1())).getOrNull()
						: null))
				.filter(tuple -> tuple._3() == null)
				.flatMap(tuple -> tuple._2().map(item ->
						Pair.of(item.getCheckResult().getParentPageId(), item)))
				.toList();
		//
		Flux<ConfluenceMergeItem> updateFlux = WorkProcessingUtils.processConcurrentGrowingWork(
				initialItems,
				(pair, sink) -> {
					ConfluenceMergeItem mergeItem = pair.getRight();
					//
					Try<String> pageIdTry = Try.ofSupplier(() -> publishItem(connectorData, pair.getLeft(), mergeItem));
					String pageId = pageIdTry.getOrNull();
					//
					if (!pageIdTry.isSuccess()) {
						logger.error("", pageIdTry.getCause());
					}
					//
					sink.accept(mergeItems.get(mergeItem.getPath()).getOrElse(List.empty())
							.map(item -> Pair.of(pageId, item)));
					//
					return mergeItem.withCheckResult(ConfluenceMergeItemCheckResult.builder()
							.pageId(pageId)
							.status(pageIdTry.isSuccess()
									? ConfluenceMergeItemStatus.EXISTS
									: ConfluenceMergeItemStatus.ERROR)
							.build());
				},
				Comparator.comparing(pair -> pair.getRight().getPath().toString()),
				Math.min(itemsToPublish.size(), 4));
		//
		return ReactiveVaadin.uiFlux(UI.getCurrent(), updateFlux);
	}

	@Deprecated
	public void publish(ConfluenceProjectionConnectConfiguration connectConfiguration, ConfluenceMergeItem item) {
		ConfluenceConnectorData connectorData = createConfluenceConnectorData(connectConfiguration);
		//
		confluencePublisher.publishPage(
				connectorData,
				item.getComputedPageLocator(),
				item.getCheckResult().getParentPageId(),
				item.getComputedPageTitle(),
				item.getNode().getBodyTemplateReference(),
				item.getVariableObject()
						//
						// TODO: Replace with global Freemarker function now() (probably with format).
						.withAddedAttribute(GenericObjectAttribute.ofSimple("now", Instant.now().toString())));
	}

	private static ConfluenceMergeItem checkItem(
			ConfluenceConnectorData connectorData,
			@Nullable String parentPageId,
			ConfluenceMergeItem item) {
		ConfluenceMergeItem result;
		{
			if (parentPageId != null ||
					!ConfluenceContentLocatorKind.detect(item.getComputedPageLocator()).dependsOnParent()) {
				ConfluencePublishTarget publishTarget = ConfluencePublishingUtils.getPublishTarget(
						connectorData,
						parentPageId,
						item.getComputedPageLocator());
				//
				result = item.withCheckResult(ConfluenceMergeItemCheckResult.builder()
						.pageId(publishTarget.getContentId())
						.parentPageId(parentPageId)
						.status(publishTarget.getContentId() != null
								? ConfluenceMergeItemStatus.EXISTS
								: ConfluenceMergeItemStatus.NOT_EXISTS)
						.build());
			} else {
				result = item.withCheckResult(ConfluenceMergeItemCheckResult.builder()
						.pageId(null)
						.status(ConfluenceMergeItemStatus.NOT_CHECKED)
						.build());
			}
		}
		return result;
	}

	private static ConfluenceMergeItem createMergeItem(
			GenericObject variableObject,
			ConfluenceMergeNode mergeNode,
			List<String> path,
			List<ConfluenceMergeItem> children) {
		return ConfluenceMergeItem.builder()
				.checkResult(null)
				.children(children)
				.computedPageLocator(Optional.ofNullable(mergeNode.getLocator())
						.map(locator -> PandoraPattern.from(locator)
								.render(placeholder -> lookupString(variableObject, placeholder)))
						.orElse(null))
				.computedPageTitle(Optional.ofNullable(mergeNode.getTitleTemplate())
						.map(titleTemplate -> PandoraPattern.from(titleTemplate)
								.render(placeholder -> lookupString(variableObject, placeholder)))
						.orElse(null))
				.node(mergeNode)
				.path(path)
				.variableObject(variableObject)
				.build();
	}

	public static List<TypeSignature> getTypeSignatures(TypeDescription typeDescription) {
		return List.of(typeDescription.getSignature()).appendAll(
				List.ofAll(typeDescription.getParametersOrEmpty())
						.flatMap(ConfluenceProjectionConfigurationWorker::getTypeSignatures));
	}

	@Nullable
	public static String lookupString(GenericObject variableObject, String path) {
		return Optional.ofNullable(variableObject.lookup(path))
				.map(ValueWrapper::asSimple)
				.map(SimpleValueWrapper::getValue)
				.orElse(null);
	}

	public static List<DisplayableModel> selectModels(
			ArtifactManifestIndex manifestIndex,
			List<TypeDescription> typeDescriptions) {
		List<DisplayableModel> result;
		{
			List<ClassSignature> directClassSignatures = typeDescriptions
					.filter(Objects::nonNull)
					.flatMap(ConfluenceProjectionConfigurationWorker::getTypeSignatures)
					.filter(TypeSignature::isDeclaredOrVariable)
					.map(TypeSignature::getClassSignature)
					.distinct();
			//
			result = RecursiveProcessor.process(
					directClassSignatures,
					classSignature -> {
						Pair<List<ClassSignature>, List<DisplayableModel>> processResult;
						{
							DisplayableModel foundModel = manifestIndex.getModels()
									.find(model -> Objects.equals(model.getJavaSignature(), classSignature))
									.getOrNull();
							//
							if (foundModel != null) {
								processResult = Pair.of(
										foundModel.getAttributes()
												.map(DisplayableModelAttribute::getDatatype)
												.flatMap(TypeDescription::breakDown)
												.map(TypeDescription::getSignature)
												.filter(TypeSignature::isDeclaredOrVariable)
												.map(TypeSignature::getClassSignature),
										List.of(foundModel));
							} else {
								processResult = Pair.of(List.empty(), List.empty());
							}
						}
						return processResult;
					});
		}
		return result;
	}
}
