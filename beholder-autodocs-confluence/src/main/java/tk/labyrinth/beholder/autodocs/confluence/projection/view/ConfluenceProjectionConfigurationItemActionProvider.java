package tk.labyrinth.beholder.autodocs.confluence.projection.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.router.QueryParameters;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.beholder.autodocs.confluence.projection.ConfluenceProjectionConfiguration;
import tk.labyrinth.pandora.functionalcomponents.vaadin.RouterLinkRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.objectaction.TypedObjectActionProvider;

@LazyComponent
public class ConfluenceProjectionConfigurationItemActionProvider extends
		TypedObjectActionProvider<ConfluenceProjectionConfiguration> {

	@Nullable
	@Override
	protected Component provideActionForTypedObject(Context<ConfluenceProjectionConfiguration> context) {
		return RouterLinkRenderer.render(builder -> builder
				.queryParameters(QueryParameters.of("uid", context.getObject().getUid().toString()))
				.route(ConfluenceProjectionConfigurationPage.class)
				.target(AnchorTarget.BLANK)
				.text("Dashboard")
				.build());
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}
}
