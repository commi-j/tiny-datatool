package tk.labyrinth.beholder.autodocs.confluence.projection.endpoint;

import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicBase;

@PolymorphicBase(qualifierAttributeName = "kind")
public interface EndpointProjectionConfiguration {
	// empty
}
