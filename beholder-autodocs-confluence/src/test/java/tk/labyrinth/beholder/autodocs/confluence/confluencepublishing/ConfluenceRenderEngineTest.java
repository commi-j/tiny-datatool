package tk.labyrinth.beholder.autodocs.confluence.confluencepublishing;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModelAttribute;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestEndpoint;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestEndpointCoordinates;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestEndpointGroup;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestParametersEntry;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.KeyConfluencePageTemplateReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.ConfluenceRenderEngine;

// TODO: Sync this one with tested class.
@SpringBootTest
class ConfluenceRenderEngineTest {

	@Autowired
	private ConfluenceRenderEngine confluenceRenderEngine;

	@Test
	void testResolvePageHeaderWithTocLevel() {
		Assertions
				.assertThat(confluenceRenderEngine
						.resolve(
								"""
										<@layout>
											<@page_header tocLevel=2/>
										</@layout>""",
								HashMap.empty())
						.replace("\r\n", "\n"))
				.isEqualTo("""
							<ac:layout>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
							<ac:structured-macro ac:name="toc">
								<ac:parameter ac:name="maxLevel">2</ac:parameter>
						\t\t
							</ac:structured-macro>
								<h1>Children Pages</h1>
							<ac:structured-macro ac:name="expand">
						\t\t
								<ac:rich-text-body>
							<ac:structured-macro ac:name="children">
								<ac:parameter ac:name="all">true</ac:parameter>
							</ac:structured-macro>
								</ac:rich-text-body>
							</ac:structured-macro>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
							</ac:layout>
						""");
	}

	@Test
	void testResolveRestEndpointGroupPage() {
		Assertions
				.assertThat(confluenceRenderEngine
						.resolve(
								KeyConfluencePageTemplateReference.of("rest-endpoint-group-page"),
								HashMap.of(
										"now",
										"2023-06-02T20:13:45Z",
										"relevantModels",
										List.of(
												DisplayableModel.builder()
														.attributes(List.of(
																DisplayableModelAttribute.builder().build()))
														.javaSignature(ClassSignature.from("my.very:MyClass"))
														.tags(List.empty())
														.build()),
										"restEndpointGroup",
										RestEndpointGroup.builder()
												.endpoints(List.of(
														RestEndpoint.builder()
																.coordinateses(List.of(
																		RestEndpointCoordinates.builder()
																				.method(HttpMethod.GET)
																				.path("/")
																				.build()))
																.parameters(List.of(RestParametersEntry.builder()
																		.comments(List.of("foo", "bar"))
																		.datatype(TypeDescription.of("java.lang:List<java.lang:String>"))
																		.build()))
																.result(List.empty())
																.build(),
														RestEndpoint.builder()
																.coordinateses(List.of(
																		RestEndpointCoordinates.builder()
																				.method(HttpMethod.GET)
																				.path("/")
																				.build(),
																		RestEndpointCoordinates.builder()
																				.method(HttpMethod.GET)
																				.path("/list")
																				.build()))
																.parameters(List.empty())
																.result(List.empty())
																.build()))
												.name("ClassLongName")
												.normalizedPath("/some-api")
												.rawPath("some-api")
												.build()))
						.replace("\r\n", "\n"))
				.isEqualTo("""
							<ac:layout>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
							<ac:structured-macro ac:name="toc">
								<ac:parameter ac:name="maxLevel">2</ac:parameter>
						\t\t
							</ac:structured-macro>
								<h1>Children Pages</h1>
							<ac:structured-macro ac:name="expand">
						\t\t
								<ac:rich-text-body>
							<ac:structured-macro ac:name="children">
								<ac:parameter ac:name="all">true</ac:parameter>
							</ac:structured-macro>
								</ac:rich-text-body>
							</ac:structured-macro>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
								<h1>Contract</h1>
								<ul>
									<li>Normalized path: /some-api</li>
									<li>Raw path: some-api</li>
									<li>Signature: NULL</li>
								</ul>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
								<h1>Endpoints</h1>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
									<h2>GET /</h2>
									<h3>Contract</h3>
									<ul>
										<li>Full path: GET /some-api/</li>
										<li>Signature: NULL</li>
									</ul>
									<h3>Parameters</h3>
								<table>
									<tbody>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Constraints</th>
											<th>Kind</th>
											<th>Comments</th>
										</tr>
											<tr>
												<td>-</td>
												<td>List&lt;String&gt;</td>
												<td>-</td>
												<td>-</td>
												<td>foo
						bar</td>
											</tr>
									</tbody>
								</table>
									<h3>Result</h3>
								<ul><li>None</li></ul>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
									<h2>GET /</h2>
									<h3>Contract</h3>
									<ul>
										<li>Full path: GET /some-api/</li>
													<li>Alias: GET /list</li>
										<li>Signature: NULL</li>
									</ul>
									<h3>Parameters</h3>
								<ul><li>None</li></ul>
									<h3>Result</h3>
								<ul><li>None</li></ul>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
								<h1>Models</h1>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
							<ac:layout-section ac:type="single">
							<ac:layout-cell>
									<h2>MyClass</h2>
									<h3>Contract</h3>
									<ul>
										<li>Signature: my.very:MyClass</li>
									</ul>
								<table>
									<tbody>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Constraints</th>
											<th>Comments</th>
										</tr>
											<tr>
												<td>-</td>
												<td>-</td>
												<td>-</td>
												<td>-</td>
											</tr>
									</tbody>
								</table>
							<br/><hr/><br/>
							</ac:layout-cell>
							</ac:layout-section>
								<ac:layout-section ac:type="single">
							<ac:layout-cell>
						<sub>Generated by Beholder at 2023-06-02T20:13:45Z</sub>	</ac:layout-cell>
							</ac:layout-section>
						      
							</ac:layout>
						""");
	}
}