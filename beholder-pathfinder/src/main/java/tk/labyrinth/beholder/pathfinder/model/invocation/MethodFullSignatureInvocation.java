package tk.labyrinth.beholder.pathfinder.model.invocation;

import lombok.Value;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;

@Value(staticConstructor = "of")
public class MethodFullSignatureInvocation implements PathfinderInvocation {

	MethodFullSignature methodFullSignature;
}
