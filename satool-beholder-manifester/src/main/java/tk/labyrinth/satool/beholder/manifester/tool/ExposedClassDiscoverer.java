package tk.labyrinth.satool.beholder.manifester.tool;

import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtEnum;
import spoon.reflect.declaration.CtEnumValue;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.misc4j.tool.RecursiveProcessor;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedField;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedType;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.spoon.CtModelUtils;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.CtTypeUtils;

import java.util.Objects;

public class ExposedClassDiscoverer {

	public static List<ExposedClass> discoverForFunctions(CtModel javaModel, List<ManifestFunction> functions) {
		return ExposedClassDiscoverer.discoverRecursively(
				javaModel,
				functions.flatMap(ManifestFunction::collectClassSignatures));
	}

	public static List<ExposedClass> discoverForPersistentModels(
			CtModel javaModel,
			List<PersistentModel> persistentModels) {
		return ExposedClassDiscoverer.discoverRecursively(
				javaModel,
				persistentModels
						.flatMap(PersistentModel::collectClassSignatures)
						.filter(classSignature -> !persistentModels.exists(persistentModel -> Objects.equals(
								persistentModel.getJavaClassSignature(),
								classSignature))));
	}

	public static List<ExposedClass> discoverRecursively(CtModel javaModel, List<ClassSignature> classSignatures) {
		return RecursiveProcessor
				.process(
						classSignatures,
						classSignature -> {
							Pair<List<ClassSignature>, List<ExposedClass>> result;
							{
								CtType<?> type = CtModelUtils.findType(javaModel, classSignature);
								//
								if (type != null) {
									ExposedClass exposedClass = ExposedClass.builder()
											.enumValues(type.isEnum()
													? List.ofAll(((CtEnum<?>) type).getEnumValues()).map(CtEnumValue::getSimpleName)
													: null)
											.fields(List.ofAll(type.getAllFields())
													.filter(fieldReference -> !fieldReference.isStatic())
													//
													// TODO: There was a case with class extending ArrayList<String>,
													//  need to find smarter way to handle it.
													.filter(fieldReference -> !CtTypeReferenceUtils
															.getPackageName(fieldReference.getDeclaringType())
															.startsWith("java."))
													.map(CtFieldReference::getDeclaration)
													.map(ExposedClassDiscoverer::processField))
											.signature(CtTypeUtils.getClassSignature(type))
											.tags(type.isEnum() ? List.of(Tag.of("enum")) : List.empty())
											.build();
									//
									result = Pair.of(
											exposedClass.getFields()
													.map(ExposedField::getType)
													.filter(Objects::nonNull)
													.flatMap(ExposedType::collectTypeSignature)
													.filter(TypeSignature::isDeclaredOrVariable)
													.map(TypeSignature::getClassSignature),
											List.of(exposedClass));
								} else {
									result = Pair.of(List.empty(), List.empty());
								}
							}
							return result;
						})
				.distinct();
	}

	public static ExposedField processField(CtField<?> field) {
		return ExposedField.builder()
				.name(field.getSimpleName())
				.type(processTypeReference(field.getType()))
				.build();
	}

	public static ExposedType processTypeReference(CtTypeReference<?> typeReference) {
		return ExposedType.builder()
				.core(CtTypeReferenceUtils.getTypeDescription(typeReference).getSignature())
				.parameters(!typeReference.getActualTypeArguments().isEmpty()
						? List.ofAll(typeReference.getActualTypeArguments())
						.map(ExposedClassDiscoverer::processTypeReference)
						: null)
				.build();
	}
}
