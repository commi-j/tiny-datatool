package tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode;

import io.vavr.collection.List;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.datatypes.object.GenericObject;

@PandoraExtensionPoint
public interface ConfluenceMergeNodeScopeHandler {

	List<String> getAttributeNames();

	String getScopeName();

	List<GenericObject> getVariablesObjects(String parentScopeName, GenericObject parentObject);
}
