package tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.ui.component.valuebox.suggest.HasSuggests;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ShallowAttributeReference {

	String attributeName;

	// FIXME: This should be named reference but this name collides with modelReference - common attribute of Pandora.
	//  Need to rework Pandora to not have this attribute if it's not required.
	@HasSuggests(provider = ManifestPersistentModelSuggestProvider.class)
	ClassSignature modelSignature;
}
