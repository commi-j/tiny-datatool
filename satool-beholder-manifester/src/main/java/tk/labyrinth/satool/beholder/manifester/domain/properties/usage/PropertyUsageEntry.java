package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.kafka.annotation.KafkaListener;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;

import javax.annotation.CheckForNull;

/**
 * ConditionalOnProperty - classes, methods;<br>
 * ConfigurationProperties - classes;<br>
 * KafkaListener - methods;<br>
 * Value - fields, method parameters;<br>
 *
 * @see ConditionalOnProperty
 * @see ConfigurationProperties
 * @see KafkaListener
 * @see org.springframework.beans.factory.annotation.Value
 */
@Builder(toBuilder = true)
@Model
@Value
public class PropertyUsageEntry {

	String defaultValue;

	@CheckForNull
	String javaDefaultValue;

	@CheckForNull
	TypeDescription javaType;

	/**
	 * Either JavaLocation or FileLocation.
	 */
	String location;

	String name;

	List<String> tags;
}
