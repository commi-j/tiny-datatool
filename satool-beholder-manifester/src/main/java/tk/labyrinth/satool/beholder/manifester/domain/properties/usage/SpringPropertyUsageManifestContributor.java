package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import spoon.reflect.CtModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;

import java.nio.file.Path;
import java.util.function.Function;

@LazyComponent
public class SpringPropertyUsageManifestContributor implements ManifestContributor {

	@Override
	public List<PropertyUsageEntry> contribute(Path rootDirectory, CtModel javaModel) {
		return List
				.of(
						PropertyUsageConfigurationPropertiesResolver.resolve(javaModel),
						PropertyUsageKafkaListenerResolver.resolve(javaModel),
						PropertyUsageValueResolver.resolve(javaModel))
				.flatMap(Function.identity());
	}
}
