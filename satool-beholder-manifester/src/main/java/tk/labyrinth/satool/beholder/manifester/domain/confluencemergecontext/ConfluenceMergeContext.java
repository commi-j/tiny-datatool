package tk.labyrinth.satool.beholder.manifester.domain.confluencemergecontext;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.object.GenericObject;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceMergeContext {

	List<String> path;

	@NonNull
	String scopeName;

	@NonNull
	GenericObject variablesObject;
}
