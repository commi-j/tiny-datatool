package tk.labyrinth.satool.beholder.manifester.domain.exposed;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ExposedAnnotationValue {

	String name;

	// TODO: Complex object since it could be array or other annotation.
	String value;

	public static ExposedAnnotationValue merge(List<ExposedAnnotationValue> annotationValues) {
		return ExposedAnnotationValue.builder()
				.name(annotationValues.map(ExposedAnnotationValue::getName).distinct().single())
				.value(annotationValues.map(ExposedAnnotationValue::getValue).distinct().single())
				.build();
	}

	@Nullable
	public static List<ExposedAnnotationValue> mergeMultiple(
			List<@Nullable List<ExposedAnnotationValue>> annotationValueLists) {
		return ExposedUtils.mergeMultiple(
				annotationValueLists,
				ExposedAnnotationValue::getName,
				ExposedAnnotationValue::merge);
	}
}
