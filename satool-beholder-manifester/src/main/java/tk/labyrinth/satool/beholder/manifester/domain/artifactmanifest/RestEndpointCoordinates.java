package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.springframework.http.HttpMethod;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RestEndpointCoordinates {

	HttpMethod method;

	String path;
}
