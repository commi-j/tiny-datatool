package tk.labyrinth.satool.beholder.manifester.domain.gitbranch;

import com.vaadin.flow.component.Component;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.domain.object.table.item.SpecificObjectItemActionProvider;
import tk.labyrinth.satool.beholder.manifester.domain.gitrepositoryclone.GitRepositoryClone;

import javax.annotation.Nullable;

@LazyComponent
@RequiredArgsConstructor
public class GitRepositoryCloneBuildArtifactManifestActionHandler extends
		SpecificObjectItemActionProvider<GitRepositoryClone> {

	@Nullable
	@Override
	protected Component provideItemAction(GitRepositoryClone item) {
		return ButtonRenderer.render(builder -> builder
				.enabled(item.getDirectoryPath() != null)
				.onClick(event -> {
					// TODO
				})
				.text("Build Artifact Manifest")
				.themeVariants()
				.build());
	}
}
