package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram;

import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicBase;

@PolymorphicBase(qualifierAttributeName = "kind")
public interface ConfluenceDiagramPublishingStrategy {
	// empty
}
