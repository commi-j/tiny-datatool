package tk.labyrinth.satool.beholder.manifester.domain.repository;

import com.google.gerrit.extensions.common.ProjectInfo;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.objectmanipulator.WildcardObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.GitBranch;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.RepositoryReferenceAndNameGitBranchReference;
import tk.labyrinth.satool.gerritconnector.api.GerritConnectionParameters;
import tk.labyrinth.satool.gerritconnector.api.GerritFetcher;
import tk.labyrinth.satool.jgit.GitHandler;

import javax.annotation.CheckForNull;
import java.time.Instant;
import java.util.Objects;

@Component
@ConditionalOnBean(GerritFetcher.class)
@RequiredArgsConstructor
@Slf4j
public class FetchRepositoriesJobRunner implements JobRunner<FetchRepositoriesJobRunner.Parameters> {

	private final ExecutionContextHandler executionContextHandler;

	private final GerritFetcher gerritFetcher;

	private final WildcardObjectManipulator objectManipulator;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@SmartAutowired
	private TypedObjectSearcher<GitBranch> gitBranchSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Repository> repositorySearcher;

	private void processProjectInfo(String host, ProjectInfo projectInfo, @CheckForNull Credentials credentials) {
		String repositoryUrl = resolveUrl(host, projectInfo);
		//
		{
			Repository repository = Repository.builder()
					.name(projectInfo.name)
					.updatedAt(Instant.now())
					.url(repositoryUrl)
					.build();
			//
			saveRepository(repository);
		}
		{
			Ref head = GitHandler.getHead(
					repositoryUrl,
					credentials != null ? new UsernamePasswordCredentialsProvider(
							credentials.getUsername(),
							credentials.getPassword()) : null);
			//
			saveGitBranch(GitBranch.builder()
					.name(head.getTarget().getName().substring("refs/heads/".length()))
					.repositoryReference(UrlRepositoryReference.of(repositoryUrl))
					.build());
		}
		//
	}

	private void saveGitBranch(GitBranch gitBranch) {
		RepositoryReferenceAndNameGitBranchReference qualifyingReference =
				RepositoryReferenceAndNameGitBranchReference.from(gitBranch);
		//
		GitBranch foundGitBranch = gitBranchSearcher.findSingle(qualifyingReference);
		if (foundGitBranch != null) {
			logger.info("Updating GitBranch: qualifyingReference = {}", qualifyingReference);
			//
			GitBranch gitBranchToUpdate = gitBranch.toBuilder()
					.uid(foundGitBranch.getUid())
					.build();
			//
			objectManipulator.update(gitBranchToUpdate);
		} else {
			logger.info("Creating GitBranch: qualifyingReference = {}", qualifyingReference);
			//
			objectManipulator.createWithGeneratedUid(gitBranch);
		}
	}

	private void saveRepository(Repository repository) {
		UrlRepositoryReference qualifyingReference = UrlRepositoryReference.from(repository);
		//
		Repository foundRepository = repositorySearcher.findSingle(qualifyingReference);
		if (foundRepository != null) {
			logger.info("Updating Repository: qualifyingReference = {}", qualifyingReference);
			//
			Repository repositoryToUpdate = repository.toBuilder()
					.uid(foundRepository.getUid())
					.build();
			//
			objectManipulator.update(repositoryToUpdate);
		} else {
			logger.info("Creating Repository: qualifyingReference = {}", qualifyingReference);
			//
			objectManipulator.createWithGeneratedUid(repository);
		}
	}

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder()
				.spaceReference(executionContextHandler.getExecutionContext().findAttributeValueInferred(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME))
				.build();
	}

	@Override
	public void run(Context<Parameters> context) {
		Parameters parameters = context.getParameters();
		Credentials credentials = parameters.getCredentialsReference() != null
				? credentialsSearcher.findSingle(parameters.getCredentialsReference())
				: null;
		//
		gerritFetcher
				.searchProjectInfosFlux(
						GerritConnectionParameters.builder()
								.host(parameters.getHost())
								.password(credentials != null ? credentials.getPassword() : null)
								.username(credentials != null ? credentials.getUsername() : null)
								.build(),
						null)
				.subscribe(next -> next.forEach(projectInfo -> {
					try {
						processProjectInfo(parameters.getHost(), projectInfo, credentials);
					} catch (RuntimeException ex) {
						logger.warn("projectInfo.id = {}", projectInfo.id, ex);
					}
				}));
	}

	public static String resolveUrl(String host, ProjectInfo projectInfo) {
		return List.ofAll(projectInfo.webLinks)
				.find(webLink -> Objects.equals(webLink.name, "browse"))
				.map(webLink -> webLink.url)
				.getOrElse(() -> host + "/" + projectInfo.name);
	}

	@Builder(toBuilder = true)
	@Value
	public static class Parameters implements HasSpaceReference {

		@CheckForNull
		KeyCredentialsReference credentialsReference;

		String host;

		KeySpaceReference spaceReference;
	}
}
