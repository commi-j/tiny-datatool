package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.stereotype.Component;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.dependencies.expresso.MatchesMaskPredicate;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.objectmanipulator.WildcardObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.GitBranch;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.RepositoryReferenceAndNameGitBranchReference;
import tk.labyrinth.satool.beholder.manifester.domain.repository.Repository;
import tk.labyrinth.satool.beholder.manifester.domain.repository.UrlRepositoryReference;
import tk.labyrinth.satool.jgit.RepositoryHandler;
import tk.labyrinth.satool.maven.domain.MavenDependency;

import javax.annotation.CheckForNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;

@Component
@RequiredArgsConstructor
@Slf4j
public class DiscoverMavenModulesJobRunner implements JobRunner<DiscoverMavenModulesJobRunner.Parameters> {

	private final ExecutionContextHandler executionContextHandler;

	private final WildcardObjectManipulator objectManipulator;

	private final MavenXpp3Reader pomReader = new MavenXpp3Reader();

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@SmartAutowired
	private TypedObjectSearcher<GitBranch> gitBranchSearcher;

	@SmartAutowired
	private TypedObjectSearcher<MavenModule> mavenModuleSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Repository> repositorySearcher;

	private List<MavenModule> processPomXmlDirectory(
			RepositoryReferenceAndNameGitBranchReference branchReference,
			Path rootDirectoryPath,
			@CheckForNull MavenModule parent,
			Path pomXmlDirectoryPath) {
		List<MavenModule> result;
		{
			File pomXmlFile = pomXmlDirectoryPath.resolve("pom.xml").toFile();
			if (pomXmlFile.exists()) {
				Model pomModel;
				try (FileInputStream inputStream = new FileInputStream(pomXmlFile)) {
					pomModel = pomReader.read(inputStream);
				} catch (IOException | XmlPullParserException ex) {
					throw new RuntimeException(ex);
				}
				//
				MavenModule mavenModule = MavenModule.builder()
						.artifactId(pomModel.getArtifactId())
						.branchReference(branchReference)
						.dependencies(resolveDependencies(pomModel))
						.groupId(resolveGroupId(pomModel))
						.modules(List.ofAll(pomModel.getModules()))
						.ownerReference(null) // TODO
						.path(Option.of(rootDirectoryPath.relativize(pomXmlDirectoryPath).toString())
								.map(path -> !path.isEmpty() ? "/%s/".formatted(path) : "/")
								.get())
						.updatedAt(Instant.now())
						.build();
				//
				result = List.of(mavenModule).appendAll(mavenModule.getModules().flatMap(module ->
						processPomXmlDirectory(
								branchReference,
								rootDirectoryPath,
								mavenModule,
								pomXmlDirectoryPath.resolve(module))));
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	private void processRepository(Repository repository, @CheckForNull Credentials credentials) {
		List<GitBranch> gitBranches = gitBranchSearcher.search(PlainQuery.builder()
				.predicate(Predicates.equalTo(
						GitBranch.REPOSITORY_REFERENCE_ATTRIBUTE_NAME,
						UrlRepositoryReference.from(repository)))
				.build());
		//
		if (gitBranches.size() > 1) {
			throw new NotImplementedException("Does not support more than 1 branch: " + repository);
		}
		GitBranch gitBranch = gitBranches.single();
		//
		Path rootDirectoryPath = RepositoryHandler.loadRevision(
				repository.getUrl(),
				gitBranches.single().getName(),
				credentials != null
						? new UsernamePasswordCredentialsProvider(credentials.getUsername(), credentials.getPassword())
						: null,
				"discover-maven-modules-");
		try {
			List<MavenModule> mavenModules = processPomXmlDirectory(
					RepositoryReferenceAndNameGitBranchReference.from(gitBranch),
					rootDirectoryPath,
					null,
					rootDirectoryPath);
			//
			logger.info("Found {} MavenModules", mavenModules.size());
			//
			mavenModules.forEach(this::saveMavenModule);
		} finally {
			if (!FileUtils.deleteQuietly(rootDirectoryPath.toFile())) {
				logger.warn("Was unable to delete: {}", rootDirectoryPath);
			}
		}
	}

	private void saveMavenModule(MavenModule mavenModule) {
		BranchReferenceAndPathMavenModuleReference qualifyingReference =
				BranchReferenceAndPathMavenModuleReference.from(mavenModule);
		//
		MavenModule foundMavenModule = mavenModuleSearcher.findSingle(qualifyingReference);
		if (foundMavenModule != null) {
			logger.info("Updating MavenModule: qualifyingReference = {}", qualifyingReference);
			//
			MavenModule mavenModuleToUpdate = mavenModule.toBuilder()
					.uid(foundMavenModule.getUid())
					.build();
			//
			objectManipulator.update(mavenModuleToUpdate);
		} else {
			logger.info("Creating MavenModule: qualifyingReference = {}", qualifyingReference);
			//
			objectManipulator.createWithGeneratedUid(mavenModule);
		}
	}

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder()
				.spaceReference(executionContextHandler.getExecutionContext()
						.findAttributeValueInferred(BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME))
				.build();
	}

	@Override
	public void run(Context<Parameters> context) {
		Parameters parameters = context.getParameters();
		//
		List<Repository> repositories = repositorySearcher.search(PlainQuery.builder()
				.predicate(parameters.getRepositoryUrlMask() != null
						? MatchesMaskPredicate.of("url", parameters.getRepositoryUrlMask())
						: null)
				.build());
		Credentials credentials = parameters.getCredentialsReference() != null
				? credentialsSearcher.findSingle(parameters.getCredentialsReference())
				: null;
		//
		logger.info("Found {} repositories with url matching mask '{}'",
				repositories.size(),
				parameters.getRepositoryUrlMask());
		//
		repositories.forEach(repository -> {
			try {
				logger.info("Processing Repository: repository.name = {}", repository.getName());
				//
				processRepository(repository, credentials);
			} catch (RuntimeException ex) {
				logger.error("repository.name = {}", repository.getName(), ex);
			}
		});
	}

	public static List<MavenDependency> resolveDependencies(Model pomModel) {
		List<MavenDependency> dependencies = List.ofAll(pomModel.getDependencies()).map(MavenDependency::from);
		return pomModel.getParent() != null
				? List.of(MavenDependency.from(pomModel.getParent())).appendAll(dependencies)
				: dependencies;
	}

	public static String resolveGroupId(Model pomModel) {
		return pomModel.getGroupId() != null
				? pomModel.getGroupId()
				: pomModel.getParent().getGroupId();
	}

	@Builder(toBuilder = true)
	@Value
	public static class Parameters implements HasSpaceReference {

		@CheckForNull
		KeyCredentialsReference credentialsReference;

		@CheckForNull
		String repositoryUrlMask;

		@NonNull
		KeySpaceReference spaceReference;
	}
}
