package tk.labyrinth.satool.beholder.manifester.domain.function;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;

import java.util.function.Function;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderAttribute("javaSignature")
@Value
@With
public class ManifestFunction {

	// TODO: Invent a good object for this one.
	@Nullable
	String accessControl;

	List<? extends FunctionCoordinates> coordinateses;

	MethodFullSignature javaSignature;

	String name;

	List<FunctionParametersEntry> parameters;

	List<FunctionResultEntry> result;

	public List<ClassSignature> collectClassSignatures() {
		return List
				.of(
						getParametersOrEmpty().map(FunctionParametersEntry::getDatatype),
						getResultOrEmpty().map(FunctionResultEntry::getDatatype))
				.flatMap(Function.identity())
				.flatMap(TypeDescription::breakDown)
				.map(TypeDescription::getSignature)
				.filter(TypeSignature::isDeclaredOrVariable)
				.map(TypeSignature::getClassSignature)
				.distinct();
	}

	public List<? extends FunctionCoordinates> getCoordinatesesOrEmpty() {
		return coordinateses != null ? coordinateses : List.empty();
	}

	public List<FunctionParametersEntry> getParametersOrEmpty() {
		return parameters != null ? parameters : List.empty();
	}

	public List<FunctionResultEntry> getResultOrEmpty() {
		return result != null ? result : List.empty();
	}

	public boolean isHttp() {
		return getCoordinateses().exists(FunctionCoordinates::isHttp);
	}
}
