package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import org.springframework.beans.factory.annotation.Value;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtConstructor;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtFieldReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.SpoonStaticFactory;
import tk.labyrinth.satool.spoon.SpoonUtils;

import java.util.Objects;
import java.util.regex.MatchResult;

public class PropertyUsageValueResolver {

	private static List<PropertyUsageEntry> resolveElement(CtElement element) {
		List<PropertyUsageEntry> result;
		{
			if (element instanceof CtField<?> field) {
				result = resolveField(field);
			} else if (element instanceof CtParameter<?> formalParameter) {
				result = resolveFormalParameter(formalParameter);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	private static List<PropertyUsageEntry> resolveField(CtField<?> field) {
		String valueAnnotationValue = field.getAnnotation(SpoonStaticFactory.createReference(Value.class))
				.getValueAsString("value");
		List<MatchResult> placeholderValues = SpelUtils.findPlaceholderValues(valueAnnotationValue);
		//
		return placeholderValues.map(placeholderValue -> PropertyUsageEntry.builder()
				.defaultValue(placeholderValue.group(3))
				.javaDefaultValue(null)
				.javaType(CtTypeReferenceUtils.getTypeDescription(field.getType()))
				.name(placeholderValue.group(1))
				.location(SpoonUtils.elementToSignature(field).appendAnnotation(Value.class).toString())
				.tags(List.of("field", "java"))
				.build());
	}

	private static List<PropertyUsageEntry> resolveFormalParameter(CtParameter<?> formalParameter) {
		String valueAnnotationValue = formalParameter.getAnnotation(SpoonStaticFactory.createReference(Value.class))
				.getValueAsString("value");
		List<MatchResult> placeholderValues = SpelUtils.findPlaceholderValues(valueAnnotationValue);
		//
		return placeholderValues.map(placeholderValue -> PropertyUsageEntry.builder()
				.defaultValue(placeholderValue.group(3))
				.javaDefaultValue(null)
				.javaType(CtTypeReferenceUtils.getTypeDescription(formalParameter.getType()))
				.name(placeholderValue.group(1))
				.location(SpoonUtils.elementToSignature(formalParameter).appendAnnotation(Value.class)
						.toString())
				.tags(List.of(
						(formalParameter.getParent() instanceof CtConstructor) ? "constructor" : "method",
						"java"))
				.build());
	}

	public static List<PropertyUsageEntry> resolve(CtModel javaModel) {
		List<CtElement> valueAnnotatedFormalParameters = Stream.ofAll(javaModel.getAllTypes())
				.flatMap(CtType::getAllExecutables)
				.map(CtExecutableReference::getDeclaration)
				.filter(Objects::nonNull)
				.flatMap(executable -> Stream.<CtElement>ofAll(executable.getParameters()))
				.filter(element -> element.hasAnnotation(Value.class))
				.distinct()
				.toList();
		List<? extends CtField<?>> valueAnnotatedFields = Stream.ofAll(javaModel.getAllTypes())
				.flatMap(CtType::getAllFields)
				.map(CtFieldReference::getDeclaration)
				.filter(Objects::nonNull)
				.filter(field -> field.hasAnnotation(Value.class))
				.distinct()
				.toList();
		//
		return valueAnnotatedFormalParameters
				.appendAll(valueAnnotatedFields)
				.flatMap(PropertyUsageValueResolver::resolveElement);
	}
}
