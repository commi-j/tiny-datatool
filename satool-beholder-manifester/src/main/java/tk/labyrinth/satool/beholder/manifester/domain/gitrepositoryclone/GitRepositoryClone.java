package tk.labyrinth.satool.beholder.manifester.domain.gitrepositoryclone;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.core.PandoraConstants;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.RepositoryReferenceAndNameGitBranchReference;

import java.nio.file.Path;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(GitRepositoryClone.MODEL_CODE)
@ModelTag("repos")
@ModelTag(PandoraConstants.TEMPORAL_MODEL_TAG_NAME)
@RenderPattern("$gitBranchReference")
@Value
@With
public class GitRepositoryClone {

	public static final String MODEL_CODE = "gitrepositoryclone";

	Path directoryPath;

	RepositoryReferenceAndNameGitBranchReference gitBranchReference;
}
