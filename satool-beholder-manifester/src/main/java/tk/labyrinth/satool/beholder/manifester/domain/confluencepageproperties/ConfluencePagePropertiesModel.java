package tk.labyrinth.satool.beholder.manifester.domain.confluencepageproperties;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;

@Builder(toBuilder = true)
@Model
@ModelTag("documentary")
@RenderPattern("$spaceKey:$label")
@Value
public class ConfluencePagePropertiesModel {

	public static final String LABEL_ATTRIBUTE_NAME = "label";

	public static final String MODEL_CODE = "confluencepagepropertiesmodel";

	public static final String SPACE_KEY_ATTRIBUTE_NAME = "spaceKey";

	String label;

	CodeObjectModelReference objectModelReference;

	List<ConfluencePagePropertyMapping> propertyMappings;

	String spaceKey;
}
