package tk.labyrinth.satool.beholder.manifester.domain.gitbranch;

import com.vaadin.flow.component.Component;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViewFactory;
import tk.labyrinth.satool.beholder.domain.object.table.item.SpecificObjectItemActionProvider;
import tk.labyrinth.satool.beholder.manifester.domain.gitrepositoryclone.GitRepositoryClone;
import tk.labyrinth.satool.jgit.RepositoryHandler;

import javax.annotation.Nullable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
public class GitBranchCloneActionHandler extends SpecificObjectItemActionProvider<GitBranch> {

	private final ConfirmationViewFactory confirmationViewFactory;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@SmartAutowired
	private TypedObjectManipulator<GitRepositoryClone> gitRepositoryCloneManipulator;

	@Nullable
	@Override
	protected Component provideItemAction(GitBranch item) {
		return ButtonRenderer.render(builder -> builder
				.enabled(item.getRepositoryReference() != null)
				.onClick(event -> confirmationViewFactory
						.showTypedViewDialog(Parameters.builder().build())
						.subscribeAlwaysAccepted(parameters -> {
							Path localDirectory = IoUtils.unchecked(
									() -> Files.createTempDirectory("beholder-repository-"));
							//
							gitRepositoryCloneManipulator.createWithGeneratedUid(GitRepositoryClone.builder()
									.directoryPath(localDirectory)
									.gitBranchReference(RepositoryReferenceAndNameGitBranchReference.from(item))
									.build());
							//
							RepositoryHandler.loadRevision(
									item.getRepositoryReference().getUrl(),
									item.getName(),
									Optional.ofNullable(parameters.getCredentialsReference())
											.map(credentialsReference -> credentialsSearcher.getSingle(credentialsReference))
											.map(credentials -> new UsernamePasswordCredentialsProvider(
													credentials.getUsername(),
													credentials.getPassword()))
											.orElse(null),
									localDirectory);
						}))
				.text("Clone")
				.themeVariants()
				.build());
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
//	@CustomizeFormLayout(gridTemplateColumns = "24em")
	@Value
	@With
	public static class Parameters {

		KeyCredentialsReference credentialsReference;
	}
}
