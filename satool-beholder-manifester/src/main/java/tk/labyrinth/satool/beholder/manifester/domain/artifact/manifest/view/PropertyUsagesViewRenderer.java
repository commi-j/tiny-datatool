package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;

import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PropertyUsagesViewRenderer {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	private Component renderLocation(String location, List<String> tags) {
		Component result;
		{
			if (tags.contains("java")) {
				result = toVaadinComponentRendererRegistry.render(
						ToVaadinComponentRendererRegistry.Context.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(ElementSignature.class))
								.hints(List.empty())
								.build(),
						ElementSignature.of(location));
			} else {
				result = new Span(location);
			}
		}
		return result;
	}

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			layout.add("Usages");
			//
			properties.getPropertyUsageEntries().forEach(propertyUsageEntry -> {
				CssVerticalLayout entryLayout = new CssVerticalLayout();
				{
					{
						CssHorizontalLayout locationLayout = new CssHorizontalLayout();
						{
							StyleUtils.setCssProperty(locationLayout, WhiteSpace.PRE_WRAP);
						}
						{
							locationLayout.add("Location: ");
							locationLayout.add(renderLocation(
									propertyUsageEntry.getLocation(),
									propertyUsageEntry.getTags()));
						}
						entryLayout.add(locationLayout);
					}
					{
						CssHorizontalLayout javaTypeLayout = new CssHorizontalLayout();
						{
							StyleUtils.setCssProperty(javaTypeLayout, WhiteSpace.PRE_WRAP);
						}
						{
							javaTypeLayout.add(new Span("Java Type: "));
							javaTypeLayout.add(toVaadinComponentRendererRegistry.render(
									ToVaadinComponentRendererRegistry.Context.builder()
											.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
													TypeDescription.class))
											.hints(List.empty())
											.build(),
									propertyUsageEntry.getJavaType()));
						}
						entryLayout.add(javaTypeLayout);
					}
					entryLayout.add(new Span("Java Default Value: %s"
							.formatted(propertyUsageEntry.getJavaDefaultValue())));
					entryLayout.add(new Span("Default value: %s".formatted(propertyUsageEntry.getDefaultValue())));
					entryLayout.add(new Span("Tags: %s".formatted(propertyUsageEntry.getTags())));
				}
				layout.add(entryLayout);
			});
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		List<PropertyUsageEntry> propertyUsageEntries;

		public static class Builder {
			// Lomboked
		}
	}
}