package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class ScenarioTool {

	@SmartAutowired
	private TypedObjectSearcher<Scenario> scenarioSearcher;

	public List<ScenarioInvocationStep> getDownstreamInvocationTree(Scenario entryScenario) {
		java.util.List<Pair<String, ResolvedReference<Scenario, ConfluenceSignatureScenarioReference>>>
				scenariosToProcess = new ArrayList<>();
		scenariosToProcess.add(Pair.of(
				null,
				ResolvedReference.ofNonNull(ConfluenceSignatureScenarioReference.from(entryScenario), entryScenario)));
		Set<ConfluenceSignatureScenarioReference> discoveredScenarioReferences = new HashSet<>();
		discoveredScenarioReferences.add(ConfluenceSignatureScenarioReference.from(entryScenario));
		//
		java.util.List<ScenarioInvocationStep> result = new ArrayList<>();
		while (!scenariosToProcess.isEmpty()) {
			Pair<String, ResolvedReference<Scenario, ConfluenceSignatureScenarioReference>>
					stepCodeAndResolvedScenarioReference = scenariosToProcess.remove(0);
			String stepCode = stepCodeAndResolvedScenarioReference.getLeft();
			ResolvedReference<Scenario, ConfluenceSignatureScenarioReference> resolvedScenarioReference =
					stepCodeAndResolvedScenarioReference.getRight();
			Scenario scenario = resolvedScenarioReference.getSingle();
			//
			if (scenario != null) {
				List<ConfluenceSignatureScenarioReference> dependencyReferences = Option
						.of(scenario.getDependencyReferences())
						.getOrElse(List.empty());
				dependencyReferences.zipWithIndex().forEach(pair -> {
					ConfluenceSignatureScenarioReference dependencyReference = pair._1();
					int index = pair._2();
					//
					ResolvedReference<Scenario, ConfluenceSignatureScenarioReference> resolvedDependencyReference =
							scenarioSearcher.resolve(dependencyReference);
					String nextStepCode = stepCode != null
							? stepCode + "." + (index + 1)
							: Integer.toString(index + 1);
					//
					result.add(new ScenarioInvocationStep(
							resolvedScenarioReference,
							nextStepCode,
							resolvedDependencyReference));
					//
					if (discoveredScenarioReferences.add(dependencyReference)) {
						scenariosToProcess.add(Pair.of(nextStepCode, resolvedDependencyReference));
					}
				});
			}
		}
		return List.ofAll(result);
	}

	public List<ScenarioInvocationStep> getUpstreamInvocationTree(Scenario entryScenario) {
		java.util.List<Pair<String, ResolvedReference<Scenario, ConfluenceSignatureScenarioReference>>>
				scenariosToProcess = new ArrayList<>();
		scenariosToProcess.add(Pair.of(
				null,
				ResolvedReference.ofNonNull(ConfluenceSignatureScenarioReference.from(entryScenario), entryScenario)));
		Set<ConfluenceSignatureScenarioReference> discoveredScenarioReferences = new HashSet<>();
		discoveredScenarioReferences.add(ConfluenceSignatureScenarioReference.from(entryScenario));
		//
		java.util.List<ScenarioInvocationStep> result = new ArrayList<>();
		while (!scenariosToProcess.isEmpty()) {
			Pair<String, ResolvedReference<Scenario, ConfluenceSignatureScenarioReference>>
					stepCodeAndResolvedScenarioReference = scenariosToProcess.remove(0);
			String stepCode = stepCodeAndResolvedScenarioReference.getLeft();
			ResolvedReference<Scenario, ConfluenceSignatureScenarioReference> resolvedScenarioReference =
					stepCodeAndResolvedScenarioReference.getRight();
			Scenario scenario = resolvedScenarioReference.findSingle();
			//
			if (scenario != null) {
				List<ConfluenceSignatureScenarioReference> dependencyReferences = scenarioSearcher
						.search(PlainQuery.builder()
								.predicate(Predicates.contains(
										Scenario.DEPENDENCY_REFERENCES_ATTRIBUTE_NAME,
										ConfluenceSignatureScenarioReference.from(entryScenario)))
								.build())
						.map(ConfluenceSignatureScenarioReference::from);
				dependencyReferences.zipWithIndex().forEach(pair -> {
					ConfluenceSignatureScenarioReference dependencyReference = pair._1();
					int index = pair._2();
					//
					ResolvedReference<Scenario, ConfluenceSignatureScenarioReference> resolvedDependencyReference =
							scenarioSearcher.resolve(dependencyReference);
					String nextStepCode = stepCode != null
							? "%s.%s".formatted(stepCode, index + 1)
							: "-%s".formatted(index + 1);
					//
					result.add(new ScenarioInvocationStep(
							resolvedDependencyReference,
							nextStepCode,
							resolvedScenarioReference));
					//
					if (discoveredScenarioReferences.add(dependencyReference)) {
						scenariosToProcess.add(Pair.of(nextStepCode, resolvedDependencyReference));
					}
				});
			}
		}
		return List.ofAll(result);
		//
		//
//		List<Scenario> upstreamScenarios = scenarioSearcher.search(TypelessSearchQuery.builder()
//				.predicate(Predicates.contains(
//						Scenario.DEPENDENCY_REFERENCES_ATTRIBUTE_NAME,
//						ConfluenceSignatureScenarioReference.from(entryScenario)))
//				.build());
//		//
//		throw new NotImplementedException();
	}

	@Value
	public static class ScenarioInvocationStep {

		ResolvedReference<Scenario, ConfluenceSignatureScenarioReference> from;

		String stepCode;

		ResolvedReference<Scenario, ConfluenceSignatureScenarioReference> to;

		private String renderFrom(boolean addName) {
			Scenario toScenario = from.findSingle();
			String code = toScenario != null ? toScenario.getCode() : null;
			return code != null
					? (addName ?
					"%s %s".formatted(code, toScenario.getName())
					: code)
					: (toScenario != null
					? toScenario.getName()
					: from.getReference().toString());
		}

		private String renderTo(boolean addName) {
			Scenario toScenario = to.findSingle();
			String code = toScenario != null ? toScenario.getCode() : null;
			return code != null
					? (addName ?
					"%s %s".formatted(code, toScenario.getName())
					: code)
					: (toScenario != null
					? toScenario.getName()
					: to.getReference().toString());
		}

		public String renderLongFrom() {
			return "%s. %s".formatted(stepCode, renderFrom(true));
		}

		public String renderLongTo() {
			return "%s. %s".formatted(stepCode, renderTo(true));
		}

		public String renderShortFrom() {
			return "%s. %s".formatted(stepCode, renderFrom(false));
		}

		public String renderShortTo() {
			return "%s. %s".formatted(stepCode, renderTo(false));
		}
	}
}
