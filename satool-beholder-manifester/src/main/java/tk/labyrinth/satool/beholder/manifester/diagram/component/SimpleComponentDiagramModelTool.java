package tk.labyrinth.satool.beholder.manifester.diagram.component;

import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.component.CodeComponentReference;
import tk.labyrinth.satool.beholder.manifester.domain.component.Component;
import tk.labyrinth.satool.beholder.manifester.domain.component.RenderableComponentDependency;
import tk.labyrinth.satool.beholder.manifester.domain.componentdependency.ComponentDependency;
import tk.labyrinth.satool.beholder.manifester.domain.componentdependency.ComponentDependencyLayer;
import tk.labyrinth.satool.beholder.manifester.domain.mavenmodule.MavenModule;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramComponent;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramLink;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramModel;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class SimpleComponentDiagramModelTool {

	@SmartAutowired
	private TypedObjectSearcher<ComponentDependency> componentDependencySearcher;

	@SmartAutowired
	private TypedObjectSearcher<Component> componentSearcher;

	private List<Pair<CodeComponentReference, List<ComponentDependency>>> collectDownstreamDependencies(
			Set<ComponentDependencyLayer> dependencyLayers,
			CodeComponentReference targetComponentReference,
			Integer depth) {
		List<Pair<CodeComponentReference, List<ComponentDependency>>> result;
		{
			if (depth > 0) {
				Pair<CodeComponentReference, List<ComponentDependency>> shallowDependencies = Pair.of(
						targetComponentReference,
						componentDependencySearcher
								.search(PlainQuery.builder()
										.predicate(Predicates.and(
												Predicates.equalTo(
														ComponentDependency.FROM_REFERENCE_ATTRIBUTE_NAME,
														CodeComponentReference.of(targetComponentReference.getCode())),
												Predicates.in(
														ComponentDependency.LAYER_ATTRIBUTE_NAME,
														dependencyLayers.toJavaSet())))
										.build()));
				//
				if (depth > 1) {
					List<Pair<CodeComponentReference, List<ComponentDependency>>> deepDependencies =
							shallowDependencies.getRight().flatMap(shallowDependency -> collectDownstreamDependencies(
									dependencyLayers,
									shallowDependency.getToReference(),
									depth - 1));
					//
					result = List.of(shallowDependencies).appendAll(deepDependencies);
				} else {
					result = List.of(shallowDependencies);
				}
			} else {
				result = List.of(Pair.of(targetComponentReference, List.empty()));
			}
		}
		return result;
	}

	private List<Pair<CodeComponentReference, List<ComponentDependency>>> collectUpstreamDependencies(
			Set<ComponentDependencyLayer> dependencyLayers,
			CodeComponentReference targetComponentReference,
			Integer depth) {
		List<Pair<CodeComponentReference, List<ComponentDependency>>> result;
		{
			if (depth > 0) {
				Pair<CodeComponentReference, List<ComponentDependency>> shallowDependencies = Pair.of(
						targetComponentReference,
						componentDependencySearcher
								.search(PlainQuery.builder()
										.predicate(Predicates.and(
												Predicates.equalTo(
														ComponentDependency.TO_REFERENCE_ATTRIBUTE_NAME,
														CodeComponentReference.of(targetComponentReference.getCode())),
												Predicates.in(
														ComponentDependency.LAYER_ATTRIBUTE_NAME,
														dependencyLayers.toJavaSet())))
										.build()));
				//
				if (depth > 1) {
					List<Pair<CodeComponentReference, List<ComponentDependency>>> deepDependencies =
							shallowDependencies.getRight().flatMap(shallowDependency -> collectDownstreamDependencies(
									dependencyLayers,
									shallowDependency.getFromReference(),
									depth - 1));
					//
					result = List.of(shallowDependencies).appendAll(deepDependencies);
				} else {
					result = List.of(shallowDependencies);
				}
			} else {
				result = List.of(Pair.of(targetComponentReference, List.empty()));
			}
		}
		return result;
	}

	public ComponentDiagramModel buildModel(ComponentDiagramModelConfiguration configuration) {
		List<ComponentDiagramPage.State.FocusEntry> focusEntries = configuration.getFocusEntries();
		boolean renderAll = configuration.getRenderAll();
		//
		Set<String> focusedComponentCodes = focusEntries
				.map(ComponentDiagramPage.State.FocusEntry::getComponentCode)
				.toSet();
		//
		List<ResolvedReference<Component, CodeComponentReference>> resolvedComponents;
		List<ComponentDependency> componentDependencies;
		{
			if (renderAll) {
				componentDependencies = componentDependencySearcher.search(PlainQuery.builder()
						.predicate(Predicates.in(
								ComponentDependency.LAYER_ATTRIBUTE_NAME,
								configuration.getDependencyLayers().toJavaSet()))
						.build());
				resolvedComponents = componentSearcher.searchAll().map(component -> ResolvedReference.of(
						CodeComponentReference.from(component),
						List.of(component)));
			} else {
				List<Pair<CodeComponentReference, List<ComponentDependency>>> collectedDependencies = focusEntries
						.flatMap(focusEntry -> collectDependencies(
								configuration.getDependencyLayers(),
								focusEntry.getComponentReference(),
								focusEntry.getUp(),
								focusEntry.getDown()));
				componentDependencies = collectedDependencies
						.flatMap(List::of)
						.flatMap(Pair::getRight)
						.distinctBy(ComponentDependency::getUid)
						.sortBy(ComponentDependency::getUid);
				resolvedComponents = collectedDependencies
						.flatMap(pair -> List.of(pair.getLeft()).appendAll(
								pair.getRight().flatMap(ComponentDependency::getNonNullComponentReferences)))
						.distinct()
						.sortBy(CodeComponentReference::getCode)
						.map(componentReference -> componentSearcher.resolve(componentReference));
			}
		}
		Set<String> componentCodes = resolvedComponents
				.map(resolvedComponent -> resolvedComponent.getReference().getCode())
				.toSet();
		//
		return ComponentDiagramModel.builder()
				.components(resolvedComponents.map(resolvedComponent -> ComponentDiagramComponent.builder()
						.code(PlantumlCode.ofRaw(resolvedComponent.getReference().getCode()))
						.groupCode(resolvedComponent.hasObjects()
								? PlantumlCode.ofRaw(resolvedComponent.getSingle().getModuleCode())
								: null)
						.name(resolvedComponent.hasObjects()
								? resolvedComponent.getSingle().getName()
								: resolvedComponent.getReference().getCode())
						.style(List
								.of(
										focusedComponentCodes.contains(resolvedComponent.getReference().getCode())
												? List.of(resolvedComponent.hasObjects() ? "lightblue" : "pink")
												: null,
										!resolvedComponent.hasObjects()
												? List.of("line.dashed", "line:red")
												: null)
								.filter(Objects::nonNull)
								.flatMap(List::ofAll))
						.build()))
				.groups(List.empty())
				.links(componentDependencies.map(componentDependency -> ComponentDiagramLink.builder()
						.fromCode(PlantumlCode.ofRaw(componentDependency.getFromReference().getCode()))
						.toCode(PlantumlCode.ofRaw(componentDependency.getToReference().getCode()))
						.build()))
				.prefixLines(List.of("skinparam componentStyle rectangle"))
				.build();
	}

	public List<Pair<CodeComponentReference, List<ComponentDependency>>> collectDependencies(
			Set<ComponentDependencyLayer> dependencyLayers,
			CodeComponentReference componentReference,
			Integer upstreamDepth,
			Integer downstreamDepth) {
		List<Pair<CodeComponentReference, List<ComponentDependency>>> result;
		{
			result = List.<Pair<CodeComponentReference, List<ComponentDependency>>>empty()
					.appendAll(collectUpstreamDependencies(dependencyLayers, componentReference, upstreamDepth))
					.appendAll(collectDownstreamDependencies(dependencyLayers, componentReference, downstreamDepth));
		}
		return result;
	}

	public List<Component> getAllComponents() {
		return componentSearcher.searchAll()
				.sortBy(Component::getName);
	}

	public static List<String> renderComponentDependencies(List<MavenModule> mavenModules) {
		Set<PlantumlCode> presentCodes = mavenModules
				.map(mavenModule -> PlantumlCode.ofRaw(mavenModule.getGroupIdArtifactId()))
				.toSet();
		return mavenModules
				.flatMap(mavenModule -> mavenModule.getDependencies().map(dependency ->
						RenderableComponentDependency.builder()
								.fromCode(PlantumlCode.ofRaw(mavenModule.getGroupIdArtifactId()))
								.text(dependency.getVersion())
								.toCode(PlantumlCode.ofRaw(dependency.getGroupIdArtifactId()))
								.build()))
				.filter(dependency -> presentCodes.contains(dependency.getToCode()))
				.map(RenderableComponentDependency::render);
	}
}
