package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.init.temporal.PandoraTemporalConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.WildcardObjectManipulator;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode.FunctionMergeNodeScopeHandler;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.KeyConfluencePageTemplateReference;

@LazyComponent
@RequiredArgsConstructor
public class PandoraDefaultConfluenceMergeStructureContributor implements ApplicationRunner {

	private final WildcardObjectManipulator objectManipulator;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		String code = "pandora-default";
		//
		ConfluenceMergeStructure confluenceMergeStructure = ConfluenceMergeStructure.builder()
				.code(code)
				.rootNodes(List.of(
						ConfluenceMergeNode.builder()
								.children(
										ConfluenceMergeNode.builder()
												.bodyTemplateReference(KeyConfluencePageTemplateReference.of("empty-page"))
												.children(ConfluenceMergeNode.builder()
														.bodyTemplateReference(KeyConfluencePageTemplateReference.of("function-page"))
														.locator("childTitle:${function.name} (${manifest.name})")
														.scope(FunctionMergeNodeScopeHandler.SCOPE_NAME)
														.titleTemplate("${function.name} (${manifest.name})")
														.build())
												.locator("childTitle:Functions (${manifest.name})")
												.scope(ConfluenceMergeNodeScope.MANIFEST.toString())
												.titleTemplate("Functions (${manifest.name})")
												.build(),
										ConfluenceMergeNode.builder()
												.bodyTemplateReference(KeyConfluencePageTemplateReference.of("empty-page"))
												.children(ConfluenceMergeNode.builder()
														.bodyTemplateReference(KeyConfluencePageTemplateReference.of("empty-page"))
														.children(ConfluenceMergeNode.builder()
																.bodyTemplateReference(KeyConfluencePageTemplateReference.of("rest-endpoint-page"))
																.locator("childTitle:${restEndpoint.name} (${restEndpoint.firstCoordinates}) (${manifest.name})")
																.scope(ConfluenceMergeNodeScope.REST_ENDPOINT.toString())
																.titleTemplate("${restEndpoint.name} (${restEndpoint.firstCoordinates}) (${manifest.name})")
																.build())
														.locator("childTitle:${restEndpointGroup.name} (${restEndpointGroup.normalizedPath}) (${manifest.name})")
														.scope(ConfluenceMergeNodeScope.REST_ENDPOINT_GROUP.toString())
														.titleTemplate("${restEndpointGroup.name} (${restEndpointGroup.normalizedPath}) (${manifest.name})")
														.build())
												.locator("childTitle:Endpoints (${manifest.name})")
												.scope(ConfluenceMergeNodeScope.MANIFEST.toString())
												.titleTemplate("Endpoints (${manifest.name})")
												.build(),
										ConfluenceMergeNode.builder()
												.bodyTemplateReference(KeyConfluencePageTemplateReference.of("persistent-models-diagram-page"))
												.children(ConfluenceMergeNode.builder()
														.bodyTemplateReference(KeyConfluencePageTemplateReference.of("persistent-model-page"))
														.locator("childTitle:${persistentModel.name} (${persistentModel.databaseName}) (${manifest.name})")
														.scope(ConfluenceMergeNodeScope.PERSISTENT_MODEL.toString())
														.titleTemplate("${persistentModel.name} (${persistentModel.databaseName}) (${manifest.name})")
														.build())
												.locator("childTitle:Persistent Models (${manifest.name})")
												.scope(ConfluenceMergeNodeScope.MANIFEST.toString())
												.titleTemplate("Persistent Models (${manifest.name})")
												.build(),
										ConfluenceMergeNode.builder()
												.bodyTemplateReference(KeyConfluencePageTemplateReference.of("properties-page"))
												.locator("childTitle:Properties (${manifest.name})")
												.scope(ConfluenceMergeNodeScope.MANIFEST.toString())
												.titleTemplate("Properties (${manifest.name})")
												.build())
								.locator("pageId:$rootPageId")
								.scope(ConfluenceMergeNodeScope.MANIFEST.toString())
								.build()))
				.uid(RootObjectUtils.computeUidFromValueHashCode(code))
				.build();
		//
		objectManipulator.createWithProvidedUid(
				confluenceMergeStructure,
				genericObject -> genericObject
						.withAddedAttribute(PandoraTemporalConstants.ATTRIBUTE));
	}
}
