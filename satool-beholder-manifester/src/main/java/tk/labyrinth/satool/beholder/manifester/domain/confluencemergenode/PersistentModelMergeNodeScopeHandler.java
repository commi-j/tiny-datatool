package tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.value.wrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class PersistentModelMergeNodeScopeHandler implements ConfluenceMergeNodeScopeHandler {

	@Override
	public List<String> getAttributeNames() {
		return List.of("persistentModel");
	}

	@Override
	public String getScopeName() {
		return "PERSISTENT_MODEL";
	}

	@Override
	public List<GenericObject> getVariablesObjects(String parentScopeName, GenericObject parentObject) {
		return parentObject.getAttributeValueAsObject("manifest")
				.getAttributeValueAsList("persistentModels")
				.map(ValueWrapper::asObject)
				.map(ObjectValueWrapper::unwrap)
				.map(persistentModel -> parentObject.withAddedObjectAttribute("persistentModel", persistentModel));
	}
}
