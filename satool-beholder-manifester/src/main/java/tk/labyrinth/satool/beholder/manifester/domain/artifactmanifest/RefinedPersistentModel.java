package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderPattern("$name ($databaseName)")
@Value
@With
public class RefinedPersistentModel {

	List<PersistentModelAttribute> attributes;

	String databaseName;

	ClassSignature javaSignature;

	@Deprecated // FIXME: Used in pattern, but must be function of signature.
	String name;

	List<Tag> tags;

	public List<ClassSignature> collectClassSignatures() {
		return getAttributesOrEmpty()
				.map(PersistentModelAttribute::getJavaFieldType)
				.flatMap(TypeDescription::breakDown)
				.map(TypeDescription::getSignature)
				.filter(TypeSignature::isDeclaredOrVariable)
				.map(TypeSignature::getClassSignature)
				.prepend(javaSignature)
				.distinct();
	}

	public List<PersistentModelAttribute> getAttributesOrEmpty() {
		return attributes != null ? attributes : List.empty();
	}
}
