package tk.labyrinth.satool.beholder.manifester.domain.persistentmodel;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model
@ModelTag("persistence")
@Value
@With
public class PersistentModelIndex implements ObjectIndex<ArtifactManifest> {

	List<PersistentModelAttribute> attributes;

	String databaseName;

	ClassSignature javaClassSignature;

	List<Tag> tags;

	UidReference<ArtifactManifest> targetReference;
}
