package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model;

import org.checkerframework.checker.nullness.qual.Nullable;

public interface ConfluencePublishTarget {

	@Nullable
	String getContentId();
}
