package tk.labyrinth.satool.beholder.manifester.domain.azurefunctions;

import com.microsoft.azure.functions.annotation.BindingName;
import com.microsoft.azure.functions.annotation.BlobTrigger;
import com.microsoft.azure.functions.annotation.CosmosDBTrigger;
import com.microsoft.azure.functions.annotation.EventGridTrigger;
import com.microsoft.azure.functions.annotation.EventHubTrigger;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpOutput;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import com.microsoft.azure.functions.annotation.KafkaTrigger;
import com.microsoft.azure.functions.annotation.QueueTrigger;
import com.microsoft.azure.functions.annotation.ServiceBusQueueTrigger;
import com.microsoft.azure.functions.annotation.ServiceBusTopicTrigger;
import com.microsoft.azure.functions.annotation.TimerTrigger;
import com.microsoft.azure.functions.annotation.WarmupTrigger;
import com.microsoft.durabletask.azurefunctions.DurableActivityTrigger;
import com.microsoft.durabletask.azurefunctions.DurableOrchestrationTrigger;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.gradle.internal.Pair;
import org.springframework.http.HttpMethod;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributorUtils;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionParametersEntry;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionResultEntry;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;
import tk.labyrinth.satool.beholder.manifester.domain.function.http.HttpFunctionCoordinates;
import tk.labyrinth.satool.beholder.manifester.tool.ExposedClassDiscoverer;
import tk.labyrinth.satool.spoon.CtMethodUtils;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;

import java.lang.annotation.Annotation;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class AzureFunctionsManifestContributor implements ManifestContributor {

	private static final CtTypeReference<? extends Annotation> functionNameTypeReference = CtTypeReferenceUtils
			.createFrom(FunctionName.class);

	private static final List<Class<? extends Annotation>> httpFunctionAnnotations = List.of(
			HttpTrigger.class);

	private static final List<CtTypeReference<? extends Annotation>> triggerTypeReferences = List
			.of(
					BlobTrigger.class,
					CosmosDBTrigger.class,
					DurableActivityTrigger.class,
					DurableOrchestrationTrigger.class,
					EventGridTrigger.class,
					EventHubTrigger.class,
					HttpTrigger.class,
					KafkaTrigger.class,
					QueueTrigger.class,
					ServiceBusQueueTrigger.class,
					ServiceBusTopicTrigger.class,
					TimerTrigger.class,
					WarmupTrigger.class)
			.map(CtTypeReferenceUtils::createFrom);

	@Override
	public List<?> contribute(Path rootDirectory, CtModel javaModel) {
		List<?> result;
		{
			List<CtMethod<?>> functionMethods = List.ofAll(javaModel.getAllTypes())
					.flatMap(type -> List.ofAll(type.getMethods()))
					.filter(method -> method.getAnnotation(functionNameTypeReference) != null);
			//
			List<ManifestFunction> functions = functionMethods
					.map(AzureFunctionsManifestContributor::resolveFunction)
					.filter(Objects::nonNull);
			//
			List<ExposedClass> exposedClasses = ExposedClassDiscoverer.discoverForFunctions(javaModel, functions);
			//
			result = List.of(functions, exposedClasses).flatMap(Function.identity());
		}
		return result;
	}

	@Override
	public List<String> getExtraClasspathEntries() {
		return List
				.of(
						DurableOrchestrationTrigger.class, // com.microsoft:durable-azure-functions
						FunctionName.class, // com.microsoft.azure.functions:azure-functions-java-library
						com.microsoft.azure.functions.HttpMethod.class) // com.microsoft.azure.functions:azure-functions-java-core-library
				.map(ManifestContributorUtils::getLocationOfClassSource);
	}

	@Nullable
	public static ManifestFunction resolveFunction(CtMethod<?> method) {
		ManifestFunction result;
		{
			List<? extends CtAnnotation<? extends Annotation>> triggerAnnotations = List.ofAll(method.getParameters())
					.flatMap(parameter -> triggerTypeReferences
							.map(parameter::getAnnotation)
							.filter(Objects::nonNull));
			//
			if (triggerAnnotations.size() != 1) {
				throw new IllegalArgumentException(triggerAnnotations.toString());
			}
			//
			CtAnnotation<? extends Annotation> triggerAnnotation = triggerAnnotations.get();
			//
			if (CtTypeReferenceUtils.equals(triggerAnnotation.getType(), HttpTrigger.class)) {
				result = resolveHttpFunction(method);
			} else {
				// TODO: Support other triggers.
				//
				result = null;
			}
		}
		return result;
	}

	public static ManifestFunction resolveHttpFunction(CtMethod<?> method) {
		ManifestFunction result;
		{
			FunctionName functionNameAnnotation = method.getAnnotation(FunctionName.class);
			//
			val httpTriggerParameterAndAnnotation = List.ofAll(method.getParameters())
					.map(parameter -> Pair.of(parameter, parameter.getAnnotation(HttpTrigger.class)))
					.filter(pair -> pair.getRight() != null)
					.single();
			//
			result = ManifestFunction.builder()
					.accessControl(resolveHttpFunctionAccessControl(httpTriggerParameterAndAnnotation.getRight()))
					.coordinateses(resolveHttpFunctionCoordinates(
							httpTriggerParameterAndAnnotation.getRight(),
							functionNameAnnotation))
					.javaSignature(CtMethodUtils.getFullSignature(method))
					.name(functionNameAnnotation.value())
					.parameters(List.ofAll(method.getParameters())
							.map(parameter -> resolveHttpParameter(
									httpTriggerParameterAndAnnotation.getRight().route(),
									parameter))
							.filter(Objects::nonNull))
					.result(resolveHttpResult(method))
					.build();
		}
		return result;
	}

	public static String resolveHttpFunctionAccessControl(HttpTrigger httpTriggerAnnotation) {
		return httpTriggerAnnotation.authLevel().toString();
	}

	public static List<HttpFunctionCoordinates> resolveHttpFunctionCoordinates(
			HttpTrigger httpTriggerAnnotation,
			FunctionName functionNameAnnotation) {
		List<HttpFunctionCoordinates> result;
		{
			if (httpTriggerAnnotation.methods().length > 0) {
				result = List.of(httpTriggerAnnotation.methods()).map(httpMethod -> HttpFunctionCoordinates.builder()
						.method(HttpMethod.valueOf(httpMethod.name()))
						.path(resolveHttpFunctionPath(httpTriggerAnnotation, functionNameAnnotation))
						.build());
			} else {
				result = List.of(HttpFunctionCoordinates.builder()
						.method(null)
						.path(resolveHttpFunctionPath(httpTriggerAnnotation, functionNameAnnotation))
						.build());
			}
		}
		return result;
	}

	public static String resolveHttpFunctionPath(
			HttpTrigger httpTriggerAnnotation,
			FunctionName functionNameAnnotation) {
		String result;
		{
			if (!httpTriggerAnnotation.route().isEmpty()) {
				result = httpTriggerAnnotation.route();
			} else {
				// TODO: Not sure if leading slash should be appended here.
				result = "/api/%s".formatted(functionNameAnnotation.value());
			}
		}
		return result;
	}

	@Nullable
	public static FunctionParametersEntry resolveHttpParameter(String path, CtParameter<?> parameter) {
		FunctionParametersEntry result;
		{
			BindingName bindingName = parameter.getAnnotation(BindingName.class);
			HttpTrigger httpTrigger = parameter.getAnnotation(HttpTrigger.class);
			//
			if (bindingName != null) {
				String name = bindingName.value();
				//
				result = FunctionParametersEntry.builder()
						.comments(List.of("-"))
						.constraints(List.of("-"))
						.datatype(CtTypeReferenceUtils.getTypeDescription(parameter.getType()))
						.kind(path.contains("{%s}".formatted(name))
								? FunctionParametersEntry.Kind.PATH
								: FunctionParametersEntry.Kind.UNDEFINED)
						.name(name)
						.build();
			} else if (httpTrigger != null) {
				result = FunctionParametersEntry.builder()
						.comments(List.of("-"))
						.constraints(List.of("-"))
						.datatype(CtTypeReferenceUtils.getTypeDescription(parameter.getType()))
						.kind(FunctionParametersEntry.Kind.UNDEFINED)
						.name("-")
						.build();
			} else {
				result = null;
			}
		}
		return result;
	}

	public static List<FunctionResultEntry> resolveHttpResult(CtMethod<?> method) {
		List<FunctionResultEntry> result;
		{
			CtParameter<?> httpOutputParameter = List.ofAll(method.getParameters())
					.filter(parameter -> parameter.hasAnnotation(HttpOutput.class))
					.getOrNull();
			//
			if (httpOutputParameter != null) {
				result = List.of(FunctionResultEntry.builder()
						.comments(List.of("-"))
						.constraints(List.of("-"))
						.datatype(CtTypeReferenceUtils.getTypeDescription(
								httpOutputParameter.getType().getActualTypeArguments().get(0)))
						.kind(FunctionResultEntry.Kind.BODY)
						.name("-")
						.build());
			} else {
				result = List.of(FunctionResultEntry.builder()
						.comments(List.of("-"))
						.constraints(List.of("-"))
						.datatype(CtTypeReferenceUtils.getTypeDescription(method.getType()))
						.kind(FunctionResultEntry.Kind.UNDEFINED)
						.name("-")
						.build());
			}
		}
		return result;
	}
}
