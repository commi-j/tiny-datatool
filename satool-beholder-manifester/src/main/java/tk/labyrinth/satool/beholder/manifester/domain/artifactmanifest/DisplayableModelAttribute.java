package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class DisplayableModelAttribute {

	List<String> comments;

	List<String> constraints;

	TypeDescription datatype;

	String name;
}
