package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceConnectorData;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceCreateTarget;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluencePublishTarget;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceUpdateTarget;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceGetContentQuery;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceGetContentSearchQuery;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceQueryLanguagePredicate;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceSpace;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceVersion;
import tk.labyrinth.satool.confluenceconnector.publish.ConfluenceContentLocatorKind;

public class ConfluencePublishingUtils {

	public static ConfluencePublishTarget getPublishTarget(
			ConfluenceConnectorData confluenceConnectorData,
			@Nullable String parentContentId,
			String locator) {
		ConfluencePublishTarget result;
		{
			ConfluenceContentLocatorKind locatorKind = ConfluenceContentLocatorKind.detect(locator);
			String locatorPayload = locatorKind.getPayload(locator);
			//
			result = switch (locatorKind) {
				case CHILD_TITLE -> {
					if (parentContentId == null) {
						throw new IllegalArgumentException();
					}
					//
					List<ConfluenceContent> children = confluenceConnectorData.getFeignClient()
							.getContentSearch(
									confluenceConnectorData.getAuthorization(),
									ConfluenceGetContentSearchQuery.builder()
											.cql(ConfluenceQueryLanguagePredicate.builder()
													.parent(parentContentId)
													.titles(List.of(locatorPayload))
													.build()
													.toString())
											.expand(ConfluenceVersion.EXPAND_KEY)
											.build())
							.getResults();
					//
					if (children.size() > 1) {
						throw new IllegalArgumentException();
					}
					//
					ConfluenceContent content = children.getOrNull();
					//
					ConfluenceContent parentContent = confluenceConnectorData.getFeignClient().getContent(
							confluenceConnectorData.getAuthorization(),
							parentContentId,
							ConfluenceGetContentQuery.builder()
									.expand(ConfluenceSpace.EXPAND_KEY)
									.build());
					//
					yield content != null
							? ConfluenceUpdateTarget.builder()
							.contentId(content.getId())
							.versionNumber(content.getVersion().getNumber() + 1)
							.build()
							: ConfluenceCreateTarget.builder()
							.parentContentId(parentContent.getId())
							.parentSpaceKey(parentContent.getSpace().getKey())
							.build();
				}
				case CONTENT_ID, PAGE_ID -> {
					ConfluenceContent content = confluenceConnectorData.getFeignClient().getContent(
							confluenceConnectorData.getAuthorization(),
							locatorPayload,
							ConfluenceGetContentQuery.builder()
									.expand(ConfluenceVersion.EXPAND_KEY)
									.build());
					//
					if (content == null) {
						throw new IllegalArgumentException();
					}
					//
					yield ConfluenceUpdateTarget.builder()
							.contentId(content.getId())
							.versionNumber(content.getVersion().getNumber() + 1)
							.build();
				}
			};
		}
		return result;
	}
}
