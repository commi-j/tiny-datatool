package tk.labyrinth.satool.beholder.manifester.dependencymanager;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.java.net.UrlUtils;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.java.util.regex.RegexUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.ArtifactSignature;
import tk.labyrinth.satool.maven.connector.MavenRepositoryConnector;
import tk.labyrinth.satool.maven.domain.MavenRepository;

import java.net.URL;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

@LazyComponent
@RequiredArgsConstructor
public class MavenDependencyManager {

	private final MavenRepositoryConnector mavenRepositoryConnector;

	@SmartAutowired
	private TypedObjectManipulator<MavenArtifactRepositoryStatus> mavenArtifactRepositoryStatusManipulator;

	@SmartAutowired
	private TypedObjectSearcher<MavenArtifactRepositoryStatus> mavenArtifactRepositoryStatusSearcher;

	@SmartAutowired
	private TypedObjectManipulator<MavenBom> mavenBomManipulator;

	@SmartAutowired
	private TypedObjectSearcher<MavenBom> mavenBomSearcher;

	private List<MavenBom> findOrFetchBoms(
			List<MavenRepository> repositories,
			List<ArtifactSignature> bomArtifactSignatures) {
		bomArtifactSignatures.forEach(ArtifactSignature::requireFull);
		//
		List<MavenBom> boms = mavenBomSearcher.search(PlainQuery.builder()
				.predicate(Predicates.in(MavenBom.SIGNATURE_ATTRIBUTE_NAME, bomArtifactSignatures))
				.build());
		//
		Map<SignatureMavenBomReference, MavenBom> referenceToMavenBomMap = bomArtifactSignatures
				.map(SignatureMavenBomReference::of)
				.toMap(
						Function.identity(),
						mavenBomSearcher::findSingle);
		//
		List<MavenBom> mavenBoms = referenceToMavenBomMap
				.map(tuple -> {
					MavenBom result;
					{
						if (tuple._2() == null) {
							Instant now = Instant.now();
							//
							Pair<MavenRepository, Model> foundBomPair = repositories.toStream()
									.map(repository -> Pair.of(
											repository,
											mavenRepositoryConnector.fetchPom(
													computeUrl(repository),
													tuple._1().getSignature().toMavenCoordinates())))
									.filter(pair -> pair.getRight() != null)
									.getOrNull();
							//
							if (foundBomPair != null) {
								mavenArtifactRepositoryStatusManipulator.createWithGeneratedUid(
										MavenArtifactRepositoryStatus.builder()
												.artifactSignature(ArtifactSignature.from(foundBomPair.getRight()))
												.checkedAt(now)
												.exists(true)
												.repositoryUrl(Optional.ofNullable(foundBomPair.getLeft().getUrl())
														.map(UrlUtils::from)
														.orElse(null))
												.build());
								//
								MavenBom mavenBom = createBom(foundBomPair.getRight());
								mavenBomManipulator.createWithGeneratedUid(mavenBom);
								//
								result = mavenBom;
							} else {
								// TODO: Handle artifact not found in repository.
								//
								result = null;
							}
						} else {
							result = tuple._2();
						}
					}
					return result;
				})
				.filter(Objects::nonNull)
				.toList();
		//
		return mavenBoms;
	}

	// FIXME: Do we need it?
	private List<MavenArtifactRepositoryStatus> resolveArtifactAvailability(
			List<MavenRepository> repositories,
			List<ArtifactSignature> bomArtifactSignatures) {
		bomArtifactSignatures.crossProduct(repositories)
				.map(tuple -> ArtifactSignatureAndRepositoryUrlMavenArtifactRepositoryStatusReference.of(
						tuple._1(),
						Optional.ofNullable(tuple._2().getUrl()).map(UrlUtils::from).orElse(null)))
				.map(reference -> {
					MavenArtifactRepositoryStatus status;
					{
						MavenArtifactRepositoryStatus foundStatus = mavenArtifactRepositoryStatusSearcher.findSingle(
								reference);
						//
						if (foundStatus != null) {
							status = foundStatus;
						} else {
							Model model = mavenRepositoryConnector.fetchPom(
									reference.getRepositoryUrl(),
									reference.getArtifactSignature().toMavenCoordinates());
							//
							MavenArtifactRepositoryStatus.builder()
									.artifactSignature(reference.getArtifactSignature())
									.checkedAt(Instant.now())
									.exists(true)
									.repositoryUrl(reference.getRepositoryUrl())
									.build();
							mavenArtifactRepositoryStatusManipulator.create(null);
						}
					}
					return null;
				});
		return null;
	}

	public ArtifactSignature findVersion(
			List<MavenRepository> repositories,
			List<ArtifactSignature> bomArtifactSignatures,
			ArtifactSignature lookupArtifactSignature) {
		lookupArtifactSignature.requireNoVersion();
		//
		ArtifactSignature result;
		{
			List<MavenBom> boms = findOrFetchBoms(repositories, bomArtifactSignatures);
			//
			if (!boms.isEmpty()) {
				result = boms
						.flatMap(MavenBom::getDependencies)
						.filter(dependency -> Objects.equals(dependency.withoutVersion(), lookupArtifactSignature))
						.peekOption()
						.getOrElse(lookupArtifactSignature);
			} else {
				result = lookupArtifactSignature;
			}
		}
		return result;
	}

	private static Dependency processPlaceholders(Properties properties, Dependency dependency) {
		Dependency result;
		{
			Dependency newDependency = dependency.clone();
			newDependency.setArtifactId(processPlaceholders(properties, dependency.getArtifactId()));
			newDependency.setGroupId(processPlaceholders(properties, dependency.getGroupId()));
			newDependency.setVersion(processPlaceholders(properties, dependency.getVersion()));
			result = newDependency;
		}
		return result;
	}

	private static String processPlaceholders(Properties properties, String text) {
		String result;
		{
			List<MatchResult> matchResults = List.ofAll(RegexUtils.findAll(text, Pattern.compile("\\$\\{(.*)}")));
			if (!matchResults.isEmpty()) {
				result = matchResults.toJavaStream().reduce(
						text,
						(nextText, matchResult) -> {
							Object value = properties.get(matchResult.group(1));
							return value != null
									? nextText.replace(matchResult.group(), value.toString())
									: nextText;
						},
						FunctionUtils::throwUnreachableStateException);
			} else {
				result = text;
			}
		}
		return result;
	}

	public static URL computeUrl(MavenRepository repository) {
		URL result;
		{
			if (repository.getUrl() != null) {
				result = UrlUtils.from(repository.getUrl());
			} else if (Objects.equals(repository.getName(), "Maven Central")) {
				result = MavenRepositoryConnector.MAVEN_CENTRAL_URL;
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public static MavenBom createBom(Model model) {
		List<Dependency> processedDependencies = List.ofAll(model.getDependencyManagement().getDependencies())
				.map(dependency -> processPlaceholders(model.getProperties(), dependency));
		//
		return MavenBom.builder()
				.dependencies(processedDependencies.map(ArtifactSignature::from))
				.imports(processedDependencies
						.filter(dependency -> Objects.equals(dependency.getScope(), "import"))
						.map(ArtifactSignature::from))
				.signature(ArtifactSignature.from(model))
				.build();
	}
}
