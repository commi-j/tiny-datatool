package tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.mongodb;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import spoon.reflect.CtModel;
import spoon.reflect.code.CtExpression;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtType;
import spoon.reflect.path.CtRole;
import spoon.reflect.reference.CtFieldReference;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;
import tk.labyrinth.satool.beholder.manifester.tool.ExposedClassDiscoverer;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.SpoonStaticFactory;
import tk.labyrinth.satool.spoon.SpoonUtils;

import java.nio.file.Path;
import java.util.Map;
import java.util.function.Function;

@LazyComponent
public class SpringDataMongoDbManifestContributor implements ManifestContributor {

	@Override
	public List<?> contribute(Path rootDirectory, CtModel javaModel) {
		List<?> result;
		{
			List<CtType<?>> documentTypes = Stream.ofAll(javaModel.getAllTypes())
					.filter(SpringDataMongoDbManifestContributor::isDocument)
					.toList();
			//
			List<PersistentModel> persistentModels = documentTypes.map(documentType -> PersistentModel.builder()
					.attributes(resolveAttributes(documentType))
					.databaseName(resolveModelDatabaseName(documentType))
					.javaClassSignature(SpoonUtils.resolveJavaClassSignature(documentType))
					.tags(List.of(Tag.of("MONGODB_DOCUMENT")))
					.build());
			//
			List<ExposedClass> exposedClasses = ExposedClassDiscoverer
					.discoverForPersistentModels(javaModel, persistentModels);
			//
			result = List.of(persistentModels, exposedClasses).flatMap(Function.identity());
		}
		return result;
	}

	public static boolean isDocument(CtType<?> type) {
		return type.hasAnnotation(Document.class);
	}

	public static Boolean isPrimaryKey(CtField<?> field) {
		return field.hasAnnotation(Id.class);
	}

	public static List<String> resolveAttributeDatabaseNames(CtField<?> field) {
		List<String> result;
		{
			result = List.of(field.getSimpleName());
		}
		return result;
	}

	public static List<PersistentModelAttribute> resolveAttributes(CtType<?> documentType) {
		return List.ofAll(documentType.getAllFields())
				.map(CtFieldReference::getDeclaration)
				.filter(field -> !field.hasAnnotation(Transient.class))
				.map(field -> PersistentModelAttribute.builder()
						.databaseNames(resolveAttributeDatabaseNames(field))
						.javaFieldName(field.getSimpleName())
						.javaFieldType(CtTypeReferenceUtils.getTypeDescription(field.getType()))
						.tags(isPrimaryKey(field) ? List.of(PersistentModelAttribute.PRIMARY_KEY_TAG) : null)
						.build());
	}

	public static String resolveModelDatabaseName(CtType<?> documentType) {
		String result;
		{
			CtAnnotation<?> documentAnnotation = documentType.getAnnotation(SpoonStaticFactory.createReference(
					Document.class));
			Map<String, CtExpression> documentAnnotationValues = documentAnnotation.getValues();
			//
			CtExpression<?> collectionValue = ObjectUtils.findFirstNonDefaultValue(
					null,
					() -> documentAnnotationValues.get("collection"),
					() -> documentAnnotationValues.get("value"));
			//
			if (collectionValue != null) {
				result = collectionValue.getValueByRole(CtRole.VALUE);
			} else {
				result = documentType.getSimpleName().toLowerCase();
			}
		}
		return result;
	}
}
