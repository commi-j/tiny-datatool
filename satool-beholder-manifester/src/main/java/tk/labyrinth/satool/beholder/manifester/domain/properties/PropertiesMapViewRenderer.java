package tk.labyrinth.satool.beholder.manifester.domain.properties;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;
import tk.labyrinth.satool.plantuml.common.model.DiagramGroup;
import tk.labyrinth.satool.plantuml.common.model.DiagramLink;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramComponent;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramLink;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramModel;
import tk.labyrinth.satool.plantuml.view.PlantumlDiagramView;

import java.util.Objects;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PropertiesMapViewRenderer {

	public static final DiagramLink DOWN_LINK = DiagramLink.builder()
			.end(DiagramLink.Arrow.NONE)
			.direction(DiagramLink.Direction.DOWN)
			.build();

	private final ObjectProvider<PlantumlDiagramView> diagramViewProvider;

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		//
		PlantumlDiagramView view = diagramViewProvider.getObject();
		//
		view.setParameters(builder -> builder
				.value(createDiagramModel(
						parameters.getPropertyDeclarationEntries(),
						parameters.getPropertyUsageEntries(),
						parameters.getHighlightedPropertyName())
						.render())
				.build());
		//
		return view;
	}

	// TODO: Make it trim correct number of chars so that the result is always <=50 (e.g 0-9, 10-99, 100+).
	public static String composeValueText(String value) {
		return value.length() > 50
				? "%s… (%s more)".formatted(value.substring(0, 40), value.length() - 40)
				: value;
	}

	public static ComponentDiagramModel createDiagramModel(
			List<PropertyDeclarationEntry> propertyDeclarationEntries,
			List<PropertyUsageEntry> propertyUsageEntries,
			@Nullable String highlightedPropertyName) {
		Map<String, List<PropertyDeclarationEntry>> locationToDeclarationsMap = propertyDeclarationEntries
				.groupBy(PropertyDeclarationEntry::getLocation);
		//
		// Environment
		// Environment.Expected
		//			FILE_USAGES_WITHOUT_DECLARATIONS
		//			PROPERTIES USAGES?
		//
		//	Files
		List<DiagramGroup> fileGroups = locationToDeclarationsMap.map(tuple -> DiagramGroup.builder()
						.code(PlantumlCode.ofRaw(tuple._1()))
						.kind(DiagramGroup.FRAME_KIND)
						.name("%s ".formatted(tuple._1()))
						.parentCode(PlantumlCode.ofValid("files"))
						.build())
				.reverse()
				.toList();
		List<DiagramGroup> filesGroup = !fileGroups.isEmpty()
				? List.of(DiagramGroup.builder()
				.code(PlantumlCode.ofValid("files"))
				.kind(DiagramGroup.FRAME_KIND)
				.name("Files ")
				.build())
				: List.empty();
		List<ComponentDiagramComponent> fileBasedComponents = locationToDeclarationsMap.values()
				.flatMap(Function.identity())
				.map(declarationEntry -> ComponentDiagramComponent.builder()
						.code(createPlantCode(declarationEntry))
						.groupCode(PlantumlCode.ofRaw(declarationEntry.getLocation()))
						.name(declarationEntry.getName())
						.style(Objects.equals(declarationEntry.getName(), highlightedPropertyName)
								? List.of("lightblue")
								: null)
						.text(composeValueText(declarationEntry.getValue()))
						.build())
				.toList();
		List<ComponentDiagramLink> fileBasedGapLinks = locationToDeclarationsMap
				.flatMap(tuple -> !tuple._2().isEmpty()
						? tuple._2().zip(tuple._2().tail())
						.map(tuple2 -> ComponentDiagramLink.builder()
								.colour("#FFF")
								.fromCode(createPlantCode(tuple2._1()))
								.link(DOWN_LINK)
								.toCode(createPlantCode(tuple2._2()))
								.build())
						: List.empty())
				.toList();
		List<ComponentDiagramLink> fileBasedLinks = propertyUsageEntries
				.filter(usageEntry -> usageEntry.getTags().contains("file"))
				.flatMap(usageEntry -> propertyDeclarationEntries
						.filter(declarationEntry -> Objects.equals(declarationEntry.getName(), usageEntry.getName()))
						.map(declarationEntry -> ComponentDiagramLink.builder()
								.fromCode(createPlantCode(declarationEntry))
								.link(DiagramLink.builder()
										.direction(DiagramLink.Direction.HORIZONTAL)
										.build())
								.toCode(createPlantCode(usageEntry))
								.build()));
		//
		// Java
		List<ComponentDiagramComponent> javaBasedComponents = propertyUsageEntries
				.filter(usageEntry -> usageEntry.getTags().contains("java"))
				.map(usageEntry -> ComponentDiagramComponent.builder()
						.code(createPlantCode(usageEntry))
						.groupCode(PlantumlCode.ofValid("java"))
						.name(ElementSignature.of(usageEntry.getLocation()).toLongString())
						.style(Objects.equals(usageEntry.getName(), highlightedPropertyName)
								? List.of("lightblue")
								: null)
						.build());
		List<DiagramGroup> javaGroup = !javaBasedComponents.isEmpty()
				? List.of(DiagramGroup.builder()
				.code(PlantumlCode.ofValid("java"))
				.kind(DiagramGroup.FRAME_KIND)
				.name("Java ")
				.build())
				: List.empty();
		List<ComponentDiagramLink> javaBasedGapLinks = !javaBasedComponents.isEmpty()
				? javaBasedComponents.zip(javaBasedComponents.tail())
				.map(tuple -> ComponentDiagramLink.builder()
						.colour("#FFF")
						.fromCode(tuple._1().getCode())
						.link(DOWN_LINK)
						.toCode(tuple._2().getCode())
						.build())
				: List.empty();
		List<ComponentDiagramLink> javaBasedReferences = propertyUsageEntries
				.filter(usageEntry -> usageEntry.getTags().contains("java"))
				.flatMap(usageEntry -> propertyDeclarationEntries
						.filter(declarationEntry -> Objects.equals(declarationEntry.getName(), usageEntry.getName()))
						.map(declarationEntry -> ComponentDiagramLink.builder()
								.fromCode(createPlantCode(declarationEntry))
								.link(DiagramLink.builder()
										.direction(DiagramLink.Direction.HORIZONTAL)
										.build())
								.toCode(createPlantCode(usageEntry))
								.build()));
		//
		return ComponentDiagramModel.builder()
				.components(List
						.of(
								fileBasedComponents,
								javaBasedComponents)
						.flatMap(Function.identity()))
				.groups(List
						.of(
								filesGroup,
								fileGroups,
								javaGroup)
						.flatMap(Function.identity()))
				.links(List
						.of(
								fileBasedGapLinks,
								javaBasedGapLinks,
								fileBasedLinks,
								javaBasedReferences)
						.flatMap(Function.identity()))
				.prefixLines(List.of("skinparam componentStyle rectangle"))
				.build();
	}

	public static PlantumlCode createPlantCode(PropertyDeclarationEntry propertyDeclarationEntry) {
		return PlantumlCode.ofRaw("%s:%s".formatted(
				propertyDeclarationEntry.getLocation(),
				propertyDeclarationEntry.getName()));
	}

	public static PlantumlCode createPlantCode(PropertyUsageEntry propertyUsageEntry) {
		String rawCode;
		{
			if (propertyUsageEntry.getTags().contains("java")) {
				rawCode = propertyUsageEntry.getLocation();
			} else if (propertyUsageEntry.getTags().contains("file")) {
				rawCode = propertyUsageEntry.getLocation();
			} else {
				throw new NotImplementedException();
			}
		}
		return PlantumlCode.ofRaw(rawCode);
	}

	public static String renderToString(Parameters parameters) {
		return createDiagramModel(
				parameters.getPropertyDeclarationEntries(),
				parameters.getPropertyUsageEntries(),
				parameters.getHighlightedPropertyName())
				.render();
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		String highlightedPropertyName;

		List<PropertyDeclarationEntry> propertyDeclarationEntries;

		List<PropertyUsageEntry> propertyUsageEntries;

		public static class Builder {
			// Lomboked
		}
	}
}