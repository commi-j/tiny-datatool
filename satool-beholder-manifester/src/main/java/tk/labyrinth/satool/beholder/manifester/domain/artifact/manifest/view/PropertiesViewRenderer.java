package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.VavrListToVaadinComponentRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.properties.PropertiesMapViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.properties.PropertiesUtils;
import tk.labyrinth.satool.beholder.manifester.domain.properties.Property;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PropertiesViewRenderer {

	private final PropertyDeclarationsViewRenderer declarationsViewRenderer;

	private final PropertiesMapViewRenderer propertiesMapViewRenderer;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	private final PropertyUsagesViewRenderer usagesViewRenderer;

	private Component createFilterComponent(String name, @Nullable String filter, Consumer<String> onFilterChange) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.add(name);
			layout.addStretch();
			{
				if (filter != null) {
					layout.add(filter);
					layout.addStretch();
				}
			}
			{
				layout.add(ButtonRenderer.render(buttonBuilder -> buttonBuilder
						.icon(VaadinIcon.FILTER.create())
						.onClick(event -> ConfirmationViews
								.showFunctionDialog(
										Optional.ofNullable(filter),
										(currentFilter, filterSink) -> StringBoxRenderer.render(stringBoxBuilder ->
												stringBoxBuilder
														.onValueChange(nextValue ->
																filterSink.accept(Optional.ofNullable(nextValue)))
														.value(currentFilter.orElse(null))
														.build()))
								.subscribeAlwaysAccepted(result -> onFilterChange.accept(result.orElse(null))))
						.themeVariants(ButtonVariant.LUMO_SMALL)
						.build()));
			}
		}
		//
		return layout;
	}

	private Component doRender(State state, Consumer<State> sink) {
		List<Property> properties = PropertiesUtils.groupProperties(
				state.getProperties().getPropertyDeclarationEntries(),
				state.getProperties().getPropertyUsageEntries());
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			CssHorizontalLayout toolbar = new CssHorizontalLayout();
			{
				CssHorizontalLayout tagsSelector = new CssHorizontalLayout();
				{
					tagsSelector.setAlignItems(AlignItems.BASELINE);
				}
				{
					tagsSelector.add("Tags: ");
					//
					CheckboxGroup<String> checkboxGroup = new CheckboxGroup<>();
					checkboxGroup.setItems(properties
							.flatMap(Property::computeTags)
							.distinct()
							.sorted()
							.asJava());
					tagsSelector.add(checkboxGroup);
				}
				toolbar.add(tagsSelector);
			}
			{
				toolbar.addStretch();
			}
			{
				toolbar.add(ButtonRenderer.render(builder -> builder
						.onClick(event -> ConfirmationViews.showFunctionDialog(
								PropertiesMapViewRenderer.Parameters.builder()
										.highlightedPropertyName(null)
										.propertyDeclarationEntries(state.getProperties()
												.getPropertyDeclarationEntries())
										.propertyUsageEntries(state.getProperties().getPropertyUsageEntries())
										.build(),
								(propertiesMapProperties, propertiesMapSink) ->
										propertiesMapViewRenderer.render(propertiesMapProperties)))
						.text("Diagram")
						.build()));
			}
			layout.add(toolbar);
		}
		{
			Grid<Property> grid = new Grid<>();
			grid.setHeightFull();
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			//
			grid
					.addColumn(Property::getName)
					.setHeader(createFilterComponent(
							"Name",
							state.getNameFilter(),
							nextFilter -> sink.accept(state.withNameFilter(nextFilter))))
					.setResizable(true);
			grid
					.addColumn(item -> "%s declarations, %s usages"
							.formatted(item.getDeclarationEntries().size(), item.getUsageEntries().size()))
					.setHeader("Declarations / Usages")
					.setResizable(true);
			grid
					.addComponentColumn(item -> GridUtils.renderValueList(item.getUsageEntries()
							.map(PropertyUsageEntry::getDefaultValue)
							.filter(Objects::nonNull)))
					.setHeader("Default Value")
					.setResizable(true);
			grid
					.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
							ToVaadinComponentRendererRegistry.Context.builder()
									.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
											TypeUtils.parameterize(List.class, TypeDescription.class)))
									.hints(List.empty())
									.build(),
							resolveTypes(state.getProperties(), item)))
					.setHeader("Type")
					.setResizable(true);
			grid
					.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
							ToVaadinComponentRendererRegistry.Context.builder()
									.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
											TypeUtils.parameterize(List.class, String.class)))
									.hints(List.of(VavrListToVaadinComponentRenderer.HORIZONTAL_HINT))
									.build(),
							item.computeTags()))
					.setHeader("Tags")
					.setResizable(true);
			grid
					.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
							.onClick(event -> {
								val pair = PropertiesUtils.selectPropertyTree(
										state.getProperties().getPropertyDeclarationEntries(),
										state.getProperties().getPropertyUsageEntries(),
										item.getName());
								//
								ConfirmationViews.showFunctionDialog(
										PropertiesMapViewRenderer.Parameters.builder()
												.highlightedPropertyName(item.getName())
												.propertyDeclarationEntries(pair.getLeft())
												.propertyUsageEntries(pair.getRight())
												.build(),
										(propertiesMapProperties, propertiesMapSink) ->
												propertiesMapViewRenderer.render(propertiesMapProperties));
							})
							.text("Diagram")
							.build()))
					.setResizable(true);
			//
			grid.setItemDetailsRenderer(new ComponentRenderer<>(item -> renderDetails(state.getProperties(), item)));
			//
			grid.setItems(properties
					.filter(propertyEntry -> state.getNameFilter() == null ||
							StringUtils.containsIgnoreCase(propertyEntry.getName(), state.getNameFilter()))
					.asJava());
			//
			layout.add(grid);
		}
		return layout;
	}

	private Component renderDetails(Properties properties, Property item) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			Component declarations = declarationsViewRenderer.render(builder -> builder
					.propertyDeclarationEntries(item.getDeclarationEntries())
					.build());
			CssFlexItem.setFlexGrow(declarations, 1);
			layout.add(declarations);
		}
		{
			Component usages = usagesViewRenderer.render(builder -> builder
					.propertyUsageEntries(item.getUsageEntries())
					.build());
			CssFlexItem.setFlexGrow(usages, 1);
			layout.add(usages);
		}
		return layout;
	}

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	@WithSpan
	public Component render(Properties properties) {
		return FunctionalComponent.createWithExternalObservable(
				Observable.withInitialValue(State.builder()
						.nameFilter(null)
						.properties(properties)
						.build()),
				this::doRender);
	}

	@Nullable
	public static List<TypeDescription> resolveTypes(Properties properties, Property property) {
		List<TypeDescription> result;
		{
			if (!property.getUsageEntries().isEmpty()) {
				result = property.getUsageEntries()
						.map(PropertyUsageEntry::getJavaType)
						.filter(Objects::nonNull)
						.distinct();
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		List<PropertyDeclarationEntry> propertyDeclarationEntries;

		List<PropertyUsageEntry> propertyUsageEntries;

		public static class Builder {
			// Lomboked
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@Nullable
		String nameFilter;

		@NonNull
		Properties properties;
	}
}