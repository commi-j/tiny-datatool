package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.anchor.SmartAnchorRenderer;
import tk.labyrinth.pandora.ui.component.objectaction.TypedObjectActionProvider;

@LazyComponent
@RequiredArgsConstructor
public class ArtifactManifestDashboardActionHandler extends TypedObjectActionProvider<ArtifactManifest> {

	private final SmartAnchorRenderer smartAnchorRenderer;

	@Nullable
	@Override
	protected Component provideActionForTypedObject(Context<ArtifactManifest> context) {
		return smartAnchorRenderer.render(SmartAnchorRenderer.Properties.builder()
				.navigationTarget(ArtifactManifestDashboardPage.class)
				.navigationTargetParameter(context.getObject().getUid().toString())
				.target(AnchorTarget.BLANK)
				.text("Dashboard")
				.build());
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}
}
