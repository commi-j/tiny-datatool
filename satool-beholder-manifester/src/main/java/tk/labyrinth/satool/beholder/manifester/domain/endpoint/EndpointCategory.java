package tk.labyrinth.satool.beholder.manifester.domain.endpoint;

public enum EndpointCategory {
	KAFKA,
	REST,
}
