package tk.labyrinth.satool.beholder.manifester.domain.repos;

import io.vavr.collection.List;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import tk.labyrinth.satool.beholder.domain.space.initializer.SpaceInitializerBase;
import tk.labyrinth.satool.beholder.domain.uiconfiguration.BeholderMenuConfiguration;
import tk.labyrinth.satool.beholder.domain.uiconfiguration.BeholderUiConfiguration;

import javax.annotation.CheckForNull;

@Component
@ConditionalOnProperty("beholder.enableReposSpace")
//@Order(DefaultStoresInitializer.PRIORITY + 1)
public class ReposSpaceInitializer extends SpaceInitializerBase {

	@Override
	protected String getSpaceKey() {
		return "repos";
	}

	@CheckForNull
	@Override
	protected BeholderUiConfiguration getUiConfiguration() {
		return BeholderUiConfiguration.builder()
				.menuConfigurations(List.of(
						BeholderMenuConfiguration.builder()
								.filterTag("confluence")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("docs")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("repos")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("persistence")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("properties")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("jobs")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("pandora")
								.build()))
				.priority(1)
				.scope("space")
				.build();
	}
}
