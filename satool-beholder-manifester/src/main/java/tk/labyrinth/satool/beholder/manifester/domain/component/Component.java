package tk.labyrinth.satool.beholder.manifester.domain.component;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.SearchDataAttributes;
import tk.labyrinth.pandora.stores.rootobject.HasUid;
import tk.labyrinth.satool.beholder.manifester.domain.repository.UrlRepositoryReference;

import java.util.UUID;

@Builder(toBuilder = true)
@Model(code = Component.MODEL_CODE)
@ModelTag("documentary")
@ModelTag("repos")
@RenderAttribute("name")
@SearchDataAttributes("code")
@Value
public class Component implements HasUid {

	public static final String CODE_ATTRIBUTE_NAME = "code";

	public static final String MODEL_CODE = "component";

	public static final String REPOSITORY_REFERENCE_ATTRIBUTE_NAME = "repositoryReference";

	public static final String SCENARIO_PREFIX_ATTRIBUTE_NAME = "scenarioPrefix";

	String code;

	String moduleCode;

	String name;

	UrlRepositoryReference repositoryReference;

	String scenarioPrefix;

	List<String> tags;

	UUID uid;
}
