package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;

import java.util.function.Function;

@LazyComponent
public class ExposedClassesViewRenderer {

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		Grid<ExposedClass> grid = new Grid<>();
		{
			grid.setAllRowsVisible(true);
		}
		{
			grid
					.addColumn(ExposedClass::getSignature)
					.setHeader("Signature")
					.setResizable(true);
			grid
					.addColumn(item -> item.getFields().size())
					.setHeader("Field Count")
					.setResizable(true);
		}
		{
			grid.setItems(parameters.artifactManifest().getExposedClasses().asJava());
		}
		return grid;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		ArtifactManifest artifactManifest;

		public static class Builder {
			// Lomboked
		}
	}
}