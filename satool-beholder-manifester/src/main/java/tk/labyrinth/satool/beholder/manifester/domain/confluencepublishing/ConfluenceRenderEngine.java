package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.StringTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.core.XHTMLOutputFormat;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.ConfluencePageTemplate;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.KeyConfluencePageTemplateReference;
import tk.labyrinth.satool.freemarker.FreemarkerConfigurationFactory;
import tk.labyrinth.satool.freemarker.SatoolFreemarkerObjectWrapper;
import tk.labyrinth.satool.freemarker.templatemodel.NamedTemplateModel;

import java.io.IOException;
import java.io.StringWriter;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// TODO: Move to autodocs module.
@LazyComponent
@RequiredArgsConstructor
public class ConfluenceRenderEngine {

	private final ConverterRegistry converterRegistry;

	private final Lock lock = new ReentrantLock();

	private Configuration configuration;

	@SmartAutowired
	private TypedObjectSearcher<ConfluencePageTemplate> confluencePageTemplateSearcher;

	@SmartAutowired
	private List<NamedTemplateModel> namedTemplateModels;

	private StringTemplateLoader stringTemplateLoader;

	@PostConstruct
	private void postConstruct() {
		{
			stringTemplateLoader = new StringTemplateLoader();
		}
		{
			configuration = FreemarkerConfigurationFactory.createDefault();
			configuration.setObjectWrapper(new SatoolFreemarkerObjectWrapper(
					FreemarkerConfigurationFactory.DEFAULT_VERSION,
					converterRegistry));
			configuration.setOutputFormat(XHTMLOutputFormat.INSTANCE);
			configuration.setTemplateLoader(new MultiTemplateLoader(new TemplateLoader[]{
					stringTemplateLoader,
					new ClassTemplateLoader(Thread.currentThread().getContextClassLoader(), "freemarker")
			}));
			//
			configuration.addAutoInclude("satool-macros.ftl");
			configuration.addAutoInclude("confluence-layout.ftl");
			configuration.addAutoInclude("confluence-widgets.ftl");
			configuration.addAutoInclude("beholder-confluence-macros.ftl");
			configuration.addAutoInclude("beholder-manifester-confluence-macros.ftl");
		}
	}

	public String resolve(KeyConfluencePageTemplateReference confluencePageTemplateReference, Map<String, ?> model) {
		return resolve(
				confluencePageTemplateSearcher.getSingle(confluencePageTemplateReference).getBodyTemplate(),
				model);
	}

	@SuppressWarnings("unchecked")
	public String resolve(String template, Map<String, ?> model) {
		String result;
		{
			lock.lock();
			try {
				stringTemplateLoader.putTemplate("tmp", template);
				Template freemarkerTemplate = IoUtils.unchecked(() -> configuration.getTemplate("tmp"));
				stringTemplateLoader.removeTemplate("tmp");
				configuration.clearTemplateCache();
				//
				Map<String, Object> enrichedModel = model.toList()
						.map(tuple -> (Pair<String, Object>) Pair.of(tuple._1(), tuple._2()))
						.appendAll(namedTemplateModels.map(templateMethodModel -> Pair.of(
								templateMethodModel.getName(),
								templateMethodModel)))
						.toMap(Pair::getLeft, Pair::getRight);
				//
				try (StringWriter writer = new StringWriter()) {
					freemarkerTemplate.process(enrichedModel, writer);
					result = writer.toString();
				} catch (IOException | TemplateException ex) {
					throw new RuntimeException(ex);
				}
			} finally {
				lock.unlock();
			}
		}
		return result;
	}
}
