package tk.labyrinth.satool.beholder.manifester.domain.scenario.job;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Element;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.exception.wrapping.WrappingError;
import tk.labyrinth.pandora.misc4j.integration.auth.AuthorizationUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.extra.objectproxy.ObjectProxyHandler;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.manifester.domain.scenario.ConfluenceSignatureScenarioReference;
import tk.labyrinth.satool.beholder.manifester.domain.scenario.Scenario;
import tk.labyrinth.satool.beholder.manifester.domain.scenario.ScenarioBuilder;
import tk.labyrinth.satool.beholder.manifester.domain.scenario.ScenarioUtils;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceConnectionParameters;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFetcher;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceDetailsElement;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceDocument;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceCellElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceRowElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceTableElement;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceLastUpdated;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceQueryLanguagePredicate;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceSpace;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceStorage;

import javax.annotation.CheckForNull;
import java.time.Instant;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class FetchScenariosJobRunner implements JobRunner<FetchScenariosJobRunner.Parameters> {

	public static final Pattern PAGE_TITLE_PATTERN = Pattern.compile(".*(Сценарий|Алгоритм) (?<code>.+?) (?<name>.*)");

	public static final Pattern PAGE_URL_PATTERN = Pattern.compile(".+/pages/viewpage.action?pageId=(?<pageId>[\\d]*)");

	public static final Pair<String, String> TEMPLATE_VERSION_ONE_ONE_ROW_PAIR = Pair.of(
			"01.01 Версия шаблона",
			"1.1");

	private final ConverterRegistry converterRegistry;

	private final ExecutionContextHandler executionContextHandler;

	private final ConfluenceFetcher fetcher;

	private final ObjectProxyHandler objectProxyHandler;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Scenario> scenarioSearcher;

	@CheckForNull
	private ConfluenceConnectionParameters createConnectionParameters(Parameters parameters) {
		String authorization;
		{
			if (parameters.credentialsReference() != null) {
				Credentials credentials = credentialsSearcher.findSingle(parameters.credentialsReference());
				authorization = AuthorizationUtils.encodeBasic(credentials.getUsername(), credentials.getPassword());
			} else {
				authorization = null;
			}
		}
		//
		return ConfluenceConnectionParameters.builder()
				.authorization(authorization)
				.baseUrl(parameters.baseUrl())
				.build();
	}

	@CheckForNull
	private ConfluenceSignatureScenarioReference findScenarioReferenceFromContentTitle(
			ConfluenceConnectionParameters connectionParameters,
			String spaceKey,
			String contentTitle) {
		ConfluenceSignatureScenarioReference result;
		{
			ConfluenceContent content = fetcher.findOne(
					connectionParameters,
					ConfluenceQueryLanguagePredicate.builder()
							.titles(List.of(contentTitle))
							.spaceKeys(List.of(spaceKey))
							.build(),
					null);
			if (content != null) {
				result = ConfluenceSignatureScenarioReference.of(resolveConfluenceSignature(content));
			} else {
				result = null;
			}
		}
		return result;
	}

	@CheckForNull
	private ConfluenceSignatureScenarioReference findScenarioReferenceFromHref(
			ConfluenceConnectionParameters connectionParameters,
			String href) {
		ConfluenceSignatureScenarioReference result;
		{
			Matcher urlMatcher = PAGE_URL_PATTERN.matcher(href);
			if (urlMatcher.matches()) {
				ConfluenceContent content = fetcher.findOne(
						connectionParameters,
						ConfluenceQueryLanguagePredicate.builder()
								.ids(List.of(urlMatcher.group("pageId")))
								.build(),
						null);
				if (content != null) {
					result = ConfluenceSignatureScenarioReference.of(resolveConfluenceSignature(content));
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	private Scenario parseScenario(
			ConfluenceConnectionParameters connectionParameters,
			ConfluenceContent content,
			List<ConfluenceRowElement> rows) {
		Scenario result;
		{
			List<Pair<String, ConfluenceCellElement>> attributes = rows.map(row -> {
				List<ConfluenceCellElement> cells = row.getCells();
				ConfluenceCellElement keyCell = cells.get(0);
				ConfluenceCellElement valueCell = cells.get(1);
				//
				return Pair.of(keyCell.getJsoupElement().text(), valueCell);
			});
			//
			if (attributes.exists(attribute -> Objects.equals(
					attribute.getLeft(),
					TEMPLATE_VERSION_ONE_ONE_ROW_PAIR.getLeft()) &&
					Objects.equals(
							attribute.getRight().getJsoupElement().text(),
							TEMPLATE_VERSION_ONE_ONE_ROW_PAIR.getRight()))) {
				result = parseScenarioVersionOneOne(
						connectionParameters,
						content,
						attributes.toLinkedMap(Pair::getLeft, Pair::getRight));
			} else {
				throw new IllegalArgumentException("Unknown template version: content.id = %s".formatted(
						content.getId()));
			}
		}
		return result;
	}

	private Scenario parseScenarioVersionOneOne(
			ConfluenceConnectionParameters connectionParameters,
			ConfluenceContent content,
			Map<String, ConfluenceCellElement> attributes) {
		String confluenceSignature = resolveConfluenceSignature(content);
		ConfluenceCellElement dependenciesCell = attributes.get("03.02 Исходящие зависимости").getOrNull();
		String title = content.getTitle();
		//
		List<ConfluenceSignatureScenarioReference> dependencyReferences = resolveDependencyReferences(
				connectionParameters,
				dependenciesCell,
				content.getSpace().getKey());
		Matcher titleMatcher = PAGE_TITLE_PATTERN.matcher(title);
		boolean titleMatches = titleMatcher.matches();
		//
		String code = titleMatches ? titleMatcher.group("code") : null;
		//
		Scenario scenario = ScenarioBuilder.create()
				.componentReference(ScenarioUtils.componentReferenceFromCode(code))
				.code(code)
				.confluenceLastUpdated(content.getHistory().getLastUpdated().getWhen())
				.confluenceSignature(confluenceSignature)
				.dependencyReferences(dependencyReferences)
				.name(titleMatches ? titleMatcher.group("name") : title)
				.templateVersion(TEMPLATE_VERSION_ONE_ONE_ROW_PAIR.getRight())
				.updatedAt(Instant.now())
				.build(objectProxyHandler);
		//
		return scenario;
	}

	private void processContent(ConfluenceConnectionParameters connectionParameters, ConfluenceContent content) {
		ConfluenceDocument root = ConfluenceDocument.from(content.getBody().getStorage().getValue());
		//
		List<ConfluenceElement> detailses = root.select(ConfluenceDetailsElement.selector());
		if (detailses.size() != 1) {
			throw new IllegalArgumentException("Require detailses size = 1: " + detailses);
		}
		//
		List<ConfluenceTableElement> tables = ConfluenceTableElement.select(detailses.get(0));
		if (tables.size() != 1) {
			throw new IllegalArgumentException("Require tables size = 1: " + tables);
		}
		//
		Scenario scenario = parseScenario(connectionParameters, content, tables.get(0).getRows());
		//
		saveScenario(scenario);
	}

	private String resolveConfluenceSignature(ConfluenceContent content) {
		return content.get_links().get("self").asText();
	}

	private List<ConfluenceSignatureScenarioReference> resolveDependencyReferences(
			ConfluenceConnectionParameters connectionParameters,
			ConfluenceCellElement dependenciesCell,
			String spaceKey) {
		List<ConfluenceElement> dependencyReferenceElements = dependenciesCell.select("//a | //page");
		List<ConfluenceSignatureScenarioReference> dependencyReferences = dependencyReferenceElements
				.map(dependencyReferenceElement -> {
					Element jsoupElement = dependencyReferenceElement.getJsoupElement();
					return switch (jsoupElement.tagName()) {
						case "a" -> {
							String href = jsoupElement.attr("href");
							yield !href.isEmpty() ? findScenarioReferenceFromHref(connectionParameters, href) : null;
						}
						case "ri:page" -> {
							String contentTitle = jsoupElement.attr("ri:content-title");
							yield !contentTitle.isEmpty()
									? findScenarioReferenceFromContentTitle(
									connectionParameters,
									spaceKey,
									contentTitle)
									: null;
						}
						default -> throw new IllegalArgumentException(jsoupElement.toString());
					};
				})
				.filter(Objects::nonNull);
		return dependencyReferences;
	}

	private void saveScenario(Scenario scenario) {
		Scenario foundScenario = scenarioSearcher.findSingle(ConfluenceSignatureScenarioReference.from(scenario));
		if (foundScenario != null) {
			logger.info("Updating Scenario: confluenceSignature = {}, code = {}",
					scenario.getConfluenceSignature(),
					scenario.getCode());
			//
			GenericObject objectToUpdate = ScenarioBuilder.from(scenario)
					.uid(foundScenario.getUid())
					.toObject(converterRegistry);
			// FIXME: Replace with GenericStore.
//			spaceHandlerProvider.provideSpaceHandler().updateObject(objectToUpdate);
		} else {
			logger.info("Creating Scenario: confluenceSignature = {}, code = {}",
					scenario.getConfluenceSignature(),
					scenario.getCode());
			//
			GenericObject objectToCreate = ScenarioBuilder.from(scenario)
					.toObject(converterRegistry);
			// FIXME: Replace with GenericStore.
//			spaceHandlerProvider.provideSpaceHandler().createObject(objectToCreate);
		}
	}

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder()
				.baseUrl(null)
				.cql(ConfluenceQueryLanguagePredicate.builder().build())
				.credentialsReference(null)
				.spaceReference(executionContextHandler.getExecutionContext().findAttributeValueInferred(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME))
				.build();
	}

	@Override
	public void run(Context<Parameters> context) {
		Parameters parameters = context.getParameters();
		ConfluenceConnectionParameters connectionParameters = createConnectionParameters(parameters);
		//
		fetcher
				.searchContentsFlux(
						connectionParameters,
						parameters.cql(),
						List
								.of(
										ConfluenceLastUpdated.EXPAND_KEY,
										ConfluenceSpace.EXPAND_KEY,
										ConfluenceStorage.EXPAND_KEY)
								.mkString(","),
						null)
				.doOnNext(next -> next.getResults().forEach(content -> {
					try {
						processContent(connectionParameters, content);
					} catch (RuntimeException ex) {
						logger.warn("content.id = {}", content.getId(), ex);
					}
				}))
				.doOnError(error -> {
					throw new WrappingError(error);
				})
				.subscribe();
	}

	@Accessors(fluent = true)
	@Builder(toBuilder = true)
	@Value
	public static class Parameters implements HasSpaceReference {

		String baseUrl;

		ConfluenceQueryLanguagePredicate cql;

		@CheckForNull
		KeyCredentialsReference credentialsReference;

		KeySpaceReference spaceReference;

		@Override
		public KeySpaceReference getSpaceReference() {
			return spaceReference();
		}
	}
}
