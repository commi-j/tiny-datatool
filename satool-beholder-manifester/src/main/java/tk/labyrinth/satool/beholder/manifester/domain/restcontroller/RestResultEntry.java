package tk.labyrinth.satool.beholder.manifester.domain.restcontroller;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RestResultEntry {

	public enum Kind {
		BODY,
		HEADER,
		/**
		 * This may be in the case where kind information is located in generated API class which is not available
		 * on shallow repository analysis.
		 */
		UNDEFINED,
	}

	TypeDescription datatype;

	@Nullable
	String description;

	Kind kind;

	/**
	 * Body has no name.
	 */
	@Nullable
	String name;
}
