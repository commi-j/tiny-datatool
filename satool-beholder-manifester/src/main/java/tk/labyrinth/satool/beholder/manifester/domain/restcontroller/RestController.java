package tk.labyrinth.satool.beholder.manifester.domain.restcontroller;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderAttribute("signature")
@Value
@With
public class RestController {

	List<RequestMappingMethod> methods;

	/**
	 * May be empty in case where neither 'value' nor 'path' is specified.
	 */
	String path;

	ClassSignature signature;
}
