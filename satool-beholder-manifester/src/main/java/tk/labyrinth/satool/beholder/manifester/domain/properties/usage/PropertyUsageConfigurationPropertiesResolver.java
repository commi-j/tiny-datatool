package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.SpoonStaticFactory;
import tk.labyrinth.satool.spoon.SpoonUtils;

import java.util.Objects;

public class PropertyUsageConfigurationPropertiesResolver {

	private static final CtTypeReference<ConfigurationProperties> configurationPropertiesAnnotationType =
			SpoonStaticFactory.createReference(ConfigurationProperties.class);

	private static CtType<?> getOwnerType(CtAnnotation<ConfigurationProperties> configurationPropertiesAnnotation) {
		CtType<?> result;
		{
			CtElement parent = configurationPropertiesAnnotation.getParent();
			//
			if (parent instanceof CtMethod<?> method) {
				result = method.getType().getDeclaration();
			} else if (parent instanceof CtType<?> type) {
				result = type;
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	private static List<PropertyUsageEntry> resolveAnnotation(
			CtAnnotation<ConfigurationProperties> configurationPropertiesAnnotation) {
		String prefix;
		{
			ConfigurationProperties annotation = configurationPropertiesAnnotation.getActualAnnotation();
			prefix = ObjectUtils.pickFirstNonDefaultValue(
					"",
					annotation::value,
					annotation::prefix,
					null);
		}
		CtType<?> type = getOwnerType(configurationPropertiesAnnotation);
		//
		return List.ofAll(type.getAllFields())
				.filter(fieldReference -> !fieldReference.isStatic())
				.map(CtFieldReference::getDeclaration)
				.map(field -> PropertyUsageEntry.builder()
						.javaType(CtTypeReferenceUtils.getTypeDescription(field.getType()))
						.name(prefix != null ? "%s.%s".formatted(prefix, field.getSimpleName()) : field.getSimpleName())
						.location(SpoonUtils.elementToSignature(field).toString())
						.tags(List.of("configurationProperties", "java"))
						.build());
	}

	public static List<PropertyUsageEntry> resolve(CtModel javaModel) {
		List<CtAnnotation<ConfigurationProperties>> configurationPropertiesAnnotations = List
				.ofAll(javaModel.getAllTypes())
				.flatMap(type -> List.<CtType<?>>of(type).appendAll(type.getNestedTypes()))
				.flatMap(type -> List.<CtElement>of(type).appendAll(List.ofAll(type.getAllMethods())))
				.map(element -> element.getAnnotation(configurationPropertiesAnnotationType))
				.filter(Objects::nonNull);
		//
		return configurationPropertiesAnnotations
				.flatMap(PropertyUsageConfigurationPropertiesResolver::resolveAnnotation);
	}
}
