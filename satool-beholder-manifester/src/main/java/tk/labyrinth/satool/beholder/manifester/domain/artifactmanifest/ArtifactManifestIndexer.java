package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMethod;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectindex.Indexer;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.properties.PropertiesUtils;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RequestMappingMethod;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RestController;

import java.util.Optional;

@LazyComponent
public class ArtifactManifestIndexer implements Indexer<ArtifactManifest, ArtifactManifestIndex> {

	@Override
	public List<ArtifactManifestIndex> createIndices(ArtifactManifest target) {
		List<RestEndpointGroup> restEndpointGroups = target.getRestControllers()
				.map(restController -> RestEndpointGroup.builder()
						.endpoints(restController.getMethods()
								.map(method -> createRestEndpoint(restController, method)))
						.javaSignature(restController.getSignature())
						.name(restController.getSignature().getLongName())
						.normalizedPath(computeNormalizedPath(restController.getPath()))
						.rawPath(restController.getPath())
						.build());
		//
		return List.of(ArtifactManifestIndex.builder()
				.functions(target.getFunctions() != null ? target.getFunctions() : List.empty())
				.models(target.getExposedClasses().map(DisplayableModel::createFrom))
				.name(target.getDefinitionReference().getName())
				.persistentModels(target.getPersistentModels().map(persistentModel -> RefinedPersistentModel.builder()
						.attributes(persistentModel.getAttributes())
						.databaseName(persistentModel.getDatabaseName())
						.javaSignature(persistentModel.getJavaClassSignature())
						.name(persistentModel.getJavaClassSignature().getLongName())
						.tags(persistentModel.getTags())
						.build()))
				.properties(PropertiesUtils.groupProperties(
						target.getPropertyDeclarationEntries(),
						target.getPropertyUsageEntries()))
				.restEndpointGroups(restEndpointGroups)
				.targetReference(createTargetReference(target))
				.build());
	}

	@Override
	public UidReference<ArtifactManifest> createTargetReference(ArtifactManifest target) {
		return UidReference.of(ArtifactManifest.MODEL_CODE, target.getUid());
	}

	/**
	 * - Ensures starts with '/';<br>
	 * - Ensures ends without '/';<br>
	 *
	 * @param rawPath nullable
	 *
	 * @return non-null
	 */
	public static String computeNormalizedPath(@Nullable String rawPath) {
		String result;
		{
			if (rawPath != null && !rawPath.isEmpty()) {
				StringBuilder stringBuilder = new StringBuilder();
				//
				if (!rawPath.startsWith("/")) {
					stringBuilder.append("/");
				}
				//
				if (rawPath.endsWith("/")) {
					stringBuilder.append(rawPath, 0, rawPath.length() - 1);
				} else {
					stringBuilder.append(rawPath);
				}
				//
				result = stringBuilder.toString();
			} else {
				result = "";
			}
		}
		return result;
	}

	public static RestEndpoint createRestEndpoint(
			RestController restController,
			RequestMappingMethod requestMappingMethod) {
		List<RestEndpointCoordinates> coordinateses = resolvePaths(requestMappingMethod.getPaths()).map(path -> RestEndpointCoordinates
				.builder()
				.method(Optional.ofNullable(requestMappingMethod.getMethod())
						.map(RequestMethod::name)
						.map(HttpMethod::valueOf)
						.orElse(null))
				.path(path)
				.build());
		//
		return RestEndpoint.builder()
				.coordinateses(coordinateses)
				.firstCoordinates("%s %s".formatted(coordinateses.get(0).getMethod(), coordinateses.get(0).getPath()))
				.javaSignature(requestMappingMethod.getSignature().toFull(restController.getSignature().toString()))
				.name(requestMappingMethod.getSignature().getName())
				.parameters(requestMappingMethod.getParameters().map(element -> RestParametersEntry.builder()
						.datatype(element.getDatatype())
						.comments(null) // TODO
						.constraints(null) // TODO
						.kind(RestParametersEntry.Kind.valueOf(element.getKind().name()))
						.name(element.getName())
						.build()))
				.result(requestMappingMethod.getResult()
						//
						// TODO: Not sure if it should be filtered here or during manifest creation.
						.filter(element -> !element.getDatatype().isVoid() &&
								!element.getDatatype().matches(Void.class))
						.map(element -> RestResultEntry.builder()
								.datatype(element.getDatatype())
								.comments(null) // TODO
								.kind(RestResultEntry.Kind.valueOf(element.getKind().name()))
								.name(element.getName())
								.build()))
				.build();
	}

	public static List<String> resolvePaths(@Nullable List<String> paths) {
		List<String> result;
		{
			if (paths != null) {
				if (!paths.isEmpty()) {
					result = paths.map(path -> path.startsWith("/") ? path : "/%s".formatted(path));
				} else {
					result = List.of("/");
				}
			} else {
				result = List.empty();
			}
		}
		return result;
	}
}
