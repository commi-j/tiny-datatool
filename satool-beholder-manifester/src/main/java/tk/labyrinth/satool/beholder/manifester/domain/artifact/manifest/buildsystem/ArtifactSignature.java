package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.satool.maven.domain.MavenArtifactCoordinates;
import tk.labyrinth.satool.maven.domain.MavenDependency;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ArtifactSignature {

	/**
	 * Often omitted for context-dependent default artifacts.
	 */
	@Nullable
	String group;

	String name;

	/**
	 * Often omitted when dependency management system is used.
	 */
	@Nullable
	String version;

	public ArtifactSignature requireFull() {
		return requirePresentGroup().requirePresentName().requirePresentVersion();
	}

	public ArtifactSignature requireNoVersion() {
		if (version != null) {
			throw new IllegalStateException("Required no version: %s".formatted(this));
		}
		return this;
	}

	public ArtifactSignature requirePresentGroup() {
		if (group == null) {
			throw new IllegalStateException("Required present group: %s".formatted(this));
		}
		return this;
	}

	public ArtifactSignature requirePresentName() {
		if (name == null) {
			throw new IllegalStateException("Required present name: %s".formatted(this));
		}
		return this;
	}

	public ArtifactSignature requirePresentVersion() {
		if (version == null) {
			throw new IllegalStateException("Required present version: %s".formatted(this));
		}
		return this;
	}

	public MavenArtifactCoordinates toMavenCoordinates() {
		requireFull();
		//
		return MavenArtifactCoordinates.builder()
				.artifactId(name)
				.groupId(group)
				.version(version)
				.build();
	}

	@Override
	public String toString() {
		return "%s:%s:%s".formatted(group, name, version);
	}

	public ArtifactSignature withoutVersion() {
		return withVersion(null);
	}

	public static ArtifactSignature from(Dependency dependency) {
		return ArtifactSignature.builder()
				.group(dependency.getGroupId())
				.name(dependency.getArtifactId())
				.version(dependency.getVersion())
				.build();
	}

	public static ArtifactSignature from(MavenDependency mavenDependency) {
		return ArtifactSignature.builder()
				.group(mavenDependency.getGroupId())
				.name(mavenDependency.getArtifactId())
				.version(mavenDependency.getVersion())
				.build();
	}

	public static ArtifactSignature from(Model model) {
		return ArtifactSignature.builder()
				.group(model.getGroupId())
				.name(model.getArtifactId())
				.version(model.getVersion())
				.build();
	}

	public static ArtifactSignature from(String value) {
		String[] segments = value.split(":");
		return new ArtifactSignature(segments[0], segments[1], segments[2]);
	}
}
