package tk.labyrinth.satool.beholder.manifester.domain.repository;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class UrlRepositoryReference implements Reference<Repository> {

	String url;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Repository.MODEL_CODE,
				Repository.URL_ATTRIBUTE_NAME,
				url);
	}

	public static UrlRepositoryReference from(Repository object) {
		return UrlRepositoryReference.of(object.getUrl());
	}

	@JsonCreator
	public static UrlRepositoryReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Repository.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Repository.URL_ATTRIBUTE_NAME));
	}
}
