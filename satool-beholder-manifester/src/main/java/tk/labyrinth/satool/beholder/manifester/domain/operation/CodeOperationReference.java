package tk.labyrinth.satool.beholder.manifester.domain.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class CodeOperationReference implements Reference<Operation> {

	String code;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Operation.MODEL_CODE,
				Operation.CODE_ATTRIBUTE_NAME,
				code);
	}

	public static CodeOperationReference from(Operation object) {
		return CodeOperationReference.of(object.getCode());
	}

	@JsonCreator
	public static CodeOperationReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Operation.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Operation.CODE_ATTRIBUTE_NAME));
	}
}
