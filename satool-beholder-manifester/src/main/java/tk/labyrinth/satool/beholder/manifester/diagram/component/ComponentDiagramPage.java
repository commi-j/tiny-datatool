package tk.labyrinth.satool.beholder.manifester.diagram.component;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.component.CodeComponentReference;
import tk.labyrinth.satool.beholder.manifester.domain.component.Component;
import tk.labyrinth.satool.beholder.manifester.domain.componentdependency.ComponentDependency;
import tk.labyrinth.satool.beholder.manifester.domain.componentdependency.ComponentDependencyLayer;
import tk.labyrinth.satool.beholder.root.BeholderRootLayout;
import tk.labyrinth.satool.plantuml.view.PlantumlDiagramView;

import javax.annotation.CheckForNull;
import java.util.Objects;

@RequiredArgsConstructor
@Route(value = "component-diagram", layout = BeholderRootLayout.class)
public class ComponentDiagramPage extends SplitLayout implements AfterNavigationObserver {

	private final SimpleComponentDiagramModelTool componentDiagramModelTool;

	private final CssVerticalLayout componentsLayout = new CssVerticalLayout();

	private final PlantumlDiagramView diagramView;

	private final CssHorizontalLayout layersLayout = new CssHorizontalLayout();

	private final Observable<State> stateObservable = Observable.notInitialized();

	private final RadioButtonGroup<Integer> viewAllButtonGroup = new RadioButtonGroup<>();

	@SmartAutowired
	private TypedObjectSearcher<Component> componentSearcher;

	@PostConstruct
	private void postConstruct() {
		{
			CssVerticalLayout configurationLayout = new CssVerticalLayout();
			{
				layersLayout.setAlignItems(AlignItems.BASELINE);
				configurationLayout.add(layersLayout);
			}
			{
				CssHorizontalLayout viewAllLayout = new CssHorizontalLayout();
				{
					viewAllLayout.setAlignItems(AlignItems.BASELINE);
				}
				{
					{
						viewAllLayout.add(new Span("Unchecked:"));
					}
					{
						viewAllButtonGroup.setItems(0, 1, 2);
						viewAllButtonGroup.setRenderer(new TextRenderer<>(item -> switch (item) {
							case 0 -> "Ignore";
							case 1 -> "List";
							case 2 -> "Render";
							default -> throw new UnreachableStateException();
						}));
						viewAllButtonGroup.addValueChangeListener(event -> stateObservable.update(currentState -> currentState.withViewAll(event.getValue())));
						viewAllLayout.add(viewAllButtonGroup);
					}
				}
				configurationLayout.add(viewAllLayout);
			}
			{
				configurationLayout.add(componentsLayout);
			}
			addToPrimary(configurationLayout);
		}
		{
			CssVerticalLayout diagramLayout = new CssVerticalLayout();
			{
				// Some diagram view customization.
			}
			{
				CssFlexItem.setFlexGrow(diagramView, 1);
				diagramLayout.add(diagramView);
			}
			addToSecondary(diagramLayout);
		}
		{
			stateObservable.subscribe(nextState -> {
				List<Component> components;
				if (nextState.getViewAll() != null && nextState.getViewAll() > 0) {
					List<Component> foundComponents = componentDiagramModelTool.getAllComponents();
					Set<String> foundComponentCodes = foundComponents.map(Component::getCode).toSet();
					//
					components = foundComponents.appendAll(nextState.getFocus()
							.map(State.FocusEntry::getComponentCode)
							.filter(componentCode -> !foundComponentCodes.contains(componentCode))
							.map(componentCode -> Component.builder()
									.code(componentCode)
									.name(componentCode)
									.build()));
					// TODO: Add missing components that are referenced by dependencies (e.g. missing-one).
				} else {
					List<CodeComponentReference> extendedFocusedComponents = nextState.getFocus()
							.flatMap(focusEntry -> componentDiagramModelTool.collectDependencies(
									nextState.getDependencyLayers(),
									focusEntry.getComponentReference(),
									focusEntry.getUp(),
									focusEntry.getDown()))
							.flatMap(pair -> List.of(pair.getLeft()).appendAll(
									pair.getRight().flatMap(ComponentDependency::getNonNullComponentReferences)))
							.distinct();
					components = extendedFocusedComponents
							.map(componentReference -> componentSearcher.resolve(componentReference))
							.map(resolvedComponent -> resolvedComponent.hasObjects()
									? resolvedComponent.getSingle()
									: Component.builder()
									.code(resolvedComponent.getReference().getCode())
									.build())
							.sortBy(Component::getCode);
				}
				{
					layersLayout.removeAll();
					//
					layersLayout.add(new Span("Layers:"));
					//
					List.of(ComponentDependencyLayer.values()).forEach(componentDependencyLayer -> {
						boolean activated = nextState.getDependencyLayers().contains(componentDependencyLayer);
						//
						Button layerButton = new Button(componentDependencyLayer.toString());
						layerButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
						if (activated) {
							layerButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
						}
						layerButton.addClickListener(event -> stateObservable.set(nextState.withDependencyLayers(
								activated
										? nextState.getDependencyLayers().remove(componentDependencyLayer)
										: nextState.getDependencyLayers().add(componentDependencyLayer)
						)));
						layersLayout.add(layerButton);
					});
				}
				{
					viewAllButtonGroup.setValue(nextState.getViewAll() != null ? nextState.getViewAll() : 0);
				}
				{
					componentsLayout.removeAll();
					//
					components.forEach(component -> {
						State.FocusEntry focusEntryForComponent = nextState.getFocus()
								.find(focusEntry -> Objects.equals(focusEntry.getComponentCode(), component.getCode()))
								.getOrNull();
						//
						CssHorizontalLayout componentLayout = new CssHorizontalLayout();
						{
							componentLayout.setAlignItems(AlignItems.BASELINE);
						}
						{
							Span nameLabel = new Span(component.getName() != null
									? component.getName()
									: component.getCode());
							CssFlexItem.setFlexGrow(nameLabel, 1);
							componentLayout.add(nameLabel);
						}
						if (focusEntryForComponent != null) {
							{
								IntegerField upField = new IntegerField();
								upField.setStepButtonsVisible(true);
								upField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
								upField.setValue(focusEntryForComponent.getUp());
								upField.addValueChangeListener(event -> stateObservable.update(currentState ->
										currentState.withFocus(currentState.getFocus().replace(
												focusEntryForComponent,
												focusEntryForComponent.withUp(event.getValue())))));
								upField.setWidth("6em");
								componentLayout.add(upField);
							}
							{
								IntegerField downField = new IntegerField();
								downField.setStepButtonsVisible(true);
								downField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
								downField.setValue(focusEntryForComponent.getDown());
								downField.addValueChangeListener(event -> stateObservable.update(currentState ->
										currentState.withFocus(currentState.getFocus().replace(
												focusEntryForComponent,
												focusEntryForComponent.withDown(event.getValue())))));
								downField.setWidth("6em");
								componentLayout.add(downField);
							}
						}
						{
							Button changeExpandStatusButton = new Button();
							//
							changeExpandStatusButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
							//
							if (focusEntryForComponent != null) {
								changeExpandStatusButton.setIcon(VaadinIcon.MINUS.create());
								//
								changeExpandStatusButton.addClickListener(event ->
										stateObservable.update(currentState -> currentState.withFocus(
												currentState.getFocus().remove(focusEntryForComponent))));
							} else {
								changeExpandStatusButton.setIcon(VaadinIcon.PLUS.create());
								//
								changeExpandStatusButton.addClickListener(event ->
										stateObservable.update(currentState -> currentState.withFocus(
												currentState.getFocus().append(State.FocusEntry.builder()
														.componentCode(component.getCode())
														.down(0)
														.up(0)
														.build()))));
							}
							//
							componentLayout.add(changeExpandStatusButton);
						}
						componentsLayout.add(componentLayout);
					});
				}
				{
					diagramView.setParameters(builder -> builder
							.value(componentDiagramModelTool
									.buildModel(ComponentDiagramModelConfiguration.builder()
											.dependencyLayers(nextState.getDependencyLayers())
											.focusEntries(nextState.getFocus())
											.renderAll(Objects.equals(nextState.getViewAll(), 2))
											.build())
									.render())
							.build());
				}
				{
					UI.getCurrent().getPage().getHistory().replaceState(
							null,
							new Location(
									"beholder/%s".formatted("component-diagram"),
									nextState.toQueryParameters()));
				}
			});
		}
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		stateObservable.set(State.fromQueryParameters(event.getLocation().getQueryParameters()));
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State {

		Set<ComponentDependencyLayer> dependencyLayers;

		@NonNull
		List<FocusEntry> focus;

		@CheckForNull
		QueryParameters queryParameters;

		/**
		 * 0 - nope;<br>
		 * 1 - list;<br>
		 * 2 - render;<br>
		 */
		@CheckForNull
		Integer viewAll;

		public QueryParameters toQueryParameters() {
			return queryParameters != null
					? queryParameters
					: QueryParameters.fromString(List.of(
							viewAll != null
									? "v=%s".formatted(viewAll)
									: null,
							!focus.isEmpty()
									? "focus=%s".formatted(focus.map(FocusEntry::toQuerySegment).mkString(","))
									: null,
							"layers=%s".formatted(dependencyLayers))
					.filter(Objects::nonNull)
					.mkString("&"));
		}

		public static State fromQueryParameters(QueryParameters queryParameters) {
			List<String> focus = Option.of(queryParameters.getParameters().get("focus")).map(List::ofAll).getOrNull();
			Integer viewAll = Option.of(queryParameters.getParameters().get("v"))
					.map(list -> list.get(0))
					.map(Integer::parseInt)
					.getOrNull();
			return State.builder()
					.dependencyLayers(HashSet.of(ComponentDependencyLayer.values()))
					.focus(focus.size() == 1
							? List.of(focus.get(0).split(",")).map(FocusEntry::fromQuerySegment)
							: List.empty())
					.viewAll(viewAll)
//					.queryParameters(queryParameters) // TODO: Set if fail parsing
					.build();
		}

		@Builder(toBuilder = true)
		@Value
		@With
		public static class FocusEntry {

			String componentCode;

			Integer down;

			Integer up;

			public CodeComponentReference getComponentReference() {
				return CodeComponentReference.of(componentCode);
			}

			public String toQuerySegment() {
				return "%s/%s/%s".formatted(componentCode, up, down);
			}

			public static FocusEntry fromQuerySegment(String querySegment) {
				List<String> segmentParts = List.of(querySegment.split("/"));
				return FocusEntry.builder()
						.componentCode(segmentParts.get(0))
						.down(Integer.parseInt(segmentParts.get(2)))
						.up(Integer.parseInt(segmentParts.get(1)))
						.build();
			}
		}
	}
}
