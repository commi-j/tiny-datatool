package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.router.QueryParameters;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.vaadin.RouterLinkRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.objectaction.TypedObjectActionProvider;

@LazyComponent
public class ConfluenceMergeStructureItemActionProvider extends
		TypedObjectActionProvider<ConfluenceMergeStructure> {

	@Nullable
	@Override
	protected Component provideActionForTypedObject(Context<ConfluenceMergeStructure> context) {
		return RouterLinkRenderer.render(builder -> builder
				.queryParameters(QueryParameters.of("code", context.getObject().getCode()))
				.route(ConfluenceMergeStructurePage.class)
				.target(AnchorTarget.BLANK)
				.text("Dashboard")
				.build());
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}
}
