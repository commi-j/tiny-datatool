package tk.labyrinth.satool.beholder.manifester.domain.endpoint;

public enum InteractionKind {
	COMMAND,
	EVENT,
	REQUEST_RESPONSE,
	UNDEFINED,
}
