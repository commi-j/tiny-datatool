package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectcontributor.ObjectContributor;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeNode;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeStructure;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.KeyConfluencePageTemplateReference;

import java.util.UUID;

@LazyComponent
public class ConfluenceMergeStructuresContributor implements ObjectContributor<ConfluenceMergeStructure> {

	@Override
	public UUID computeUid(ConfluenceMergeStructure object) {
		return RootObjectUtils.computeUidFromValueHashCode(object.getCode());
	}

	@Override
	public List<ConfluenceMergeStructure> contributeObjects() {
		return List.of(
				ConfluenceMergeStructure.builder()
						.code("single-tree")
						.rootNodes(List.of(ConfluenceMergeNode.builder()
								.locator("contentId:PAGE_ID_PLACEHOLDER")
								.children(ConfluenceMergeNode.builder()
										.locator("childTitle:Generated Manifest for ${artifactDefinition.name}")
										.titleTemplate("Generated Manifest for ${artifactDefinition.name}")
										.bodyTemplateReference(KeyConfluencePageTemplateReference.of("empty-page"))
										.children(
												ConfluenceMergeNode.builder()
														.locator("childTitle:Persistent Models (${artifactDefinition.name})")
														.titleTemplate("Persistent Models (${artifactDefinition.name})")
														.bodyTemplateReference(KeyConfluencePageTemplateReference.of("persistent-models-page"))
														.build(),
												ConfluenceMergeNode.builder()
														.locator("childTitle:REST Endpoints (${artifactDefinition.name})")
														.titleTemplate("REST Endpoints (${artifactDefinition.name})")
														.bodyTemplateReference(KeyConfluencePageTemplateReference.of("rest-endpoints-page"))
														.build())
										.build())
								.build()))
						.build(),
				ConfluenceMergeStructure.builder()
						.code("distributed")
						.build());
	}
}
