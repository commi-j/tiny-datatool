package tk.labyrinth.satool.beholder.manifester.domain.operation;

import io.vavr.Tuple;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.component.ComponentDiagramRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.microservice.CodeMicroserviceReference;
import tk.labyrinth.satool.beholder.manifester.domain.microservice.Microservice;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Component
@Lazy
@RequiredArgsConstructor
public class OperationComponentDiagramDescriptionBuilder {

	private final TypedObjectSearcher<Microservice> microserviceSearcher;

	private final TypedObjectSearcher<Operation> operationSearcher;

	public String build(Operation operation) {
		List<InvocationTreeEntry> operationTree = getOperationTree(
				operation,
				null);
		//
		Map<ServiceReferencePair, List<InvocationTreeEntry>> groupedByServiceEntries = operationTree.groupBy(entry ->
				new ServiceReferencePair(entry.getFrom().getService(), entry.getTo().getService()));
		//
		List<Microservice> distinctServices = groupedByServiceEntries
				.keySet()
				.flatMap(pair -> List.of(pair.getFrom(), pair.getTo()))
				.distinct()
				.map(microserviceSearcher::findSingle)
				.toList();
		//
		return """
				@startuml
				!pragma layout smetana
								
				%s
								
				%s
								
				@enduml"""
				.formatted(
						distinctServices
								.map(service -> Tuple.of(
										service,
										renderInternalOperations(
												groupedByServiceEntries,
												CodeMicroserviceReference.from(service))))
								.map(pair -> ComponentDiagramRenderer.renderComponent(pair._1(), pair._2()))
								.mkString("\n"),
						groupedByServiceEntries
								.filter(group -> !Objects.equals(group._1().getFrom(), group._1().getTo()))
								.map(group -> renderInvocationEntryGroup(group._1(), group._2()))
								.mkString("\n"));
	}

	public List<InvocationTreeEntry> getOperationTree(
			Operation entryOperation,
			@Nullable Integer depth) {
		java.util.List<Pair<String, Operation>> operationsToProcess = new ArrayList<>();
		operationsToProcess.add(Pair.of(null, entryOperation));
		Set<CodeOperationReference> discoveredOperations = new HashSet<>();
		discoveredOperations.add(CodeOperationReference.from(entryOperation));
		//
		java.util.List<InvocationTreeEntry> result = new ArrayList<>();
		while (!operationsToProcess.isEmpty()) {
			Pair<String, Operation> stepCodeAndOperation = operationsToProcess.remove(0);
			String stepCode = stepCodeAndOperation.getLeft();
			Operation operation = stepCodeAndOperation.getRight();
			//
			if (operation != null) {
				List<CodeOperationReference> invokedOperationReferences = Option.of(operation.getInvokes())
						.getOrElse(List.empty());
				invokedOperationReferences.zipWithIndex().forEach(pair -> {
					CodeOperationReference invokedOperationReference = pair._1();
					int index = pair._2();
					//
					Operation invokedOperation = operationSearcher.findSingle(invokedOperationReference);
					String nextStepCode = stepCode != null ? stepCode + "." + (index + 1) : Integer.toString(index + 1);
					//
					result.add(new InvocationTreeEntry(operation, nextStepCode, invokedOperation));
					//
					if (discoveredOperations.add(invokedOperationReference)) {
						operationsToProcess.add(Pair.of(nextStepCode, invokedOperation));
					}
				});
			}
		}
		return List.ofAll(result);
	}

	@Nullable
	private static String renderInvocationEntries(
			List<InvocationTreeEntry> invocationEntries,
			boolean escapeLineBreak) {
		String result;
		{
			if (invocationEntries != null) {
				result = invocationEntries
						.map(entry -> "%s %s".formatted(entry.getStepCode(), entry.getTo().getCode()))
						.mkString(escapeLineBreak ? "\\n" : "\n");
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static String renderInternalOperations(
			Map<ServiceReferencePair, List<InvocationTreeEntry>> groupedByServiceEntries,
			CodeMicroserviceReference microserviceReference) {
		List<InvocationTreeEntry> entries = groupedByServiceEntries
				.get(new ServiceReferencePair(microserviceReference, microserviceReference))
				.getOrNull();
		//
		return renderInvocationEntries(entries, false);
	}

	public static String renderInvocationEntryGroup(
			ServiceReferencePair serviceReferencePair,
			List<InvocationTreeEntry> invocationEntries) {
		return ComponentDiagramRenderer
				.createDependency(
						serviceReferencePair.getFrom(),
						serviceReferencePair.getTo(),
						renderInvocationEntries(invocationEntries, true),
						false)
				.render();
	}

	@Value
	public static class InvocationTreeEntry {

		Operation from;

		String stepCode;

		Operation to;
	}

	@Value
	public static class ServiceReferencePair {

		CodeMicroserviceReference from;

		CodeMicroserviceReference to;
	}
}
