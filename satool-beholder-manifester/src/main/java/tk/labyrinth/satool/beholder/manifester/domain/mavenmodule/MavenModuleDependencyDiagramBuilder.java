package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.component.RenderableComponent;
import tk.labyrinth.satool.beholder.manifester.domain.component.RenderableComponentDependency;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.GitBranch;
import tk.labyrinth.satool.beholder.manifester.domain.repository.Repository;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;

import javax.annotation.CheckForNull;

@LazyComponent
@RequiredArgsConstructor
public class MavenModuleDependencyDiagramBuilder {

	@SmartAutowired
	private TypedObjectSearcher<GitBranch> gitBranchSearcher;

	@SmartAutowired
	private TypedObjectSearcher<MavenModule> mavenModuleSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Repository> repositorySearcher;

	public String build() {
		return build(mavenModuleSearcher.searchAll());
	}

	public String build(List<MavenModule> mavenModules) {
		List<Pair<Repository, List<MavenModule>>> repositoryNameAndMavenModulesPairs = mavenModules
				.groupBy(MavenModule::getBranchReference)
				.map(tuple -> Pair.of(
						repositorySearcher.findSingle(gitBranchSearcher.findSingle(tuple._1())
								.getRepositoryReference()),
						tuple._2().sortBy(MavenModule::getGroupIdArtifactId)))
				.toList();
		Map<String, Set<String>> dependencyToRepositoryNamesMap = repositoryNameAndMavenModulesPairs
				.flatMap(pair -> pair.getRight().map(mavenModule ->
						Pair.of(mavenModule.getGroupIdArtifactId(), pair.getLeft())))
				.groupBy(Pair::getLeft)
				.mapValues(pairs -> pairs.map(pair -> pair.getRight().getName()).toSet());
		//
		return """
				'@startuml
								
				%s
								
				%s
								
				'@enduml
				""".formatted(
				repositoryNameAndMavenModulesPairs
						.map(pair -> renderObjects(pair.getLeft().getName(), pair.getRight()))
						.map(MavenModuleDependencyDiagramBuilder::escapeForObjectDiagram)
						.mkString("\n\n"),
				repositoryNameAndMavenModulesPairs
						.flatMap(pair -> renderDependencies(
								dependencyToRepositoryNamesMap,
								pair.getLeft().getName(),
								pair.getRight()))
						.sorted()
						.mkString("\n"));
	}

	public String buildComponentDiagramString(
			List<MavenModule> mavenModules,
			List<GroupIdAndArtifactIdMavenModuleReference> highlightedModuleReferences) {
		List<RenderableComponentGroup> componentGroups = mavenModules
				.groupBy(MavenModule::getBranchReference)
				.map(tuple -> RenderableComponentGroup.builder()
						.mavenModuleAndHighlightIndexPairs(tuple._2()
								.sortBy(MavenModule::getGroupIdArtifactId)
								.map(mavenModule -> Pair.of(
										mavenModule,
										computeHighlightIndex(highlightedModuleReferences, mavenModule))))
						.repository(repositorySearcher.getSingle(gitBranchSearcher.getSingle(tuple._1())
								.getRepositoryReference()))
						.build())
				.toList();
		Map<PlantumlCode, Set<String>> dependencyToRepositoryNamesMap = componentGroups
				.flatMap(componentGroup -> componentGroup.getMavenModuleAndHighlightIndexPairs()
						.map(Pair::getLeft)
						.map(mavenModule -> Pair.of(
								PlantumlCode.ofRaw(mavenModule.getGroupIdArtifactId()),
								componentGroup.getRepository())))
				.groupBy(Pair::getLeft)
				.mapValues(pairs -> pairs.map(pair -> pair.getRight().getName()).toSet());
		//
		return """
				'@startuml
				skinparam componentStyle rectangle
								
				%s
								
				%s
								
				'@enduml
				""".formatted(
				componentGroups
						.map(RenderableComponentGroup::render)
						.mkString("\n\n"),
				renderComponentDependencies(mavenModules)
						.mkString("\n"));
	}

	@CheckForNull
	public static Integer computeHighlightIndex(
			List<GroupIdAndArtifactIdMavenModuleReference> highlightedModuleReferences,
			MavenModule mavenModule) {
		int index = highlightedModuleReferences.indexOf(GroupIdAndArtifactIdMavenModuleReference.from(mavenModule));
		return index != -1 ? index : null;
	}

	public static String escapeForObjectDiagram(String string) {
		return string.replaceAll("[.-]", "_").replace(":", "__");
	}

	public static List<String> renderComponentDependencies(List<MavenModule> mavenModules) {
		Set<PlantumlCode> presentCodes = mavenModules
				.map(mavenModule -> PlantumlCode.ofRaw(mavenModule.getGroupIdArtifactId()))
				.toSet();
		//
		return mavenModules
				.flatMap(mavenModule -> mavenModule.getDependencies().map(dependency ->
						RenderableComponentDependency.builder()
								.fromCode(PlantumlCode.ofRaw(mavenModule.getGroupIdArtifactId()))
								.text(dependency.getVersion())
								.toCode(PlantumlCode.ofRaw(dependency.getGroupIdArtifactId()))
								.build()))
				.filter(dependency -> presentCodes.contains(dependency.getToCode()))
				.map(RenderableComponentDependency::render);
	}

	public static List<String> renderDependencies(
			Map<String, Set<String>> dependencyToRepositoryNamesMap,
			String repositoryName,
			List<MavenModule> mavenModules) {
		return mavenModules
				.flatMap(mavenModule -> mavenModule.getDependencies().map(dependency -> Pair.of(
						mavenModule,
						Pair.of(
								dependency,
								dependencyToRepositoryNamesMap
										.get(dependency.getGroupIdArtifactId())
										.getOrNull()))))
				.filter(pair -> pair.getRight().getRight() != null &&
						!pair.getRight().getRight().contains(repositoryName))
				.flatMap(pair -> pair.getRight().getRight().map(dependencyRepositoryName -> "%s::%s --> %s::%s"
						.formatted(
								PlantumlCode.ofRaw(repositoryName),
								escapeForObjectDiagram(pair.getLeft().getGroupIdArtifactId()),
								PlantumlCode.ofRaw(dependencyRepositoryName),
								PlantumlCode.ofRaw(escapeForObjectDiagram(
										pair.getRight().getLeft().getGroupIdArtifactId())))));
	}

	public static String renderObjects(String repositoryName, List<MavenModule> mavenModules) {
		PlantumlCode plantCode = PlantumlCode.ofRaw(repositoryName);
		//
		return """
				object "%s" as %s {
				%s
				}""".formatted(
				repositoryName,
				plantCode,
				mavenModules
						.map(MavenModule::getGroupIdArtifactId)
						.mkString("\n"));
	}

	@Builder(toBuilder = true)
	@Value
	public static class RenderableComponentGroup {

		List<Pair<MavenModule, @Nullable Integer>> mavenModuleAndHighlightIndexPairs;

		Repository repository;

		public String render() {
			return """
					folder [%s]{
					%s
					}""".formatted(
					repository.getName(),
					mavenModuleAndHighlightIndexPairs
							.map(pair -> RenderableComponent.builder()
									.code(PlantumlCode.ofRaw(pair.getLeft().getGroupIdArtifactId()))
									.colour(pair.getRight() != null ? "lightblue" : null)
									.name("%s\\n%s".formatted(pair.getLeft().getGroupId(), pair.getLeft().getArtifactId()))
									.build())
							.map(RenderableComponent::render)
							.mkString("\n"));
		}
	}
}
