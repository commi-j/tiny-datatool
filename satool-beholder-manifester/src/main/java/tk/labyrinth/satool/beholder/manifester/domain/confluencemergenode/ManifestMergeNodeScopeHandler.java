package tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
public class ManifestMergeNodeScopeHandler implements ConfluenceMergeNodeScopeHandler {

	@Override
	public List<String> getAttributeNames() {
		return List.of("manifest", "manifestEnrichment");
	}

	@Override
	public String getScopeName() {
		return "MANIFEST";
	}

	@Override
	public List<GenericObject> getVariablesObjects(
			String parentScopeName,
			@Nullable GenericObject parentVariablesObject) {
		return List.of(parentVariablesObject);
	}
}
