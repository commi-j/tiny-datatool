package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.ClassSignature;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RestEndpointGroup {

	List<RestEndpoint> endpoints;

	ClassSignature javaSignature;

	@Deprecated // FIXME: Replace with javaSignature#getLongName() (It is used in page name, not in Freemarker).
	String name;

	String normalizedPath;

	@Nullable
	String rawPath;
}
