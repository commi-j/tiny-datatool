package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import net.sourceforge.plantuml.FileFormat;
import org.apache.commons.lang3.NotImplementedException;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.pattern.PandoraPattern;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.ArtifactManifestIndex;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RefinedPersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.diagram.ArtifactManifestPredefinedDiagramKind;
import tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment.ManifestEnrichment;
import tk.labyrinth.satool.beholder.manifester.domain.persistence.PersistenceMapViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.properties.PropertiesMapViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.properties.PropertiesUtils;
import tk.labyrinth.satool.beholder.manifester.domain.properties.Property;
import tk.labyrinth.satool.freemarker.TemplateGenericObjectModel;
import tk.labyrinth.satool.freemarker.templatemodel.NamedTemplateDirectiveModel;
import tk.labyrinth.satool.plantuml.common.PlantumlUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

/**
 * <a href="https://stackoverflow.com/questions/28472751/is-it-possible-to-create-a-freemarker-macro-programmatically">https://stackoverflow.com/questions/28472751/is-it-possible-to-create-a-freemarker-macro-programmatically</a>
 */
@LazyComponent
@RequiredArgsConstructor
public class ConfluenceDiagramTemplateDirectiveModel implements NamedTemplateDirectiveModel {

	private final ConverterRegistry converterRegistry;

	private String createPlantumlSource(
			Environment env,
			ArtifactManifestPredefinedDiagramKind diagramKind) throws TemplateModelException {
		ArtifactManifestIndex manifest = converterRegistry.convert(
				((TemplateGenericObjectModel) env.getVariable("manifest")).getGenericObject(),
				ArtifactManifestIndex.class);
		Optional<ManifestEnrichment> manifestEnrichmentOptional = Optional.ofNullable(env.getVariable("manifestEnrichment"))
				.map(TemplateGenericObjectModel.class::cast)
				.map(TemplateGenericObjectModel::getGenericObject)
				.map(genericObject -> converterRegistry.convert(
						genericObject,
						ManifestEnrichment.class));
		//
		return switch (diagramKind) {
			case PERSISTENT_MODEL_MAP -> {
				RefinedPersistentModel persistentModel = converterRegistry.convert(
						((TemplateGenericObjectModel) env.getVariable("persistentModel")).getGenericObject(),
						RefinedPersistentModel.class);
				//
				List<RefinedPersistentModel> selectedPersistentModels = PersistenceMapViewRenderer.pickRelated(
						manifest.getPersistentModels(),
						persistentModel,
						1,
						manifestEnrichmentOptional.map(ManifestEnrichment::getPersistentModelRelations).orElse(List.empty()));
				//
				yield PersistenceMapViewRenderer
						.createDatabaseDiagramModel(
								selectedPersistentModels,
								List.of(persistentModel.getDatabaseName()),
								manifestEnrichmentOptional.map(ManifestEnrichment::getPersistentModelRelations).orElse(List.empty()),
								PersistenceMapViewRenderer.pickRelated(
										manifest.getModels(),
										selectedPersistentModels))
						.render();
			}
			case PERSISTENT_MODEL_MAP_ALL -> PersistenceMapViewRenderer
					.createDatabaseDiagramModel(
							manifest.getPersistentModels(),
							List.empty(),
							manifestEnrichmentOptional.map(ManifestEnrichment::getPersistentModelRelations).orElse(List.empty()),
							PersistenceMapViewRenderer.pickRelated(
									manifest.getModels(),
									manifest.getPersistentModels()))
					.render();
			case PROPERTY_MAP -> {
				Property property = converterRegistry.convert(
						((TemplateGenericObjectModel) env.getVariable("property")).getGenericObject(),
						Property.class);
				//
				val selectedProperties = PropertiesUtils.selectPropertyTree(
						manifest.getProperties().flatMap(Property::getDeclarationEntries),
						manifest.getProperties().flatMap(Property::getUsageEntries),
						property.getName());
				//
				yield PropertiesMapViewRenderer.createDiagramModel(
								selectedProperties.getLeft(),
								selectedProperties.getRight(),
								property.getName())
						.render();
			}
		};
	}

	private void renderImage(
			Environment env,
			ImageDiagramPublishingStrategy imageDiagramPublishingStrategy,
			String plantumlSource) throws IOException {
		FileFormat fileFormat = switch (imageDiagramPublishingStrategy.getFileFormat()) {
			case "PNG" -> FileFormat.PNG;
			case "SVG" -> FileFormat.SVG;
			default -> throw new NotImplementedException();
		};
		//
		String filename = "%s%s".formatted(
				imageDiagramPublishingStrategy.getFilenamePattern().render(placeholder -> {
					try {
						return env.getVariable(placeholder).toString();
					} catch (TemplateModelException ex) {
						throw new RuntimeException(ex);
					}
				}),
				fileFormat.getFileSuffix());
		//
		byte[] renderedContent = PlantumlUtils.render(plantumlSource, fileFormat);
		//
		ThreadLocalConfluenceAttachmentsCache.put(filename, renderedContent);
		//
		// TODO: Find out how to invoke corresponding macro instead of duplicating xml here.
		env.getOut().write("""
				<ac:image ac:border="true" ac:height="100">
					<ri:attachment ri:filename="%s"/>
				</ac:image>
				""".formatted(filename));
	}

	private void renderPlantuml(Environment env, String plantumlSource) throws IOException {
		// TODO: Find out how to invoke corresponding macro instead of duplicating xml here.
		env.getOut().write("""
				<ac:structured-macro ac:name="plantuml">
					<ac:plain-text-body><![CDATA[%s]]></ac:plain-text-body>
				</ac:structured-macro>
				""".formatted(plantumlSource));
	}

	@Override
	public void execute(
			Environment env,
			Map params,
			TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws IOException, TemplateModelException {
		ArtifactManifestPredefinedDiagramKind diagramKind = ArtifactManifestPredefinedDiagramKind.valueOf(
				params.get("kind").toString());
		//
		ConfluenceDiagramPublishingStrategy diagramPublishingStrategy = switch (params.get("strategy").toString()) {
			case "PLANT_UML" -> PlantumlMacroDiagramPublishingStrategy.of();
			case "PNG" -> ImageDiagramPublishingStrategy.builder()
					.fileFormat("PNG")
					.filenamePattern(PandoraPattern.from(params.get("filenamePattern").toString()))
					.build();
			case "SVG" -> ImageDiagramPublishingStrategy.builder()
					.fileFormat("SVG")
					.filenamePattern(PandoraPattern.from(params.get("filenamePattern").toString()))
					.build();
			default -> throw new NotImplementedException();
		};
		//
		String plantumlSource = createPlantumlSource(env, diagramKind);
		//
		if (diagramPublishingStrategy instanceof ImageDiagramPublishingStrategy imageDiagramPublishingStrategy) {
			renderImage(env, imageDiagramPublishingStrategy, plantumlSource);
		} else if (diagramPublishingStrategy instanceof PlantumlMacroDiagramPublishingStrategy) {
			renderPlantuml(env, plantumlSource);
		} else {
			throw new NotImplementedException();
		}
	}

	@Override
	public String getName() {
		return "diagram";
	}
}
