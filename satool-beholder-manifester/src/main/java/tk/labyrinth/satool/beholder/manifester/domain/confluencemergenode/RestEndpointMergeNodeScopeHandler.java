package tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.value.wrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModelUtils;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestEndpoint;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestParametersEntry;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestResultEntry;

@LazyComponent
@RequiredArgsConstructor
public class RestEndpointMergeNodeScopeHandler implements ConfluenceMergeNodeScopeHandler {

	private final ConverterRegistry converterRegistry;

	@Override
	public List<String> getAttributeNames() {
		return List.of("restEndpoint");
	}

	@Override
	public String getScopeName() {
		return "REST_ENDPOINT";
	}

	@Override
	public List<GenericObject> getVariablesObjects(String parentScopeName, GenericObject parentObject) {
		GenericObject manifestObject = parentObject.getAttributeValueAsObject("manifest");
		//
		List<DisplayableModel> models = converterRegistry.convertInferred(
				manifestObject.getAttributeValue("models"),
				TypeUtils.parameterize(List.class, DisplayableModel.class));
		//
		return parentObject.getAttributeValueAsObject("restEndpointGroup")
				.getAttributeValueAsList("endpoints")
				.map(ValueWrapper::asObject)
				.map(ObjectValueWrapper::unwrap)
				.map(restEndpointObject -> {
					RestEndpoint restEndpoint = converterRegistry.convert(restEndpointObject, RestEndpoint.class);
					//
					return parentObject.withAddedAttributes(
							GenericObjectAttribute.ofObject("restEndpoint", restEndpointObject),
							GenericObjectAttribute.ofObjectList(
									"relevantParametersModels",
									DisplayableModelUtils.selectRecursivelyWithTypes(
													models,
													restEndpoint.getParameters().map(RestParametersEntry::getDatatype))
											.map(displayableModel -> converterRegistry.convert(displayableModel, GenericObject.class))),
							GenericObjectAttribute.ofObjectList(
									"relevantResultModels",
									DisplayableModelUtils.selectRecursivelyWithTypes(
													models,
													restEndpoint.getResult().map(RestResultEntry::getDatatype))
											.map(displayableModel -> converterRegistry.convert(displayableModel, GenericObject.class))));
				});
	}
}
