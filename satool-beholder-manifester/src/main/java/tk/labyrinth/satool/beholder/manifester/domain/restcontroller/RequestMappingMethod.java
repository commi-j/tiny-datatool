package tk.labyrinth.satool.beholder.manifester.domain.restcontroller;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.web.bind.annotation.RequestMethod;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderPattern("$method $paths")
@Value
@With
public class RequestMappingMethod {

	RequestMethod method;

	List<RestParametersEntry> parameters;

	@Nullable
	List<String> paths;

	List<RestResultEntry> result;

	MethodSimpleSignature signature;
}
