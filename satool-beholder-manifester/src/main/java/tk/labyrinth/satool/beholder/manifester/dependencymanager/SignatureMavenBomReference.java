package tk.labyrinth.satool.beholder.manifester.dependencymanager;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.ArtifactSignature;

import java.util.Objects;

@Value(staticConstructor = "of")
public class SignatureMavenBomReference implements Reference<MavenBom> {

	ArtifactSignature signature;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				MavenBom.MODEL_CODE,
				MavenBom.SIGNATURE_ATTRIBUTE_NAME,
				signature);
	}

	public static SignatureMavenBomReference from(MavenBom object) {
		return SignatureMavenBomReference.of(object.getSignature());
	}

	@JsonCreator
	public static SignatureMavenBomReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), MavenBom.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(ArtifactSignature.from(reference.getAttributeValue(MavenBom.SIGNATURE_ATTRIBUTE_NAME)));
	}
}
