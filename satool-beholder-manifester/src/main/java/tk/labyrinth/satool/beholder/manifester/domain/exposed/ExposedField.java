package tk.labyrinth.satool.beholder.manifester.domain.exposed;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ExposedField {

	@Nullable
	List<ExposedAnnotation> annotations;

	String name;

	@Nullable
	ExposedType type;

	public static ExposedField merge(List<ExposedField> fields) {
		return ExposedField.builder()
				.annotations(ExposedAnnotation.mergeMultiple(fields.map(ExposedField::getAnnotations)))
				.name(fields.map(ExposedField::getName).distinct().single())
				.type(ExposedType.merge(fields.map(ExposedField::getType)))
				.build();
	}

	@Nullable
	public static List<ExposedField> mergeMultiple(List<@Nullable List<ExposedField>> fieldLists) {
		return ExposedUtils.mergeMultiple(fieldLists, ExposedField::getName, ExposedField::merge);
	}
}
