package tk.labyrinth.satool.beholder.manifester.domain.confluencepageproperties;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.datatype.DatatypeKind;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.object.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.value.wrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.exception.wrapping.WrappingError;
import tk.labyrinth.pandora.misc4j.integration.auth.AuthorizationUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceConnectionParameters;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFetcher;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceDetailsElement;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceDocument;
import tk.labyrinth.satool.confluenceconnector.element.ConfluenceElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceCellElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceRowElement;
import tk.labyrinth.satool.confluenceconnector.element.table.ConfluenceTableElement;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceLastUpdated;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceQueryLanguagePredicate;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceSpace;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceStorage;

import javax.annotation.CheckForNull;
import java.util.ArrayList;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class FetchConfluencePagesJobRunner implements JobRunner<FetchConfluencePagesJobRunner.Parameters> {

	private final ConfluenceFetcher confluenceFetcher;

	private final ConverterRegistry converterRegistry;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final ExecutionContextHandler executionContextHandler;

	private final GenericObjectManipulator genericObjectManipulator;

	private final GenericObjectSearcher genericObjectSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ConfluencePagePropertiesModel> confluencePagePropertiesModelSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@CheckForNull
	private ConfluenceConnectionParameters createConnectionParameters(Parameters parameters) {
		String authorization;
		{
			if (parameters.getConfluenceCredentialsReference() != null) {
				Credentials credentials = credentialsSearcher.findSingle(
						parameters.getConfluenceCredentialsReference());
				authorization = AuthorizationUtils.encodeBasic(credentials.getUsername(), credentials.getPassword());
			} else {
				authorization = null;
			}
		}
		//
		return ConfluenceConnectionParameters.builder()
				.authorization(authorization)
				.baseUrl(parameters.getConfluenceBaseUrl())
				.build();
	}

	private GenericObject parsePagePropertiesTable(
			ConfluencePagePropertiesModel pagePropertiesModel,
			ConfluenceConnectionParameters connectionParameters,
			ConfluenceContent content,
			List<ConfluenceRowElement> rows) {
		GenericObject result;
		{
			ObjectModel objectModel = objectModelSearcher.getSingle(pagePropertiesModel.getObjectModelReference());
			//
			List<Pair<String, ConfluenceCellElement>> pageProperties = rows.map(row -> {
				List<ConfluenceCellElement> cells = row.getCells();
				ConfluenceCellElement keyCell = cells.get(0);
				ConfluenceCellElement valueCell = cells.get(1);
				//
				return Pair.of(keyCell.getJsoupElement().text(), valueCell);
			});
			//
			ArrayList<String> processedPageProperties = new ArrayList<>();
			List<GenericObjectAttribute> resolvedAttributes = pagePropertiesModel.getPropertyMappings()
					.map(propertyMapping -> {
						GenericObjectAttribute attribute;
						{
							ConfluenceCellElement pagePropertyValue = pageProperties
									.find(pageProperty -> Objects.equals(
											pageProperty.getKey(),
											propertyMapping.getConfluencePagePropertyName()))
									.map(Pair::getValue)
									.getOrNull();
							if (pagePropertyValue != null) {
								attribute = GenericObjectAttribute.of(
										propertyMapping.getBeholderObjectAttributeName(),
										resolveValue(
												objectModel.getAttributes()
														.find(modelAttribute -> Objects.equals(
																modelAttribute.getName(),
																propertyMapping.getBeholderObjectAttributeName()))
														.get()
														.getDatatype(),
												pagePropertyValue));
								processedPageProperties.add(propertyMapping.getConfluencePagePropertyName());
							} else {
								attribute = null;
							}
						}
						return attribute;
					})
					.filter(Objects::nonNull);
			//
			List<Pair<String, String>> unprocessedPageProperties = pageProperties
					.filter(pageProperty -> !processedPageProperties.contains(pageProperty.getKey()))
					.map(pageProperty -> Pair.of(
							pageProperty.getKey(),
							pageProperty.getValue().getJsoupElement().text()));
			//
			result = GenericObject.of(resolvedAttributes)
					.withAddedAttributes(
							GenericObjectAttribute.ofSimple("confluencePageId", content.getId()),
							RootObjectUtils.createModelReferenceAttribute(objectModel))
					.withNullableAttribute(
							"unprocessed",
							!unprocessedPageProperties.isEmpty()
									? ListValueWrapper.of(unprocessedPageProperties.map(unprocessedPageProperty ->
									ObjectValueWrapper.of(GenericObject.of(
											GenericObjectAttribute.ofSimple(
													"key",
													unprocessedPageProperty.getKey()),
											GenericObjectAttribute.ofSimple(
													"value",
													unprocessedPageProperty.getValue())))))
									: null);
		}
		return result;
	}

	private void processContent(
			ConfluencePagePropertiesModel pagePropertiesModel,
			ConfluenceConnectionParameters connectionParameters,
			ConfluenceContent content) {
		ConfluenceDocument root = ConfluenceDocument.from(content.getBody().getStorage().getValue());
		//
		List<ConfluenceElement> detailses = root.select(ConfluenceDetailsElement.selector());
		if (detailses.size() == 1) {
			List<ConfluenceTableElement> tables = ConfluenceTableElement.select(detailses.get(0));
			if (tables.size() == 1) {
				GenericObject object = parsePagePropertiesTable(
						pagePropertiesModel,
						connectionParameters,
						content,
						tables.get(0).getRows());
				//
				saveObject(object);
			} else {
				logger.warn("Skipping content with id = {}, no unique {}{} found: {}",
						content.getId(), ConfluenceDetailsElement.selector(), ConfluenceTableElement.xpathSelector(), tables);
			}
		} else {
			logger.warn("Skipping content with id = {}, no unique {} found: {}",
					content.getId(), ConfluenceDetailsElement.selector(), detailses);
		}
	}

	private ValueWrapper resolveValue(Datatype datatype, ConfluenceCellElement valueCell) {
		ValueWrapper result;
		{
			if (datatypeBaseRegistry.determineKind(datatype) == DatatypeKind.SIMPLE) {
				result = SimpleValueWrapper.of(valueCell.getJsoupElement().text());
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	private void saveObject(GenericObject object) {
		String confluencePageId = object.getAttributeValueAsString("confluencePageId");
		GenericObject foundObject = genericObjectSearcher.findSingle(GenericObjectReference.of(
				GenericReference.of(
						RootObjectUtils.getModelReference(object).getCode(),
						List.of(GenericReferenceAttribute.builder()
								.name("confluencePageId")
								.value(confluencePageId)
								.build()))));
		if (foundObject != null) {
			logger.info("Updating ConfluencePageObject: confluencePageId = {}", confluencePageId);
			//
			GenericObject objectToUpdate = object
					.withAddedAttribute(foundObject.getAttribute(RootObjectUtils.UID_ATTRIBUTE_NAME));
			genericObjectManipulator.update(objectToUpdate);
		} else {
			logger.info("Creating ConfluencePageObject: confluencePageId = {}", confluencePageId);
			//
			GenericObject objectToCreate = object
					.withAddedAttribute(RootObjectUtils.createNewUidAttribute());
			genericObjectManipulator.create(objectToCreate);
		}
	}

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder()
				.spaceReference(executionContextHandler.getExecutionContext().findAttributeValueInferred(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME))
				.build();
	}

	@Override
	public void run(Context<Parameters> context) {
		Parameters parameters = context.getParameters();
		ConfluencePagePropertiesModel pagePropertiesModel = confluencePagePropertiesModelSearcher.getSingle(
				parameters.getConfluencePagePropertiesModelReference());
		ConfluenceConnectionParameters connectionParameters = createConnectionParameters(parameters);
		//
		confluenceFetcher
				.searchContentsFlux(
						connectionParameters,
						ConfluenceQueryLanguagePredicate.builder()
								.labels(List.of(pagePropertiesModel.getLabel()))
								.spaceKeys(List.of(pagePropertiesModel.getSpaceKey()))
								.build(),
						List
								.of(
										ConfluenceLastUpdated.EXPAND_KEY,
										ConfluenceSpace.EXPAND_KEY,
										ConfluenceStorage.EXPAND_KEY)
								.mkString(","),
						null)
				.doOnNext(next -> next.getResults().forEach(content -> {
					try {
						processContent(pagePropertiesModel, connectionParameters, content);
					} catch (RuntimeException ex) {
						logger.warn("content.id = {}", content.getId(), ex);
					}
				}))
				.doOnError(error -> {
					throw new WrappingError(error);
				})
				.subscribe();
	}

	private static GenericObjectAttribute resolveAttribute(
			ConfluencePagePropertyMapping pagePropertyMapping,
			ConfluenceCellElement pagePropertyCell) {
		// TODO
		throw new NotImplementedException();
	}

	@Builder(toBuilder = true)
	@Value
	public static class Parameters implements HasSpaceReference {

		String confluenceBaseUrl;

		@CheckForNull
		KeyCredentialsReference confluenceCredentialsReference;

		SpaceKeyAndLabelConfluencePagePropertiesModelReference confluencePagePropertiesModelReference;

		KeySpaceReference spaceReference;
	}
}
