package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.diagram;

public enum ArtifactManifestPredefinedDiagramKind {
	PERSISTENT_MODEL_MAP,
	PERSISTENT_MODEL_MAP_ALL,
	PROPERTY_MAP,
}
