package tk.labyrinth.satool.beholder.manifester.domain.persistentmodel;

import io.vavr.collection.List;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.stores.objectindex.Indexer;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;

@Component
public class PersistentModelIndexer implements Indexer<ArtifactManifest, PersistentModelIndex> {

	@Override
	public List<PersistentModelIndex> createIndices(ArtifactManifest target) {
		return ObjectUtils.<List<PersistentModel>>orElse(target.getPersistentModels(), List.empty())
				.map(persistentModel -> PersistentModelIndex.builder()
						.attributes(persistentModel.getAttributes())
						.databaseName(persistentModel.getDatabaseName())
						.javaClassSignature(persistentModel.getJavaClassSignature())
						.tags(persistentModel.getTags())
						.targetReference(createTargetReference(target))
						.build());
	}

	@Override
	public UidReference<ArtifactManifest> createTargetReference(ArtifactManifest target) {
		return UidReference.of(ArtifactManifest.MODEL_CODE, target.getUid());
	}
}
