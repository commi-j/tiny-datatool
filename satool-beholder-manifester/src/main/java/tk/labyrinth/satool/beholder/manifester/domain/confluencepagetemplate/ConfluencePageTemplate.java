package tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;

import javax.annotation.CheckForNull;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(ConfluencePageTemplate.MODEL_CODE)
@ModelTag("docs")
@RenderAttribute("key")
@Value
@With
public class ConfluencePageTemplate {

	public static final String KEY_ATTRIBUTE_NAME = "key";

	public static final String MODEL_CODE = "confluencepagetemplate";

	@Nullable
	String bodyTemplate;

	@Nullable
	String bodyTemplateName;

	String key;
//	Datatype modelType;
}
