package tk.labyrinth.satool.beholder.manifester.domain.operation;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.objectproxy.ObjectProxyHandler;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.object.change.ObjectChangeInterceptor;
import tk.labyrinth.satool.beholder.manifester.domain.microservice.Microservice;

@LazyComponent
@RequiredArgsConstructor
public class OperationChangeInterceptor implements ObjectChangeInterceptor<Operation> {

	private final ObjectProxyHandler objectProxyHandler;

	@SmartAutowired
	private TypedObjectSearcher<Microservice> microserviceSearcher;

	@Override
	public Operation interceptObjectChange(Operation current, Operation next) {
		Operation result;
		{
			if (next.getCode() == null && next.getService() != null) {
				Microservice microservice = microserviceSearcher.findSingle(next.getService());
				if (microservice != null && microservice.getOperationPrefix() != null) {
					result = objectProxyHandler.wrap(
							OperationBuilder.from(next)
									.code(microservice.getOperationPrefix())
									.toObject(objectProxyHandler.getConverterRegistry()),
							Operation.class);
				} else {
					result = next;
				}
			} else {
				result = next;
			}
		}
		return result;
	}
}
