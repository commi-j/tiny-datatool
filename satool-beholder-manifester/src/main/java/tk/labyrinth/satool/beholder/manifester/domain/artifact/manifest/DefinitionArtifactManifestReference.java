package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.beholder.manifester.artifact.NameArtifactDefinitionReference;

@RenderAttribute("definitionReference")
@Value(staticConstructor = "of")
public class DefinitionArtifactManifestReference implements Reference<ArtifactManifest> {

	NameArtifactDefinitionReference definitionReference;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				ArtifactManifest.MODEL_CODE,
				ArtifactManifest.DEFINITION_REFERENCE_ATTRIBUTE_NAME,
				definitionReference);
	}

	public static DefinitionArtifactManifestReference from(ArtifactManifest value) {
		return of(value.getDefinitionReference());
	}

	@JsonCreator
	public static DefinitionArtifactManifestReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		return of(NameArtifactDefinitionReference.from(reference.getAttributeValue(
				ArtifactManifest.DEFINITION_REFERENCE_ATTRIBUTE_NAME)));
	}
}
