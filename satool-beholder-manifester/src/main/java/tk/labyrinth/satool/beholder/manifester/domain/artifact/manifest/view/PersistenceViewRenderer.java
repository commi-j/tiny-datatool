package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RefinedPersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment.CustomPersistentModelRelation;
import tk.labyrinth.satool.beholder.manifester.domain.persistence.PersistenceMapViewRenderer;

import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PersistenceViewRenderer {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final PersistenceMapViewRenderer persistenceMapViewRenderer;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	@WithSpan
	private Component renderDetails(Properties properties, RefinedPersistentModel item) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.add("TODO");
		}
		return layout;
	}

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			Grid<RefinedPersistentModel> grid = new Grid<>();
			grid.setHeightFull();
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			//
			grid
					.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
							ToVaadinComponentRendererRegistry.Context.builder()
									.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(ClassSignature.class))
									.hints(List.empty())
									.build(),
							item.getJavaSignature()
					))
					.setHeader("Java Class")
					.setResizable(true);
			grid
					.addColumn(RefinedPersistentModel::getDatabaseName)
					.setHeader("Database Name")
					.setResizable(true);
			grid
					.addComponentColumn(item -> GridUtils.renderValueList(item.getTags()))
					.setHeader("Tags")
					.setResizable(true);
			grid
					.addColumn(item -> item.getAttributes().size())
					.setHeader("Attribute Count")
					.setResizable(true);
			grid
					.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
							.onClick(event -> {
//								val pair = PropertiesMapViewRenderer.selectPropertyTree(
//										state.getProperties().getPropertyDeclarationEntries(),
//										state.getProperties().getPropertyUsageEntries(),
//										item.getName());
								//
								List<RefinedPersistentModel> relatedModels = PersistenceMapViewRenderer.pickRelated(
										properties.getPersistentModels(),
										item,
										1,
										properties.getCustomPersistentModelRelations());
								//
								ConfirmationViews.showFunctionDialog(
										PersistenceMapViewRenderer.Properties.builder()
												.customPersistentModelRelations(PersistenceMapViewRenderer.pickRelatedCustomRelations(
														properties.getCustomPersistentModelRelations(),
														relatedModels))
												.highlightedDatabaseNames(List.of(item.getDatabaseName()))
												.persistentModels(relatedModels)
												.relatedModels(PersistenceMapViewRenderer.pickRelated(
														properties.getDisplayableModels(),
														relatedModels))
												.build(),
										(persistenceMapProperties, persistenceMapSink) ->
												persistenceMapViewRenderer.render(persistenceMapProperties));
							})
							.text("Diagram")
							.build()))
					.setResizable(true);
			//
			grid.setItemDetailsRenderer(new ComponentRenderer<>(item -> renderDetails(properties, item)));
			//
			grid.setItems(properties.getPersistentModels().asJava());
			//
			layout.add(grid);
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		List<CustomPersistentModelRelation> customPersistentModelRelations;

		List<DisplayableModel> displayableModels;

		List<RefinedPersistentModel> persistentModels;

		public static class Builder {
			// Lomboked
		}
	}
}