package tk.labyrinth.satool.beholder.manifester.diagram.component;

import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.satool.beholder.manifester.domain.componentdependency.ComponentDependencyLayer;

@Builder(toBuilder = true)
@Value
@With
public class ComponentDiagramModelConfiguration {

	@NonNull
	Set<ComponentDependencyLayer> dependencyLayers;

	@NonNull
	List<ComponentDiagramPage.State.FocusEntry> focusEntries;

	@NonNull
	Boolean renderAll;
}
