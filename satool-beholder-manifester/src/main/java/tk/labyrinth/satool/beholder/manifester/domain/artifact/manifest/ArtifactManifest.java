package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.stores.rootobject.HasUid;
import tk.labyrinth.satool.beholder.manifester.artifact.NameArtifactDefinitionReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.BuildSystem;
import tk.labyrinth.satool.beholder.manifester.domain.endpoint.kafka.KafkaEndpoint;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RestController;

import java.time.Instant;
import java.util.UUID;

@Builder(toBuilder = true)
@Model(ArtifactManifest.MODEL_CODE)
@ModelTag("properties")
@ModelTag("repos")
@RenderAttribute("definitionReference")
@Value
public class ArtifactManifest implements HasUid {

	public static final String DEFINITION_REFERENCE_ATTRIBUTE_NAME = "definitionReference";

	public static final String MODEL_CODE = "artifactmanifest";

	List<BuildSystem> buildSystems;

	NameArtifactDefinitionReference definitionReference;

	List<ExposedClass> exposedClasses;

	List<ManifestFunction> functions;

	@Deprecated
	List<KafkaEndpoint> kafkaEndpoints;

	List<PersistentModel> persistentModels;

	List<PropertyDeclarationEntry> propertyDeclarationEntries;

	List<PropertyUsageEntry> propertyUsageEntries;

	@Deprecated
	List<RestController> restControllers;

	List<Tag> tags;

	UUID uid;

	Instant updatedAt;
}
