package tk.labyrinth.satool.beholder.manifester.domain.component;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;

import javax.annotation.CheckForNull;

@Builder(toBuilder = true)
@Value
@With
public class RenderableComponent {

	PlantumlCode code;

	@CheckForNull
	String colour;

	String name;

	@CheckForNull
	String text;

	public String render() {
		String result;
		{
			// We use long form if text is present and short otherwise.
			//
			if (text != null) {
				result = """
						component %s%s [
						%s
						%s
						]""".formatted(
						code,
						colour != null ? " #%s".formatted(colour) : "",
						name,
						text);
			} else {
				result = "[%s] as %s%s".formatted(
						name,
						code,
						colour != null ? " #%s".formatted(colour) : "");
			}
		}
		return result;
	}
}
