package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.component.object.custom.CustomizeTable;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;

@Builder(toBuilder = true)
@Model
@ModelTag("properties")
@CustomizeTable(attributesOrder = {
		"name",
		"type",
		"defaultValue",
		"location",
		"tags",
		"targetReference",
})
@Value
public class PropertyUsageEntryIndex implements ObjectIndex<ArtifactManifest> {

	String defaultValue;

	String location;

	String name;

	List<String> tags;

	UidReference<ArtifactManifest> targetReference;

	TypeDescription type;
}
