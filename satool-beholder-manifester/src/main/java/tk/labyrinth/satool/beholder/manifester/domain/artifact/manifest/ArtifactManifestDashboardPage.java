package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.page.HasUuidUrlParameter;
import tk.labyrinth.satool.beholder.root.BeholderRootLayout;

import java.util.UUID;

@RequiredArgsConstructor
@Route(value = "artifact-manifest-dashboard", layout = BeholderRootLayout.class)
public class ArtifactManifestDashboardPage extends CssVerticalLayout implements HasUrlParameter<String> {

	private final ArtifactManifestDashboardViewRenderer dashboardViewRenderer;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifest> artifactManifestSearcher;

	@Override
	@WithSpan
	public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
		if (parameter != null) {
			Try<UUID> uidTry = Try.ofSupplier(() -> HasUuidUrlParameter.parse(parameter));
			//
			if (uidTry.isSuccess()) {
				ArtifactManifest artifactManifest = artifactManifestSearcher.findSingle(
						UidReference.of(ArtifactManifest.MODEL_CODE, uidTry.get()));
				//
				if (artifactManifest != null) {
					Component component = FunctionalComponent.create(
							artifactManifest,
							properties -> dashboardViewRenderer.render(artifactManifest));
					CssFlexItem.setFlexGrow(component, 1);
					add(component);
				} else {
					add(new Label("No ArtifactManifest found with uid: %s".formatted(uidTry.get())));
				}
			} else {
				add(new Label("Malformed uid: %s".formatted(parameter)));
				add(new Label("Fault: %s".formatted(uidTry.getCause())));
			}
		} else {
			add(new Label("Missing uid"));
		}
	}
}
