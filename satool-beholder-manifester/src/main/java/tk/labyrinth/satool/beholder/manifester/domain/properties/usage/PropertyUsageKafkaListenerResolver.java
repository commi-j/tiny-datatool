package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import org.springframework.kafka.annotation.KafkaListener;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.satool.spoon.SpoonStaticFactory;
import tk.labyrinth.satool.spoon.SpoonUtils;

import java.util.Objects;
import java.util.regex.MatchResult;

public class PropertyUsageKafkaListenerResolver {

	private static final CtTypeReference<KafkaListener> kafkaListenerAnnotationType = SpoonStaticFactory
			.createReference(KafkaListener.class);

	private static List<PropertyUsageEntry> resolveAnnotation(CtAnnotation<KafkaListener> kafkaListenerAnnotation) {
		List<String> topicsValues = List.of((String[]) kafkaListenerAnnotation.getValueAsObject("topics"));
		//
		return topicsValues.flatMap(topicValue -> {
			List<MatchResult> placeholderValues = SpelUtils.findPlaceholderValues(topicValue);
			//
			return placeholderValues.map(placeholderValue -> PropertyUsageEntry.builder()
					.defaultValue(placeholderValue.group(3))
					.javaDefaultValue(null)
					.javaType(TypeDescription.ofNonParameterized(String.class))
					.name(placeholderValue.group(1))
					.location(SpoonUtils.elementToSignature(kafkaListenerAnnotation).toString())
					.tags(List.of("java"))
					.build());
		});
	}

	public static List<PropertyUsageEntry> resolve(CtModel javaModel) {
		List<CtAnnotation<KafkaListener>> kafkaListenerAnnotatedElements = List.ofAll(javaModel.getAllTypes())
				.flatMap(type -> List.<CtType<?>>of(type).appendAll(type.getNestedTypes()))
				.flatMap(type -> List.<CtElement>of(type).appendAll(List.ofAll(type.getAllMethods())))
				.map(element -> element.getAnnotation(kafkaListenerAnnotationType))
				.filter(Objects::nonNull);
		//
		return kafkaListenerAnnotatedElements.flatMap(PropertyUsageKafkaListenerResolver::resolveAnnotation);
	}
}
