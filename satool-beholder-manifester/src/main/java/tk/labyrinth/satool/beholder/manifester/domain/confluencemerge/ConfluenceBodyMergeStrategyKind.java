package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

public enum ConfluenceBodyMergeStrategyKind {
	DO_NOTHING,
	USE_TEMPLATE,
}
