package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;
import tk.labyrinth.satool.beholder.manifester.domain.properties.Property;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(code = ArtifactManifestIndex.MODEL_CODE)
@ModelTag("confluence")
@ModelTag("repos")
@Value
@With
public class ArtifactManifestIndex implements ObjectIndex<ArtifactManifest> {

	public static final String MODEL_CODE = "artifactmanifestindex";

	@NonNull
	List<ManifestFunction> functions;

	@NonNull
	List<DisplayableModel> models;

	@NonNull
	String name;

	@NonNull
	List<RefinedPersistentModel> persistentModels;

	@NonNull
	List<Property> properties;

	@NonNull
	List<RestEndpointGroup> restEndpointGroups;

	@NonNull
	UidReference<ArtifactManifest> targetReference;

	public static ArtifactManifestIndex empty() {
		return ArtifactManifestIndex.builder()
				.functions(List.empty())
				.models(List.empty())
				.name("empty")
				.persistentModels(List.empty())
				.properties(List.empty())
				.restEndpointGroups(List.empty())
				.targetReference(UidReference.of("dummy", UUID.randomUUID()))
				.build();
	}
}
