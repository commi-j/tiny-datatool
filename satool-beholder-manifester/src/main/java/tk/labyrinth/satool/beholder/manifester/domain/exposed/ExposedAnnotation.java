package tk.labyrinth.satool.beholder.manifester.domain.exposed;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.TypeSignature;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ExposedAnnotation {

	TypeSignature signature;

	List<ExposedAnnotationValue> values;

	public static ExposedAnnotation merge(List<ExposedAnnotation> annotations) {
		return ExposedAnnotation.builder()
				.signature(annotations.map(ExposedAnnotation::getSignature).distinct().single())
				.values(ExposedAnnotationValue.mergeMultiple(annotations.map(ExposedAnnotation::getValues)))
				.build();
	}

	@Nullable
	public static List<ExposedAnnotation> mergeMultiple(List<@Nullable List<ExposedAnnotation>> annotationLists) {
		return ExposedUtils.mergeMultiple(annotationLists, ExposedAnnotation::getSignature, ExposedAnnotation::merge);
	}
}
