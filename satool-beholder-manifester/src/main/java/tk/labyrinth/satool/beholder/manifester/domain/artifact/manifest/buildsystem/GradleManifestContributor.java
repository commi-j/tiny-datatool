package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.SystemUtils;
import spoon.reflect.CtModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.util.ExecUtils;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.gradle.model.BuildGradleModel;
import tk.labyrinth.satool.gradle.model.DependenciesNode;
import tk.labyrinth.satool.gradle.model.GradleModelFactory;
import tk.labyrinth.satool.gradle.model.PluginsNode;
import tk.labyrinth.satool.gradle.model.RepositoriesNode;
import tk.labyrinth.satool.gradle.model.RepositoryNode;
import tk.labyrinth.satool.maven.domain.MavenDependency;
import tk.labyrinth.satool.maven.domain.MavenRepository;

import java.io.File;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@LazyComponent
@RequiredArgsConstructor
public class GradleManifestContributor implements ManifestContributor {

	public static final String BUILD_FILE_NAME = "build.gradle";

	public static final String SETTINGS_FILE_NAME = "settings.gradle";

	public static final String BUILD_SYSTEM_NAME = "Gradle";

	private final GradleModelFactory gradleModelFactory = new GradleModelFactory();

	@Override
	public List<BuildSystem> contribute(Path rootDirectory, CtModel javaModel) {
		List<BuildSystem> result;
		{
			File buildGradleFile = rootDirectory.resolve(BUILD_FILE_NAME).toFile();
			if (buildGradleFile.exists()) {
				BuildGradleModel buildGradleModel = gradleModelFactory.createBuildGradleModel(buildGradleFile);
				//
				result = List.of(BuildSystem.builder()
						.category("Java")
						.dependencies(Optional.ofNullable(buildGradleModel.getDependencies())
								.map(DependenciesNode::getDependencies)
								.orElse(List.empty())
								.map(dependencyNode -> MavenDependency.builder()
										.artifactId(dependencyNode.getName())
										.groupId(dependencyNode.getGroup())
										.scope(dependencyNode.getScope())
										.version(dependencyNode.getVersion())
										.build()))
						.name(BUILD_SYSTEM_NAME)
						.plugins(Optional.ofNullable(buildGradleModel.getPlugins())
								.map(PluginsNode::getPlugins)
								.orElse(List.empty())
								.map(pluginNode -> ArtifactSignature.builder()
										.name(pluginNode.getId())
										.version(pluginNode.getVersion())
										.build()))
						.repositories(Optional.ofNullable(buildGradleModel.getRepositories())
								.map(RepositoriesNode::getRepositories)
								.map(repositories -> repositories
										.map(GradleManifestContributor::repositoryNodeToMavenRepository))
								.orElse(null))
						.build());
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	public void invokeGradleDependencies(Path path) {
		ExecUtils.Outcome outcome = ExecUtils.exec(
				"%s dependencies".formatted(path.resolve(getGradleExecutableName())),
				List.of("JAVA_HOME=%s".formatted("C:/Users/user/.jdks/azul-13.0.12")).asJava(),
				path,
				Duration.ofMinutes(1));
		//
		System.out.println();
	}

	public void invokeGradleHelp(Path path) {
		ExecUtils.Outcome outcome = ExecUtils.exec(
				"%s help".formatted(path.resolve(getGradleExecutableName())),
				List.of("JAVA_HOME=%s".formatted(getJavaHome())).asJava(),
				path,
				Duration.ofMinutes(1));
		//
		System.out.println();
	}

	public void invokeGradleInit(Path path) {
		ExecUtils.Outcome outcome = ExecUtils.exec(
				"%s init".formatted(path.resolve(getGradleExecutableName())),
				List.of("JAVA_HOME=%s".formatted("C:/Users/user/.jdks/azul-13.0.12")).asJava(),
				path,
				Duration.ofSeconds(15));
		//
		System.out.println();
	}

	public static boolean containsGradleExecutable(Path path) {
		return path.resolve(getGradleExecutableName()).toFile().exists();
	}

	/**
	 * <a href="https://stackoverflow.com/questions/49876189/how-to-run-a-gradle-task-from-a-java-code">https://stackoverflow.com/questions/49876189/how-to-run-a-gradle-task-from-a-java-code</a>
	 *
	 * @return non-null
	 */
	public static String getGradleExecutableName() {
		//
		//
		String result;
		{
			if (SystemUtils.IS_OS_WINDOWS) {
				result = "gradlew.bat";
			} else {
				result = "gradlew";
			}
		}
		return result;
	}

	public static String getJavaHome() {
		ExecUtils.Outcome outcome = ExecUtils.exec(
				"java -XshowSettings:properties -version",
				null,
				null,
				Duration.ofSeconds(15));
		//
		Matcher matcher = Pattern.compile(".*java\\.home = (.*)\n").matcher(outcome.getError());
		matcher.find();
		//
		return matcher.group(1);
	}

	public static MavenRepository repositoryNodeToMavenRepository(RepositoryNode repositoryNode) {
		return MavenRepository.builder()
				.name(repositoryNode.getName() != null
						? switch (repositoryNode.getName()) {
					case "google" -> "Google Maven";
					case "gradlePluginPortal" -> "Gradle Plugins";
					case "jcenter" -> "JCenter";
					case "mavenCentral" -> "Maven Central";
					case "mavenLocal" -> "Maven Local";
					default -> repositoryNode.getName();
				}
						: null)
				.url(repositoryNode.getUrl())
				.build();
	}
}
