package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.manifester.artifact.ArtifactDefinition;
import tk.labyrinth.satool.beholder.manifester.artifact.ArtifactOrigin;
import tk.labyrinth.satool.beholder.manifester.artifact.GitBranchArtifactOrigin;
import tk.labyrinth.satool.beholder.manifester.artifact.LocalDirectoryArtifactOrigin;
import tk.labyrinth.satool.beholder.manifester.artifact.NameArtifactDefinitionReference;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.GitBranch;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.repository.Repository;
import tk.labyrinth.satool.jgit.RepositoryHandler;

import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class BuildArtifactManifestJobRunner implements JobRunner<BuildArtifactManifestJobRunner.Parameters> {

	private final ArtifactManifestBuilder artifactManifestBuilder;

	private final ExecutionContextHandler executionContextHandler;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactDefinition> artifactDefinitionSearcher;

	@SmartAutowired
	private TypedObjectManipulator<ArtifactManifest> artifactManifestManipulator;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifest> artifactManifestSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@SmartAutowired
	private TypedObjectSearcher<GitBranch> gitBranchSearcher;

	@SmartAutowired
	private TypedObjectSearcher<PersistentModel> persistentModelSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Repository> repositorySearcher;

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder()
				.spaceReference(executionContextHandler.getExecutionContext()
						.findAttributeValueInferred(BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME))
				.build();
	}

	public Path resolveDirectory(ArtifactOrigin origin) {
		Path result;
		{
			if (origin instanceof GitBranchArtifactOrigin gitBranchArtifactOrigin) {
				GitBranch gitBranch = gitBranchSearcher.getSingle(gitBranchArtifactOrigin.getGitBranchReference());
				Repository repository = repositorySearcher.getSingle(gitBranch.getRepositoryReference());
				Credentials credentials = Optional.ofNullable(repository.getCredentialsReference())
						.map(credentialsSearcher::getSingle)
						.orElse(null);
				//
				logger.info("Loading revision, url = {}, branch = {}", repository.getUrl(), gitBranch.getName());
				//
				Path revisionDirectory = RepositoryHandler.loadRevision(
						repository.getUrl(),
						gitBranch.getName(),
						credentials != null
								? new UsernamePasswordCredentialsProvider(
								credentials.getUsername(),
								credentials.getPassword())
								: null,
						"build-artifact-manifest-");
				//
				result = computeArtifactManifestDirectory(revisionDirectory, gitBranchArtifactOrigin.getPath());
				//
				logger.info("Done");
			} else if (origin instanceof LocalDirectoryArtifactOrigin localDirectoryArtifactOrigin) {
				result = Objects.requireNonNull(
						localDirectoryArtifactOrigin.getPath(),
						"localDirectoryArtifactOrigin.path");
			} else {
				throw new NotImplementedException(origin.toString());
			}
		}
		return result;
	}

	@Override
	public void run(Context<Parameters> context) {
		Parameters parameters = context.getParameters();
		//
		NameArtifactDefinitionReference artifactDefinitionReference = parameters
				.getArtifactDefinitionReference();
		//
		ArtifactDefinition artifactDefinition = artifactDefinitionSearcher.getSingle(artifactDefinitionReference);
		//
		Path directory;
		{
			ArtifactOrigin origin = artifactDefinition.getOrigin();
			//
			if (origin != null) {
				directory = resolveDirectory(origin);
			} else {
				throw new NotImplementedException(parameters.toString());
			}
		}
		//
		ArtifactManifest artifactManifest = artifactManifestBuilder.buildJavaModuleManifestFromDirectory(directory)
				.toBuilder()
				.definitionReference(artifactDefinitionReference)
				.build();
		//
		ArtifactManifest foundArtifactManifest = artifactManifestSearcher.findSingle(
				DefinitionArtifactManifestReference.of(artifactDefinitionReference));
		//
		if (foundArtifactManifest != null) {
			artifactManifestManipulator.update(artifactManifest.toBuilder()
					.uid(foundArtifactManifest.getUid())
					.build());
		} else {
			artifactManifestManipulator.createWithGeneratedUid(artifactManifest);
		}
	}

	public static Path computeArtifactManifestDirectory(Path revisionDirectory, @Nullable String relativePath) {
		return relativePath != null ? revisionDirectory.resolve(relativePath) : revisionDirectory;
	}

	@Builder(toBuilder = true)
	@Value
	public static class Parameters implements HasSpaceReference {

		NameArtifactDefinitionReference artifactDefinitionReference;

		KeySpaceReference spaceReference;
	}
}
