package tk.labyrinth.satool.beholder.manifester.domain.validations;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import jakarta.validation.constraints.AssertFalse;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Negative;
import jakarta.validation.constraints.NegativeOrZero;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import org.checkerframework.checker.nullness.qual.Nullable;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributorUtils;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedAnnotation;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedAnnotationValue;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedField;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedType;
import tk.labyrinth.satool.beholder.manifester.util.JakartaJavaxUtils;
import tk.labyrinth.satool.spoon.CtAnnotationUtils;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.CtTypeUtils;

import java.lang.annotation.Annotation;
import java.nio.file.Path;
import java.util.Objects;

@LazyComponent
public class ValidationsManifestContributor implements ManifestContributor {

	public static final List<Class<? extends Annotation>> PREDEFINED_ANNOTATION_CLASSES = List.of(
			AssertFalse.class,
			AssertTrue.class,
			DecimalMax.class,
			DecimalMin.class,
			Digits.class,
			Email.class,
			Future.class,
			FutureOrPresent.class,
			Max.class,
			Min.class,
			Negative.class,
			NegativeOrZero.class,
			NotBlank.class,
			NotEmpty.class,
			NotNull.class,
			Null.class,
			Past.class,
			PastOrPresent.class,
			Pattern.class,
			Positive.class,
			PositiveOrZero.class,
			Size.class);

	@Override
	public List<?> contribute(Path rootDirectory, CtModel javaModel) {
		List<ExposedClass> result;
		{
			List<CtType<?>> allTypes = List.ofAll(javaModel.getAllTypes());
			//
			List<ExposedClass> exposedClasses = allTypes
					.map(this::processType)
					.filter(Objects::nonNull);
			//
			result = exposedClasses;
		}
		return result;
	}

	@Override
	public List<String> getExtraClasspathEntries() {
		return List
				.of(
						jakarta.validation.Constraint.class,
						javax.validation.Constraint.class)
				.map(ManifestContributorUtils::getLocationOfClassSource);
	}

	public ExposedAnnotation processAnnotation(CtAnnotation<?> annotation) {
		return ExposedAnnotation.builder()
				.signature(CtTypeReferenceUtils.getTypeSignature(annotation.getType()))
				.values(HashMap.ofAll(annotation.getValues())
						.map(tuple -> ExposedAnnotationValue.builder()
								.name(tuple._1())
								.value(CtAnnotationUtils.getAttributeValueAsString(tuple._2()))
								.build())
						.toList())
				.build();
	}

	@Nullable
	public ExposedField processField(CtField<?> field) {
		ExposedField result;
		{
			ExposedType exposedType = processTypeReference(field.getType(), true);
			//
			List<? extends CtAnnotation<?>> annotations = PREDEFINED_ANNOTATION_CLASSES
					.map(predefinedAnnotationClass -> JakartaJavaxUtils.findAnnotation(
							field,
							predefinedAnnotationClass))
					.filter(Objects::nonNull);
			//
			if (exposedType != null || !annotations.isEmpty()) {
				result = ExposedField.builder()
						.annotations(annotations.map(this::processAnnotation))
						.name(field.getSimpleName())
						.type(exposedType)
						.build();
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public ExposedClass processType(CtType<?> type) {
		ExposedClass result;
		{
			Object exposedAnnotations = null;
			//
			List<ExposedField> exposedFields = List.ofAll(type.getDeclaredFields())
					.map(CtFieldReference::getDeclaration)
					.map(this::processField)
					.filter(Objects::nonNull);
			//
			if (exposedAnnotations != null || !exposedFields.isEmpty()) {
				result = ExposedClass.builder()
						.fields(exposedFields)
						.signature(CtTypeUtils.getClassSignature(type))
						.build();
			} else {
				result = null;
			}
		}
		return result;
	}

	/**
	 * @param typeReference     non-null
	 * @param ignoreAnnotations true for field types and method return types, since they are present on parents
	 *
	 * @return nullable
	 */
	@Nullable
	public ExposedType processTypeReference(CtTypeReference<?> typeReference, boolean ignoreAnnotations) {
		ExposedType result;
		{
			List<ExposedAnnotation> annotations = !ignoreAnnotations
					? PREDEFINED_ANNOTATION_CLASSES
					.map(predefinedAnnotationClass -> JakartaJavaxUtils.findAnnotation(
							typeReference,
							predefinedAnnotationClass))
					.filter(Objects::nonNull)
					.map(this::processAnnotation)
					: List.empty();
			//
			List<ExposedType> parameters = List.ofAll(typeReference.getActualTypeArguments())
					.map(actualTypeArgument -> processTypeReference(actualTypeArgument, false));
			//
			if (!annotations.isEmpty() || parameters.exists(Objects::nonNull)) {
				result = ExposedType.builder()
						.annotations(annotations)
						.core(CtTypeReferenceUtils.getTypeSignature(typeReference))
						.parameters(!parameters.isEmpty()
								? parameters.zip(typeReference.getActualTypeArguments())
								.map(tuple -> tuple._1() != null
										? tuple._1()
										: ExposedType.builder()
										.core(CtTypeReferenceUtils.getTypeSignature(tuple._2()))
										.build())
								: null)
						.build();
			} else {
				result = null;
			}
		}
		return result;
	}
}
