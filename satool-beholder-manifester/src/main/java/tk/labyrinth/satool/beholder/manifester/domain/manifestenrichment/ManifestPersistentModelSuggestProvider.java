package tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment;

import io.vavr.collection.List;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.component.valuebox.suggest.SuggestContext;
import tk.labyrinth.pandora.ui.component.valuebox.suggest.SuggestProvider;
import tk.labyrinth.satool.beholder.manifester.artifact.NameArtifactDefinitionReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.DefinitionArtifactManifestReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.ArtifactManifestIndex;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModelUtils;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RefinedPersistentModel;

@LazyComponent
public class ManifestPersistentModelSuggestProvider implements SuggestProvider<ClassSignature> {

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifestIndex> artifactManifestIndexSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifest> artifactManifestSearcher;

	@Override
	public List<ClassSignature> provideSuggests(SuggestContext context) {
		List<ClassSignature> result;
		{
			ManifestEnrichment manifestEnrichment = context.getAncestorValue(3, ManifestEnrichment.class);
			//
			NameArtifactDefinitionReference artifactDefinitionReference = manifestEnrichment.getArtifactDefinitionReference();
			//
			if (artifactDefinitionReference != null) {
				ArtifactManifest artifactManifest = artifactManifestSearcher.getSingle(
						DefinitionArtifactManifestReference.of(artifactDefinitionReference));
				//
				ArtifactManifestIndex artifactManifestIndex = artifactManifestIndexSearcher
						.search(Predicates.equalTo(
								ArtifactManifestIndex.TARGET_REFERENCE_ATTRIBUTE_NAME,
								UidReference.of(ArtifactManifest.MODEL_CODE, artifactManifest.getUid())))
						.single();
				//
				List<ClassSignature> persistentModelSignatures = artifactManifestIndex.getPersistentModels()
						.map(RefinedPersistentModel::getJavaSignature);
				//
				List<ClassSignature> displayModelSignatures = DisplayableModelUtils
						.selectRecursivelyWithClasses(artifactManifestIndex.getModels(), persistentModelSignatures)
						.map(DisplayableModel::getJavaSignature);
				//
				result = displayModelSignatures.sortBy(ClassSignature::getLongName);
			} else {
				result = List.empty();
			}
		}
		return result;
	}
}
