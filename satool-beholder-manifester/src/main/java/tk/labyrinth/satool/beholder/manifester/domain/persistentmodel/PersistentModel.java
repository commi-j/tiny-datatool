package tk.labyrinth.satool.beholder.manifester.domain.persistentmodel;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model
@ModelTag("persistence")
@RenderAttribute("javaClassSignature")
@Value
@With
public class PersistentModel implements HasUid {

	List<PersistentModelAttribute> attributes;

	String databaseName;

	ClassSignature javaClassSignature;

	List<Tag> tags;

	UUID uid;

	public List<ClassSignature> collectClassSignatures() {
		return getAttributesOrEmpty()
				.map(PersistentModelAttribute::getJavaFieldType)
				.flatMap(TypeDescription::breakDown)
				.map(TypeDescription::getSignature)
				.filter(TypeSignature::isDeclaredOrVariable)
				.map(TypeSignature::getClassSignature)
				.prepend(javaClassSignature)
				.distinct();
	}

	public List<PersistentModelAttribute> getAttributesOrEmpty() {
		return attributes != null ? attributes : List.empty();
	}
}
