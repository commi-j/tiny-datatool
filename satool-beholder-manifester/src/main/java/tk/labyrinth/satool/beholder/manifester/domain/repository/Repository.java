package tk.labyrinth.satool.beholder.manifester.domain.repository;

import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.SearchDataAttributes;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.time.Instant;
import java.util.UUID;

@Builder(toBuilder = true)
@Model(code = Repository.MODEL_CODE)
@ModelTag("repos")
@RenderAttribute(Repository.NAME_ATTRIBUTE_NAME)
@SearchDataAttributes(Repository.NAME_ATTRIBUTE_NAME)
@Value
public class Repository implements HasUid {

	public static final String MODEL_CODE = "repository";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	public static final String URL_ATTRIBUTE_NAME = "url";

	@Nullable
	KeyCredentialsReference credentialsReference;

	String name;

	UUID uid;

	Instant updatedAt;

	String url;
}
