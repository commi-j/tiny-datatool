package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectcontributor.ObjectContributor;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.ConfluencePageTemplate;

import java.util.UUID;

@LazyComponent
public class ArtifactManifestConfluencePageTemplateContributor implements ObjectContributor<ConfluencePageTemplate> {

	@Override
	public UUID computeUid(ConfluencePageTemplate object) {
		return RootObjectUtils.computeUidFromValueHashCode(object.getKey());
	}

	@Override
	public List<ConfluencePageTemplate> contributeObjects() {
		// TODO
		return List.of();
	}
}
