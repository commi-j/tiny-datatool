package tk.labyrinth.satool.beholder.manifester.domain.component;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.reference.Reference;

@Builder(toBuilder = true)
@Model(code = ComponentIndex.MODEL_CODE)
@ModelTag("documentary")
@ModelTag("repos")
@Value
public class ComponentIndex {

	public static final String MODEL_CODE = "componentindex";

	String code;

	// TODO: Support wildcards in ReflectionObjectModelFactory.getTypeDescription
	Reference contributorReference;

	String moduleCode;

	String name;
}
