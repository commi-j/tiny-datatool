package tk.labyrinth.satool.beholder.manifester.domain.exposed;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;

import java.util.Objects;
import java.util.function.Function;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderAttribute("signature")
@Value
@With
public class ExposedClass {

	@Nullable
	List<String> enumValues;

	List<ExposedField> fields;

	ClassSignature signature;

	List<Tag> tags;

	public List<ClassSignature> collectClassSignatures() {
		return getFieldsOrEmpty()
				.map(field -> field.getType().toTypeDescription())
				.flatMap(TypeDescription::breakDown)
				.map(TypeDescription::getSignature)
				.filter(TypeSignature::isDeclaredOrVariable)
				.map(TypeSignature::getClassSignature)
				.prepend(signature)
				.distinct();
	}

	public List<ExposedField> getFieldsOrEmpty() {
		return fields != null ? fields : List.empty();
	}

	public List<Tag> getTagsOrEmpty() {
		return tags != null ? tags : List.empty();
	}

	public boolean isEnum() {
		return getTagsOrEmpty().contains(Tag.of("enum"));
	}

	public static ExposedClass merge(List<ExposedClass> classes) {
		return ExposedClass.builder()
				.enumValues(classes
						.map(ExposedClass::getEnumValues)
						.filter(Objects::nonNull)
						.distinct()
						.transform(valueses -> !valueses.isEmpty() ? valueses.single() : null))
				.fields(ExposedField.mergeMultiple(classes.map(ExposedClass::getFields)))
				.signature(classes.map(ExposedClass::getSignature).distinct().single())
				.tags(classes.map(ExposedClass::getTags).filter(Objects::nonNull).flatMap(Function.identity())) // TODO: if all are null, should be null?
				.build();
	}
}
