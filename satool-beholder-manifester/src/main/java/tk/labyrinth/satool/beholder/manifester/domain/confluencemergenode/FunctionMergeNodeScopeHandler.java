package tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.value.wrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModelUtils;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionParametersEntry;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionResultEntry;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;

@LazyComponent
@RequiredArgsConstructor
public class FunctionMergeNodeScopeHandler implements ConfluenceMergeNodeScopeHandler {

	public static final String SCOPE_NAME = "FUNCTION";

	private final ConverterRegistry converterRegistry;

	@Override
	public List<String> getAttributeNames() {
		return List.of("function");
	}

	@Override
	public String getScopeName() {
		return SCOPE_NAME;
	}

	@Override
	public List<GenericObject> getVariablesObjects(String parentScopeName, GenericObject parentObject) {
		GenericObject manifestObject = parentObject.getAttributeValueAsObject("manifest");
		//
		List<DisplayableModel> models = converterRegistry.convertInferred(
				manifestObject.getAttributeValue("models"),
				TypeUtils.parameterize(List.class, DisplayableModel.class));
		//
		return parentObject.getAttributeValueAsObject("manifest")
				.getAttributeValueAsList("functions")
				.map(ValueWrapper::asObject)
				.map(ObjectValueWrapper::unwrap)
				.map(functionObject -> {
					ManifestFunction function = converterRegistry.convert(functionObject, ManifestFunction.class);
					//
					return parentObject.withAddedAttributes(
							GenericObjectAttribute.ofObject("function", functionObject),
							GenericObjectAttribute.ofObjectList(
									"relevantParametersModels",
									DisplayableModelUtils.selectRecursivelyWithTypes(
													models,
													function.getParameters().map(FunctionParametersEntry::getDatatype))
											.map(displayableModel -> converterRegistry.convert(displayableModel, GenericObject.class))),
							GenericObjectAttribute.ofObjectList(
									"relevantResultModels",
									DisplayableModelUtils.selectRecursivelyWithTypes(
													models,
													function.getResult().map(FunctionResultEntry::getDatatype))
											.map(displayableModel -> converterRegistry.convert(displayableModel, GenericObject.class))));
				});
	}
}
