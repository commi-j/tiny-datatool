package tk.labyrinth.satool.beholder.manifester.domain.operation;

import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.extra.objectproxy.ObjectProxyHandler;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.page.HasUuidUrlParameter;
import tk.labyrinth.satool.beholder.root.BeholderRootLayout;
import tk.labyrinth.satool.plantuml.view.PlantumlDiagramView;

import java.util.UUID;

@RequiredArgsConstructor
@Route(value = "operation", layout = BeholderRootLayout.class)
public class OperationPage extends CssHorizontalLayout implements HasUuidUrlParameter {

	private final OperationActivityDiagramDescriptionBuilder activityDiagramDescriptionBuilder;

	private final OperationComponentDiagramDescriptionBuilder componentDiagramDescriptionBuilder;

	private final RadioButtonGroup<Boolean> diagramKindButtonGroup = new RadioButtonGroup<>();

	private final PlantumlDiagramView diagramView;

	private final GenericObjectSearcher genericObjectSearcher;

	private final ObjectProxyHandler objectProxyHandler;

	private Operation operation;

	@SmartAutowired
	private ObjectForm<Operation> operationForm;

	@PostConstruct
	private void postConstruct() {
		{
//			operationForm.initialize(modelSearcher.resolve(Operation.MODEL_REFERENCE));
			add(operationForm.asVaadinComponent());
		}
		{
			CssVerticalLayout diagramLayout = new CssVerticalLayout();
			{
				diagramKindButtonGroup.setItems(true, false);
				diagramKindButtonGroup.setRenderer(new TextRenderer<>(item -> item ? "Activity" : "Component"));
				diagramKindButtonGroup.addValueChangeListener(event -> {
					if (event.getValue()) {
						diagramView.setParameters(builder -> builder
								.value(activityDiagramDescriptionBuilder.build(operation))
								.build());
					} else {
						diagramView.setParameters(builder -> builder
								.value(componentDiagramDescriptionBuilder.build(operation))
								.build());
					}
				});
				diagramLayout.add(diagramKindButtonGroup);
			}
			{
				CssFlexItem.setFlexGrow(diagramView, 1);
				diagramLayout.add(diagramView);
			}
			CssFlexItem.setFlexGrow(diagramLayout, 1);
			add(diagramLayout);
		}
	}

	@Override
	public void acceptValidParameter(BeforeEvent beforeEvent, UUID uuid) {
		GenericObject object = genericObjectSearcher
				.search(ParameterizedQuery.<CodeObjectModelReference>builder()
						.parameter(Operation.MODEL_REFERENCE)
						.predicate(Predicates.and(
								Predicates.equalTo(
										RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME,
										Operation.MODEL_REFERENCE),
								Predicates.equalTo(RootObjectUtils.UID_ATTRIBUTE_NAME, uuid)))
						.build())
				.getOrNull();
		if (object != null) {
			operation = objectProxyHandler.wrap(object, Operation.class);
//			operationForm.setValue(operation); // FIXME
			diagramKindButtonGroup.setValue(true);
		} else {
			throw new NotImplementedException();
		}
	}
}
