package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RestEndpoint {

	List<RestEndpointCoordinates> coordinateses;

	@Deprecated // FIXME: Used in pattern, but must be function of coordinateses.
	String firstCoordinates;

	MethodFullSignature javaSignature;

	@Deprecated // FIXME: Used in pattern, but must be function of signature.
	String name;

	List<RestParametersEntry> parameters;

	List<RestResultEntry> result;
}
