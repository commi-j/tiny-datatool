package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import io.vavr.collection.List;
import spoon.reflect.CtModel;

import java.nio.file.Path;

public interface ManifestContributor {

	List<?> contribute(Path rootDirectory, CtModel javaModel);

	default List<String> getExtraClasspathEntries() {
		return List.empty();
	}
}
