package tk.labyrinth.satool.beholder.manifester.domain.properties;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class Property {

	/**
	 * 0..n
	 */
	@NonNull
	List<PropertyDeclarationEntry> declarationEntries;

	@NonNull
	String name;

	/**
	 * 0..n
	 */
	@NonNull
	List<PropertyUsageEntry> usageEntries;

	public List<String> computeTags() {
		return declarationEntries.flatMap(PropertyDeclarationEntry::getTags)
				.appendAll(usageEntries.flatMap(PropertyUsageEntry::getTags))
				.distinct()
				.sorted();
	}
}
