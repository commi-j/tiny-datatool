package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;

import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PropertyDeclarationsViewRenderer {

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			layout.add("Declarations");
			//
			properties.getPropertyDeclarationEntries().forEach(propertyDeclarationEntry -> {
				CssVerticalLayout entryLayout = new CssVerticalLayout();
				{
					entryLayout.add(new Label("Location: %s".formatted(propertyDeclarationEntry.getLocation())));
					entryLayout.add(new Label("Value: %s".formatted(propertyDeclarationEntry.getValue())));
					entryLayout.add(new Label("Tags: %s".formatted(propertyDeclarationEntry.getTags())));
				}
				layout.add(entryLayout);
			});
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		List<PropertyDeclarationEntry> propertyDeclarationEntries;

		public static class Builder {
			// Lomboked
		}
	}
}