package tk.labyrinth.satool.beholder.manifester.tool;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.io.IOException;

@LazyComponent
public class MethodSimpleSignatureObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule();
			{
				registerWith(module);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer());
		return module;
	}

	public static class Deserializer extends StdNodeBasedDeserializer<MethodSimpleSignature> {

		public Deserializer() {
			super(MethodSimpleSignature.class);
		}

		@Override
		public MethodSimpleSignature convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			return MethodSimpleSignature.of(root.textValue());
		}
	}

	public static class Serializer extends StdSerializer<MethodSimpleSignature> {

		public Serializer() {
			super(MethodSimpleSignature.class);
		}

		@Override
		public void serialize(
				MethodSimpleSignature value,
				JsonGenerator gen,
				SerializerProvider provider) throws IOException {
			gen.writeString(value.toString());
		}
	}
}
