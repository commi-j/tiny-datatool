package tk.labyrinth.satool.beholder.manifester.domain.properties;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.boot.env.PropertiesPropertySourceLoader;
import org.springframework.boot.env.PropertySourceLoader;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.util.Pair;
import spoon.reflect.CtModel;
import tk.labyrinth.misc4j2.java.io.FileUtils;
import tk.labyrinth.misc4j2.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.java.util.regex.RegexUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.SpelUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#appendix.application-properties
 */
@LazyComponent
@RequiredArgsConstructor
public class SpringPropertiesManifestContributor implements ManifestContributor {

	private final Pattern PROPERTIES_FILE_PATTERN = Pattern.compile("application-?(?<profile>.+)?\\.(properties|yaml|yml)");

	private final List<PropertySourceLoader> propertySourceLoaders = List.of(
			new PropertiesPropertySourceLoader(),
			new YamlPropertySourceLoader());

	private final ResourceLoader resourceLoader;

	@Override
	public List<?> contribute(Path rootDirectory, CtModel javaModel) {
		List<?> result;
		{
			File srcMainResourcesFile = rootDirectory.resolve("src/main/resources").toFile();
			if (srcMainResourcesFile.isDirectory()) {
				File[] files = srcMainResourcesFile.listFiles();
				if (files != null) {
					List<File> propertiesFiles = List.of(files).filter(file ->
							RegexUtils.matches(file.getName(), PROPERTIES_FILE_PATTERN));
					result = propertiesFiles.flatMap(propertiesFile -> resolveFile(rootDirectory, propertiesFile));
				} else {
					result = List.of();
				}
			} else {
				result = List.of();
			}
		}
		return result;
	}

	public List<?> resolveFile(Path rootDirectory, File propertiesFile) {
		List<?> result;
		{
			String profile = RegexUtils.findGroupValue(propertiesFile.getName(), PROPERTIES_FILE_PATTERN, "profile");
			//
			Resource resource = resourceLoader.getResource("file:" + propertiesFile.getAbsolutePath());
			//
			PropertySourceLoader propertySourceLoader = propertySourceLoaders
					.find(propertySourceLoaderElement -> ObjectUtils.in(
							FileUtils.getExtension(propertiesFile),
							propertySourceLoaderElement.getFileExtensions()))
					.get();
			//
			EnumerablePropertySource<?> propertySource = (EnumerablePropertySource<?>) IoUtils
					.unchecked(() -> propertySourceLoader.load(null, resource))
					.get(0);
			//
			String location = rootDirectory.relativize(propertiesFile.toPath()).toString().replace('\\', '/');
			//
			List<PropertyDeclarationEntry> propertyDeclarationEntries = List.of(propertySource.getPropertyNames())
					.map(propertyName -> PropertyDeclarationEntry.builder()
							.location(location)
							.name(propertyName)
							.tags(List
									.of(
											"file",
											profile != null ? "profile:%s".formatted(profile) : null)
									.filter(Objects::nonNull))
							.value(propertySource.getProperty(propertyName).toString())
							.build());
			//
			List<PropertyUsageEntry> propertyUsageEntries = propertyDeclarationEntries
					.map(propertyDeclarationEntry -> Pair.of(
							propertyDeclarationEntry,
							SpelUtils.findPlaceholderValues(propertyDeclarationEntry.getValue())))
					.filter(pair -> !pair.getSecond().isEmpty())
					.flatMap(pair -> pair.getSecond().map(placeholderMatchResult -> PropertyUsageEntry.builder()
							.defaultValue(placeholderMatchResult.group(3))
							.javaDefaultValue(null)
							.javaType(null)
							.location("%s:%s".formatted(location, pair.getFirst().getName()))
							.name(placeholderMatchResult.group(1))
							.tags(List
									.of(
											"file",
											"property",
											profile != null ? "profile:%s".formatted(profile) : null)
									.filter(Objects::nonNull))
							.build()));
			//
			result = List.<Object>ofAll(propertyDeclarationEntries).appendAll(propertyUsageEntries);
		}
		return result;
	}

	public List<Object> resolvePropertiesFile(File propertiesFile, @Nullable String profile) {
		throw new NotImplementedException();
	}
}
