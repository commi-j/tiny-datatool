package tk.labyrinth.satool.beholder.manifester.domain.documentary;

import io.vavr.collection.List;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import tk.labyrinth.satool.beholder.domain.space.initializer.SpaceInitializerBase;
import tk.labyrinth.satool.beholder.domain.uiconfiguration.BeholderMenuConfiguration;
import tk.labyrinth.satool.beholder.domain.uiconfiguration.BeholderUiConfiguration;

import javax.annotation.CheckForNull;

@Component
@ConditionalOnProperty("beholder.enableDocumentarySpace")
//@Order(DefaultStoresInitializer.PRIORITY + 1)
public class DocumentarySpaceInitializer extends SpaceInitializerBase {

	@Override
	protected String getSpaceKey() {
		return "documentary";
	}

	@CheckForNull
	@Override
	protected BeholderUiConfiguration getUiConfiguration() {
		return BeholderUiConfiguration.builder()
				.menuConfigurations(List.of(
						BeholderMenuConfiguration.builder()
								.filterTag("documentary")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("jobs")
								.build(),
						BeholderMenuConfiguration.builder()
								.filterTag("pandora")
								.build()))
				.priority(1)
				.scope("space")
				.build();
	}
}
