package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.core.task.TaskExecutor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFetcher;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Route("confluence-merge-structure")
@Slf4j
public class ConfluenceMergeStructurePage extends FunctionalPage<ConfluenceMergeStructurePage.Parameters> {

	private final ConfluenceFetcher confluenceFetcher;

	private final ConfluenceMergeStructureViewRenderer confluenceMergeStructureViewRenderer;

	private final TaskExecutor taskExecutor;

	@SmartAutowired
	private TypedObjectManipulator<ConfluenceMergeStructure> confluenceMergeStructureManipulator;

	@SmartAutowired
	private TypedObjectSearcher<ConfluenceMergeStructure> confluenceMergeStructureSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	@PostConstruct
	private void postConstruct() {
		getContent().setHeightFull();
	}

	private Component renderObject(CodeConfluenceMergeStructureReference reference) {
		Component result;
		{
			ConfluenceMergeStructure value = confluenceMergeStructureSearcher.findSingle(reference);
			//
			if (value != null) {
				result = FunctionalComponent.create(
						Pair.of(value, value),
						(parameters, sink) -> confluenceMergeStructureViewRenderer.render(builder -> builder
								.currentValue(parameters.getRight())
								.initialValue(parameters.getLeft())
								.onSave(() -> {
									confluenceMergeStructureManipulator.update(parameters.getRight());
									//
									sink.accept(Pair.of(parameters.getRight(), parameters.getRight()));
								})
								.onValueChange(nextValue -> sink.accept(Pair.of(parameters.getLeft(), nextValue)))
								.build()));
			} else {
				result = new Span("Object not found: %s".formatted(reference));
			}
		}
		return result;
	}

	@Override
	protected Parameters getInitialProperties() {
		return Parameters.builder()
				.code(null)
				.build();
	}

	@Override
	protected Component render(Parameters parameters, Consumer<Parameters> sink) {
		Component result;
		{
			String code = parameters.code();
			//
			if (code != null) {
				CodeConfluenceMergeStructureReference reference = CodeConfluenceMergeStructureReference.of(code);
				//
				result = renderObject(reference);
			} else {
				result = new Span("No code specified.");
			}
		}
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@UiStateAttribute
		String code;
	}
}
