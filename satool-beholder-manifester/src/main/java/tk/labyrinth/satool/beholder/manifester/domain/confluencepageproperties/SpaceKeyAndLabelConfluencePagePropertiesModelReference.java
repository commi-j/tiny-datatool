package tk.labyrinth.satool.beholder.manifester.domain.confluencepageproperties;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class SpaceKeyAndLabelConfluencePagePropertiesModelReference implements
		Reference<ConfluencePagePropertiesModel> {

	@NonNull
	String label;

	@NonNull
	String spaceKey;

	@Override
	public String toString() {
		return "%s(%s=%s,%s=%s)".formatted(
				ConfluencePagePropertiesModel.MODEL_CODE,
				ConfluencePagePropertiesModel.SPACE_KEY_ATTRIBUTE_NAME,
				spaceKey,
				ConfluencePagePropertiesModel.LABEL_ATTRIBUTE_NAME,
				label);
	}

	public static SpaceKeyAndLabelConfluencePagePropertiesModelReference from(ConfluencePagePropertiesModel object) {
		return SpaceKeyAndLabelConfluencePagePropertiesModelReference.of(object.getLabel(), object.getSpaceKey());
	}

	@JsonCreator
	public static SpaceKeyAndLabelConfluencePagePropertiesModelReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), ConfluencePagePropertiesModel.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(
				reference.getAttributeValue(ConfluencePagePropertiesModel.SPACE_KEY_ATTRIBUTE_NAME),
				reference.getAttributeValue(ConfluencePagePropertiesModel.LABEL_ATTRIBUTE_NAME));
	}

	public static SpaceKeyAndLabelConfluencePagePropertiesModelReference of(String spaceKey, String label) {
		return new SpaceKeyAndLabelConfluencePagePropertiesModelReference(label, spaceKey);
	}
}
