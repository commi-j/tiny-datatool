package tk.labyrinth.satool.beholder.manifester.domain.operation;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.microservice.Microservice;

import java.util.function.Function;

@Component
@Lazy
@RequiredArgsConstructor
public class OperationActivityDiagramDescriptionBuilder {

	private final TypedObjectSearcher<Microservice> microserviceSearcher;

	private final TypedObjectSearcher<Operation> operationSearcher;

	public String build(Operation operation) {
		Microservice service = microserviceSearcher.findSingle(operation.getService());
		Map<Operation, Microservice> invokedOperations = operation.getInvokes()
				.map(operationSearcher::findSingle)
				.toLinkedMap(
						Function.identity(),
						invokedOperation -> microserviceSearcher.findSingle(invokedOperation.getService()));
		String serviceCode = service != null ? service.getCode() : "missing";
		//
		return """
				@startuml
								
				%s
								
				| |
				start
								
				|%s|
				:handle parameters;
								
				%s
								
				|%s|
				:compose result;
								
				| |
				stop
								
				@enduml
				""".formatted(
				renderActors(service, invokedOperations.values()).mkString("\n"),
				serviceCode,
				renderInvokedOperations(serviceCode, invokedOperations).mkString("\n"),
				serviceCode);
	}

	public List<String> renderActors(Microservice service, Seq<Microservice> invokedOperationServices) {
		return Stream
				.concat(
						Stream.of(service),
						invokedOperationServices)
				.distinct()
				.map(microservice -> microservice != null
						? "|%s|%s".formatted(microservice.getCode(), microservice.getCode())
						: "|missing|")
				.prepend("| |")
				.toList();
	}

	public List<String> renderInvokedOperations(String serviceCode, Map<Operation, Microservice> invokedOperations) {
		return invokedOperations.toStream()
				.map(pair -> """
						|%s|
						:%s;
												
						|%s|
						(.)
						""".formatted(
						pair._2() != null ? pair._2().getCode() : "missing",
						pair._1().getCode(),
						serviceCode))
				.toList();
	}
}
