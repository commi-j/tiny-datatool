package tk.labyrinth.satool.beholder.manifester.artifact;

import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicBase;

@PolymorphicBase(qualifierAttributeName = "kind")
public interface ArtifactOrigin {
	// empty
}
