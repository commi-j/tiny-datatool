package tk.labyrinth.satool.beholder.manifester.domain.function.http;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.treegrid.TreeGrid;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionCoordinates;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionParametersEntry;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionResultEntry;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;

import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class HttpFunctionViewRenderer {

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(ManifestFunction function) {
		return render(builder -> builder
				.function(function)
				.build());
	}

	public Component render(Parameters parameters) {
		List<HttpFunctionCoordinates> httpCoordinateses = parameters.function().getCoordinateses()
				.filter(FunctionCoordinates::isHttp)
				.map(HttpFunctionCoordinates.class::cast);
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				if (httpCoordinateses.size() > 1) {
					layout.add(new Span("Endpoints:"));
					httpCoordinateses.forEach(httpCoordinates -> layout.add(
							new Span("%s %s".formatted(httpCoordinates.getMethod(), httpCoordinates.getPath()))));
				} else {
					layout.add(new Span("Endpoint: %s %s".formatted(
							httpCoordinateses.get(0).getMethod(), httpCoordinateses.get(0).getPath())));
				}
			}
			{
				CssHorizontalLayout signatureLayout = new CssHorizontalLayout();
				StyleUtils.setCssProperty(signatureLayout, WhiteSpace.PRE_WRAP);
				//
				signatureLayout.add("Java Signature: ");
				signatureLayout.add(toVaadinComponentRendererRegistry.render(
						ToVaadinComponentRendererRegistry.Context.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(MethodFullSignature.class))
								.hints(List.empty())
								.build(),
						parameters.function().getJavaSignature().toString()));
				//
				layout.add(signatureLayout);
			}
			{
				CssVerticalLayout parametersLayout = new CssVerticalLayout();
				{
					parametersLayout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					{
						parametersLayout.add(new Span("Parameters"));
					}
					{
						TreeGrid<FunctionParametersEntry> parametersGrid = new TreeGrid<>();
						{
							parametersGrid.setAllRowsVisible(true);
							//
							parametersGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
						}
						{
							parametersGrid
									.addColumn(FunctionParametersEntry::getName)
									.setHeader("Name")
									.setResizable(true);
							parametersGrid
									.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
											ToVaadinComponentRendererRegistry.Context.builder()
													.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
															TypeDescription.class))
													.hints(List.empty())
													.build(),
											item.getDatatype()))
									.setHeader("Type")
									.setResizable(true);
							parametersGrid
									.addComponentColumn(item -> item.getConstraints() != null
											? GridUtils.renderValueList(item.getConstraints())
											: null)
									.setHeader("Constraints")
									.setResizable(true);
							parametersGrid
									.addColumn(FunctionParametersEntry::getKind)
									.setHeader("Kind")
									.setResizable(true);
							parametersGrid
									.addComponentColumn(item -> item.getComments() != null
											? GridUtils.renderValueList(item.getComments())
											: null)
									.setHeader("Comments")
									.setResizable(true);
						}
						{
							parametersGrid.setItems(
									parameters.function().getParameters().asJava(),
									item -> java.util.List.of());
						}
						parametersLayout.add(parametersGrid);
					}
				}
				layout.add(parametersLayout);
			}
			{
				CssVerticalLayout resultLayout = new CssVerticalLayout();
				{
					resultLayout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					{
						resultLayout.add(new Span("Result"));
					}
					{
						TreeGrid<FunctionResultEntry> resultGrid = new TreeGrid<>();
						{
							resultGrid.setAllRowsVisible(true);
							//
							resultGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
						}
						{
							resultGrid
									.addColumn(FunctionResultEntry::getName)
									.setHeader("Name")
									.setResizable(true);
							resultGrid
									.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
											ToVaadinComponentRendererRegistry.Context.builder()
													.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
															TypeDescription.class))
													.hints(List.empty())
													.build(),
											item.getDatatype()))
									.setHeader("Type")
									.setResizable(true);
							resultGrid
									.addComponentColumn(item -> item.getConstraints() != null
											? GridUtils.renderValueList(item.getConstraints())
											: null)
									.setHeader("Constraints")
									.setResizable(true);
							resultGrid
									.addColumn(FunctionResultEntry::getKind)
									.setHeader("Kind")
									.setResizable(true);
							resultGrid
									.addComponentColumn(item -> item.getComments() != null
											? GridUtils.renderValueList(item.getComments())
											: null)
									.setHeader("Comments")
									.setResizable(true);
						}
						{
							resultGrid.setItems(
									parameters.function().getResult().asJava(),
									item -> java.util.List.of());
						}
						resultLayout.add(resultGrid);
					}
				}
				layout.add(resultLayout);
			}
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		ManifestFunction function;

		public static class Builder {
			// Lomboked
		}
	}
}