package tk.labyrinth.satool.beholder.manifester.domain.properties;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PropertiesUtils {

	public static List<Property> groupProperties(
			List<PropertyDeclarationEntry> propertyDeclarationEntries,
			List<PropertyUsageEntry> propertyUsageEntries) {
		Map<String, List<PropertyDeclarationEntry>> declarationsMap = propertyDeclarationEntries
				.groupBy(PropertyDeclarationEntry::getName);
		Map<String, List<PropertyUsageEntry>> usagesMap = propertyUsageEntries
				.groupBy(PropertyUsageEntry::getName);
		//
		return declarationsMap.keySet().addAll(usagesMap.keySet())
				.toList()
				.sorted()
				.map(propertyName -> Property.builder()
						.declarationEntries(declarationsMap.getOrElse(propertyName, List.empty()))
						.name(propertyName)
						.usageEntries(usagesMap.getOrElse(propertyName, List.empty()))
						.build());
	}

	public static Pair<List<PropertyDeclarationEntry>, List<PropertyUsageEntry>> selectPropertyTree(
			List<PropertyDeclarationEntry> propertyDeclarationEntries,
			List<PropertyUsageEntry> propertyUsageEntries,
			String propertyName) {
		Set<PropertyDeclarationEntry> selectedDeclarationEntries = new HashSet<>();
		Set<PropertyUsageEntry> selectedUsageEntries = new HashSet<>();
		{
			ArrayList<String> namesToProcess = new ArrayList<>();
			namesToProcess.add(propertyName);
			Set<String> processedNames = new HashSet<>();
			//
			while (!namesToProcess.isEmpty()) {
				String currentName = namesToProcess.remove(0);
				//
				processedNames.add(currentName);
				//
				propertyDeclarationEntries
						.filter(declarationEntry -> Objects.equals(declarationEntry.getName(), currentName))
						.forEach(selectedDeclarationEntries::add);
				propertyUsageEntries // Upstream.
						.filter(usageEntry -> Objects.equals(usageEntry.getName(), currentName))
						.forEach(usageEntry -> {
							selectedUsageEntries.add(usageEntry);
							//
							String location = usageEntry.getLocation();
							//
							String propertyUsageName = location.substring(location.indexOf(':') + 1);
							//
							if (processedNames.add(propertyUsageName)) {
								namesToProcess.add(propertyUsageName);
							}
						});
				propertyUsageEntries // Downstream.
						.filter(usageEntry -> usageEntry.getTags().contains("property"))
						.forEach(usageEntry -> {
							String location = usageEntry.getLocation();
							//
							String propertyUsageName = location.substring(location.indexOf(':') + 1);
							//
							if (Objects.equals(propertyUsageName, currentName)) {
								selectedUsageEntries.add(usageEntry);
								//
								if (processedNames.add(usageEntry.getName())) {
									namesToProcess.add(usageEntry.getName());
								}
							}
						});
			}
		}
		return Pair.of(
				List.ofAll(selectedDeclarationEntries).sortBy(PropertyDeclarationEntry::getName),
				List.ofAll(selectedUsageEntries).sortBy(PropertyUsageEntry::getName));
	}
}
