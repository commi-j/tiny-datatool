package tk.labyrinth.satool.beholder.manifester.domain.gitbranch;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.beholder.manifester.domain.repository.UrlRepositoryReference;

import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class RepositoryReferenceAndNameGitBranchReference implements Reference<GitBranch> {

	@NonNull
	String name;

	@NonNull
	UrlRepositoryReference repositoryReference;

	@Override
	public String toString() {
		return "%s(%s=%s,%s=%s)".formatted(
				GitBranch.MODEL_CODE,
				GitBranch.REPOSITORY_REFERENCE_ATTRIBUTE_NAME,
				repositoryReference,
				GitBranch.NAME_ATTRIBUTE_NAME,
				name);
	}

	public static RepositoryReferenceAndNameGitBranchReference from(GitBranch object) {
		return of(object.getRepositoryReference(), object.getName());
	}

	@JsonCreator
	public static RepositoryReferenceAndNameGitBranchReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), GitBranch.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		//
		return of(
				UrlRepositoryReference.from(reference.getAttributeValue(
						GitBranch.REPOSITORY_REFERENCE_ATTRIBUTE_NAME)),
				reference.getAttributeValue(GitBranch.NAME_ATTRIBUTE_NAME));
	}

	public static RepositoryReferenceAndNameGitBranchReference of(
			UrlRepositoryReference repositoryReference,
			String name) {
		return new RepositoryReferenceAndNameGitBranchReference(name, repositoryReference);
	}
}
