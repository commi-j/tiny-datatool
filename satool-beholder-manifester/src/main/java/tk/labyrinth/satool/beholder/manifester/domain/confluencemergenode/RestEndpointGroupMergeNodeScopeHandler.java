package tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.object.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.value.wrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModelUtils;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestEndpointGroup;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestParametersEntry;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestResultEntry;

import java.util.Objects;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class RestEndpointGroupMergeNodeScopeHandler implements ConfluenceMergeNodeScopeHandler {

	private final ConverterRegistry converterRegistry;

	@Override
	public List<String> getAttributeNames() {
		return List.of("restEndpointGroup");
	}

	@Override
	public String getScopeName() {
		return "REST_ENDPOINT_GROUP";
	}

	@Override
	public List<GenericObject> getVariablesObjects(String parentScopeName, GenericObject parentObject) {
		GenericObject manifestObject = parentObject.getAttributeValueAsObject("manifest");
		//
		List<DisplayableModel> models = converterRegistry.convertInferred(
				manifestObject.getAttributeValue("models"),
				TypeUtils.parameterize(List.class, DisplayableModel.class));
		//
		return manifestObject
				.getAttributeValueAsList("restEndpointGroups")
				.map(ValueWrapper::asObject)
				.map(ObjectValueWrapper::unwrap)
				.map(restEndpointGroupObject -> {
					RestEndpointGroup restEndpointGroup = converterRegistry.convert(
							restEndpointGroupObject,
							RestEndpointGroup.class);
					//
					return parentObject.withAddedAttributes(
							GenericObjectAttribute.ofObject("restEndpointGroup", restEndpointGroupObject),
							GenericObjectAttribute.ofObjectList(
									"relevantModels",
									selectModels(models, restEndpointGroup)
											.map(displayableModel -> converterRegistry.convert(displayableModel, GenericObject.class))));
				});
	}

	public static List<DisplayableModel> selectModels(
			List<DisplayableModel> allModels,
			RestEndpointGroup restEndpointGroup) {
		List<DisplayableModel> result;
		{
			List<ClassSignature> classSignatures;
			//
			classSignatures = restEndpointGroup.getEndpoints()
					.flatMap(restEndpointElement -> List
							.of(
									restEndpointElement.getParameters().map(RestParametersEntry::getDatatype),
									restEndpointElement.getResult().map(RestResultEntry::getDatatype))
							.flatMap(Function.identity())
							.filter(Objects::nonNull)
							.flatMap(TypeDescription::breakDown)
							.map(TypeDescription::getSignature)
							.filter(TypeSignature::isDeclaredOrVariable)
							.map(TypeSignature::getClassSignature))
					.distinct();
			//
			result = DisplayableModelUtils.selectRecursivelyWithClasses(allModels, classSignatures);
		}
		return result;
	}
}
