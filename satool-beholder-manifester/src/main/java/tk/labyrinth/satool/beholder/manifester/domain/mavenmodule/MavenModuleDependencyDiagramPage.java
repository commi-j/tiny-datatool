package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.control.Option;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.satool.beholder.root.BeholderRootLayout;
import tk.labyrinth.satool.plantuml.view.PlantumlDiagramView;

import javax.annotation.CheckForNull;
import java.util.Objects;

@RequiredArgsConstructor
@Route(value = "maven-module-dependency-diagram", layout = BeholderRootLayout.class)
@Slf4j
public class MavenModuleDependencyDiagramPage extends SplitLayout implements AfterNavigationObserver {

	private final MavenModuleDependencyDiagramBuilder diagramBuilder;

	private final RadioButtonGroup<Boolean> diagramKindGroup = new RadioButtonGroup<>();

	private final PlantumlDiagramView diagramView;

	private final MavenModuleDependencyTool mavenModuleDependencyTool;

	private final CssVerticalLayout mavenModulesLayout = new CssVerticalLayout();

	private final Observable<State> stateObservable = Observable.notInitialized();

	@PostConstruct
	private void postConstruct() {
		{
			addToPrimary(mavenModulesLayout);
		}
		{
			CssVerticalLayout diagramLayout = new CssVerticalLayout();
			{
				diagramKindGroup.setItems(true, false);
				diagramKindGroup.setRenderer(new ComponentRenderer<>(item -> item
						? new Span("Object diagram")
						: new Span("Component diagram")));
				diagramKindGroup.addValueChangeListener(event -> stateObservable.update(currentState ->
						currentState.withObjectOrComponent(event.getValue())));
				diagramLayout.add(diagramKindGroup);
			}
			{
				CssFlexItem.setFlexGrow(diagramView, 1);
				diagramLayout.add(diagramView);
			}
			addToSecondary(diagramLayout);
		}
		{
			stateObservable.subscribe(nextState -> {
				List<MavenModule> mavenModules = nextState.getExpand()
						.flatMap(expandEntry -> mavenModuleDependencyTool.collectDependencies(
								expandEntry.getGroupId(),
								expandEntry.getArtifactId(),
								expandEntry.getUp(),
								expandEntry.getDown()))
						.distinctBy(MavenModule::getUid)
						.sortBy(MavenModule::getGroupIdArtifactId);
				List<GroupIdAndArtifactIdMavenModuleReference> highlightedModuleReferences = nextState.getExpand()
						.map(State.ExpandEntry::getGroupIdArtifactIdReference);
				{
					mavenModulesLayout.removeAll();
					//
					mavenModules.forEach(mavenModule -> {
						State.ExpandEntry expandEntryForMavenModule = nextState.getExpand()
								.find(expandEntry ->
										Objects.equals(expandEntry.getArtifactId(), mavenModule.getArtifactId()) &&
												Objects.equals(expandEntry.getGroupId(), mavenModule.getGroupId()))
								.getOrNull();
						//
						CssHorizontalLayout mavenModuleLayout = new CssHorizontalLayout();
						{
							mavenModuleLayout.setAlignItems(AlignItems.BASELINE);
						}
						{
							Span groupIdArtifactIdlabel = new Span(mavenModule.getGroupIdArtifactId());
							CssFlexItem.setFlexGrow(groupIdArtifactIdlabel, 1);
							mavenModuleLayout.add(groupIdArtifactIdlabel);
						}
						if (expandEntryForMavenModule != null) {
							{
								IntegerField upField = new IntegerField();
								upField.setStepButtonsVisible(true);
								upField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
								upField.setValue(expandEntryForMavenModule != null
										? expandEntryForMavenModule.getUp()
										: null);
								upField.addValueChangeListener(event -> stateObservable.update(currentState ->
										currentState.withExpand(currentState.getExpand().replace(
												expandEntryForMavenModule,
												expandEntryForMavenModule.withUp(event.getValue())))));
								upField.setWidth("6em");
								mavenModuleLayout.add(upField);
							}
							{
								IntegerField downField = new IntegerField();
								downField.setStepButtonsVisible(true);
								downField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
								downField.setValue(expandEntryForMavenModule != null
										? expandEntryForMavenModule.getDown()
										: null);
								downField.addValueChangeListener(event -> stateObservable.update(currentState ->
										currentState.withExpand(currentState.getExpand().replace(
												expandEntryForMavenModule,
												expandEntryForMavenModule.withDown(event.getValue())))));
								downField.setWidth("6em");
								mavenModuleLayout.add(downField);
							}
						}
						{
							Button changeExpandStatusButton = new Button();
							//
							changeExpandStatusButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
							//
							if (expandEntryForMavenModule != null) {
								changeExpandStatusButton.setEnabled(highlightedModuleReferences.size() > 1);
								changeExpandStatusButton.setIcon(VaadinIcon.MINUS.create());
								//
								changeExpandStatusButton.addClickListener(event ->
										stateObservable.update(currentState -> currentState.withExpand(
												currentState.getExpand().remove(expandEntryForMavenModule))));
							} else {
								changeExpandStatusButton.setIcon(VaadinIcon.PLUS.create());
								//
								changeExpandStatusButton.addClickListener(event ->
										stateObservable.update(currentState -> currentState.withExpand(
												currentState.getExpand().append(State.ExpandEntry.builder()
														.artifactId(mavenModule.getArtifactId())
														.down(0)
														.groupId(mavenModule.getGroupId())
														.up(0)
														.build()))));
							}
							//
							mavenModuleLayout.add(changeExpandStatusButton);
						}
						mavenModulesLayout.add(mavenModuleLayout);
					});
				}
				{
					diagramKindGroup.setValue(nextState.getObjectOrComponent());
				}
				{
					diagramView.setParameters(builder -> builder
							.value(nextState.getObjectOrComponent()
									? diagramBuilder.build(mavenModules)
									: diagramBuilder.buildComponentDiagramString(mavenModules, highlightedModuleReferences))
							.build());
				}
				{
					UI.getCurrent().getPage().getHistory().replaceState(
							null,
							new Location(
									"beholder/%s".formatted("maven-module-dependency-diagram"),
									nextState.toQueryParameters()));
				}
			});
		}
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		stateObservable.set(State.fromQueryParameters(event.getLocation().getQueryParameters()));
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State {

		@CheckForNull
		List<ExpandEntry> expand;

		Boolean objectOrComponent;

		@CheckForNull
		QueryParameters queryParameters;

		public QueryParameters toQueryParameters() {
			return expand != null
					? QueryParameters.fromString("expand=%s".formatted(
					expand.map(ExpandEntry::toQuerySegment).mkString(",")))
					: queryParameters;
		}

		public static State fromQueryParameters(QueryParameters queryParameters) {
			List<String> expand = Option.of(queryParameters.getParameters().get("expand")).map(List::ofAll).getOrNull();
			return State.builder()
					.expand(expand.size() == 1
							? List.of(expand.get(0).split(",")).map(ExpandEntry::fromQuerySegment)
							: null)
					.objectOrComponent(true)
					.queryParameters(queryParameters)
					.build();
		}

		@Builder(toBuilder = true)
		@Value
		@With
		public static class ExpandEntry {

			String artifactId;

			Integer down;

			String groupId;

			Integer up;

			public GroupIdAndArtifactIdMavenModuleReference getGroupIdArtifactIdReference() {
				return GroupIdAndArtifactIdMavenModuleReference.of(groupId, artifactId);
			}

			public String toQuerySegment() {
				return "%s:%s/%s/%s".formatted(groupId, artifactId, up, down);
			}

			public static ExpandEntry fromQuerySegment(String querySegment) {
				List<String> segmentParts = List.of(querySegment.split("/"));
				List<String> gaParts = List.of(segmentParts.get(0).split(":"));
				return ExpandEntry.builder()
						.artifactId(gaParts.get(1))
						.down(Integer.parseInt(segmentParts.get(2)))
						.groupId(gaParts.get(0))
						.up(Integer.parseInt(segmentParts.get(1)))
						.build();
			}
		}
	}
}
