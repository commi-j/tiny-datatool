package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.credentials.Credentials;
import tk.labyrinth.pandora.stores.extra.credentials.CredentialsUtils;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.manifester.artifact.ArtifactDefinition;
import tk.labyrinth.satool.beholder.manifester.artifact.NameArtifactDefinitionReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.CodeConfluenceMergeStructureReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeNode;
import tk.labyrinth.satool.beholder.manifester.domain.confluencemerge.ConfluenceMergeStructure;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.ConfluencePageTemplate;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceConnectorData;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceCreateTarget;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluencePublishTarget;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceUpdateTarget;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClientFactory;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceBody;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceSpace;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceStorage;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceVersion;

import java.util.Objects;
import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class PublishManifestsToConfluenceJobRunner implements
		JobRunner<PublishManifestsToConfluenceJobRunner.Parameters> {

	private final ConfluenceFeignClientFactory confluenceFeignClientFactory;

	private final ExecutionContextHandler executionContextHandler;

	private final ConfluenceRenderEngine renderEngine;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactDefinition> artifactDefinitionSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifest> artifactManifestSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ConfluenceMergeStructure> confluenceMergeStructureSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ConfluencePageTemplate> confluencePageTemplateSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Credentials> credentialsSearcher;

	private void processArtifactManifest(
			Parameters parameters,
			ConfluenceConnectorData confluenceConnectorData,
			ArtifactDefinition artifactDefinition,
			ArtifactManifest artifactManifest) {
		ConfluenceMergeStructure confluenceMergeStructure = confluenceMergeStructureSearcher.getSingle(
				parameters.getConfluenceMergeStructureReference());
		//
		confluenceMergeStructure.getRootNodes().forEach(rootNode -> {
			processConfluenceMergeNode(confluenceConnectorData, artifactDefinition, artifactManifest, null, rootNode);
		});
	}

	private void processConfluenceMergeNode(
			ConfluenceConnectorData confluenceConnectorData,
			ArtifactDefinition artifactDefinition,
			ArtifactManifest artifactManifest,
			String parentContentId,
			ConfluenceMergeNode mergeNode) {
		Map<String, Object> data = HashMap.of(
				"artifactDefinition", artifactDefinition,
				"artifactManifest", artifactManifest);
		//
		String contentId;
		{
			ConfluencePublishTarget publishTarget = ConfluencePublishingUtils.getPublishTarget(
					confluenceConnectorData,
					parentContentId,
					renderEngine.resolve(mergeNode.getLocator(), data));
			//
			logger.info("publishTarget = {}", publishTarget);
			//
			if (mergeNode.getTitleTemplate() != null) {
				String title = renderEngine.resolve(mergeNode.getTitleTemplate(), data);
				String body = mergeNode.getBodyTemplateReference() != null
						? renderEngine.resolve(mergeNode.getBodyTemplateReference(), data)
						: "";
				//
				if (publishTarget instanceof ConfluenceCreateTarget createTarget) {
					ConfluenceContent confluenceContent = confluenceConnectorData.getFeignClient().postContent(
							confluenceConnectorData.getAuthorization(),
							ConfluenceContent.builder()
									.ancestors(List.of(ConfluenceContent.builder()
											.id(createTarget.getParentContentId())
											.build()))
									.space(ConfluenceSpace.builder()
											.key(createTarget.getParentSpaceKey())
											.build())
									.body(ConfluenceBody.builder()
											.storage(ConfluenceStorage.builder()
													.representation("storage")
													.value(body)
													.build())
											.build())
									.title(title)
									.type("page")
									.build());
					//
					contentId = confluenceContent.getId();
				} else if (publishTarget instanceof ConfluenceUpdateTarget updateTarget) {
					ConfluenceContent confluenceContent = confluenceConnectorData.getFeignClient().putContent(
							confluenceConnectorData.getAuthorization(),
							updateTarget.getContentId(),
							ConfluenceContent.builder()
									.body(ConfluenceBody.builder()
											.storage(ConfluenceStorage.builder()
													.representation("storage")
													.value(body)
													.build())
											.build())
									.title(title)
									.type("page")
									.version(ConfluenceVersion.builder()
											.number(updateTarget.getVersionNumber())
											.build())
									.build());
					//
					contentId = confluenceContent.getId();
				} else {
					throw new NotImplementedException();
				}
			} else {
				if (publishTarget instanceof ConfluenceUpdateTarget updateTarget) {
					contentId = updateTarget.getContentId();
				} else {
					// Page not create and locator is not for update -> can't determine contentId;
					contentId = null;
				}
			}
		}
		//
		Optional.ofNullable(mergeNode.getChildren()).ifPresent(children -> children.forEach(child ->
				processConfluenceMergeNode(
						confluenceConnectorData,
						artifactDefinition,
						artifactManifest,
						contentId,
						child)));
	}

	@Override
	public Parameters getInitialParameters() {
		return PublishManifestsToConfluenceJobRunner.Parameters.builder()
				.spaceReference(executionContextHandler.getExecutionContext().findAttributeValueInferred(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME))
				.build();
	}

	@Override
	public void run(Context<Parameters> context) {
		Parameters parameters = context.getParameters();
		//
		List<ArtifactDefinition> artifactDefinitions = artifactDefinitionSearcher.search(PlainQuery.builder()
				.predicate(parameters.getArtifactDefinitionNameFilter() != null
						? Predicates.contains(
						ArtifactDefinition.NAME_ATTRIBUTE_NAME,
						parameters.getArtifactDefinitionNameFilter(),
						false)
						: null)
				.build());
		//
		List<ArtifactManifest> artifactManifests = artifactManifestSearcher.search(PlainQuery.builder()
				.predicate(Predicates.in(
						ArtifactManifest.DEFINITION_REFERENCE_ATTRIBUTE_NAME,
						artifactDefinitions.map(NameArtifactDefinitionReference::from).toJavaArray()))
				.build());
		//
		ConfluenceConnectorData confluenceConnectorData = ConfluenceConnectorData.builder()
				.authorization(Optional.ofNullable(parameters.getConfluenceCredentialsReference())
						.map(confluenceCredentialsReference -> {
							Credentials credentials = credentialsSearcher.getSingle(confluenceCredentialsReference);
							//
							return CredentialsUtils.toAuthorizationString(credentials);
						})
						.orElse(null))
				.baseUrl(parameters.getConfluenceBaseUrl())
				.feignClient(confluenceFeignClientFactory.create(
						parameters.getConfluenceBaseUrl()))
				.build();
		//
		artifactManifests.forEach(artifactManifest -> {
			ArtifactDefinition artifactDefinition = artifactDefinitions
					.find(innerArtifactDefinition -> Objects.equals(
							NameArtifactDefinitionReference.from(innerArtifactDefinition),
							artifactManifest.getDefinitionReference()))
					.get();
			//
			logger.debug("Processing Artifact with name = {}", artifactDefinition.getName());
			//
			try {
				processArtifactManifest(parameters, confluenceConnectorData, artifactDefinition, artifactManifest);
			} catch (RuntimeException ex) {
				logger.warn("artifactDefinition = {}, artifactManifest = {}",
						artifactDefinitions, artifactManifest, ex);
			}
		});
	}

	@Builder(toBuilder = true)
	@Value
	public static class Parameters implements HasSpaceReference {

		@Nullable
		String artifactDefinitionNameFilter;

		String confluenceBaseUrl;

		@Nullable
		KeyCredentialsReference confluenceCredentialsReference;

		CodeConfluenceMergeStructureReference confluenceMergeStructureReference;

		KeySpaceReference spaceReference;
	}
}
