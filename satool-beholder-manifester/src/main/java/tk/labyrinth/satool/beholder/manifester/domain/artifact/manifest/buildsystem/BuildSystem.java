package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.satool.maven.domain.MavenDependency;
import tk.labyrinth.satool.maven.domain.MavenRepository;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderAttribute("name")
@Value
@With
public class BuildSystem {

	String category;

	List<MavenDependency> dependencies;

	String name;

	List<ArtifactSignature> plugins;

	List<MavenRepository> repositories;

	public List<ArtifactSignature> getPluginsOrEmpty() {
		return plugins != null ? plugins : List.empty();
	}

	public List<MavenRepository> getRepositoriesOrEmpty() {
		return repositories != null ? repositories : List.empty();
	}
}
