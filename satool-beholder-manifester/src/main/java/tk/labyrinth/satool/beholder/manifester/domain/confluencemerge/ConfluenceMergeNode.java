package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.pandora.pattern.PandoraPattern;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.diagram.ArtifactManifestPredefinedDiagramKind;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.KeyConfluencePageTemplateReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram.ConfluenceDiagramPublishingStrategy;

import java.lang.reflect.Type;
import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderAttribute("locator")
//@RenderAttribute("${titleTemplate:$locator}")
@Value
@With
public class ConfluenceMergeNode {

	public static final Type DIAGRAMS_ATTRIBUTE_TYPE = TypeUtils.parameterize(
			List.class,
			TypeUtils.parameterize(
					Pair.class,
					ArtifactManifestPredefinedDiagramKind.class, ConfluenceDiagramPublishingStrategy.class));

	/**
	 * DO_NOTHING (for root);
	 * USE_TEMPLATE;
	 */
	@Nullable
	KeyConfluencePageTemplateReference bodyTemplateReference;

	List<ConfluenceMergeNode> children;

	List<Pair<ArtifactManifestPredefinedDiagramKind, ConfluenceDiagramPublishingStrategy>> diagrams;

	/**
	 * Id or title page.
	 */
	String locator;

	// TODO: Rename to scopeName.
	@Nullable
	String scope;

	@Nullable
	String titleTemplate;

	public List<String> computeVariables() {
		return List.of(locator, titleTemplate)
				.filter(Objects::nonNull)
				.flatMap(string -> PandoraPattern.from(string).getPlaceholderNames())
				.appendAll(getChildrenOrEmpty().flatMap(ConfluenceMergeNode::computeVariables));
	}

	public List<ConfluenceMergeNode> getChildrenOrEmpty() {
		return children != null ? children : List.empty();
	}

	@Nullable
	public ConfluenceMergeNode updateNested(ConfluenceMergeNode currentValue, ConfluenceMergeNode nextValue) {
		return currentValue == this
				? nextValue
				: withChildren(children != null
				? children
				.map(child -> child.updateNested(currentValue, nextValue))
				.filter(Objects::nonNull)
				: null);
	}

	public static class Builder {

		public Builder children(ConfluenceMergeNode... children) {
			return children(List.of(children));
		}

		public Builder children(List<ConfluenceMergeNode> children) {
			this.children = children;
			return this;
		}
	}
}
