package tk.labyrinth.satool.beholder.manifester.dependencymanager;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.misc4j.java.net.UrlUtils;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.ArtifactSignature;
import tk.labyrinth.satool.beholder.manifester.domain.mavenmodule.MavenModule;

import java.net.URL;
import java.util.Objects;

@Value(staticConstructor = "of")
public class ArtifactSignatureAndRepositoryUrlMavenArtifactRepositoryStatusReference implements
		Reference<MavenArtifactRepositoryStatus> {

	ArtifactSignature artifactSignature;

	URL repositoryUrl;

	private ArtifactSignatureAndRepositoryUrlMavenArtifactRepositoryStatusReference(
			@NonNull ArtifactSignature artifactSignature,
			@NonNull URL repositoryUrl) {
		this.artifactSignature = artifactSignature.requireFull();
		this.repositoryUrl = repositoryUrl;
	}

	@Override
	public String toString() {
		return "%s(%s=%s,%s=%s)".formatted(
				MavenArtifactRepositoryStatus.MODEL_CODE,
				MavenArtifactRepositoryStatus.ARTIFACT_SIGNATURE_ATTRIBUTE_NAME,
				artifactSignature,
				MavenArtifactRepositoryStatus.REPOSITORY_URL_ATTRIBUTE_NAME,
				repositoryUrl);
	}

	public static ArtifactSignatureAndRepositoryUrlMavenArtifactRepositoryStatusReference from(
			MavenArtifactRepositoryStatus object) {
		return of(object.getArtifactSignature(), object.getRepositoryUrl());
	}

	@JsonCreator
	public static ArtifactSignatureAndRepositoryUrlMavenArtifactRepositoryStatusReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), MavenModule.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		//
		return of(
				ArtifactSignature.from(reference.getAttributeValue(
						MavenArtifactRepositoryStatus.ARTIFACT_SIGNATURE_ATTRIBUTE_NAME)),
				UrlUtils.from(reference.getAttributeValue(
						MavenArtifactRepositoryStatus.REPOSITORY_URL_ATTRIBUTE_NAME)));
	}
}
