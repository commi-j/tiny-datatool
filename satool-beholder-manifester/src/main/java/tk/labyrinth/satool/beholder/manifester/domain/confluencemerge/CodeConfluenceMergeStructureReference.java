package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class CodeConfluenceMergeStructureReference implements Reference<ConfluenceMergeStructure> {

	String code;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				ConfluenceMergeStructure.MODEL_CODE,
				ConfluenceMergeStructure.CODE_ATTRIBUTE_NAME,
				code);
	}

	public static CodeConfluenceMergeStructureReference from(ConfluenceMergeStructure object) {
		return CodeConfluenceMergeStructureReference.of(object.getCode());
	}

	@JsonCreator
	public static CodeConfluenceMergeStructureReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), ConfluenceMergeStructure.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(ConfluenceMergeStructure.CODE_ATTRIBUTE_NAME));
	}
}