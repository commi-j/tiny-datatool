package tk.labyrinth.satool.beholder.manifester.domain.persistentmodel;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class PersistentModelRelation {

	Boolean hasRelationColumn;

	String toColumnName;

	String toTableName;
}
