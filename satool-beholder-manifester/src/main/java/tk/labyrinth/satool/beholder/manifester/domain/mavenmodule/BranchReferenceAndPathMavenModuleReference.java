package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.RepositoryReferenceAndNameGitBranchReference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class BranchReferenceAndPathMavenModuleReference implements Reference<MavenModule> {

	@NonNull
	RepositoryReferenceAndNameGitBranchReference branchReference;

	@NonNull
	String path;

	@Override
	public String toString() {
		return "%s(%s=%s,%s=%s)".formatted(
				MavenModule.MODEL_CODE,
				MavenModule.BRANCH_REFERENCE_ATTRIBUTE_NAME,
				branchReference,
				MavenModule.PATH_ATTRIBUTE_NAME,
				path);
	}

	public static BranchReferenceAndPathMavenModuleReference from(MavenModule object) {
		return of(object.getBranchReference(), object.getPath());
	}

	@JsonCreator
	public static BranchReferenceAndPathMavenModuleReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), MavenModule.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		//
		return of(
				RepositoryReferenceAndNameGitBranchReference.from(reference.getAttributeValue(
						MavenModule.BRANCH_REFERENCE_ATTRIBUTE_NAME)),
				reference.getAttributeValue(MavenModule.PATH_ATTRIBUTE_NAME));
	}
}
