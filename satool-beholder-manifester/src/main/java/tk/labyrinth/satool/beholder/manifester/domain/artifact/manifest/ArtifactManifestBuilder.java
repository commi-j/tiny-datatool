package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import spoon.Launcher;
import spoon.reflect.CtModel;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.BuildSystem;
import tk.labyrinth.satool.beholder.manifester.domain.endpoint.kafka.KafkaEndpoint;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RestController;

import java.nio.file.Path;
import java.time.Instant;

@Component
@RequiredArgsConstructor
@Slf4j
public class ArtifactManifestBuilder {

	@SmartAutowired
	private List<ManifestContributor> manifestContributors;

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized: manifestContributors = {}", manifestContributors);
	}

	public ArtifactManifest buildJavaModuleManifestFromDirectory(Path directory) {
		Launcher launcher = new Launcher();
		//
		launcher.getEnvironment().setSourceClasspath(manifestContributors
				.flatMap(ManifestContributor::getExtraClasspathEntries)
				.toJavaArray(String[]::new));
		//
		launcher.addInputResource(directory.toString() + "/src/main/java");
		//
		launcher.buildModel();
		//
		CtModel model = launcher.getModel();
		//
		List<?> manifestEntries = manifestContributors
				.flatMap(manifestContributor -> manifestContributor.contribute(directory, model))
				.toList();
		//
		return ArtifactManifest.builder()
				.buildSystems(manifestEntries
						.filter(BuildSystem.class::isInstance)
						.map(BuildSystem.class::cast))
				.definitionReference(null)
				.functions(manifestEntries
						.filter(ManifestFunction.class::isInstance)
						.map(ManifestFunction.class::cast))
				.exposedClasses(manifestEntries
						.filter(ExposedClass.class::isInstance)
						.map(ExposedClass.class::cast)
						.groupBy(ExposedClass::getSignature)
						.values()
						.map(ExposedClass::merge)
						.toList())
				.kafkaEndpoints(manifestEntries
						.filter(KafkaEndpoint.class::isInstance)
						.map(KafkaEndpoint.class::cast))
				.persistentModels(manifestEntries
						.filter(PersistentModel.class::isInstance)
						.map(PersistentModel.class::cast))
				.propertyDeclarationEntries(manifestEntries
						.filter(PropertyDeclarationEntry.class::isInstance)
						.map(PropertyDeclarationEntry.class::cast))
				.propertyUsageEntries(manifestEntries
						.filter(PropertyUsageEntry.class::isInstance)
						.map(PropertyUsageEntry.class::cast))
				.restControllers(manifestEntries
						.filter(RestController.class::isInstance)
						.map(RestController.class::cast))
				.updatedAt(Instant.now())
				.build();
	}
}
