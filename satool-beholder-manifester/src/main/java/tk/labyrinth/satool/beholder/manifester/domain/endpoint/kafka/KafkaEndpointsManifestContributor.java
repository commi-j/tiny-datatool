package tk.labyrinth.satool.beholder.manifester.domain.endpoint.kafka;

import io.vavr.collection.List;
import org.springframework.kafka.annotation.KafkaListener;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtTypeInformation;
import spoon.reflect.reference.CtExecutableReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.beholder.manifester.domain.endpoint.InteractionKind;
import tk.labyrinth.satool.spoon.CtMethodUtils;

import java.nio.file.Path;
import java.util.Objects;

@LazyComponent
public class KafkaEndpointsManifestContributor implements ManifestContributor {

	@Override
	public List<?> contribute(Path rootDirectory, CtModel javaModel) {
		List<?> result;
		{
			List<? extends CtMethod<?>> kafkaEndpointMethods = List.ofAll(javaModel.getAllTypes())
					.flatMap(CtTypeInformation::getDeclaredExecutables)
					.map(CtExecutableReference::getDeclaration)
					//
					// FIXME: Somewhy #getDeclaredExecutables() on interfaces returns executables declared in superinterfaces,
					//  which may be not available here.
					.filter(Objects::nonNull)
					//
					.filter(KafkaEndpointsManifestContributor::isKafkaListener)
					.map(executable -> (CtMethod<?>) executable)
					.toList();
			//
			List<KafkaEndpoint> kafkaEndpoints = kafkaEndpointMethods.map(kafkaEndpointMethod -> KafkaEndpoint.builder()
					.inboundTopic(resolveInboundTopic(kafkaEndpointMethod))
					.interactionKind(InteractionKind.UNDEFINED)
					.signature(CtMethodUtils.getFullSignature(kafkaEndpointMethod))
					.build());
			//
			result = kafkaEndpoints;
		}
		return result;
	}

	public static boolean isKafkaListener(CtExecutable<?> executable) {
		return executable.hasAnnotation(KafkaListener.class);
	}

	public static String resolveInboundTopic(CtMethod<?> method) {
		KafkaListener kafkaListenerAnnotation = method.getAnnotation(KafkaListener.class);
		return List.of(kafkaListenerAnnotation.topics()).single();
	}
}
