package tk.labyrinth.satool.beholder.manifester.domain.restcontroller;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import spoon.reflect.CtModel;
import spoon.reflect.code.CtExpression;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.declaration.CtType;
import spoon.reflect.path.CtRole;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.misc4j.exception.ExceptionUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributorUtils;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.tool.ExposedClassDiscoverer;
import tk.labyrinth.satool.spoon.CtMethodUtils;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.SpoonStaticFactory;
import tk.labyrinth.satool.spoon.SpoonUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;

@LazyComponent
public class RestControllersManifestContributor implements ManifestContributor {

	private static final Set<CtTypeReference<?>> requestMappingAnnotations;

	static {
		requestMappingAnnotations = HashSet
				.of(
						DeleteMapping.class,
						GetMapping.class,
						PatchMapping.class,
						PostMapping.class,
						PutMapping.class,
						RequestMapping.class)
				.map(SpoonStaticFactory::createReference);
	}

	@Override
	public List<?> contribute(Path rootDirectory, CtModel javaModel) {
		List<?> result;
		{
			List<CtType<?>> restControllerTypes = List.ofAll(javaModel.getAllTypes())
					.filter(RestControllersManifestContributor::isRestController)
					.toList();
			//
			List<RestController> restControllers = restControllerTypes.map(restControllerType ->
					RestController.builder()
							.methods(List.ofAll(restControllerType.getMethods())
									.filter(RestControllersManifestContributor::hasRequestMappingAnnotation)
									.map(method -> {
										Annotation requestMappingAnnotation = getRequestMappingAnnotation(method);
										//
										return RequestMappingMethod.builder()
												.method(resolveMethod(requestMappingAnnotation))
												.parameters(List.ofAll(method.getParameters())
														.map(RestControllersManifestContributor::resolveParameter))
												.paths(resolveRequestMappingPaths(requestMappingAnnotation))
												.result(List.of(resolveResult(method)))
												.signature(CtMethodUtils.getSimpleSignature(method))
												.build();
									}))
							.path(resolvePath(restControllerType))
							.signature(SpoonUtils.resolveJavaClassSignature(restControllerType))
							.build());
			//
			List<ClassSignature> directClassSignature = restControllers
					.flatMap(RestController::getMethods)
					.flatMap(requestMappingMethod -> requestMappingMethod.getParameters()
							.map(RestParametersEntry::getDatatype)
							.flatMap(TypeDescription::breakDown)
							.appendAll(requestMappingMethod.getResult()
									.map(RestResultEntry::getDatatype)
									.flatMap(TypeDescription::breakDown)))
					.filter(TypeDescription::isDeclared)
					.map(TypeDescription::getSignature)
					.map(TypeSignature::getClassSignature)
					.distinct();
			//
			List<ExposedClass> exposedClasses = ExposedClassDiscoverer.discoverRecursively(
					javaModel,
					directClassSignature);
			//
			result = restControllers.map(Object.class::cast).appendAll(exposedClasses);
		}
		return result;
	}

	@Override
	public List<String> getExtraClasspathEntries() {
		return List
				.of(org.springframework.web.bind.annotation.RestController.class)
				.map(ManifestContributorUtils::getLocationOfClassSource);
	}

	private static List<CtAnnotation<?>> getRequestMappingAnnotations(CtMethod<?> method) {
		return List.ofAll(method.getAnnotations())
				.filter(annotation -> requestMappingAnnotations.contains(annotation.getAnnotationType()));
	}

	@Nullable
	public static Annotation findParameterAnnotation(CtParameter<?> parameter) {
		Annotation result;
		{
			List<? extends Annotation> annotations = List.of(
							PathVariable.class,
							RequestBody.class,
							RequestHeader.class,
							RequestParam.class)
					.map(parameter::getAnnotation)
					.filter(Objects::nonNull);
			//
			if (annotations.size() > 1) {
				throw new IllegalArgumentException();
			}
			//
			result = annotations.getOrNull();
		}
		return result;
	}

	@Nullable
	public static CtAnnotation<?> findParameterAnnotation2(CtParameter<?> parameter) {
		CtAnnotation<?> result;
		{
			List<? extends CtAnnotation<?>> annotations = List.of(
							PathVariable.class,
							RequestBody.class,
							RequestHeader.class,
							RequestParam.class)
					.map(annotation -> parameter.getAnnotation(SpoonStaticFactory.createReference(annotation)))
					.filter(Objects::nonNull);
			//
			if (annotations.size() > 1) {
				throw new IllegalArgumentException();
			}
			//
			result = annotations.getOrNull();
		}
		return result;
	}

	@Nullable
	public static Annotation getRequestMappingAnnotation(AnnotatedElement element) {
		Annotation result;
		{
			List<? extends Annotation> annotations = List.of(
							DeleteMapping.class,
							GetMapping.class,
							PatchMapping.class,
							PostMapping.class,
							PutMapping.class,
							RequestMapping.class)
					.map(element::getAnnotation)
					.filter(Objects::nonNull);
			//
			if (annotations.size() > 1) {
				throw new IllegalArgumentException();
			}
			result = annotations.getOrNull();
		}
		return result;
	}

	@Nullable
	public static Annotation getRequestMappingAnnotation(CtElement element) {
		Annotation result;
		{
			List<? extends Annotation> annotations = List.of(
							DeleteMapping.class,
							GetMapping.class,
							PatchMapping.class,
							PostMapping.class,
							PutMapping.class,
							RequestMapping.class)
					.map(element::getAnnotation)
					.filter(Objects::nonNull);
			//
			if (annotations.size() > 1) {
				throw new IllegalArgumentException();
			}
			result = annotations.getOrNull();
		}
		return result;
	}

	public static boolean hasRequestMappingAnnotation(CtElement element) {
		return getRequestMappingAnnotation(element) != null;
	}

	public static boolean isRestController(CtType<?> type) {
		return type.hasAnnotation(org.springframework.web.bind.annotation.RestController.class);
	}

	public static RestParametersEntry.Kind resolveKind(CtParameter<?> parameter) {
		RestParametersEntry.Kind result;
		{
			Annotation annotation = findParameterAnnotation(parameter);
			//
			if (annotation instanceof PathVariable) {
				result = RestParametersEntry.Kind.PATH;
			} else if (annotation instanceof RequestBody) {
				result = RestParametersEntry.Kind.BODY;
			} else if (annotation instanceof RequestHeader) {
				result = RestParametersEntry.Kind.HEADER;
			} else if (annotation instanceof RequestParam) {
				result = RestParametersEntry.Kind.QUERY;
			} else {
				result = RestParametersEntry.Kind.UNDEFINED;
			}
		}
		return result;
	}

	public static RequestMethod resolveMethod(Annotation requestMappingAnnotation) {
		RequestMethod result;
		{
			RequestMapping requestMapping;
			if (requestMappingAnnotation instanceof RequestMapping directRequestMapping) {
				requestMapping = directRequestMapping;
			} else {
				requestMapping = ClassUtils.resolveClass(requestMappingAnnotation).getAnnotation(RequestMapping.class);
			}
			//
			List<RequestMethod> requestMethods = List.of(requestMapping.method());
			//
			if (requestMethods.size() > 1) {
				throw new IllegalArgumentException();
			} else {
				result = requestMethods.getOrNull();
			}
		}
		return result;
	}

	public static RestParametersEntry resolveParameter(CtParameter<?> parameter) {
		return RestParametersEntry.builder()
				.datatype(CtTypeReferenceUtils.getTypeDescription(parameter.getType()))
				.description(null) // TODO
				.kind(resolveKind(parameter))
				.name(resolveParameterName(parameter))
				.required(resolveRequired(parameter))
				.build();
	}

	@Nullable
	public static String resolveParameterName(CtParameter<?> parameter) {
		String result;
		{
			Annotation annotation = findParameterAnnotation(parameter);
			//
			if (annotation instanceof PathVariable pathVariable) {
				result = ObjectUtils.pickFirstNonDefaultValue(
						"",
						pathVariable::value,
						pathVariable::name,
						parameter::getSimpleName);
			} else if (annotation instanceof RequestBody requestBody) {
				result = null;
			} else if (annotation instanceof RequestHeader requestHeader) {
				result = ObjectUtils.pickFirstNonDefaultValue(
						"",
						requestHeader::value,
						requestHeader::name,
						parameter::getSimpleName);
			} else if (annotation instanceof RequestParam requestParam) {
				result = ObjectUtils.pickFirstNonDefaultValue(
						"",
						requestParam::value,
						requestParam::name,
						parameter::getSimpleName);
			} else {
				result = "UNDEFINED";
			}
		}
		return result;
	}

	@Nullable
	public static String resolvePath(CtElement element) {
		String result;
		{
			MergedAnnotation<RequestMapping> requestMapping = MergedAnnotation.from(
					element.getAnnotation(RequestMapping.class));
			//
			if (requestMapping.isPresent()) {
				if (requestMapping.getStringArray("path").length > 1) {
					throw new IllegalArgumentException();
				}
				result = requestMapping.getStringArray("path")[0];
			} else {
				result = null;
			}
		}
		return result;
	}

	public static List<RequestMethod> resolveRequestMappingMethods(CtAnnotation<?> requestMappingAnnotation) {
		List<RequestMethod> result;
		{
			Annotation actualAnnotation = requestMappingAnnotation.getActualAnnotation();
			if (actualAnnotation instanceof DeleteMapping) {
				result = List.of(RequestMethod.DELETE);
			} else if (actualAnnotation instanceof GetMapping) {
				result = List.of(RequestMethod.GET);
			} else if (actualAnnotation instanceof PatchMapping) {
				result = List.of(RequestMethod.PATCH);
			} else if (actualAnnotation instanceof PostMapping) {
				result = List.of(RequestMethod.POST);
			} else if (actualAnnotation instanceof PutMapping) {
				result = List.of(RequestMethod.PUT);
			} else if (actualAnnotation instanceof RequestMapping requestMapping) {
				result = List.of(requestMapping.method());
			} else {
				throw new NotImplementedException(ExceptionUtils.render(requestMappingAnnotation));
			}
		}
		return result;
	}

	public static List<String> resolveRequestMappingPaths(Annotation requestMappingAnnotation) {
		List<String> result;
		{
			MergedAnnotation<?> requestMapping = MergedAnnotation.from(requestMappingAnnotation);
			//
			result = List.of(requestMapping.getStringArray("path"));
		}
		return result;
	}

	public static Boolean resolveRequired(CtParameter<?> parameter) {
		Boolean result;
		{
			Boolean requiredFromAnnotation;
			{
				CtAnnotation<?> annotation = findParameterAnnotation2(parameter);
				//
				if (annotation != null) {
					CtExpression<?> requiredValue = annotation.getValues().get("required");
					//
					if (requiredValue != null) {
						requiredFromAnnotation = requiredValue.getValueByRole(CtRole.VALUE);
					} else {
						requiredFromAnnotation = null;
					}
				} else {
					requiredFromAnnotation = null;
				}
			}
			if (requiredFromAnnotation != null) {
				result = null;
			} else {
				if (Objects.equals(
						parameter.getType().getTypeErasure(),
						SpoonStaticFactory.createReference(Optional.class))) {
					result = false;
				} else {
					result = true;
				}
			}
		}
		return result;
	}

	public static RestResultEntry resolveResult(CtMethod<?> method) {
		return RestResultEntry.builder()
				.datatype(CtTypeReferenceUtils.getTypeDescription(method.getType()))
				.kind(RestResultEntry.Kind.UNDEFINED)
				.name(null)
				.build();
	}
}
