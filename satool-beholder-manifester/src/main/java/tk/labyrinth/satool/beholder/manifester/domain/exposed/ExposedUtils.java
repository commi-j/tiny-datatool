package tk.labyrinth.satool.beholder.manifester.domain.exposed;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;
import java.util.function.Function;

public class ExposedUtils {

	@Nullable
	public static <T> List<T> mergeMultiple(
			List<@Nullable List<T>> elementLists,
			Function<T, Object> keyFunction,
			Function<List<T>, T> mergeFunction) {
		return !Objects.equals(elementLists.distinct(), List.of((List<T>) null))
				? elementLists
				.filter(Objects::nonNull)
				.flatMap(Function.identity())
				.groupBy(keyFunction)
				.values()
				.map(list -> list.size() > 1 ? mergeFunction.apply(list) : list.get(0))
				.toList()
				: null;
	}
}
