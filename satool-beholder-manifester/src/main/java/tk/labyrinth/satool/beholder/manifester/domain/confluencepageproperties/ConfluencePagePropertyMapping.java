package tk.labyrinth.satool.beholder.manifester.domain.confluencepageproperties;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;

@Builder(toBuilder = true)
@RenderPattern("$confluencePagePropertyName : $beholderObjectAttributeName")
@Value
public class ConfluencePagePropertyMapping {

	String beholderObjectAttributeName;

	String confluencePagePropertyName;
}
