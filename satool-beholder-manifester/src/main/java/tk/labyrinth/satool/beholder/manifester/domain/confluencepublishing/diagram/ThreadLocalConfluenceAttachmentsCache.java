package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;

public class ThreadLocalConfluenceAttachmentsCache {

	private static final ThreadLocal<Map<String, byte[]>> attachments = ThreadLocal.withInitial(() -> null);

	public static Map<String, byte[]> collect() {
		Map<String, byte[]> localAttachments = attachments.get();
		//
		if (localAttachments == null) {
			throw new IllegalStateException("Empty");
		}
		//
		attachments.set(null);
		//
		return localAttachments;
	}

	public static void initialize() {
		if (attachments.get() != null) {
			throw new IllegalStateException("Not empty");
		}
		//
		attachments.set(HashMap.empty());
	}

	public static void put(String filename, byte[] content) {
		Map<String, byte[]> localAttachments = attachments.get();
		//
		if (localAttachments == null) {
			throw new IllegalStateException("Empty");
		}
		if (localAttachments.containsKey(filename)) {
			throw new IllegalStateException("Key already exists");
		}
		//
		attachments.set(localAttachments.put(filename, content));
	}
}
