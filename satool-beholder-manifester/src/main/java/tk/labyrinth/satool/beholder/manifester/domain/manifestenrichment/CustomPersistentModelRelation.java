package tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class CustomPersistentModelRelation {

	ShallowAttributeReference from;

	ShallowAttributeReference to;
}
