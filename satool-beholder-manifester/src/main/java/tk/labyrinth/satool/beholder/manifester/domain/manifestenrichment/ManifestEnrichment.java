package tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.satool.beholder.manifester.artifact.NameArtifactDefinitionReference;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model
@ModelTag("repos")
@Value
@With
public class ManifestEnrichment {

	public static final String ARTIFACT_DEFINITION_REFERENCE_ATTRIBUTE_NAME = "artifactDefinitionReference";

	NameArtifactDefinitionReference artifactDefinitionReference;

	List<CustomPersistentModelRelation> persistentModelRelations;
}
