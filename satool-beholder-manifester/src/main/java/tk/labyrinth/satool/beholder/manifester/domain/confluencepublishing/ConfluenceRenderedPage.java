package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import io.vavr.collection.Map;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceRenderedPage {

	@NonNull
	Map<String, byte[]> attachments;

	@NonNull
	String content;
}
