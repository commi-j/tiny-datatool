package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClient;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceConnectorData {

	@Nullable
	String authorization;

	String baseUrl;

	ConfluenceFeignClient feignClient;
}
