package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;

import java.util.function.Function;

@LazyComponent
public class CustomModelsViewRenderer {

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		return new Label("TODO");
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		ArtifactManifest artifactManifest;

		public static class Builder {
			// Lomboked
		}
	}
}