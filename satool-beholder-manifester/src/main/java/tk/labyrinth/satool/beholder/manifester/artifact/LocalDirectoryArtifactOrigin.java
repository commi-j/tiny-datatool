package tk.labyrinth.satool.beholder.manifester.artifact;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicQualifier;

import java.nio.file.Path;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model // FIXME: We don't need them here.
@PolymorphicQualifier(rootClass = ArtifactOrigin.class, qualifierAttributeValue = "local-directory")
@Value
@With
public class LocalDirectoryArtifactOrigin implements ArtifactOrigin {

	Path path;
}
