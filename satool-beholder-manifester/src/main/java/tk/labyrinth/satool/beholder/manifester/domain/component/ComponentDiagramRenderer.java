package tk.labyrinth.satool.beholder.manifester.domain.component;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.microservice.CodeMicroserviceReference;
import tk.labyrinth.satool.beholder.manifester.domain.microservice.Microservice;
import tk.labyrinth.satool.beholder.manifester.domain.operation.CodeOperationReference;
import tk.labyrinth.satool.beholder.manifester.domain.operation.Operation;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;
import tk.labyrinth.satool.plantuml.common.model.DiagramLink;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramLink;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.function.Function;

@Deprecated
@LazyComponent
@RequiredArgsConstructor
public class ComponentDiagramRenderer {

	private final TypedObjectSearcher<Microservice> microserviceSearcher;

	private final TypedObjectSearcher<Operation> operationSearcher;

	public String render() {
		List<Microservice> microservices = microserviceSearcher.searchAll();
		//
		List<Operation> operations = operationSearcher.searchAll();
		Map<String, Operation> operationsWithCodes = operations.toMap(Operation::getCode, Function.identity());
		List<ComponentDiagramLink> componentDependencies = operations
				.flatMap(operation -> {
					List<CodeOperationReference> invokedOperationReferences = operation.getInvokes();
					return invokedOperationReferences != null
							? invokedOperationReferences.map(invokedOperationReference -> createDependency(
							operation,
							operationsWithCodes.get(invokedOperationReference.getCode()).get()))
							: List.empty();
				})
				.filter(componentDependency -> !Objects.equals(
						componentDependency.getFromCode(),
						componentDependency.getToCode()));
		//
		return """
				@startuml
				!pragma layout smetana
				%s
				%s
				@enduml
				""".formatted(
				microservices
						.map(ComponentDiagramRenderer::renderComponent)
						.mkString("\n"),
				componentDependencies
						.map(ComponentDiagramLink::render)
						.mkString("\n"));
	}

	public static ComponentDiagramLink createDependency(
			Operation fromOperation,
			Operation toOperation) {
		return ComponentDiagramLink.builder()
				.fromCode(Option.of(fromOperation.getService())
						.map(reference -> PlantumlCode.ofRaw(reference.getCode()))
						.getOrNull())
				.text(null)
				.toCode(Option.of(toOperation.getService())
						.map(reference -> PlantumlCode.ofRaw(reference.getCode()))
						.getOrNull())
				.build();
	}

	public static ComponentDiagramLink createDependency(
			CodeMicroserviceReference fromMicroserviceReference,
			CodeMicroserviceReference toMicroserviceReference,
			@Nullable String text,
			boolean horizontal) {
		return ComponentDiagramLink.builder()
				.fromCode(plantCode(fromMicroserviceReference))
				.link(DiagramLink.builder()
						.direction(horizontal ? DiagramLink.Direction.HORIZONTAL : DiagramLink.Direction.VERTICAL)
						.build())
				.text(text)
				.toCode(plantCode(toMicroserviceReference))
				.build();
	}

	public static PlantumlCode plantCode(Microservice microservice) {
		String result;
		{
			String microserviceCode = microservice.getCode();
			//
			result = microserviceCode != null ? microserviceCode : "no_code";
		}
		return PlantumlCode.ofRaw(result);
	}

	public static PlantumlCode plantCode(CodeMicroserviceReference microserviceReference) {
		return PlantumlCode.ofRaw(microserviceReference.getCode());
	}

	public static String renderComponent(Microservice microservice) {
		return renderComponent(microservice, null);
	}

	public static String renderComponent(Microservice microservice, @Nullable String text) {
		PlantumlCode plantCode = plantCode(microservice);
		//
		return text != null
				? """
				component %s [
				%s
				%s
				]"""
				.formatted(
						plantCode,
						microservice.getCode(),
						text)
				: "[%s] as %s"
				.formatted(
						microservice.getCode(),
						plantCode);
	}
}
