package tk.labyrinth.satool.beholder.manifester.domain.persistentmodel;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;

@Builder(toBuilder = true)
@RenderPattern("$javaFieldName : $javaFieldType")
@Value
public class PersistentModelAttribute {

	public static final Tag PRIMARY_KEY_TAG = Tag.of("PRIMARY_KEY");

	/**
	 * For relational databases one Java field may be represented by multiple columns.
	 */
	List<String> databaseNames;

	String javaFieldName;

	TypeDescription javaFieldType;

	@Nullable
	PersistentModelRelation relation;

	List<Tag> tags;

	public List<Tag> getTagsOrEmpty() {
		return tags != null ? tags : List.empty();
	}
}
