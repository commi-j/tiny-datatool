package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

public class ConfluencePublishingModel {
	// Confluence page structure
	// Data set

	/**
	 * Root page, page layout, identifiers.
	 */
	String confluencePublishStructure;

	Object publishDataConfiguration;
}
