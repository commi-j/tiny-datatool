package tk.labyrinth.satool.beholder.manifester.domain.function;

import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicBase;
import tk.labyrinth.satool.beholder.manifester.domain.function.http.HttpFunctionCoordinates;

@PolymorphicBase(qualifierAttributeName = "kind")
public interface FunctionCoordinates {

	default boolean isHttp() {
		return this instanceof HttpFunctionCoordinates;
	}
}
