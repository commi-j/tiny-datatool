package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.treegrid.TreeGrid;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Width;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.ArtifactManifestIndexer;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RestEndpointGroup;
import tk.labyrinth.satool.beholder.manifester.domain.endpoint.kafka.KafkaEndpoint;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionCoordinates;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;
import tk.labyrinth.satool.beholder.manifester.domain.function.http.HttpFunctionCoordinates;
import tk.labyrinth.satool.beholder.manifester.domain.function.http.HttpFunctionViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RequestMappingMethod;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RestController;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RestEndpointViewRenderer;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class ArtifactManifestDashboardIntegrationsTabRenderer {

	private final HttpFunctionViewRenderer httpFunctionViewRenderer;

	private final RestEndpointViewRenderer restEndpointViewRenderer;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	public List<RestNode> composeRestControllerNodes(List<RestController> restControllers) {
		return restControllers.map(restController -> RestNode.builder()
				.children(restController.getMethods()
						.map(method -> RestNode.builder()
								.children(List.empty())
								.methodSignature(method.getSignature())
								.paths(ArtifactManifestIndexer.resolvePaths(method.getPaths()))
								.requestMappingMethod(method)
								.restController(restController)
								.uid(UUID.randomUUID())
								.build()))
				.classSignature(restController.getSignature())
				.paths(restController.getPath() != null
						? ArtifactManifestIndexer.resolvePaths(List.of(restController.getPath()))
						: null)
				.restController(restController)
				.uid(UUID.randomUUID())
				.build());
	}

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		//
		layout.add(renderHttpFunctions(properties.getFunctions()
				.filter(ManifestFunction::isHttp)));
		layout.add(renderRestOld(properties.getRestControllers()));
		layout.add(renderKafkaOld(properties.getKafkaEndpoints()));
		//
		return layout;
	}

	public Component renderHttpFunctions(List<ManifestFunction> httpFunctions) {
		Details result = new Details("HTTP (%s)".formatted(httpFunctions.size()));
		{
			Grid<ManifestFunction> grid = new Grid<>();
			{
				grid.setAllRowsVisible(true);
				grid.setSelectionMode(Grid.SelectionMode.NONE);
				grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			}
			{
				grid
						.addComponentColumn(item -> GridUtils.renderValueList(item.getCoordinateses()
								.filter(FunctionCoordinates::isHttp)
								.map(HttpFunctionCoordinates.class::cast)
								.map(HttpFunctionCoordinates::getMethod)))
						.setFlexGrow(1)
						.setHeader("Method")
						.setResizable(true);
				grid
						.addComponentColumn(item -> GridUtils.renderValueList(item.getCoordinateses()
								.filter(FunctionCoordinates::isHttp)
								.map(HttpFunctionCoordinates.class::cast)
								.map(HttpFunctionCoordinates::getPath)))
						.setFlexGrow(3)
						.setHeader("Path")
						.setResizable(true);
				grid
						.addColumn(ManifestFunction::getName)
						.setFlexGrow(3)
						.setHeader("Name")
						.setResizable(true);
				grid
						.addColumn(ManifestFunction::getAccessControl)
						.setFlexGrow(2)
						.setHeader("Access Control")
						.setResizable(true);
				grid
						.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
								ToVaadinComponentRendererRegistry.Context.builder()
										.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
												MethodFullSignature.class))
										.hints(List.empty())
										.build(),
								item.getJavaSignature()))
						.setFlexGrow(4)
						.setHeader("Signature")
						.setResizable(true);
			}
			{
				grid.addItemClickListener(event -> {
					Component component = httpFunctionViewRenderer.render(event.getItem());
					StyleUtils.setCssProperty(component.getElement(), Width.ofViewportWidth(80));
					//
					ConfirmationViews.showComponentDialog(component);
				});
				//
			}
			{
				grid.setItems(httpFunctions.asJava());
			}
			result.setContent(grid);
		}
		return result;
	}

	public Component renderKafkaOld(List<KafkaEndpoint> kafkaEndpoints) {
		Details result = new Details("Kafka (%s)".formatted(kafkaEndpoints.size()));
		{
			Grid<KafkaEndpoint> kafkaGrid = new Grid<>();
			//
			kafkaGrid.setAllRowsVisible(true);
			kafkaGrid.setSelectionMode(Grid.SelectionMode.NONE);
			kafkaGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			//
			kafkaGrid
					.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
							ToVaadinComponentRendererRegistry.Context.builder()
									.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
											MethodFullSignature.class))
									.hints(List.empty())
									.build(),
							item.getSignature()))
					.setHeader("Signature")
					.setResizable(true);
			kafkaGrid
					.addColumn(KafkaEndpoint::getInboundTopic)
					.setHeader("Inbound Topic")
					.setResizable(true);
			kafkaGrid
					.addColumn(KafkaEndpoint::getInteractionKind)
					.setHeader("Interaction Kind")
					.setResizable(true);
			//
			kafkaGrid.setItems(kafkaEndpoints.asJava());
			//
			result.setContent(kafkaGrid);
		}
		return result;
	}

	public Component renderRestOld(List<RestController> restControllers) {
		Details result = new Details("REST (%s)".formatted(
				restControllers.flatMap(RestController::getMethods).size()));
		{
			TreeGrid<RestNode> restGrid = new TreeGrid<>();
			//
			restGrid.setAllRowsVisible(true);
			restGrid.setMultiSort(true);
			restGrid.setSelectionMode(Grid.SelectionMode.NONE);
			restGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			//
			restGrid
					.addComponentHierarchyColumn(item -> {
						Component localResult;
						{
							Object value = ObjectUtils.orElse(item.getMethodSignature(), item.getClassSignature());
							//
							if (value instanceof ClassSignature) {
								CssHorizontalLayout nameLayout = new CssHorizontalLayout();
								StyleUtils.setCssProperty(nameLayout, WhiteSpace.PRE_WRAP);
								//
								nameLayout.add(toVaadinComponentRendererRegistry.render(
										ToVaadinComponentRendererRegistry.Context.builder()
												.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
														value.getClass()))
												.hints(List.empty())
												.build(),
										value));
								nameLayout.add(" (%s)".formatted(item.getChildren().size()));
								//
								localResult = nameLayout;
							} else {
								localResult = toVaadinComponentRendererRegistry.render(
										ToVaadinComponentRendererRegistry.Context.builder()
												.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
														value.getClass()))
												.hints(List.empty())
												.build(),
										value);
							}
						}
						return localResult;
					})
					.setHeader("Name")
					.setResizable(true)
					.setSortable(true);
			restGrid
					.addColumn(item -> Optional.ofNullable(item.getRequestMappingMethod())
							.map(RequestMappingMethod::getMethod)
							.orElse(null))
					.setHeader("Method")
					.setResizable(true)
					.setSortable(true);
			restGrid
					.addComponentColumn(item -> GridUtils.renderValueList(
							item.getPaths() != null ? item.getPaths() : List.empty()))
					.setHeader("Path")
					.setResizable(true)
					.setSortable(true);
			//
			restGrid.addItemClickListener(event -> {
				if (event.getItem().getRequestMappingMethod() != null) {
					Component component = restEndpointViewRenderer.render(builder -> builder
							.requestMappingMethod(event.getItem().getRequestMappingMethod())
							.restController(event.getItem().getRestController())
							.build());
					StyleUtils.setCssProperty(component.getElement(), Width.ofViewportWidth(80));
					//
					ConfirmationViews.showComponentDialog(component);
				}
			});
			//
			restGrid.setItems(
					composeRestControllerNodes(restControllers).asJava(),
					item -> item.getChildren().asJava());
			//
			result.setContent(restGrid);
		}
		return result;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		List<ManifestFunction> functions;

		@Deprecated
		List<KafkaEndpoint> kafkaEndpoints;

		List<DisplayableModel> models;

		@Deprecated
		List<RestController> restControllers;

		List<RestEndpointGroup> restEndpointGroups;

		public static class Builder {
			// Lomboked
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class RestNode {

		List<RestNode> children;

		@Nullable
		ClassSignature classSignature;

		@Nullable
		MethodSimpleSignature methodSignature;

		@Nullable
		List<String> paths;

		@Nullable
		RequestMappingMethod requestMappingMethod;

		RestController restController;

		UUID uid;
	}
}