package tk.labyrinth.satool.beholder.manifester.domain.function.http;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.http.HttpMethod;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicQualifier;
import tk.labyrinth.satool.beholder.manifester.domain.function.FunctionCoordinates;

@Builder(builderClassName = "Builder", toBuilder = true)
@PolymorphicQualifier(qualifierAttributeValue = "HTTP", rootClass = FunctionCoordinates.class)
@Value
@With
public class HttpFunctionCoordinates implements FunctionCoordinates {

	/**
	 * Declaration may be omitted.
	 */
	@Nullable
	HttpMethod method;

	String path;
}
