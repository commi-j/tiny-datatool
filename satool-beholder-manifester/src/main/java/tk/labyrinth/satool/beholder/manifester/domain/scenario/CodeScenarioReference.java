package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class CodeScenarioReference implements Reference<Scenario> {

	String code;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Scenario.MODEL_CODE,
				Scenario.CODE_ATTRIBUTE_NAME,
				code);
	}

	public static CodeScenarioReference from(Scenario object) {
		return CodeScenarioReference.of(object.getCode());
	}

	@JsonCreator
	public static CodeScenarioReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Scenario.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Scenario.CODE_ATTRIBUTE_NAME));
	}
}
