package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

import javax.annotation.CheckForNull;

@Builder(toBuilder = true)
@Model
@Value
public class MavenModuleDependencyIndex implements ObjectIndex<MavenModule> {

	public static final String TO_REFERENCE_ATTRIBUTE_NAME = "toReference";

	UidReference<MavenModule> fromReference;

	@CheckForNull
	String scope;

	GroupIdAndArtifactIdMavenModuleReference toReference;

	@CheckForNull
	String type;

	@CheckForNull
	String version;

	@Override
	public Reference<MavenModule> getTargetReference() {
		return fromReference;
	}
}
