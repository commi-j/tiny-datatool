package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import javax.annotation.CheckForNull;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceBodyMergeStrategy {

	String action;

	@CheckForNull
	String parameter;
}
