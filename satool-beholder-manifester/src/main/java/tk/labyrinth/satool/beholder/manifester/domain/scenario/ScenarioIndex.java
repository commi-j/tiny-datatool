package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.simple.hyperlink.Hyperlink;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.component.object.custom.CustomizeTable;
import tk.labyrinth.satool.beholder.manifester.domain.component.ScenarioPrefixComponentReference;

import java.time.Instant;

@Builder(toBuilder = true)
@CustomizeTable(
		attributesOrder = {
				"componentReference",
				"code",
				"nameAndConfluenceLink",
				"dependencyReferences",
				"updatedAt",
		},
		hideNotDeclaredAttributes = true)
@Model("scenarioindex")
@ModelTag("documentary")
@Value
public class ScenarioIndex implements ObjectIndex<Scenario> {

	String code;

	ScenarioPrefixComponentReference componentReference;

	List<ConfluenceSignatureScenarioReference> dependencyReferences;

	Hyperlink nameAndConfluenceLink;

	UidReference<Scenario> targetReference;

	Instant updatedAt;
}
