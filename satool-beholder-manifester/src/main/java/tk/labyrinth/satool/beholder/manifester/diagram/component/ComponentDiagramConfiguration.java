package tk.labyrinth.satool.beholder.manifester.diagram.component;

import lombok.Value;

@Value
public class ComponentDiagramConfiguration {
	// group
	// component
	//
	// dependency
	// dependency.from
	// dependency.to
}
