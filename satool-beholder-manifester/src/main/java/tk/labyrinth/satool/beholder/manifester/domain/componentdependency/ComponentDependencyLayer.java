package tk.labyrinth.satool.beholder.manifester.domain.componentdependency;

public enum ComponentDependencyLayer {
	BUILD,
	INFRASTRUCTURE,
	LOGICAL,
}
