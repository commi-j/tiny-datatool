package tk.labyrinth.satool.beholder.manifester.domain.restcontroller;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.treegrid.TreeGrid;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.ArtifactManifestIndexer;

import java.util.function.Function;

@Deprecated
@LazyComponent
@RequiredArgsConstructor
public class RestEndpointViewRenderer {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				List<String> paths = ArtifactManifestIndexer.resolvePaths(properties.getRequestMappingMethod()
						.getPaths());
				if (paths.size() > 1) {
					layout.add(new Span("Endpoints:"));
					paths.forEach(path -> layout.add(
							new Span("%s %s".formatted(properties.getRequestMappingMethod().getMethod(), path))));
				} else {
					layout.add(new Span("Endpoint: %s %s".formatted(
							properties.getRequestMappingMethod().getMethod(),
							paths.get(0))));
				}
			}
			{
				CssHorizontalLayout signatureLayout = new CssHorizontalLayout();
				StyleUtils.setCssProperty(signatureLayout, WhiteSpace.PRE_WRAP);
				//
				signatureLayout.add("Java Signature: ");
				signatureLayout.add(toVaadinComponentRendererRegistry.render(
						ToVaadinComponentRendererRegistry.Context.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(MethodFullSignature.class))
								.hints(List.empty())
								.build(),
						properties.getRequestMappingMethod().getSignature()
								.toFull(properties.getRestController().getSignature().toString())));
				//
				layout.add(signatureLayout);
			}
			{
				CssVerticalLayout parametersLayout = new CssVerticalLayout();
				{
					parametersLayout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					{
						parametersLayout.add(new Span("Parameters"));
					}
					{
						TreeGrid<RestParametersEntry> parametersGrid = new TreeGrid<>();
						parametersGrid.setAllRowsVisible(true);
						//
						parametersGrid
								.addColumn(RestParametersEntry::getKind)
								.setHeader("Kind")
								.setResizable(true);
						parametersGrid
								.addColumn(RestParametersEntry::getName)
								.setHeader("Name")
								.setResizable(true);
						parametersGrid
								.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
										ToVaadinComponentRendererRegistry.Context.builder()
												.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
														TypeDescription.class))
												.hints(List.empty())
												.build(),
										item.getDatatype()))
								.setHeader("Type")
								.setResizable(true);
						parametersGrid
								.addColumn(RestParametersEntry::getRequired)
								.setHeader("Required")
								.setResizable(true);
						parametersGrid
								.addColumn(RestParametersEntry::getDescription)
								.setHeader("Description")
								.setResizable(true);
						//
						parametersLayout.add(parametersGrid);
						//
						parametersGrid.setItems(
								properties.getRequestMappingMethod().getParameters().asJava(),
								item -> java.util.List.of());
					}
				}
				layout.add(parametersLayout);
			}
			{
				CssVerticalLayout resultLayout = new CssVerticalLayout();
				{
					resultLayout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					{
						resultLayout.add(new Span("Result"));
					}
					{
						TreeGrid<RestResultEntry> resultGrid = new TreeGrid<>();
						resultGrid.setAllRowsVisible(true);
						//
						resultGrid
								.addColumn(RestResultEntry::getKind)
								.setHeader("Kind")
								.setResizable(true);
						resultGrid
								.addColumn(RestResultEntry::getName)
								.setHeader("Name")
								.setResizable(true);
						resultGrid
								.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
										ToVaadinComponentRendererRegistry.Context.builder()
												.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(
														TypeDescription.class))
												.hints(List.empty())
												.build(),
										item.getDatatype()))
								.setHeader("Type")
								.setResizable(true);
						resultGrid
								.addColumn(RestResultEntry::getDescription)
								.setHeader("Description")
								.setResizable(true);
						//
						resultLayout.add(resultGrid);
						//
						resultGrid.setItems(
								properties.getRequestMappingMethod().getResult().asJava(),
								item -> java.util.List.of());
					}
				}
				layout.add(resultLayout);
			}
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		RequestMappingMethod requestMappingMethod;

		RestController restController;

		public static class Builder {
			// Lomboked
		}
	}
}