package tk.labyrinth.satool.beholder.manifester.domain.component;

import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;

@Builder(toBuilder = true)
@Value
public class RenderableComponentDependency {

	PlantumlCode fromCode;

	boolean horizontal;

	@Nullable
	String text;

	PlantumlCode toCode;

	public String render() {
		return "[%s] %s [%s]%s".formatted(
				fromCode,
				horizontal ? "->" : "-->",
				toCode,
				text != null ? " : " + text : "");
	}
}
