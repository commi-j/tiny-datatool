package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.simple.hyperlink.Hyperlink;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectindex.Indexer;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

import javax.annotation.CheckForNull;

@LazyComponent
public class ScenarioIndexer implements Indexer<Scenario, ScenarioIndex> {

	@Override
	public List<ScenarioIndex> createIndices(Scenario target) {
		return List.of(ScenarioIndex.builder()
				.code(target.getCode())
				.componentReference(target.getComponentReference())
				.dependencyReferences(target.getDependencyReferences())
				.nameAndConfluenceLink(Hyperlink.of(
						confluenceSignatureToConfluenceLink(target.getConfluenceSignature()),
						target.getName()))
				.targetReference(createTargetReference(target))
				.updatedAt(target.getUpdatedAt())
				.build());
	}

	@Override
	public UidReference<Scenario> createTargetReference(Scenario target) {
		return UidReference.of(Scenario.MODEL_CODE, target.getUid());
	}

	/**
	 * {host}/rest/api/content/{pageId})<br>
	 * {host}/pages/viewpage.action?pageId={pageId}<br>
	 *
	 * @param confluenceSignature nullable
	 *
	 * @return nullable
	 */
	@CheckForNull
	public static String confluenceSignatureToConfluenceLink(@CheckForNull String confluenceSignature) {
		return confluenceSignature != null
				? confluenceSignature.replace(
				"/rest/api/content/",
				"/pages/viewpage.action?pageId=")
				: null;
	}
}
