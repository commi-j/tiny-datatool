package tk.labyrinth.satool.beholder.manifester.util;

import org.checkerframework.checker.nullness.qual.Nullable;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtElement;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.satool.spoon.SpoonStaticFactory;

import java.lang.annotation.Annotation;
import java.util.Objects;
import java.util.Optional;

public class JakartaJavaxUtils {

	@Nullable
	public static CtAnnotation<?> findAnnotation(CtElement element, Class<? extends Annotation> annotationClass) {
		return Optional.ofNullable(element.getAnnotation(SpoonStaticFactory.createReference(annotationClass)))
				.orElseGet(() -> element.getAnnotation(SpoonStaticFactory.createReference(ClassUtils.getInferred(
						annotationClass.getName().replace("jakarta", "javax")))));
	}

	public static CtAnnotation<?> getAnnotation(CtElement element, Class<? extends Annotation> annotationClass) {
		return Objects.requireNonNull(findAnnotation(element, annotationClass));
	}

	public static boolean hasAnnotation(CtElement element, Class<? extends Annotation> annotationClass) {
		return element.hasAnnotation(annotationClass) ||
				element.hasAnnotation(ClassUtils.getInferred(annotationClass.getName().replace("jakarta", "javax")));
	}

	public static boolean matches(TypeSignature typeSignature, Class<? extends Annotation> annotationClass) {
		return Objects.equals(typeSignature, TypeSignature.of(annotationClass)) ||
				Objects.equals(
						typeSignature,
						TypeSignature.of(ClassUtils.getInferred(annotationClass.getName().replace(
								"jakarta",
								"javax"))));
	}
}
