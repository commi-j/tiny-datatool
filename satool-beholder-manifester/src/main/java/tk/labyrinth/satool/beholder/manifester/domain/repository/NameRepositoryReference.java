package tk.labyrinth.satool.beholder.manifester.domain.repository;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class NameRepositoryReference implements Reference<Repository> {

	String name;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Repository.MODEL_CODE,
				Repository.NAME_ATTRIBUTE_NAME,
				name);
	}

	public static NameRepositoryReference from(Repository object) {
		return NameRepositoryReference.of(object.getName());
	}

	@JsonCreator
	public static NameRepositoryReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Repository.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Repository.NAME_ATTRIBUTE_NAME));
	}
}
