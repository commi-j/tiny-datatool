package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import com.vaadin.flow.spring.annotation.SpringComponent;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.context.annotation.Lazy;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.manifester.domain.component.Component;
import tk.labyrinth.satool.beholder.manifester.domain.component.RenderableComponent;
import tk.labyrinth.satool.beholder.manifester.domain.component.RenderableComponentDependency;
import tk.labyrinth.satool.beholder.manifester.domain.component.ScenarioPrefixComponentReference;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.function.Function;

@Lazy
@RequiredArgsConstructor
@SpringComponent
public class ScenarioComponentDiagramDescriptionBuilder {

	@SmartAutowired
	private TypedObjectSearcher<Component> componentSearcher;

	@SmartAutowired
	private TypedObjectSearcher<Scenario> scenarioSearcher;

	private RenderableComponent createRenderableComponent(
			@CheckForNull ScenarioPrefixComponentReference componentReference,
			boolean highlight) {
		RenderableComponent result;
		{
			if (componentReference != null) {
				Component component = componentSearcher.findSingle(componentReference);
				String name = component != null ? component.getName() : componentReference.getScenarioPrefix();
				result = RenderableComponent.builder()
						.code(PlantumlCode.ofRaw(name))
						.colour(highlight ? "lightblue" : null)
						.name(name)
						.text(null)
						.build();
			} else {
				result = RenderableComponent.builder()
						.code(PlantumlCode.ofValid("null"))
						.colour(highlight ? "lightblue" : null)
						.name("Null")
						.build();
			}
		}
		return result;
	}

	@CheckForNull
	private ScenarioPrefixComponentReference resolveComponentReference(
			ResolvedReference<Scenario, ?> resolvedScenarioReference) {
		return resolvedScenarioReference.findSingle() != null
				? resolvedScenarioReference.getSingle().getComponentReference()
				: null;
	}

	public String build(
			Scenario mainScenario,
			List<ScenarioTool.ScenarioInvocationStep> upstreamInvocationTree,
			List<ScenarioTool.ScenarioInvocationStep> downstreamInvocationTree) {
		Map<UnresolvedComponentDependency, List<ScenarioTool.ScenarioInvocationStep>> componentDependenciesWithSteps =
				upstreamInvocationTree.appendAll(downstreamInvocationTree).groupBy(entry -> new UnresolvedComponentDependency(
						resolveComponentReference(entry.getFrom()),
						resolveComponentReference(entry.getTo())));
		//
		Map<ScenarioPrefixComponentReference, RenderableComponent> componentReferenceToRenderableComponentMap =
				componentDependenciesWithSteps
						.keySet()
						.flatMap(pair -> Stream.of(pair.getFrom(), pair.getTo()))
						.distinct()
						.toLinkedMap(
								Function.identity(),
								componentReference -> createRenderableComponent(
										componentReference,
										Objects.equals(componentReference, mainScenario.getComponentReference())));
		Map<ScenarioPrefixComponentReference, PlantumlCode> componentReferenceToPlantUmlComponentCodeMap =
				componentReferenceToRenderableComponentMap.toMap(Tuple2::_1, entry -> entry._2().getCode());
		//
		return """
				@startuml
				!pragma layout smetana
								
				%s
								
				%s
								
				@enduml"""
				.formatted(
						componentReferenceToRenderableComponentMap
								.map(entry -> entry._2().withText(renderInternalScenarios(
										componentDependenciesWithSteps,
										entry._1())))
								.map(RenderableComponent::render)
								.mkString("\n"),
						componentDependenciesWithSteps
								.filter(group -> !Objects.equals(group._1().getFrom(), group._1().getTo()))
								.map(group -> renderInvocationEntryGroup(
										componentReferenceToPlantUmlComponentCodeMap,
										group._1(),
										group._2()))
								.mkString("\n"));
	}

	@Nullable
	private static String renderInvocationEntries(
			List<ScenarioTool.ScenarioInvocationStep> invocationEntries,
			boolean escapeLineBreak) {
		String result;
		{
			if (invocationEntries != null) {
				result = invocationEntries
						.map(ScenarioTool.ScenarioInvocationStep::renderShortTo)
						.mkString(escapeLineBreak ? "\\n" : "\n");
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static String renderInternalScenarios(
			Map<UnresolvedComponentDependency, List<ScenarioTool.ScenarioInvocationStep>> groupedByServiceEntries,
			ScenarioPrefixComponentReference componentReference) {
		List<ScenarioTool.ScenarioInvocationStep> entries = groupedByServiceEntries
				.get(new UnresolvedComponentDependency(componentReference, componentReference))
				.getOrNull();
		//
		return renderInvocationEntries(entries, false);
	}

	public static String renderInvocationEntryGroup(
			Map<ScenarioPrefixComponentReference, PlantumlCode> componentReferenceToPlantUmlComponentCodeMap,
			UnresolvedComponentDependency componentDependency,
			List<ScenarioTool.ScenarioInvocationStep> invocationEntries) {
		return RenderableComponentDependency.builder()
				.fromCode(componentReferenceToPlantUmlComponentCodeMap.get(componentDependency.getFrom()).get())
				.horizontal(false)
				.text(renderInvocationEntries(invocationEntries, true))
				.toCode(componentReferenceToPlantUmlComponentCodeMap.get(componentDependency.getTo()).get())
				.build()
				.render();
	}

	@Value
	public static class UnresolvedComponentDependency {

		@CheckForNull
		ScenarioPrefixComponentReference from;

		@CheckForNull
		ScenarioPrefixComponentReference to;
	}
}
