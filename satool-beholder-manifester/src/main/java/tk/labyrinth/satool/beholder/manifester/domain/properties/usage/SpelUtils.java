package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.java.util.regex.RegexUtils;

import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class SpelUtils {

	public static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("\\$\\{(?<name>.+?)(:(?<defaultValue>.+))?}");

	/**
	 * @param spelExpression non-null
	 *
	 * @return 0..n
	 */
	public static List<MatchResult> findPlaceholderValues(String spelExpression) {
		return List.ofAll(RegexUtils.findAll(spelExpression, PLACEHOLDER_PATTERN));
	}
}
