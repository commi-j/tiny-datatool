package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;
import tk.labyrinth.pandora.stores.rootobject.HasUid;
import tk.labyrinth.pandora.ui.component.object.custom.CustomizeTable;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.RepositoryReferenceAndNameGitBranchReference;
import tk.labyrinth.satool.maven.domain.MavenDependency;

import java.time.Instant;
import java.util.UUID;

@Builder(toBuilder = true)
@CustomizeTable(attributesOrder = {
		"groupId",
		"artifactId",
})
@Model(code = MavenModule.MODEL_CODE)
@ModelTag("repos")
@RenderPattern("$groupId:$artifactId")
@Value
public class MavenModule implements HasUid {

	public static final String ARTIFACT_ID_ATTRIBUTE_NAME = "artifactId";

	public static final String BRANCH_REFERENCE_ATTRIBUTE_NAME = "branchReference";

	public static final String GROUP_ID_ATTRIBUTE_NAME = "groupId";

	public static final String MODEL_CODE = "mavenmodule";

	public static final String PATH_ATTRIBUTE_NAME = "path";

	String artifactId;

	RepositoryReferenceAndNameGitBranchReference branchReference;

	List<MavenDependency> dependencies;

	String groupId;

	List<String> modules;

	BranchReferenceAndPathMavenModuleReference ownerReference;

	/**
	 * '/' for root modules, '/$module/' for named module.
	 */
	String path;

	UUID uid;

	Instant updatedAt;

	public List<MavenDependency> getDependenciesOrEmpty() {
		return dependencies != null ? dependencies : List.empty();
	}

	public String getGroupIdArtifactId() {
		return "%s:%s".formatted(groupId, artifactId);
	}
}
