package tk.labyrinth.satool.beholder.manifester.dependencymanager;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.ArtifactSignature;

import java.net.URL;
import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(MavenArtifactRepositoryStatus.MODEL_CODE)
@RenderPattern("$artifactSignature@$repositoryUrl")
@Value
@With
public class MavenArtifactRepositoryStatus {

	public static final String ARTIFACT_SIGNATURE_ATTRIBUTE_NAME = "artifactSignature";

	public static final String MODEL_CODE = "mavenartifactrepositorystatus";

	public static final String REPOSITORY_URL_ATTRIBUTE_NAME = "repositoryUrl";

	ArtifactSignature artifactSignature;

	Instant checkedAt;

	Boolean exists;

	URL repositoryUrl;
}
