package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedAnnotation;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedAnnotationValue;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedField;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedType;
import tk.labyrinth.satool.beholder.manifester.domain.validations.ValidationsManifestContributor;
import tk.labyrinth.satool.beholder.manifester.util.JakartaJavaxUtils;

import java.util.Objects;
import java.util.Optional;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class DisplayableModel {

	@NonNull
	List<DisplayableModelAttribute> attributes;

	@Nullable
	List<String> enumValues;

	@NonNull
	ClassSignature javaSignature;

	@NonNull
	List<Tag> tags;

	public List<ClassSignature> collectClassSignatures() {
		return attributes
				.map(DisplayableModelAttribute::getDatatype)
				.flatMap(TypeDescription::breakDown)
				.map(TypeDescription::getSignature)
				.filter(TypeSignature::isDeclaredOrVariable)
				.map(TypeSignature::getClassSignature)
				.prepend(javaSignature)
				.distinct();
	}

	public boolean isEnum() {
		return tags.contains(Tag.of("enum"));
	}

	@Nullable
	public static String annotationToConstraint(ExposedAnnotation exposedAnnotation) {
		String result;
		{
			TypeSignature signature = exposedAnnotation.getSignature();
			//
			if (ValidationsManifestContributor.PREDEFINED_ANNOTATION_CLASSES
					.exists(cl -> JakartaJavaxUtils.matches(signature, cl))) {
				result = "@%s".formatted(exposedAnnotation.getSignature().getLongName());
			} else {
				result = null;
			}
		}
		return result;
	}

	public static List<String> computeComments(ExposedField field) {
		List<String> result;
		{
			List<String> fieldComments;
			{
				if (field.getAnnotations() != null) {
					List<String> annotationComments = field.getAnnotations()
							.map(DisplayableModel::fieldAnnotationToComment)
							.filter(Objects::nonNull);
					//
					fieldComments = !annotationComments.isEmpty() ? annotationComments : List.empty();
				} else {
					fieldComments = List.empty();
				}
			}
			List<String> parametersComments;
			{
				// TODO: Not supported as for now.
				//
				if (field.getType() != null && field.getType().getParameters() != null) {
					parametersComments = List.empty();
				} else {
					parametersComments = List.empty();
				}
			}
			List<String> comments = fieldComments.appendAll(parametersComments);
			//
			result = !comments.isEmpty() ? comments : List.of("-");
		}
		return result;
	}

	public static List<String> computeConstraints(ExposedField field) {
		List<String> result;
		{
			List<String> fieldConstraints;
			{
				if (field.getAnnotations() != null) {
					List<String> annotationConstraints = field.getAnnotations()
							.map(DisplayableModel::annotationToConstraint)
							.filter(Objects::nonNull);
					//
					fieldConstraints = !annotationConstraints.isEmpty() ? annotationConstraints : List.empty();
				} else {
					fieldConstraints = List.empty();
				}
			}
			List<String> parametersConstraints;
			{
				if (field.getType() != null && field.getType().getParameters() != null) {
					if (Objects.equals(field.getType().getCore(), TypeSignature.of(java.util.List.class))) {
						ExposedType firstParameter = field.getType().getParameters().get(0);
						//
						List<String> constraints = firstParameter.getAnnotationsOrEmpty()
								.map(DisplayableModel::annotationToConstraint);
						//
						parametersConstraints = !constraints.isEmpty()
								? List.of("Elements: %s".formatted(constraints.single()))
								: List.empty();
					} else if (Objects.equals(field.getType().getCore(), TypeSignature.of(Optional.class))) {
						ExposedType firstParameter = field.getType().getParameters().get(0);
						//
						parametersConstraints = firstParameter.getAnnotationsOrEmpty()
								.map(DisplayableModel::annotationToConstraint);
					} else {
						parametersConstraints = List.empty();
					}
				} else {
					parametersConstraints = List.empty();
				}
			}
			List<String> constraints = fieldConstraints.appendAll(parametersConstraints);
			//
			result = !constraints.isEmpty() ? constraints : List.of("-");
		}
		return result;
	}

	public static DisplayableModel createFrom(ExposedClass exposedClass) {
		return DisplayableModel.builder()
				.attributes(exposedClass.getFields().map(field -> DisplayableModelAttribute.builder()
						.comments(computeComments(field))
						.constraints(computeConstraints(field))
						.datatype(field.getType() != null ? field.getType().toTypeDescription() : null)
						.name(field.getName())
						.build()))
				.enumValues(exposedClass.getEnumValues())
				.javaSignature(exposedClass.getSignature())
				.tags(exposedClass.getTagsOrEmpty())
				.build();
	}

	@Nullable
	public static String fieldAnnotationToComment(ExposedAnnotation exposedAnnotation) {
		String result;
		{
			TypeSignature signature = exposedAnnotation.getSignature();
			//
			if (ValidationsManifestContributor.PREDEFINED_ANNOTATION_CLASSES
					.exists(cl -> JakartaJavaxUtils.matches(signature, cl))) {
				result = exposedAnnotation.getValues()
						.filter(value -> Objects.equals(value.getName(), "message"))
						.map(ExposedAnnotationValue::getValue)
						.getOrNull();
			} else {
				result = null;
			}
		}
		return result;
	}
}
