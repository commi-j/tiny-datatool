package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;

public class DisplayableModelUtils {

	public static List<DisplayableModel> selectRecursivelyWithClasses(
			List<DisplayableModel> displayableModels,
			List<ClassSignature> classSignatures) {
		List<DisplayableModel> selectedModels = displayableModels
				.filter(displayableModel -> classSignatures.contains(displayableModel.getJavaSignature()));
		//
		return !selectedModels.isEmpty()
				? selectedModels.appendAll(selectRecursivelyWithClasses(
				displayableModels.removeAll(selectedModels),
				selectedModels.flatMap(DisplayableModel::collectClassSignatures)))
				: List.empty();
	}

	public static List<DisplayableModel> selectRecursivelyWithTypes(
			List<DisplayableModel> displayableModels,
			List<TypeDescription> typeDescriptions) {
		return selectRecursivelyWithClasses(
				displayableModels,
				typeDescriptions
						.flatMap(TypeDescription::breakDown)
						.map(TypeDescription::getSignature)
						.filter(TypeSignature::isDeclaredOrVariable)
						.map(TypeSignature::getClassSignature));
	}
}
