package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.io.ByteArrayResource;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.core.NamedResourceWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate.KeyConfluencePageTemplateReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram.ThreadLocalConfluenceAttachmentsCache;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceConnectorData;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceCreateTarget;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluencePublishTarget;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model.ConfluenceUpdateTarget;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceConnector;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceFeignClient;
import tk.labyrinth.satool.confluenceconnector.api.ConfluenceGetContentChildAttachmentQuery;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceBody;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceChunk;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceContent;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceSpace;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceStorage;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceVersion;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class ConfluencePublisher {

	private final ConfluenceConnector confluenceConnector;

	private final ConfluenceRenderEngine renderEngine;

	private void publishAttachments(
			ConfluenceConnectorData connectorData,
			String pageId,
			Map<String, byte[]> attachments,
			boolean deleteExisting) {
		if (deleteExisting) {
			ConfluenceFeignClient feignClient = connectorData.getFeignClient();
			//
			int start = 0;
			boolean depleted = false;
			do {
				ConfluenceChunk<ConfluenceContent> chunk = feignClient.getContentChildAttachment(
						connectorData.getAuthorization(),
						pageId,
						ConfluenceGetContentChildAttachmentQuery.builder()
								.start(start)
								.build());
				//
				chunk.getResults().forEach(result -> feignClient.deleteContent(
						connectorData.getAuthorization(),
						result.getId()));
				//
				if (chunk.getSize() == 0) {
					depleted = true;
				}
			}
			while (!depleted);
		}
		//
		attachments.forEach(tuple -> confluenceConnector.postContentChildAttachment(
				connectorData.getBaseUrl(),
				connectorData.getAuthorization(),
				pageId,
				new NamedResourceWrapper(tuple._1(), new ByteArrayResource(tuple._2())),
				null));
	}

	/**
	 * @param connectorData         non-null
	 * @param locator               non-null, locates page to update
	 * @param parentContentId       nullable, serves as a context to locate page
	 * @param title                 nullable, can be omitted if updating page with no title change
	 * @param pageTemplateReference non-null
	 * @param data                  non-null
	 *
	 * @return non-null, id of affected page
	 */
	public String publishPage(
			ConfluenceConnectorData connectorData,
			String locator,
			@Nullable String parentContentId,
			@Nullable String title,
			KeyConfluencePageTemplateReference pageTemplateReference,
			GenericObject data) {
		String result;
		{
			Map<String, Object> model = data.unwrap();
			//
			ConfluencePublishTarget publishTarget = ConfluencePublishingUtils.getPublishTarget(
					connectorData,
					parentContentId,
					renderEngine.resolve(locator, model));
			//
			logger.info("publishTarget = {}", publishTarget);
			//
			if (title != null) {
				ConfluenceRenderedPage renderedPage = renderPage(pageTemplateReference, model);
				//
				Pair<String, Boolean> pageIdAndDeleteAttachments;
				//
				if (publishTarget instanceof ConfluenceCreateTarget createTarget) {
					ConfluenceContent confluenceContent = connectorData.getFeignClient().postContent(
							connectorData.getAuthorization(),
							ConfluenceContent.builder()
									.ancestors(List.of(ConfluenceContent.builder()
											.id(createTarget.getParentContentId())
											.build()))
									.space(ConfluenceSpace.builder()
											.key(createTarget.getParentSpaceKey())
											.build())
									.body(ConfluenceBody.builder()
											.storage(ConfluenceStorage.builder()
													.representation("storage")
													.value(renderedPage.getContent())
													.build())
											.build())
									.title(title)
									.type("page")
									.build());
					//
					pageIdAndDeleteAttachments = Pair.of(confluenceContent.getId(), false);
				} else if (publishTarget instanceof ConfluenceUpdateTarget updateTarget) {
					ConfluenceContent confluenceContent = connectorData.getFeignClient().putContent(
							connectorData.getAuthorization(),
							updateTarget.getContentId(),
							ConfluenceContent.builder()
									.body(ConfluenceBody.builder()
											.storage(ConfluenceStorage.builder()
													.representation("storage")
													.value(renderedPage.getContent())
													.build())
											.build())
									.title(title)
									.type("page")
									.version(ConfluenceVersion.builder()
											.number(updateTarget.getVersionNumber())
											.build())
									.build());
					//
					pageIdAndDeleteAttachments = Pair.of(confluenceContent.getId(), true);
				} else {
					throw new NotImplementedException();
				}
				//
				publishAttachments(
						connectorData,
						pageIdAndDeleteAttachments.getLeft(),
						renderedPage.getAttachments(),
						pageIdAndDeleteAttachments.getRight());
				//
				result = pageIdAndDeleteAttachments.getLeft();
			} else {
				if (publishTarget instanceof ConfluenceUpdateTarget updateTarget) {
//					result = updateTarget.getContentId();
					//
					// TODO: perform update
					throw new NotImplementedException();
				} else {
					// Page not create and locator is not for update -> can't determine contentId;
					result = null;
				}
			}
		}
		return result;
	}

	// TODO: Move to ConfluenceRenderer (create it first).
	public ConfluenceRenderedPage renderPage(
			KeyConfluencePageTemplateReference pageTemplateReference,
			Map<String, Object> model) {
		ConfluenceRenderedPage result;
		{
			String content;
			Map<String, byte[]> attachments;
			//
			{
				ThreadLocalConfluenceAttachmentsCache.initialize();
				try {
					content = pageTemplateReference != null
							? renderEngine.resolve(pageTemplateReference, model)
							: "";
				} finally {
					attachments = ThreadLocalConfluenceAttachmentsCache.collect();
				}
			}
			//
			result = ConfluenceRenderedPage.builder()
					.attachments(attachments)
					.content(content)
					.build();
		}
		return result;
	}
}
