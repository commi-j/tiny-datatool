package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.models.tool.builder.GenerateBuilder;
import tk.labyrinth.pandora.stores.rootobject.HasUid;
import tk.labyrinth.pandora.ui.component.object.custom.CustomizeTable;
import tk.labyrinth.satool.beholder.manifester.domain.component.ScenarioPrefixComponentReference;

import java.time.Instant;
import java.util.UUID;

@CustomizeTable(
		attributesOrder = {
				Scenario.COMPONENT_REFERENCES_ATTRIBUTE_NAME,
				Scenario.CODE_ATTRIBUTE_NAME,
				Scenario.NAME_ATTRIBUTE_NAME,
				Scenario.DEPENDENCY_REFERENCES_ATTRIBUTE_NAME,
		},
		hideNotDeclaredAttributes = true)
@GenerateBuilder
@Model(Scenario.MODEL_CODE)
public interface Scenario extends HasUid {

	String CODE_ATTRIBUTE_NAME = "code";

	String COMPONENT_REFERENCES_ATTRIBUTE_NAME = "componentReference";

	String CONFLUENCE_SIGNATURE_ATTRIBUTE_NAME = "confluenceSignature";

	String DEPENDENCY_REFERENCES_ATTRIBUTE_NAME = "dependencyReferences";

	String NAME_ATTRIBUTE_NAME = "name";

	String MODEL_CODE = "scenario";

	String getCode();

	ScenarioPrefixComponentReference getComponentReference();

	Instant getConfluenceLastUpdated();

	/**
	 * Options:
	 * - baseUrl::contentId<br>
	 * - self (current choice - {host}/rest/api/content/{pageId})<br>
	 */
	String getConfluenceSignature();

	List<ConfluenceSignatureScenarioReference> getDependencyReferences();

	String getName();

	String getTemplateVersion();

	@Override
	UUID getUid();

	Instant getUpdatedAt();
}
