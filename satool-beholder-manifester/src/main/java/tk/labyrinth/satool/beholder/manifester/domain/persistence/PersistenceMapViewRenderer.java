package tk.labyrinth.satool.beholder.manifester.domain.persistence;

import com.vaadin.flow.component.Component;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModelUtils;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RefinedPersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment.CustomPersistentModelRelation;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;
import tk.labyrinth.satool.plantuml.common.PlantumlCode;
import tk.labyrinth.satool.plantuml.objectdiagram.ObjectDiagramField;
import tk.labyrinth.satool.plantuml.objectdiagram.ObjectDiagramModel;
import tk.labyrinth.satool.plantuml.objectdiagram.ObjectDiagramObject;
import tk.labyrinth.satool.plantuml.objectdiagram.ObjectDiagramReference;
import tk.labyrinth.satool.plantuml.view.PlantumlDiagramView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.Function;

// TODO: InfomodelEntity#skus
// TODO: UserRoleEntity
// TODO: CategoryTreeEntity
// TODO: LocaleCatalogDataEntity
// TODO: SkuEntity#sku
@LazyComponent
@RequiredArgsConstructor
public class PersistenceMapViewRenderer {

	public static final Set<String> utilityFieldNames = io.vavr.collection.HashSet.of(
			"created",
			"created_at",
			"created_by",
			"createdAt",
			"createdBy",
			"insertedAt",
			"modified_at",
			"modified_by",
			"updated",
			"updated_at",
			"updated_by",
			"updatedAt",
			"updatedBy");

	private final ObjectProvider<PlantumlDiagramView> diagramViewProvider;

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		//
		PlantumlDiagramView view = diagramViewProvider.getObject();
		//
		view.setParameters(builder -> builder
				.value(createDatabaseDiagramModel(
						properties.getPersistentModels(),
						properties.getHighlightedDatabaseNames(),
						properties.getCustomPersistentModelRelations(),
						properties.getRelatedModels())
						.render())
				.build());
		//
		return view;
	}

	public static ObjectDiagramModel createDatabaseDiagramModel(
			@NonNull List<RefinedPersistentModel> persistentModels,
			@NonNull List<String> highlightedTableNames,
			@NonNull List<CustomPersistentModelRelation> customRelations,
			@NonNull List<DisplayableModel> relatedModels) {
		Map<ClassSignature, PlantumlCode> modelJavaSignatureToPlantCodeMap = persistentModels
				.toMap(
						RefinedPersistentModel::getJavaSignature,
						persistentModel -> PlantumlCode.ofRaw(persistentModel.getDatabaseName()))
				.merge(relatedModels.toMap(
						DisplayableModel::getJavaSignature,
						relatedModel -> PlantumlCode.ofRaw(relatedModel.getJavaSignature().toString())));
		//
		List<ObjectDiagramObject> rootObjects = persistentModels.map(persistentModel -> ObjectDiagramObject.builder()
				.code(PlantumlCode.ofRaw(persistentModel.getDatabaseName()))
				.colour(highlightedTableNames.contains(persistentModel.getDatabaseName()) ? "lightblue" : "lightgrey")
				.fields(persistentModel.getAttributes()
						.flatMap(attributes -> attributes.getDatabaseNames()
								.map(databaseColumnName -> Pair.of(databaseColumnName, attributes)))
						.groupBy(Pair::getLeft)
						.mapValues(pairs -> pairs.map(Pair::getRight))
						.groupBy(PersistenceMapViewRenderer::determineGroupIndex)
						.toList()
						.sortBy(Tuple2::_1)
						.flatMap(groupIndexAndMap -> List.of(groupIndexToDiagramField(groupIndexAndMap._1()))
								.appendAll(groupIndexAndMap._2()
										.map(databaseNameAndAttributes -> ObjectDiagramField.builder()
												.name("%s : %s".formatted(
														databaseNameAndAttributes._1(),
														databaseNameAndAttributes._2().get(0).getJavaFieldType().toLongForm()))
												.build())
										.sortBy(ObjectDiagramField::getName)
										.toList())))
				.name("**%s**\\n%s".formatted(
						persistentModel.getJavaSignature().getLongName(),
						persistentModel.getDatabaseName()))
				.build());
		//
		List<ObjectDiagramObject> nestedObjects = relatedModels
				.filter(relatedModel -> !relatedModel.isEnum())
				.map(relatedModel -> ObjectDiagramObject.builder()
						.code(PlantumlCode.ofRaw(relatedModel.getJavaSignature().toString()))
						.fields(relatedModel.getAttributes().map(field -> ObjectDiagramField.builder()
								.name("%s : %s".formatted(
										field.getName(),
										field.getDatatype().toLongForm()))
								.build()))
						.name("**%s**\\n(Nested)".formatted(
								relatedModel.getJavaSignature().getLongName()))
						.build());
		//
		List<ObjectDiagramObject> enumerationObjects = relatedModels
				.filter(DisplayableModel::isEnum)
				.map(relatedModel -> ObjectDiagramObject.builder()
						.code(PlantumlCode.ofRaw(relatedModel.getJavaSignature().toString()))
						.fields(relatedModel.getEnumValues().map(enumValue -> ObjectDiagramField.builder()
								.name(enumValue)
								.build()))
						.name("**%s**\\n(Enumeration)".formatted(
								relatedModel.getJavaSignature().getLongName()))
						.build());
		//
		List<ObjectDiagramReference> explicitReferences = persistentModels
				.flatMap(persistentModel -> persistentModel.getAttributes()
						.filter(attribute -> attribute.getRelation() != null &&
								attribute.getRelation().getHasRelationColumn() &&
								findPersistentModel(persistentModels, attribute.getRelation().getToTableName())
										!= null)
						.flatMap(attribute -> attribute.getDatabaseNames().map(databaseColumnName ->
								ObjectDiagramReference.builder()
										.fromCode(PlantumlCode.ofRaw(persistentModel.getDatabaseName(), databaseColumnName))
										.toCode(PlantumlCode.ofRaw(
												findPersistentModel(
														persistentModels,
														attribute.getRelation().getToTableName())
														.getDatabaseName(),
												attribute.getRelation().getToColumnName()))
										.build())));
		//
		Set<ClassSignature> encounteredClasses = persistentModels.flatMap(RefinedPersistentModel::collectClassSignatures)
				.appendAll(relatedModels.flatMap(DisplayableModel::collectClassSignatures))
				.toSet();
		List<ObjectDiagramReference> customReferences = customRelations
				.filter(customRelation -> encounteredClasses.contains(customRelation.getFrom().getModelSignature()) &&
						encounteredClasses.contains(customRelation.getTo().getModelSignature()))
				.map(customRelation -> ObjectDiagramReference.builder()
						.fromCode(modelJavaSignatureToPlantCodeMap.get(customRelation.getFrom().getModelSignature()).get()
								.appendRawSecond(customRelation.getFrom().getAttributeName()))
						.toCode(modelJavaSignatureToPlantCodeMap.get(customRelation.getTo().getModelSignature()).get()
								.appendRawSecond(customRelation.getTo().getAttributeName()))
						.build());
		//
		List<ObjectDiagramReference> rootNestedReferences = persistentModels
				.flatMap(persistentModel -> {
					PlantumlCode objectCode = PlantumlCode.ofRaw(persistentModel.getDatabaseName());
					//
					return persistentModel.getAttributesOrEmpty().map(attribute -> Tuple.of(objectCode, attribute));
				})
				.flatMap(tuple -> List.ofAll(tuple._2().getJavaFieldType().breakDown())
						.map(TypeDescription::getSignature)
						.filter(TypeSignature::isDeclaredOrVariable)
						.map(TypeSignature::getClassSignature)
						.filter(classSignature -> relatedModels.exists(relatedModel ->
								Objects.equals(relatedModel.getJavaSignature(), classSignature)))
						.map(classSignature -> ObjectDiagramReference.builder()
								.fromCode(tuple._1().appendRawSecond(tuple._2().getJavaFieldName()))
								.toCode(PlantumlCode.ofRaw(classSignature.toString()))
								.build()));
		//
		List<ObjectDiagramReference> nestedNestedReferences = relatedModels
				.flatMap(relatedModel -> {
					PlantumlCode objectCode = PlantumlCode.ofRaw(relatedModel.getJavaSignature().toString());
					//
					return relatedModel.getAttributes().map(attribute -> Tuple.of(objectCode, attribute));
				})
				.flatMap(tuple -> List.ofAll(tuple._2().getDatatype().breakDown())
						.map(TypeDescription::getSignature)
						.filter(TypeSignature::isDeclaredOrVariable)
						.map(TypeSignature::getClassSignature)
						.filter(classSignature -> relatedModels.exists(relatedClass ->
								Objects.equals(relatedClass.getJavaSignature(), classSignature)))
						.map(classSignature -> ObjectDiagramReference.builder()
								.fromCode(tuple._1().appendRawSecond(tuple._2().getName()))
								.toCode(PlantumlCode.ofRaw(classSignature.toString()))
								.build()));
		//
		return ObjectDiagramModel.builder()
				.objects(rootObjects
						.appendAll(nestedObjects)
						.appendAll(enumerationObjects))
				.references(explicitReferences
						.appendAll(customReferences)
						.appendAll(rootNestedReferences)
						.appendAll(nestedNestedReferences))
				.build();
	}

	/**
	 * 0 - primary keys;<br>
	 * 1 - business attributes;<br>
	 * 2 - utility attributes;<br>
	 *
	 * @param tuple non-null
	 *
	 * @return 0-2
	 */
	public static int determineGroupIndex(Tuple2<String, List<PersistentModelAttribute>> tuple) {
		return tuple._2().exists(attribute -> attribute.getTagsOrEmpty().contains(PersistentModelAttribute.PRIMARY_KEY_TAG))
				? 0
				: utilityFieldNames.contains(tuple._1()) ? 2 : 1;
	}

	public static RefinedPersistentModel findPersistentModel(
			List<RefinedPersistentModel> persistentModels,
			String tableName) {
		return persistentModels
				.find(persistentModelElement -> Objects.equals(
						persistentModelElement.getDatabaseName(),
						tableName))
				.getOrNull();
	}

	public static ObjectDiagramField groupIndexToDiagramField(int groupIndex) {
		return ObjectDiagramField.builder()
				.name(".. %s ..".formatted(switch (groupIndex) {
					case 0 -> "Primary Key";
					case 1 -> "Business Attributes";
					case 2 -> "Utility Attributes";
					default -> throw new NotImplementedException();
				}))
				.build();
	}

	public static List<DisplayableModel> pickRelated(
			List<DisplayableModel> displayableModels,
			List<RefinedPersistentModel> persistentModels) {
		return DisplayableModelUtils
				.selectRecursivelyWithClasses(
						displayableModels,
						persistentModels.flatMap(RefinedPersistentModel::collectClassSignatures))
				.filter(displayableModel -> !persistentModels.exists(persistentModel ->
						Objects.equals(persistentModel.getJavaSignature(), displayableModel.getJavaSignature())));
	}

	public static List<RefinedPersistentModel> pickRelated(
			List<RefinedPersistentModel> persistentModels,
			RefinedPersistentModel highlightedPersistentModel,
			int depth,
			List<CustomPersistentModelRelation> customRelations) {
		Map<ClassSignature, String> modelJavaSignatureToDatabaseNameMap = persistentModels.toMap(
				RefinedPersistentModel::getJavaSignature,
				RefinedPersistentModel::getDatabaseName);
		//
		Set<Pair<String, String>> allRelations = persistentModels
				.flatMap(persistentModel -> persistentModel.getAttributes()
						.map(PersistentModelAttribute::getRelation)
						.filter(Objects::nonNull)
						.map(relation -> Pair.of(
								persistentModel.getDatabaseName(),
								relation.getToTableName())))
				.toSet()
				.addAll(customRelations
						.filter(customRelation -> ObjectUtils.in(
								highlightedPersistentModel.getJavaSignature(),
								customRelation.getFrom().getModelSignature(),
								customRelation.getTo().getModelSignature()))
						.map(customRelation -> Pair.of(
								modelJavaSignatureToDatabaseNameMap.get(customRelation.getFrom().getModelSignature()).get(),
								modelJavaSignatureToDatabaseNameMap.get(customRelation.getTo().getModelSignature()).get())));
		//
		HashSet<RefinedPersistentModel> selectedPersistentModels = new HashSet<>();
		{
			ArrayList<Pair<RefinedPersistentModel, Integer>> modelsToProcess = new ArrayList<>();
			modelsToProcess.add(Pair.of(highlightedPersistentModel, depth));
			HashSet<String> processedTableNames = new HashSet<>();
			//
			while (!modelsToProcess.isEmpty()) {
				Pair<RefinedPersistentModel, Integer> currentPair = modelsToProcess.remove(0);
				RefinedPersistentModel currentModel = currentPair.getLeft();
				int currentDepth = currentPair.getRight();
				//
				selectedPersistentModels.add(currentModel);
				processedTableNames.add(currentModel.getDatabaseName());
				//
				if (currentDepth > 0) {
					allRelations
							.filter(relation -> ObjectUtils.in(
									currentModel.getDatabaseName(),
									relation.getLeft(),
									relation.getRight()))
							.flatMap(relation -> io.vavr.collection.HashSet.of(relation.getLeft(), relation.getRight()))
							.filter(tableName -> !processedTableNames.contains(tableName))
							.forEach(tableName -> modelsToProcess.add(Pair.of(
									persistentModels
											.find(persistentModel -> Objects.equals(
													persistentModel.getDatabaseName(),
													tableName))
											.get(),
									currentDepth - 1)));
				}
			}
		}
		//
		return List.ofAll(selectedPersistentModels);
	}

	public static List<CustomPersistentModelRelation> pickRelatedCustomRelations(
			List<CustomPersistentModelRelation> customRelations,
			List<RefinedPersistentModel> persistentModels) {
		List<ClassSignature> classSignatures = persistentModels.map(RefinedPersistentModel::getJavaSignature);
		//
		return customRelations.filter(customRelation ->
				classSignatures.contains(customRelation.getFrom().getModelSignature()) &&
						classSignatures.contains(customRelation.getTo().getModelSignature()));
	}

	public static String renderDatatype(List<PersistentModelAttribute> attributes) {
		String result;
		{
			if (attributes.size() == 1) {
				result = attributes.single().getJavaFieldType().toLongForm();
			} else {
				result = "TODO:Datatype(%s)".formatted(attributes.size());
			}
		}
		return result;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		List<CustomPersistentModelRelation> customPersistentModelRelations;

		Boolean databaseView;

		List<String> highlightedDatabaseNames;

		List<RefinedPersistentModel> persistentModels;

		@NonNull
		List<DisplayableModel> relatedModels;

		public static class Builder {
			// Lomboked
		}
	}
}