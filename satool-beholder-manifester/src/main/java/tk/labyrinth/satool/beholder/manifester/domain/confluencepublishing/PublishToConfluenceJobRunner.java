package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;
import tk.labyrinth.satool.beholder.domain.space.HasSpaceReference;
import tk.labyrinth.satool.beholder.domain.space.KeySpaceReference;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepageproperties.SpaceKeyAndLabelConfluencePagePropertiesModelReference;

import javax.annotation.CheckForNull;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class PublishToConfluenceJobRunner implements JobRunner<PublishToConfluenceJobRunner.Parameters> {

	private final ExecutionContextHandler executionContextHandler;

	@Override
	public Parameters getInitialParameters() {
		return PublishToConfluenceJobRunner.Parameters.builder()
				.spaceReference(executionContextHandler.getExecutionContext().findAttributeValueInferred(
						BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME))
				.build();
	}

	@Override
	public void run(Context<Parameters> context) {
		// TODO
	}

	@Builder(toBuilder = true)
	@Value
	public static class Parameters implements HasSpaceReference {

		String confluenceBaseUrl;

		@CheckForNull
		KeyCredentialsReference confluenceCredentialsReference;

		SpaceKeyAndLabelConfluencePagePropertiesModelReference confluencePagePropertiesModelReference;

		KeySpaceReference spaceReference;
	}
}
