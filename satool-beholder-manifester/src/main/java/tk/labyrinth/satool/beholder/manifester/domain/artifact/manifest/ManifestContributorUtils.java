package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ManifestContributorUtils {

	private static final Logger logger = LoggerFactory.getLogger(ManifestContributorUtils.class);

	/**
	 * @param javaClass non-null
	 *
	 * @return non-null
	 */
	public static String getLocationOfClassSource(@NonNull Class<?> javaClass) {
		String rawLocation = javaClass.getProtectionDomain().getCodeSource().getLocation().toString();
		//
		// TODO: Make it info and load once during lifecycle.
		logger.debug("getLocationOfClassSource: javaClass = {}, rawLocation = {}", javaClass.getName(), rawLocation);
		//
		Matcher matcher = Pattern.compile("file:(.*)").matcher(rawLocation);
		//
		if (!matcher.matches()) {
			throw new IllegalStateException(rawLocation);
		}
		//
		String group = matcher.group(1);
		//
		return group;
	}
}
