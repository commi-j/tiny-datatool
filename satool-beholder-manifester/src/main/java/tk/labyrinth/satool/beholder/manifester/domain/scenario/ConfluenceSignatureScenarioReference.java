package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class ConfluenceSignatureScenarioReference implements Reference<Scenario> {

	String confluenceSignature;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Scenario.MODEL_CODE,
				Scenario.CONFLUENCE_SIGNATURE_ATTRIBUTE_NAME,
				confluenceSignature);
	}

	public static ConfluenceSignatureScenarioReference from(Scenario object) {
		return ConfluenceSignatureScenarioReference.of(object.getConfluenceSignature());
	}

	@JsonCreator
	public static ConfluenceSignatureScenarioReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Scenario.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Scenario.CONFLUENCE_SIGNATURE_ATTRIBUTE_NAME));
	}
}
