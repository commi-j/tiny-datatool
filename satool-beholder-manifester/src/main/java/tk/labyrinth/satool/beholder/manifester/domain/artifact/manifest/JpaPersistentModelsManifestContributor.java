package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import spoon.reflect.CtModel;
import spoon.reflect.code.CtNewArray;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelRelation;
import tk.labyrinth.satool.beholder.manifester.util.JakartaJavaxUtils;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.SpoonUtils;

import javax.annotation.CheckForNull;
import java.nio.file.Path;

@LazyComponent
public class JpaPersistentModelsManifestContributor implements ManifestContributor {

	@Override
	public List<PersistentModel> contribute(Path rootDirectory, CtModel javaModel) {
		List<PersistentModel> result;
		{
			List<CtType<?>> entityTypes = Stream.ofAll(javaModel.getAllTypes())
					.filter(JpaPersistentModelsManifestContributor::isEntity)
					.toList();
			//
			List<PersistentModel> persistentModels = entityTypes.map(entityType -> PersistentModel.builder()
					.attributes(resolveAttributes(entityType))
					.databaseName(resolveModelDatabaseName(entityType))
					.javaClassSignature(SpoonUtils.resolveJavaClassSignature(entityType))
					.tags(List.of(Tag.of("JPA_ENTITY")))
					.build());
			//
			result = persistentModels;
		}
		return result;
	}

	@Override
	public List<String> getExtraClasspathEntries() {
		return List
				.of(
						jakarta.persistence.Entity.class,
						javax.persistence.Entity.class)
				.map(ManifestContributorUtils::getLocationOfClassSource);
	}

	public static CtField<?> getPrimaryKeyField(CtType<?> entityType) {
		return List.ofAll(entityType.getAllFields())
				.map(CtFieldReference::getDeclaration)
				.filter(JpaPersistentModelsManifestContributor::isPrimaryKey)
				.single();
	}

	public static boolean isEntity(CtType<?> type) {
		return JakartaJavaxUtils.hasAnnotation(type, Entity.class);
	}

	public static Boolean isPrimaryKey(CtField<?> field) {
		return JakartaJavaxUtils.hasAnnotation(field, EmbeddedId.class) ||
				JakartaJavaxUtils.hasAnnotation(field, Id.class);
	}

	public static List<String> resolveAttributeDatabaseNames(CtField<?> field) {
		List<String> result;
		{
			if (JakartaJavaxUtils.hasAnnotation(field, Column.class)) {
				CtAnnotation<?> columnAnnotation = JakartaJavaxUtils.getAnnotation(field, Column.class);
				//
				if (!columnAnnotation.getValueAsString("name").isEmpty()) {
					result = List.of(columnAnnotation.getValueAsString("name"));
				} else {
					result = List.of(field.getSimpleName());
				}
			} else if (JakartaJavaxUtils.hasAnnotation(field, EmbeddedId.class)) {
				result = List.ofAll(field.getType().getAllFields())
						.map(CtFieldReference::getDeclaration)
						.flatMap(JpaPersistentModelsManifestContributor::resolveAttributeDatabaseNames);
			} else if (JakartaJavaxUtils.hasAnnotation(field, ManyToMany.class) ||
					JakartaJavaxUtils.hasAnnotation(field, OneToMany.class)) {
				result = List.empty();
			} else if (JakartaJavaxUtils.hasAnnotation(field, ManyToOne.class) ||
					JakartaJavaxUtils.hasAnnotation(field, OneToOne.class)) {
				if (JakartaJavaxUtils.hasAnnotation(field, JoinColumn.class)) {
					String name = JakartaJavaxUtils.getAnnotation(field, JoinColumn.class).getValueAsString("name");
					//
					result = List.of(!name.isEmpty() ? name : field.getSimpleName());
				} else if (JakartaJavaxUtils.hasAnnotation(field, MapsId.class)) {
					String name = JakartaJavaxUtils.getAnnotation(field, MapsId.class).getValueAsString("value");
					//
					result = List.of("TODO:MapsId:%s".formatted(name));
				} else {
					result = List.of(field.getSimpleName());
				}
			} else {
				result = List.of(field.getSimpleName());
			}
		}
		return result;
	}

	public static List<PersistentModelAttribute> resolveAttributes(CtType<?> entityType) {
		return List.ofAll(entityType.getAllFields())
				.map(CtFieldReference::getDeclaration)
				.filter(field -> !JakartaJavaxUtils.hasAnnotation(field, Transient.class))
				.map(field -> PersistentModelAttribute.builder()
						.databaseNames(resolveAttributeDatabaseNames(field))
						.javaFieldName(field.getSimpleName())
						.javaFieldType(CtTypeReferenceUtils.getTypeDescription(field.getType()))
						.relation(resolveRelation(field))
						.tags(isPrimaryKey(field) ? List.of(PersistentModelAttribute.PRIMARY_KEY_TAG) : null)
						.build());
	}

	public static String resolveModelDatabaseName(CtType<?> entityType) {
		String result;
		{
			CtAnnotation<?> tableAnnotation = JakartaJavaxUtils.findAnnotation(entityType, Table.class);
			//
			if (tableAnnotation != null) {
				if (!tableAnnotation.getValueAsString("name").isEmpty()) {
					result = tableAnnotation.getValueAsString("name");
				} else {
					result = entityType.getSimpleName();
				}
			} else {
				result = entityType.getSimpleName();
			}
		}
		return result;
	}

	@CheckForNull
	public static PersistentModelRelation resolveRelation(CtField<?> field) {
		PersistentModelRelation result;
		{
			if (JakartaJavaxUtils.hasAnnotation(field, JoinColumn.class)) {
				CtTypeReference<?> relationType = SpoonUtils.unwrapIfCollection(field.getType());
				String tableName = resolveModelDatabaseName(relationType.getDeclaration());
				//
				result = PersistentModelRelation.builder()
						.hasRelationColumn(JakartaJavaxUtils.hasAnnotation(field, ManyToMany.class) ||
								JakartaJavaxUtils.hasAnnotation(field, ManyToOne.class))
						.toColumnName(resolveRelationToDatabaseColumnName(field))
						.toTableName(tableName)
						.build();
			} else if (JakartaJavaxUtils.hasAnnotation(field, JoinTable.class)) {
				CtAnnotation<?> joinTableAnnotation = JakartaJavaxUtils.getAnnotation(field, JoinTable.class);
				List<CtAnnotation<?>> joinColumns = List
						.ofAll(((CtNewArray<?>) joinTableAnnotation.getWrappedValue("joinColumns")).getElements())
						.map(element -> ((CtAnnotation<?>) element));
				//
				if (joinColumns.size() > 1) {
					throw new IllegalArgumentException();
				}
				//
				result = PersistentModelRelation.builder()
						.hasRelationColumn(false)
						.toColumnName(joinColumns.single().getValueAsString("name"))
						.toTableName(joinTableAnnotation.getValueAsString("name"))
						.build();
			} else {
				result = null;
			}
		}
		return result;
	}

	public static String resolveRelationToDatabaseColumnName(CtField<?> relationField) {
		String result;
		{
			if (JakartaJavaxUtils.hasAnnotation(relationField, ManyToOne.class)) {
				CtField<?> toPrimaryKeyField = getPrimaryKeyField(SpoonUtils.unwrapIfCollection(relationField.getType())
						.getDeclaration());
				result = resolveAttributeDatabaseNames(toPrimaryKeyField).single();
			} else {
				result = JakartaJavaxUtils.getAnnotation(relationField, JoinColumn.class).getValueAsString("name");
			}
		}
		return result;
	}
}
