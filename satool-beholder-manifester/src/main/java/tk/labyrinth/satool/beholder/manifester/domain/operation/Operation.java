package tk.labyrinth.satool.beholder.manifester.domain.operation;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.pandora.models.tool.builder.GenerateBuilder;
import tk.labyrinth.satool.beholder.manifester.domain.microservice.CodeMicroserviceReference;

import java.util.UUID;

@GenerateBuilder
@Model(Operation.MODEL_CODE)
public interface Operation {

	String CODE_ATTRIBUTE_NAME = "code";

	String MODEL_CODE = "operation";

	CodeObjectModelReference MODEL_REFERENCE = CodeObjectModelReference.of(MODEL_CODE);

	String getCode();

	List<CodeOperationReference> getInvokes();

	Reference<GenericObject> getKind();

	CodeMicroserviceReference getService();

	UUID getUid();
}
