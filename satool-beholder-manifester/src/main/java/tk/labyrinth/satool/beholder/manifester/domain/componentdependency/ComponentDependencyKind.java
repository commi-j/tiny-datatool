package tk.labyrinth.satool.beholder.manifester.domain.componentdependency;

public enum ComponentDependencyKind {
	INCLUDES,
	INVOKES,
	LISTENS,
}
