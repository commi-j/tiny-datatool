package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.router.Location;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.anchor.SmartAnchorRenderer;
import tk.labyrinth.satool.beholder.domain.object.table.item.SpecificObjectItemActionProvider;

import javax.annotation.Nullable;

@LazyComponent
@RequiredArgsConstructor
public class MavenModuleShowDiagramItemActionProvider extends SpecificObjectItemActionProvider<MavenModule> {

	private final SmartAnchorRenderer anchorRenderer;

	@Nullable
	@Override
	protected Component provideItemAction(MavenModule item) {
		return anchorRenderer.render(SmartAnchorRenderer.Properties.builder()
				// TODO: Compute location from class.
				.location(new Location(
						"beholder/maven-module-dependency-diagram",
						MavenModuleDependencyDiagramPage.State.builder()
								.objectOrComponent(true)
								.expand(List.of(MavenModuleDependencyDiagramPage.State.ExpandEntry.builder()
										.artifactId(item.getArtifactId())
										.down(1)
										.groupId(item.getGroupId())
										.up(1)
										.build()))
								.build()
								.toQueryParameters()))
				.target(AnchorTarget.BLANK)
				.text("Dependency Diagram")
				.build());
	}
}
