package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.treegrid.TreeGrid;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.typedobject.TypedObjectEditorDialogHandler;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class ConfluenceMergeStructureViewRenderer {

	private final TypedObjectEditorDialogHandler<ConfluenceMergeNode> confluenceMergeNodeEditorDialogHandler;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	private Component renderActionsLayout(Parameters parameters, ConfluenceMergeNode item) {
		return ObjectUtils.consumeAndReturn(
				new CssHorizontalLayout(),
				actionLayout -> actionLayout.add(
						ButtonRenderer.render(builder -> builder
								.icon(VaadinIcon.PLUS.create())
								.onClick(event -> parameters.onValueChange().accept(
										parameters.currentValue().updateNested(
												item,
												item.withChildren(item.getChildrenOrEmpty()
														.append(ConfluenceMergeNode.builder().build())))))
								.themeVariants(ButtonVariant.LUMO_SMALL)
								.build()),
						ButtonRenderer.render(builder -> builder
								.icon(VaadinIcon.EDIT.create())
								.onClick(event -> confluenceMergeNodeEditorDialogHandler
										.showDialog(TypedObjectEditorDialogHandler.Parameters
												.<ConfluenceMergeNode>builder()
												.currentValue(item)
												.initialValue(item)
												.javaType(ConfluenceMergeNode.class)
												.build())
										.subscribeAlwaysAccepted(value -> parameters.onValueChange()
												.accept(parameters.currentValue().updateNested(item, value))))
								.themeVariants(ButtonVariant.LUMO_SMALL)
								.build()),
						ButtonRenderer.render(builder -> builder
								.icon(VaadinIcon.MINUS.create())
								.onClick(event -> parameters.onValueChange()
										.accept(parameters.currentValue().updateNested(item, null)))
								.themeVariants(ButtonVariant.LUMO_SMALL)
								.build())));
	}

	public Component render(Function<Parameters.Builder, Parameters> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout header = new CssHorizontalLayout();
				{
					header.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					{
						header.add(StringBoxRenderer.render(builder -> builder
								.label("Code")
								.nonEditableReasons(List.of("Read-only"))
								.value(parameters.currentValue().getCode())
								.width(PandoraStyles.defaultCellWidth())
								.build()));
					}
					{
						header.add(StringBoxRenderer.render(builder -> builder
								.label("Variables")
								.nonEditableReasons(List.of("Read-only"))
								.value(parameters.currentValue().computeVariables().mkString(", "))
								.width(PandoraStyles.multiplyDefaultCellWidth(2))
								.build()));
					}
					{
						header.addStretch();
					}
					{
						header.add(ButtonRenderer.render(builder -> builder
								.enabled(!Objects.equals(parameters.currentValue(), parameters.initialValue()))
								.onClick(event -> parameters.onSave().run())
								.text("Save")
								.themeVariants(ButtonVariant.LUMO_PRIMARY)
								.build()));
					}
				}
				layout.add(header);
			}
			{
				TreeGrid<ConfluenceMergeNode> grid = new TreeGrid<>();
				{
					grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
				}
				{
					grid
							.addHierarchyColumn(ConfluenceMergeNode::getLocator)
							.setFlexGrow(5)
							.setHeader("Locator")
							.setResizable(true);
					grid
							.addColumn(ConfluenceMergeNode::getTitleTemplate)
							.setFlexGrow(3)
							.setHeader("Title Template")
							.setResizable(true);
					grid
							.addColumn(ConfluenceMergeNode::getScope)
							.setHeader("Scope")
							.setResizable(true);
					grid
							.addColumn(ConfluenceMergeNode::getBodyTemplateReference)
							.setFlexGrow(3)
							.setHeader("Body Template Reference")
							.setResizable(true);
					grid
							.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
									ToVaadinComponentRendererRegistry.Context.builder()
											.datatype(ConfluenceMergeNode.DIAGRAMS_ATTRIBUTE_TYPE)
											.hints(List.empty())
											.build(),
									item.getDiagrams()))
							.setFlexGrow(1)
							.setHeader("Diagrams")
							.setResizable(true);
					grid
							.addComponentColumn(item -> renderActionsLayout(parameters, item))
							.setFlexGrow(1)
							.setHeader(ButtonRenderer.render(builder -> builder
									.icon(VaadinIcon.PLUS.create())
									.onClick(event -> {
										ConfluenceMergeStructure value = parameters.currentValue();
										//
										parameters.onValueChange().accept(value.withRootNodes(
												value.getRootNodesOrEmpty().append(
														ConfluenceMergeNode.builder().build())));
									})
									.themeVariants(ButtonVariant.LUMO_SMALL)
									.build()));
				}
				{
					grid.setItems(
							parameters.currentValue().getRootNodesOrEmpty().asJava(),
							item -> item.getChildrenOrEmpty().asJava());
					//
					grid.expandRecursively(parameters.currentValue().getRootNodesOrEmpty().asJava(), 999);
				}
				layout.add(grid);
			}
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		ConfluenceMergeStructure currentValue;

		@NonNull
		ConfluenceMergeStructure initialValue;

		Runnable onSave;

		@NonNull
		Consumer<ConfluenceMergeStructure> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}