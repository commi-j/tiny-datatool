package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.vaadin.LabelRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Color;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.satool.beholder.manifester.dependencymanager.MavenDependencyManager;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.ArtifactSignature;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.BuildSystem;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.GradleManifestContributor;
import tk.labyrinth.satool.maven.domain.MavenDependency;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class BuildAndDependenciesViewRenderer {

	public static final String DEPENDENCY_MANAGEMENT_GRADLE_PLUGIN_ID = "io.spring.dependency-management";

	public static final String SPRING_BOOT_GRADLE_PLUGIN_ID = "org.springframework.boot";

	private final MavenDependencyManager mavenDependencyManager;

	@Nullable
	@WithSpan
	private String computeVersion(BuildSystem buildSystem, ArtifactSignature artifactSignature) {
		artifactSignature.requireNoVersion();
		//
		String result;
		{
			List<ArtifactSignature> bomArtifactSignatures;
			{
				if (Objects.equals(buildSystem.getName(), GradleManifestContributor.BUILD_SYSTEM_NAME)) {
					if (buildSystem.getPluginsOrEmpty().exists(plugin ->
							Objects.equals(plugin.getName(), DEPENDENCY_MANAGEMENT_GRADLE_PLUGIN_ID))) {
						// Special case where Spring Boot plugin adds Spring Boot Dependencies BOM if
						// Dependency Management plugin is applied.
						// https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/htmlsingle/#managing-dependencies.dependency-management-plugin
						//
						ArtifactSignature springBootPluginSignature = buildSystem.getPluginsOrEmpty()
								.find(plugin -> Objects.equals(plugin.getName(), SPRING_BOOT_GRADLE_PLUGIN_ID))
								.getOrNull();
						//
						bomArtifactSignatures = springBootPluginSignature != null &&
								springBootPluginSignature.getVersion() != null
								? List.of(ArtifactSignature.builder()
								.group("org.springframework.boot")
								.name("spring-boot-dependencies")
								.version(springBootPluginSignature.getVersion())
								.build())
								: List.empty();
					} else {
						bomArtifactSignatures = List.empty();
					}
				} else {
					bomArtifactSignatures = List.empty();
				}
			}
			if (!bomArtifactSignatures.isEmpty()) {
				result = mavenDependencyManager
						.findVersion(
								buildSystem.getRepositoriesOrEmpty(),
								bomArtifactSignatures,
								artifactSignature)
						.getVersion();
			} else {
				result = null;
			}
		}
		return result;
	}

	private Component createVersionComponent(BuildSystem buildSystem, MavenDependency dependency) {
		String version;
		boolean explicit;
		{
			String explicitVersion = dependency.getVersion();
			if (explicitVersion != null) {
				version = explicitVersion;
				explicit = true;
			} else {
				version = computeVersion(buildSystem, ArtifactSignature.from(dependency));
				explicit = false;
			}
		}
		//
		return LabelRenderer.render(builder -> builder
				.customizer(label -> {
					if (version != null && !explicit) {
						StyleUtils.setCssProperty(label, Color.LIGHTGREY);
					}
				})
				.text(dependency.getVersion() != null ? dependency.getVersion() : version)
				.build());
	}

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.add("Build tool");
			layout.add("Plugins");
			layout.add("Artifact repositories");
			layout.add("Dependencies");
		}
		{
			List<BuildSystem> buildSystems = Optional.ofNullable(properties.getArtifactManifest().getBuildSystems())
					.orElse(List.empty());
			//
			if (!buildSystems.isEmpty()) {
				BuildSystem buildSystem = buildSystems.single();
				//
				Grid<MavenDependency> dependenciesGrid = new Grid<>();
				{
					dependenciesGrid.setMultiSort(true);
					dependenciesGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
				}
				{
					dependenciesGrid
							.addColumn(MavenDependency::getScope)
							.setHeader("Scope")
							.setResizable(true)
							.setSortable(true);
					dependenciesGrid
							.addColumn(MavenDependency::getGroupId)
							.setHeader("Group Id")
							.setResizable(true)
							.setSortable(true);
					dependenciesGrid
							.addColumn(MavenDependency::getArtifactId)
							.setHeader("Artifact Id")
							.setResizable(true)
							.setSortable(true);
					dependenciesGrid
							.addComponentColumn(item -> createVersionComponent(buildSystem, item))
							.setHeader("Version")
							.setResizable(true)
							.setSortable(true);
					dependenciesGrid
							.addColumn(MavenDependency::getType)
							.setHeader("Type")
							.setResizable(true)
							.setSortable(true);
				}
				{
					dependenciesGrid.setItems(buildSystem.getDependencies().asJava());
				}
				layout.add(dependenciesGrid);
			}
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		ArtifactManifest artifactManifest;

		public static class Builder {
			// Lomboked
		}
	}
}