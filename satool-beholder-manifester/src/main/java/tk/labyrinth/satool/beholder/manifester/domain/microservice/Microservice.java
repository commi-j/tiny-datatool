package tk.labyrinth.satool.beholder.manifester.domain.microservice;

import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;

@Model(Microservice.MODEL_CODE)
public interface Microservice {

	String CODE_ATTRIBUTE_NAME = "code";

	String MODEL_CODE = "microservice";

	String getCode();

	String getOperationPrefix();
}
