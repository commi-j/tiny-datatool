package tk.labyrinth.satool.beholder.manifester.domain.component;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.beholder.manifester.domain.repository.UrlRepositoryReference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class RepositoryReferenceComponentReference implements Reference<Component> {

	UrlRepositoryReference repositoryReference;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Component.MODEL_CODE,
				Component.REPOSITORY_REFERENCE_ATTRIBUTE_NAME,
				repositoryReference);
	}

	public static RepositoryReferenceComponentReference from(Component object) {
		return RepositoryReferenceComponentReference.of(object.getRepositoryReference());
	}

	@JsonCreator
	public static RepositoryReferenceComponentReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Component.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(UrlRepositoryReference.from(reference.getAttributeValue(
				Component.REPOSITORY_REFERENCE_ATTRIBUTE_NAME)));
	}
}
