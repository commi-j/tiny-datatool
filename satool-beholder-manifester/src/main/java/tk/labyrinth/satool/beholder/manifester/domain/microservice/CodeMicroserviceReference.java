package tk.labyrinth.satool.beholder.manifester.domain.microservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class CodeMicroserviceReference implements Reference<Microservice> {

	String code;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Microservice.MODEL_CODE,
				Microservice.CODE_ATTRIBUTE_NAME,
				code);
	}

	public static CodeMicroserviceReference from(Microservice object) {
		return CodeMicroserviceReference.of(object.getCode());
	}

	@JsonCreator
	public static CodeMicroserviceReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Microservice.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Microservice.CODE_ATTRIBUTE_NAME));
	}
}
