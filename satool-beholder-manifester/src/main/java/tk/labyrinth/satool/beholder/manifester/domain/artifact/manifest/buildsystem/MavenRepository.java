package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class MavenRepository {

	String name;

	String url;
}
