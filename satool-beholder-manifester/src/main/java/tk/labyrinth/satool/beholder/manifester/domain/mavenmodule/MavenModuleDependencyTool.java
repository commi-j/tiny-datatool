package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.Objects;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class MavenModuleDependencyTool {

	@SmartAutowired
	private TypedObjectSearcher<MavenModuleDependencyIndex> mavenModuleDependencyIndexSearcher;

	@SmartAutowired
	private TypedObjectSearcher<MavenModule> mavenModuleSearcher;

	private List<MavenModule> collectDownstreamDependencies(MavenModule mavenModule, Integer depth) {
		List<MavenModule> result;
		{
			if (depth > 0) {
				List<MavenModule> shallowDependencies = mavenModule.getDependencies()
						.map(dependency -> mavenModuleSearcher.findSingle(GroupIdAndArtifactIdMavenModuleReference.from(
								dependency)))
						.filter(Objects::nonNull);
				//
				if (depth > 1) {
					List<List<MavenModule>> deepDependencies = shallowDependencies.map(dependencyMavenModule ->
							List.of(dependencyMavenModule).appendAll(
									collectDownstreamDependencies(dependencyMavenModule, depth - 1)));
					//
					result = deepDependencies.flatMap(Function.identity());
				} else {
					result = shallowDependencies;
				}
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	private List<MavenModule> collectUpstreamDependencies(MavenModule mavenModule, Integer depth) {
		List<MavenModule> result;
		{
			if (depth > 0) {
				List<MavenModule> shallowDependencies = mavenModuleDependencyIndexSearcher
						.search(PlainQuery.builder()
								.predicate(Predicates.equalTo(
										MavenModuleDependencyIndex.TO_REFERENCE_ATTRIBUTE_NAME,
										GroupIdAndArtifactIdMavenModuleReference.from(mavenModule)))
								.build())
						.map(mavenModuleDependencyIndex -> mavenModuleSearcher.findSingle(
								mavenModuleDependencyIndex.getFromReference()))
						.filter(Objects::nonNull);
				//
				if (depth > 1) {
					List<List<MavenModule>> deepDependencies = shallowDependencies.map(dependencyMavenModule ->
							List.of(dependencyMavenModule).appendAll(
									collectUpstreamDependencies(dependencyMavenModule, depth - 1)));
					//
					result = deepDependencies.flatMap(Function.identity());
				} else {
					result = shallowDependencies;
				}
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	public List<MavenModule> collectDependencies(
			String groupId,
			String artifactId,
			Integer upstreamDepth,
			Integer downstreamDepth) {
		List<MavenModule> result;
		{
			MavenModule mavenModule = mavenModuleSearcher.findSingle(
					GroupIdAndArtifactIdMavenModuleReference.of(groupId, artifactId));
			if (mavenModule != null) {
				result = List.of(mavenModule)
						.appendAll(collectUpstreamDependencies(mavenModule, upstreamDepth))
						.appendAll(collectDownstreamDependencies(mavenModule, downstreamDepth));
			} else {
				result = List.empty();
			}
		}
		return result;
	}
}
