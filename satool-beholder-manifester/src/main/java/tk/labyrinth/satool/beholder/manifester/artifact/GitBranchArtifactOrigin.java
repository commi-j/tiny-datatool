package tk.labyrinth.satool.beholder.manifester.artifact;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicQualifier;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.RepositoryReferenceAndNameGitBranchReference;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model // FIXME: We don't need them here.
@PolymorphicQualifier(rootClass = ArtifactOrigin.class, qualifierAttributeValue = "git-branch")
@RenderPattern("$gitBranchReference/$path")
@Value
@With
public class GitBranchArtifactOrigin implements ArtifactOrigin {

	RepositoryReferenceAndNameGitBranchReference gitBranchReference;

	String path;
}
