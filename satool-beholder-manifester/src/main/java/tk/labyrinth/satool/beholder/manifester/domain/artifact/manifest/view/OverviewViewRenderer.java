package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view;

import com.vaadin.flow.component.Component;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;

import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class OverviewViewRenderer {

	public Component render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.add("Artifact");
			layout.add("Revision");
			layout.add("Time");
			layout.add("Modules & module kinds (app, lib, job)");
			layout.add("Technologies: build, frameworks (helm/docker)");
			layout.add("Updated At: %s".formatted(properties.getArtifactManifest().getUpdatedAt()));
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		ArtifactManifest artifactManifest;

		public static class Builder {
			// Lomboked
		}
	}
}