package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceUpdateTarget implements ConfluencePublishTarget {

	String contentId;

	Integer versionNumber;
}
