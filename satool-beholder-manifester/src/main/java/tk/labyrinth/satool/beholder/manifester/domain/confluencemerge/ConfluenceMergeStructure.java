package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;

import java.util.Objects;
import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model
@ModelTag("confluence")
@ModelTag("docs")
@RenderAttribute(ConfluenceMergeStructure.CODE_ATTRIBUTE_NAME)
@Value
@With
public class ConfluenceMergeStructure {

	public static final String CODE_ATTRIBUTE_NAME = "code";

	public static final String MODEL_CODE = "confluencemergestructure";

	String code;

	List<ConfluenceMergeNode> rootNodes;

	UUID uid;

	public List<String> computeVariables() {
		return getRootNodesOrEmpty()
				.flatMap(ConfluenceMergeNode::computeVariables)
				.distinct()
				.sorted();
	}

	public List<ConfluenceMergeNode> getRootNodesOrEmpty() {
		return rootNodes != null ? rootNodes : List.empty();
	}

	public ConfluenceMergeStructure updateNested(ConfluenceMergeNode currentValue, ConfluenceMergeNode nextValue) {
		return withRootNodes(getRootNodesOrEmpty()
				.map(rootNode -> rootNode.updateNested(currentValue, nextValue))
				.filter(Objects::nonNull));
	}
}
