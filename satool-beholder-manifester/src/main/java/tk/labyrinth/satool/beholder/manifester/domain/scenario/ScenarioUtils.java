package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import tk.labyrinth.satool.beholder.manifester.domain.component.ScenarioPrefixComponentReference;

import javax.annotation.CheckForNull;

public class ScenarioUtils {

	@CheckForNull
	public static ScenarioPrefixComponentReference componentReferenceFromCode(@CheckForNull String code) {
		ScenarioPrefixComponentReference result;
		{
			int lastIndexOfHyphen = code != null ? code.lastIndexOf('-') : -1;
			if (lastIndexOfHyphen != -1) {
				int indexOfFirstCharacterAfterHyphen = lastIndexOfHyphen + 1;
				if (code.length() > indexOfFirstCharacterAfterHyphen &&
						code.charAt(indexOfFirstCharacterAfterHyphen) == 'U') {
					result = ScenarioPrefixComponentReference.of("ARM-");
				} else {
					result = ScenarioPrefixComponentReference.of(code.substring(0, lastIndexOfHyphen + 1));
				}
			} else {
				result = null;
			}
		}
		return result;
	}
}
