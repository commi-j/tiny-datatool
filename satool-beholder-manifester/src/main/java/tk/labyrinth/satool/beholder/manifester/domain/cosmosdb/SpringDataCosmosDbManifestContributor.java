package tk.labyrinth.satool.beholder.manifester.domain.cosmosdb;

import com.azure.spring.data.cosmos.core.mapping.Container;
import com.azure.spring.data.cosmos.core.mapping.PartitionKey;
import io.vavr.collection.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributor;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ManifestContributorUtils;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;
import tk.labyrinth.satool.beholder.manifester.tool.ExposedClassDiscoverer;
import tk.labyrinth.satool.spoon.CtTypeReferenceUtils;
import tk.labyrinth.satool.spoon.CtTypeUtils;

import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Function;

@LazyComponent
public class SpringDataCosmosDbManifestContributor implements ManifestContributor {

	@Override
	public List<?> contribute(Path rootDirectory, CtModel javaModel) {
		List<?> result;
		{
			List<CtType<?>> containerTypes = List.ofAll(javaModel.getAllTypes())
					.filter(type -> type.hasAnnotation(Container.class));
			//
			List<PersistentModel> persistentModels = containerTypes.map(SpringDataCosmosDbManifestContributor::resolveModel);
			//
			List<ExposedClass> exposedClasses = ExposedClassDiscoverer
					.discoverForPersistentModels(javaModel, persistentModels);
			//
			result = List.of(persistentModels, exposedClasses).flatMap(Function.identity());
		}
		return result;
	}

	@Override
	public List<String> getExtraClasspathEntries() {
		return List
				.of(Container.class)
				.map(ManifestContributorUtils::getLocationOfClassSource);
	}

	public static boolean isPrimaryKey(CtField<?> field) {
		return Objects.equals(field.getSimpleName(), "id") || field.hasAnnotation(Id.class);
	}

	public static List<String> resolveAttributeDatabaseNames(CtField<?> field) {
		List<String> result;
		{
			result = List.of(field.getSimpleName());
		}
		return result;
	}

	public static List<Tag> resolveAttributeTags(CtField<?> field) {
		return List
				.of(
						isPrimaryKey(field) ? PersistentModelAttribute.PRIMARY_KEY_TAG : null,
						field.hasAnnotation(PartitionKey.class) ? Tag.of("PARTITION_KEY") : null)
				.filter(Objects::nonNull);
	}

	public static List<PersistentModelAttribute> resolveAttributes(CtType<?> containerType) {
		return List.ofAll(containerType.getAllFields())
				.map(CtFieldReference::getDeclaration)
				.filter(field -> !field.hasAnnotation(Transient.class))
				.map(field -> PersistentModelAttribute.builder()
						.databaseNames(resolveAttributeDatabaseNames(field))
						.javaFieldName(field.getSimpleName())
						.javaFieldType(CtTypeReferenceUtils.getTypeDescription(field.getType()))
						.tags(resolveAttributeTags(field))
						.build());
	}

	public static PersistentModel resolveModel(CtType<?> containerType) {
		return PersistentModel.builder()
				.attributes(resolveAttributes(containerType))
				.databaseName(containerType.getAnnotation(Container.class).containerName())
				.javaClassSignature(CtTypeUtils.getClassSignature(containerType))
				.tags(List.of(Tag.of("COSMOSDB_CONTAINER")))
				.build();
	}
}
