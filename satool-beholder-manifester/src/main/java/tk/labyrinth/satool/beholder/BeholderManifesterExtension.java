package tk.labyrinth.satool.beholder;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.datatypes.PandoraDatatypesModule;
import tk.labyrinth.pandora.jobs.PandoraJobsModule;
import tk.labyrinth.satool.confluenceconnector.ConfluenceConnectorConfiguration;
import tk.labyrinth.satool.freemarker.SatoolFreemarkerModule;
import tk.labyrinth.satool.gerritconnector.GerritConnectorExtension;
import tk.labyrinth.satool.maven.SatoolMavenModule;

@Configuration
@Import({
		BeholderExtensionConfiguration.class, // TODO: This is required for @EnableScheduling, can be removed if declared in Pandora.
		ConfluenceConnectorConfiguration.class,
		GerritConnectorExtension.class,
		PandoraDatatypesModule.class,
		PandoraJobsModule.class,
		SatoolFreemarkerModule.class,
		SatoolMavenModule.class,
})
public class BeholderManifesterExtension {
	// empty
}
