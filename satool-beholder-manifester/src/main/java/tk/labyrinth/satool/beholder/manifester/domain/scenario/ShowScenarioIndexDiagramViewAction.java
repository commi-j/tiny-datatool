package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.radiobutton.RadioGroupVariant;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.component.view.View;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.ui.component.view.simple.SimpleView;
import tk.labyrinth.satool.beholder.domain.object.table.item.SpecificObjectItemActionProvider;
import tk.labyrinth.satool.plantuml.view.PlantumlDiagramView;

import javax.annotation.Nullable;

@RequiredArgsConstructor
@Slf4j
@SpringComponent
public class ShowScenarioIndexDiagramViewAction extends SpecificObjectItemActionProvider<ScenarioIndex> {

	private final ScenarioComponentDiagramDescriptionBuilder componentDiagramDescriptionBuilder;

	private final ObjectProvider<PlantumlDiagramView> diagramViewProvider;

	private final ScenarioTool scenarioTool;

	@SmartAutowired
	private TypedObjectSearcher<Scenario> scenarioSearcher;

	private View createDiagramView(ScenarioIndex scenarioIndex) {
		View result;
		{
			Scenario scenario = scenarioSearcher.findSingle(scenarioIndex.getTargetReference());
			try {
				List<ScenarioTool.ScenarioInvocationStep> upstreamInvocationTree = scenarioTool
						.getUpstreamInvocationTree(scenario);
				List<ScenarioTool.ScenarioInvocationStep> downstreamInvocationTree = scenarioTool
						.getDownstreamInvocationTree(scenario);
				//
				SplitLayout layout = new SplitLayout();
				{
					layout.setOrientation(SplitLayout.Orientation.HORIZONTAL);
					layout.setHeight("60vh");
					layout.setWidth("60vw");
					//
					layout.setSplitterPosition(30);
				}
				{
					CssVerticalLayout buttonLayout = new CssVerticalLayout();
//					{
//						RadioButtonGroup<String> upstreamButtonGroup = new RadioButtonGroup<>();
//						upstreamButtonGroup.addThemeVariants(RadioGroupVariant.LUMO_VERTICAL);
//						upstreamButtonGroup.setItems(upstreamInvocationTree
//								.map(ScenarioTool.ScenarioInvocationStep::renderLongFrom)
//								.sorted()
//								.asJava());
//						buttonLayout.add(upstreamButtonGroup);
//					}
//					{
//						buttonLayout.add(new ProgressBar(0, 1, 1));
//					}
					{
						RadioButtonGroup<String> scenarioButtonGroup = new RadioButtonGroup<>();
						scenarioButtonGroup.addThemeVariants(RadioGroupVariant.LUMO_VERTICAL);
						scenarioButtonGroup.setItems(downstreamInvocationTree
								.map(ScenarioTool.ScenarioInvocationStep::renderLongTo)
								.sorted()
								.asJava());
						buttonLayout.add(scenarioButtonGroup);
					}
					layout.addToPrimary(buttonLayout);
				}
				{
					PlantumlDiagramView diagramView = diagramViewProvider.getObject();
					diagramView.setParameters(builder -> builder
							.value(componentDiagramDescriptionBuilder.build(
									scenario,
									List.empty(),
									downstreamInvocationTree))
							.build());
					layout.addToSecondary(diagramView);
				}
				result = SimpleView.of(layout);
			} catch (RuntimeException ex) {
				logger.error("", ex);
				result = SimpleView.of(new Span(ex.toString()));
			}
		}
		return result;
	}

	@Nullable
	@Override
	protected Component provideItemAction(ScenarioIndex item) {
		return new Button(
				"Diagram",
				event -> ConfirmationViews.showViewDialog(createDiagramView(item)));
	}
}
