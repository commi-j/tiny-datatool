package tk.labyrinth.satool.beholder.manifester.artifact;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;

@Builder(toBuilder = true)
@Model
@ModelTag("repos")
@RenderAttribute("name")
@Value
public class ArtifactDefinition {

	public static final String MODEL_CODE = "artifactdefinition";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	String name;

	ArtifactOrigin origin;
}
