package tk.labyrinth.satool.beholder.manifester.domain.component;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class ScenarioPrefixComponentReference implements Reference<Component> {

	String scenarioPrefix;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Component.MODEL_CODE,
				Component.SCENARIO_PREFIX_ATTRIBUTE_NAME,
				scenarioPrefix);
	}

	public static ScenarioPrefixComponentReference from(Component object) {
		return ScenarioPrefixComponentReference.of(object.getScenarioPrefix());
	}

	@JsonCreator
	public static ScenarioPrefixComponentReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Component.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Component.SCENARIO_PREFIX_ATTRIBUTE_NAME));
	}
}
