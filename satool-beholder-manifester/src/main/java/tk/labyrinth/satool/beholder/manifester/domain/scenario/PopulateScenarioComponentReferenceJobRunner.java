package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.stores.extra.objectproxy.ObjectProxyHandler;
import tk.labyrinth.pandora.stores.objectmanipulator.WildcardObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.satool.beholder.domain.job.SimpleParameters;
import tk.labyrinth.satool.beholder.domain.space.BeholderSpace;

@Component
@RequiredArgsConstructor
public class PopulateScenarioComponentReferenceJobRunner implements JobRunner<SimpleParameters> {

	private final ExecutionContextHandler executionContextHandler;

	private final WildcardObjectManipulator objectManipulator;

	private final ObjectProxyHandler objectProxyHandler;

	@SmartAutowired
	private TypedObjectSearcher<Scenario> scenarioSearcher;

	@Override
	public SimpleParameters getInitialParameters() {
		return new SimpleParameters(executionContextHandler.getExecutionContext().findAttributeValueInferred(
				BeholderSpace.SPACE_REFERENCE_OBJECT_ATTRIBUTE_NAME));
	}

	@Override
	public void run(Context<SimpleParameters> context) {
		//
		List<Scenario> scenarios = scenarioSearcher.searchAll();
		//
		scenarios.forEach(scenario -> objectManipulator.update(ScenarioBuilder.from(scenario)
				.componentReference(ScenarioUtils.componentReferenceFromCode(scenario.getCode()))
				.build(objectProxyHandler)));
	}
}
