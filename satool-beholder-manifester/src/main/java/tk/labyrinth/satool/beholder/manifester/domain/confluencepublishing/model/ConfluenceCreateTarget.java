package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.model;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ConfluenceCreateTarget implements ConfluencePublishTarget {

	String parentContentId;

	String parentSpaceKey;

	@Nullable
	@Override
	public String getContentId() {
		return null;
	}
}
