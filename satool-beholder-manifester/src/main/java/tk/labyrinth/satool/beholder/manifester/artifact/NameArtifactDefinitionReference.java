package tk.labyrinth.satool.beholder.manifester.artifact;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

@Value(staticConstructor = "of")
public class NameArtifactDefinitionReference implements Reference<ArtifactDefinition> {

	String name;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				ArtifactDefinition.MODEL_CODE,
				ArtifactDefinition.NAME_ATTRIBUTE_NAME,
				name);
	}

	public static NameArtifactDefinitionReference from(ArtifactDefinition value) {
		return of(value.getName());
	}

	@JsonCreator
	public static NameArtifactDefinitionReference from(String value) {
		// TODO: Universalize.
		GenericReference reference = GenericReference.from(value);
		return of(reference.getAttributeValue(ArtifactDefinition.NAME_ATTRIBUTE_NAME));
	}
}
