package tk.labyrinth.satool.beholder.manifester.domain.properties.declaration;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;

@Builder(toBuilder = true)
@Model
@Value
public class PropertyDeclarationEntry {

	String location;

	String name;

	List<String> tags;

	String value;
}
