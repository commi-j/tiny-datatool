package tk.labyrinth.satool.beholder.manifester.dependencymanager;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderAttribute;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.ArtifactSignature;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(MavenBom.MODEL_CODE)
@RenderAttribute(MavenBom.SIGNATURE_ATTRIBUTE_NAME)
@Value
@With
public class MavenBom {

	public static final String MODEL_CODE = "mavenbom";

	public static final String SIGNATURE_ATTRIBUTE_NAME = "signature";

	List<ArtifactSignature> dependencies;

	List<ArtifactSignature> imports;

	ArtifactSignature signature;
}
