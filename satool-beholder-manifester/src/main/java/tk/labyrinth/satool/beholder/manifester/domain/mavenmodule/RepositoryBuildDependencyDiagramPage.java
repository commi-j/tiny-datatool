package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.satool.beholder.root.BeholderRootLayout;
import tk.labyrinth.satool.plantuml.view.PlantumlDiagramView;

@RequiredArgsConstructor
@Route(value = "repository-build-dependency-diagram", layout = BeholderRootLayout.class)
@Slf4j
public class RepositoryBuildDependencyDiagramPage extends CssVerticalLayout {

	private final MavenModuleDependencyDiagramBuilder diagramBuilder;

	private final PlantumlDiagramView diagramView;

	@PostConstruct
	private void postConstruct() {
		{
			CssFlexItem.setFlexGrow(diagramView, 1);
			add(diagramView);
		}
		{
			diagramView.setParameters(builder -> builder
					.value(diagramBuilder.build())
					.build());
		}
	}
}
