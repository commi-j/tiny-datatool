package tk.labyrinth.satool.beholder.manifester.domain.componentdependency;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.stores.rootobject.HasUid;
import tk.labyrinth.satool.beholder.manifester.domain.component.CodeComponentReference;

import java.util.Objects;
import java.util.UUID;

@Builder(toBuilder = true)
@Model(code = ComponentDependency.MODEL_CODE)
@ModelTag("documentary")
@ModelTag("repos")
@Value
public class ComponentDependency implements HasUid {

	public static final String FROM_REFERENCE_ATTRIBUTE_NAME = "fromReference";

	public static final String LAYER_ATTRIBUTE_NAME = "layer";

	public static final String MODEL_CODE = "componentdependency";

	public static final String TO_REFERENCE_ATTRIBUTE_NAME = "toReference";

	CodeComponentReference fromReference;

	ComponentDependencyKind kind;

	ComponentDependencyLayer layer;

	String text;

	CodeComponentReference toReference;

	UUID uid;

	public List<CodeComponentReference> getNonNullComponentReferences() {
		return List.of(fromReference, toReference).filter(Objects::nonNull);
	}
}
