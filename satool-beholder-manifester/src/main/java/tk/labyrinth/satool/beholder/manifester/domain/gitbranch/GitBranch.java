package tk.labyrinth.satool.beholder.manifester.domain.gitbranch;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.RenderPattern;
import tk.labyrinth.pandora.stores.rootobject.HasUid;
import tk.labyrinth.satool.beholder.manifester.domain.repository.UrlRepositoryReference;

import java.time.Instant;
import java.util.UUID;

@Builder(toBuilder = true)
@Model(GitBranch.MODEL_CODE)
@ModelTag("repos")
@RenderPattern("${repositoryReference.url}/refs/heads/$name")
@Value
public class GitBranch implements HasUid {

	public static final String MODEL_CODE = "gitbranch";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	public static final String REPOSITORY_REFERENCE_ATTRIBUTE_NAME = "repositoryReference";

	String name;

	UrlRepositoryReference repositoryReference;

	UUID uid;

	Instant updatedAt;
}
