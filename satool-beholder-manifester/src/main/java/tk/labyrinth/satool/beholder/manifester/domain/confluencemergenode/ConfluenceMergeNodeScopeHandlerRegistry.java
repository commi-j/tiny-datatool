package tk.labyrinth.satool.beholder.manifester.domain.confluencemergenode;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Objects;

@LazyComponent
public class ConfluenceMergeNodeScopeHandlerRegistry {

	@SmartAutowired
	private List<ConfluenceMergeNodeScopeHandler> mergeNodeScopeHandlers;

	@Nullable
	public ConfluenceMergeNodeScopeHandler findHandler(String scopeName) {
		return mergeNodeScopeHandlers
				.find(mergeNodeScopeHandler -> Objects.equals(mergeNodeScopeHandler.getScopeName(), scopeName))
				.getOrNull();
	}
}
