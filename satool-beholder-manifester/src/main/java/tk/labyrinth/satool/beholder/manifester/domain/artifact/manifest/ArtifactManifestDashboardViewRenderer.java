package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tabs;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.functionalcomponents.vaadin.TabRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view.BuildAndDependenciesViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view.CustomModelsViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view.ExposedClassesViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view.OverviewViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view.PersistenceViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.view.PropertiesViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.ArtifactManifestIndex;
import tk.labyrinth.satool.beholder.manifester.domain.manifestenrichment.ManifestEnrichment;

import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
public class ArtifactManifestDashboardViewRenderer {

	private final BuildAndDependenciesViewRenderer buildAndDependenciesViewRenderer;

	private final CustomModelsViewRenderer customModelsViewRenderer;

	private final ExposedClassesViewRenderer exposedClassesViewRenderer;

	private final ArtifactManifestDashboardIntegrationsTabRenderer integrationsTabRenderer;

	private final OverviewViewRenderer overviewViewRenderer;

	private final PersistenceViewRenderer persistenceViewRenderer;

	private final PropertiesViewRenderer propertiesViewRenderer;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifestIndex> artifactManifestIndexSearcher;

	@SmartAutowired
	private TypedObjectSearcher<ManifestEnrichment> manifestEnrichmentSearcher;

	private void renderTab(CssVerticalLayout layout, ArtifactManifest artifactManifest, String tabCode) {
		ArtifactManifestIndex artifactManifestIndex = artifactManifestIndexSearcher
				.search(PlainQuery.builder()
						.predicate(Predicates.equalTo(
								ObjectIndex.TARGET_REFERENCE_ATTRIBUTE_NAME,
								UidReference.of(ArtifactManifest.MODEL_CODE, artifactManifest.getUid())))
						.build())
				.single();
		Optional<ManifestEnrichment> manifestEnrichmentOptional = manifestEnrichmentSearcher
				.search(Predicates.equalTo(
						ManifestEnrichment.ARTIFACT_DEFINITION_REFERENCE_ATTRIBUTE_NAME,
						artifactManifest.getDefinitionReference()))
				.headOption()
				.toJavaOptional();
		//
		layout.getChildren().skip(1).forEach(layout::remove);
		//
		Optional
				.ofNullable(switch (tabCode) {
					case "overview" -> overviewViewRenderer.render(builder -> builder
							.artifactManifest(artifactManifest)
							.build());
					case "build-and-dependencies" -> buildAndDependenciesViewRenderer.render(builder -> builder
							.artifactManifest(artifactManifest)
							.build());
					case "properties" -> propertiesViewRenderer.render(builder -> builder
							.propertyDeclarationEntries(artifactManifest.getPropertyDeclarationEntries())
							.propertyUsageEntries(artifactManifest.getPropertyUsageEntries())
							.build());
					case "integrations" -> integrationsTabRenderer.render(builder -> builder
							.functions(artifactManifestIndex.getFunctions())
							.kafkaEndpoints(Optional.ofNullable(artifactManifest.getKafkaEndpoints())
									.orElse(List.empty()))
							.models(artifactManifestIndex.getModels())
							.restControllers(Optional.ofNullable(artifactManifest.getRestControllers())
									.orElse(List.empty()))
							.restEndpointGroups(artifactManifestIndex.getRestEndpointGroups())
							.build());
					case "persistence" -> persistenceViewRenderer.render(builder -> builder
							.customPersistentModelRelations(manifestEnrichmentOptional
									.map(ManifestEnrichment::getPersistentModelRelations)
									.orElse(List.empty()))
							.displayableModels(artifactManifestIndex.getModels())
							.persistentModels(artifactManifestIndex.getPersistentModels())
							.build());
					case "custom-models" -> customModelsViewRenderer.render(builder -> builder
							.artifactManifest(artifactManifest)
							.build());
					case "exposed-classes" -> exposedClassesViewRenderer.render(builder -> builder
							.artifactManifest(artifactManifest)
							.build());
					default -> null;
				})
				.ifPresent(component -> {
					CssFlexItem.setFlexGrow(component, 1);
					layout.add(component);
				});
	}

	@WithSpan
	public Component render(ArtifactManifest artifactManifest) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			Tabs tabs = new Tabs();
			{
				tabs.add(TabRenderer.render(builder -> builder
						.id("overview")
						.label("Overview")
						.build()));
				tabs.add(TabRenderer.render(builder -> builder
						.id("build-and-dependencies")
						.label("Build & Dependencies")
						.build()));
				tabs.add(TabRenderer.render(builder -> builder
						.id("properties")
						.label("Properties")
						.build()));
				tabs.add(TabRenderer.render(builder -> builder
						.id("integrations")
						.label("Integrations")
						.build()));
				tabs.add(TabRenderer.render(builder -> builder
						.id("persistence")
						.label("Persistence")
						.build()));
				tabs.add(TabRenderer.render(builder -> builder
						.id("telemetry")
						.label("Telemetry")
						.build()));
				tabs.add(TabRenderer.render(builder -> builder
						.id("custom-models")
						.label("Custom Models")
						.build()));
				tabs.add(TabRenderer.render(builder -> builder
						.id("exposed-classes")
						.label("Exposed Classes")
						.build()));
			}
			{
				tabs.addSelectedChangeListener(event ->
						renderTab(layout, artifactManifest, event.getSelectedTab().getId().orElseThrow()));
			}
			layout.add(tabs);
			{
				renderTab(layout, artifactManifest, "overview");
			}
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		ArtifactManifest artifactManifest;
	}
}
