package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;
import tk.labyrinth.satool.maven.domain.MavenDependency;

import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class GroupIdAndArtifactIdMavenModuleReference implements Reference<MavenModule> {

	String artifactId;

	String groupId;

	@Override
	public String toString() {
		return "%s(%s=%s,%s=%s)".formatted(
				MavenModule.MODEL_CODE,
				MavenModule.GROUP_ID_ATTRIBUTE_NAME,
				groupId,
				MavenModule.ARTIFACT_ID_ATTRIBUTE_NAME,
				artifactId);
	}

	public static GroupIdAndArtifactIdMavenModuleReference from(MavenDependency mavenDependency) {
		return of(mavenDependency.getGroupId(), mavenDependency.getArtifactId());
	}

	public static GroupIdAndArtifactIdMavenModuleReference from(MavenModule mavenModule) {
		return of(mavenModule.getGroupId(), mavenModule.getArtifactId());
	}

	@JsonCreator
	public static GroupIdAndArtifactIdMavenModuleReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), MavenModule.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		//
		return of(
				reference.getAttributeValue(MavenModule.GROUP_ID_ATTRIBUTE_NAME),
				reference.getAttributeValue(MavenModule.ARTIFACT_ID_ATTRIBUTE_NAME));
	}

	public static GroupIdAndArtifactIdMavenModuleReference of(String groupId, String artifactId) {
		return new GroupIdAndArtifactIdMavenModuleReference(artifactId, groupId);
	}
}
