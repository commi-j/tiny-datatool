package tk.labyrinth.satool.beholder.manifester.domain.properties.declaration;

import io.vavr.collection.List;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.stores.objectindex.Indexer;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;

@Component
public class PropertyDeclarationEntryIndexer implements Indexer<ArtifactManifest, PropertyDeclarationEntryIndex> {

	@Override
	public List<PropertyDeclarationEntryIndex> createIndices(ArtifactManifest target) {
		return ObjectUtils.<List<PropertyDeclarationEntry>>orElse(target.getPropertyDeclarationEntries(), List.empty())
				.map(propertyDeclarationEntry -> PropertyDeclarationEntryIndex.builder()
						.location(propertyDeclarationEntry.getLocation())
						.name(propertyDeclarationEntry.getName())
						.tags(propertyDeclarationEntry.getTags())
						.targetReference(createTargetReference(target))
						.value(propertyDeclarationEntry.getValue())
						.build());
	}

	@Override
	public UidReference<ArtifactManifest> createTargetReference(ArtifactManifest target) {
		return UidReference.of(ArtifactManifest.MODEL_CODE, target.getUid());
	}
}
