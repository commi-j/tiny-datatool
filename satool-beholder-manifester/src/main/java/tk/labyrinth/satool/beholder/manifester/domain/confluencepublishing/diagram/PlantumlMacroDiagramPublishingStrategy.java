package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram;

import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicQualifier;

@PolymorphicQualifier(qualifierAttributeValue = "plantuml", rootClass = ConfluenceDiagramPublishingStrategy.class)
@Value(staticConstructor = "of")
@With
public class PlantumlMacroDiagramPublishingStrategy implements ConfluenceDiagramPublishingStrategy {
	// empty
}
