package tk.labyrinth.satool.beholder.manifester.domain.confluencemerge;

import io.vavr.collection.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Deprecated
@RequiredArgsConstructor
public enum ConfluenceMergeNodeScope {
	FUNCTION(List.of("function")),
	MANIFEST(List.of("manifest")),
	PERSISTENT_MODEL(List.of("manifest", "persistenceModel")),
	REST_ENDPOINT(List.of("manifest", "restEndpointGroup", "restEndpoint")),
	REST_ENDPOINT_GROUP(List.of("manifest", "restEndpointGroup"));

	@Getter
	private final List<String> availableAttributes;
}
