package tk.labyrinth.satool.beholder.manifester.domain.mavenmodule;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectindex.Indexer;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

@LazyComponent
public class MavenModuleDependencyIndexer implements Indexer<MavenModule, MavenModuleDependencyIndex> {

	@Override
	public List<MavenModuleDependencyIndex> createIndices(MavenModule target) {
		return target.getDependenciesOrEmpty().map(dependency -> MavenModuleDependencyIndex.builder()
				.fromReference(createTargetReference(target))
				.scope(dependency.getScope())
				.toReference(GroupIdAndArtifactIdMavenModuleReference.from(dependency))
				.type(dependency.getType())
				.version(dependency.getVersion())
				.build());
	}

	@Override
	public UidReference<MavenModule> createTargetReference(MavenModule target) {
		return UidReference.of(MavenModule.MODEL_CODE, target.getUid());
	}
}
