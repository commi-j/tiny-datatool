package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicQualifier;
import tk.labyrinth.pandora.pattern.PandoraPattern;

@Builder(builderClassName = "Builder", toBuilder = true)
@PolymorphicQualifier(qualifierAttributeValue = "image", rootClass = ConfluenceDiagramPublishingStrategy.class)
@Value(staticConstructor = "of")
@With
public class ImageDiagramPublishingStrategy implements ConfluenceDiagramPublishingStrategy {

	/**
	 * PNG, SVG;<br>
	 */
	String fileFormat;

	PandoraPattern filenamePattern;
}
