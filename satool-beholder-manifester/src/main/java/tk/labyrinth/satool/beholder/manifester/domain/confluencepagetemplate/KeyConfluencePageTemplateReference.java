package tk.labyrinth.satool.beholder.manifester.domain.confluencepagetemplate;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class KeyConfluencePageTemplateReference implements Reference<ConfluencePageTemplate> {

	String key;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				ConfluencePageTemplate.MODEL_CODE,
				ConfluencePageTemplate.KEY_ATTRIBUTE_NAME,
				key);
	}

	public static KeyConfluencePageTemplateReference from(ConfluencePageTemplate object) {
		return KeyConfluencePageTemplateReference.of(object.getKey());
	}

	@JsonCreator
	public static KeyConfluencePageTemplateReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), ConfluencePageTemplate.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(ConfluencePageTemplate.KEY_ATTRIBUTE_NAME));
	}
}