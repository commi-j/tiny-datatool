package tk.labyrinth.satool.beholder.manifester.domain.endpoint.kafka;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.pandora.datatypes.objectmodel.meta.ModelTag;
import tk.labyrinth.satool.beholder.manifester.domain.endpoint.InteractionKind;

@Builder(toBuilder = true)
@ModelTag("repos")
@Value
public class KafkaEndpoint {

	public static final String MODEL_CODE = "kafkaendpoint";

	String inboundTopic;

	InteractionKind interactionKind;

	String outboundTopic;

	MethodFullSignature signature;
}
