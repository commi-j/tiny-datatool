package tk.labyrinth.satool.beholder.manifester.domain.component;

import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.router.Location;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.anchor.SmartAnchorRenderer;
import tk.labyrinth.satool.beholder.domain.object.table.item.SpecificObjectItemActionProvider;
import tk.labyrinth.satool.beholder.manifester.diagram.component.ComponentDiagramPage;
import tk.labyrinth.satool.beholder.manifester.domain.componentdependency.ComponentDependencyLayer;

@LazyComponent
@RequiredArgsConstructor
public class ComponentShowDiagramItemActionProvider extends SpecificObjectItemActionProvider<Component> {

	private final SmartAnchorRenderer anchorRenderer;

	@Override
	protected com.vaadin.flow.component.Component provideItemAction(Component item) {
		return anchorRenderer.render(SmartAnchorRenderer.Properties.builder()
				.disabled(item.getCode() == null)
				// TODO: Compute location from class.
				.location(new Location(
						"beholder/component-diagram",
						ComponentDiagramPage.State.builder()
								.dependencyLayers(HashSet.of(ComponentDependencyLayer.LOGICAL))
								.focus(List.of(ComponentDiagramPage.State.FocusEntry.builder()
										.componentCode(item.getCode())
										.down(1)
										.up(1)
										.build()))
								.build()
								.toQueryParameters()))
				.target(AnchorTarget.BLANK)
				.text("Diagram")
				.build());
	}
}
