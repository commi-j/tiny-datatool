package tk.labyrinth.satool.beholder.manifester.domain.component;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.reference.GenericReference;
import tk.labyrinth.pandora.datatypes.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class CodeComponentReference implements Reference<Component> {

	String code;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Component.MODEL_CODE,
				Component.CODE_ATTRIBUTE_NAME,
				code);
	}

	public static CodeComponentReference from(Component object) {
		return CodeComponentReference.of(object.getCode());
	}

	@JsonCreator
	public static CodeComponentReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Component.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Component.CODE_ATTRIBUTE_NAME));
	}
}
