package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import io.vavr.control.Option;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.stores.objectindex.Indexer;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;

@Component
public class PropertyUsageEntryIndexer implements Indexer<ArtifactManifest, PropertyUsageEntryIndex> {

	@Override
	public List<PropertyUsageEntryIndex> createIndices(ArtifactManifest target) {
		return ObjectUtils.<List<PropertyUsageEntry>>orElse(target.getPropertyUsageEntries(), List.empty())
				.map(propertyUsageEntry -> PropertyUsageEntryIndex.builder()
						.defaultValue(Option.of(propertyUsageEntry.getDefaultValue())
								.orElse(() -> Option.of(propertyUsageEntry.getJavaDefaultValue()))
								.getOrNull())
						.location(propertyUsageEntry.getLocation())
						.name(propertyUsageEntry.getName())
						.tags(propertyUsageEntry.getTags())
						.targetReference(createTargetReference(target))
						.type(propertyUsageEntry.getJavaType())
						.build());
	}

	@Override
	public UidReference<ArtifactManifest> createTargetReference(ArtifactManifest target) {
		return UidReference.of(ArtifactManifest.MODEL_CODE, target.getUid());
	}
}
