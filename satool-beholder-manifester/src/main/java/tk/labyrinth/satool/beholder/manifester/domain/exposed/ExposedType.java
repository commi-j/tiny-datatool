package tk.labyrinth.satool.beholder.manifester.domain.exposed;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ExposedType {

	@Nullable
	List<ExposedAnnotation> annotations;

	TypeSignature core;

	@Nullable
	List<ExposedType> parameters;

	public List<TypeSignature> collectTypeSignature() {
		return List.of(core)
				.appendAll(parameters != null
						? parameters.flatMap(ExposedType::collectTypeSignature)
						: List.empty());
	}

	public List<ExposedAnnotation> getAnnotationsOrEmpty() {
		return annotations != null ? annotations : List.empty();
	}

	public TypeDescription toTypeDescription() {
		return parameters != null
				? TypeDescription.builder()
				.fullName(core.toString())
				.parameters(parameters.map(ExposedType::toTypeDescription).asJava())
				.build()
				: core.toDescription();
	}

	private static ExposedType doMerge(List<ExposedType> types) {
		return ExposedType.builder()
				.annotations(ExposedAnnotation.mergeMultiple(types.map(ExposedType::getAnnotations)))
				.core(types.map(ExposedType::getCore).distinct().single())
				.parameters(doMergeParameters(types.map(ExposedType::getParameters)))
				.build();
	}

	@Nullable
	private static List<ExposedType> doMergeParameters(List<@Nullable List<ExposedType>> types) {
		return !Objects.equals(types.distinct(), List.of((List<ExposedType>) null))
				? List.transpose(types).map(ExposedType::merge)
				: null;
	}

	@Nullable
	public static ExposedType merge(List<@Nullable ExposedType> types) {
		return !Objects.equals(types.distinct(), List.of((ExposedType) null))
				? doMerge(types.filter(Objects::nonNull))
				: null;
	}
}
