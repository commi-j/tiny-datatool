
<#macro object_table elements>
	<#if (elements?size>0)>
		<table>
			<tbody>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Constraints</th>
					<th>Comments</th>
				</tr>
				<#list elements![] as element>
					<tr>
						<td>${element.name!"-"}</td>
						<td>${getLongName(element.datatype)!"-"}</td>
						<td>${(element.constraints!["-"])?join("\n")}</td>
						<td>${(element.comments!["-"])?join("\n")}</td>
					</tr>
				</#list>
			</tbody>
		</table>
	<#else>
		<ul><li>None</li></ul>
	</#if>
</#macro>

<#macro object_table_with_kind elements>
	<#if (elements?size>0)>
		<table>
			<tbody>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Constraints</th>
					<th>Kind</th>
					<th>Comments</th>
				</tr>
				<#list elements![] as element>
					<tr>
						<td>${element.name!"-"}</td>
						<td>${getLongName(element.datatype)!"-"}</td>
						<td>${(element.constraints!["-"])?join("\n")}</td>
						<td>${element.kind!"-"}</td>
						<td>${(element.comments!["-"])?join("\n")}</td>
					</tr>
				</#list>
			</tbody>
		</table>
	<#else>
		<ul><li>None</li></ul>
	</#if>
</#macro>
