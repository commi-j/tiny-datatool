package tk.labyrinth.satool.beholder.manifester.domain.gitbranch;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.object.GenericObject;
import tk.labyrinth.pandora.datatypes.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.PandoraUiModule;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectItem;
import tk.labyrinth.pandora.ui.renderer.custom.GenericObjectToItemRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.repository.UrlRepositoryReference;

@Import({
		PandoraUiModule.class
})
@SpringBootTest
class GitBranchTest {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private GenericObjectToItemRenderer genericObjectToItemRenderer;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@Test
	void testRender() {
		GenericObject genericObject = converterRegistry.convert(
				GitBranch.builder()
						.name("test-name")
						.repositoryReference(UrlRepositoryReference.of("test-url"))
						.build(),
				GenericObject.class);
		//
		Assertions
				.assertThat(genericObjectToItemRenderer.renderNonNull(
						objectModelSearcher.getSingle(CodeObjectModelReference.of(GitBranch.MODEL_CODE)),
						genericObject))
				.isEqualTo(GenericObjectItem.ofValid(genericObject, "test-url/refs/heads/test-name"));
	}
}