package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.build;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.buildsystem.GradleManifestContributor;

import java.nio.file.Path;

@Disabled
@ExtendWithSpring
@Import({
		PropertyPlaceholderAutoConfiguration.class,
})
@PropertySource("nocommit.properties")
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GradleManifestContributorTest {

	@Value("${test.path-to-survey}")
	private String pathToSurvey;
//	@BeforeAll
//	void beforeAll() throws IOException {
//		Path directory = Files.createTempDirectory("junit-tests");
//		FileUtils.copyDirectory(new File(pathToSurvey), directory.toFile());
//		pathToSurvey = directory.toString();
//	}

	@Test
	void testGetJavaHome() {
		Assertions.assertNotNull(GradleManifestContributor.getJavaHome());
	}

	@Test
	void testInvokeGradleDependencies() {
		GradleManifestContributor gradleManifestContributor = new GradleManifestContributor();
		gradleManifestContributor.invokeGradleDependencies(Path.of(pathToSurvey));
	}

	@Test
	void testInvokeGradleInit() {
		GradleManifestContributor gradleManifestContributor = new GradleManifestContributor();
		gradleManifestContributor.invokeGradleInit(Path.of(pathToSurvey));
	}
}