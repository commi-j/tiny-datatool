package tk.labyrinth.satool.beholder.manifester.domain.scenario;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.satool.beholder.manifester.domain.component.ScenarioPrefixComponentReference;

class ScenarioUtilsTest {

	@Test
	void testComponentReferenceFromCode() {
		Assertions.assertEquals(null, ScenarioUtils.componentReferenceFromCode(null));
		Assertions.assertEquals(null, ScenarioUtils.componentReferenceFromCode(""));
		Assertions.assertEquals(
				ScenarioPrefixComponentReference.of("-"),
				ScenarioUtils.componentReferenceFromCode("-"));
		Assertions.assertEquals(
				ScenarioPrefixComponentReference.of("A-"),
				ScenarioUtils.componentReferenceFromCode("A-B"));
		Assertions.assertEquals(
				ScenarioPrefixComponentReference.of("-"),
				ScenarioUtils.componentReferenceFromCode("-B"));
		Assertions.assertEquals(
				ScenarioPrefixComponentReference.of("ARM-"),
				ScenarioUtils.componentReferenceFromCode("-U"));
	}
}