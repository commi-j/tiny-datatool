package tk.labyrinth.satool.beholder.manifester.domain.restcontroller;

import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.GetMapping;

class RestControllersManifestContributorTest {

	@Test
	void testResolveRequestMappingPaths() throws NoSuchMethodException {
		Assertions.assertEquals(
				List.of(),
				RestControllersManifestContributor.resolveRequestMappingPaths(
						RestControllersManifestContributor.getRequestMappingAnnotation(
								TestClass.class.getDeclaredMethod("get"))));
		Assertions.assertEquals(
				List.of("path"),
				RestControllersManifestContributor.resolveRequestMappingPaths(
						RestControllersManifestContributor.getRequestMappingAnnotation(
								TestClass.class.getDeclaredMethod("getPath"))));
		Assertions.assertEquals(
				List.of("value"),
				RestControllersManifestContributor.resolveRequestMappingPaths(
						RestControllersManifestContributor.getRequestMappingAnnotation(
								TestClass.class.getDeclaredMethod("getValue"))));
		Assertions.assertEquals(
				List.of("value0", "value1"),
				RestControllersManifestContributor.resolveRequestMappingPaths(
						RestControllersManifestContributor.getRequestMappingAnnotation(
								TestClass.class.getDeclaredMethod("getValues"))));
	}

	static class TestClass {

		@GetMapping
		void get() {
		}

		@GetMapping(path = "path")
		void getPath() {
		}

		@GetMapping("value")
		void getValue() {
		}

		@GetMapping({"value0", "value1"})
		void getValues() {
		}
	}
}