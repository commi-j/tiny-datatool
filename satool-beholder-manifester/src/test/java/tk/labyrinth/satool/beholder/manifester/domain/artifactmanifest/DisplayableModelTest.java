package tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedField;

class DisplayableModelTest {

	@Test
	void testCreateFrom() {
		Assertions
				.assertThat(DisplayableModel.createFrom(ExposedClass.builder()
						.fields(List.of(
								ExposedField.builder()
										.name("foo")
										.build(),
								ExposedField.builder()
										.name("bar")
										.build()))
						.signature(ClassSignature.from("oh:MyClass"))
						.tags(List.empty())
						.build()))
				.isEqualTo(DisplayableModel.builder()
						.attributes(List.of(
								DisplayableModelAttribute.builder()
										.comments(List.of("-"))
										.constraints(List.of("-"))
										.name("foo")
										.build(),
								DisplayableModelAttribute.builder()
										.comments(List.of("-"))
										.constraints(List.of("-"))
										.name("bar")
										.build()))
						.javaSignature(ClassSignature.from("oh:MyClass"))
						.tags(List.empty())
						.build());
	}
}