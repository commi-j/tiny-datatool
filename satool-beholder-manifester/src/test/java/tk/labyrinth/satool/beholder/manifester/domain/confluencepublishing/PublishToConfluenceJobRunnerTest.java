package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;
import tk.labyrinth.satool.freemarker.SatoolFreemarkerObjectWrapper;

import java.io.IOException;
import java.io.StringWriter;

// FIXME: Make it use ConfluenceRenderEngine.
@Disabled
@SpringBootTest(properties = "beholder.environmentCredentialses=[]")
class PublishToConfluenceJobRunnerTest {

	@Test
	void testFreemarkerTempalateWithPersistentModels(@Autowired ConverterRegistry converterRegistry)
			throws IOException, TemplateException {
		Version version = Configuration.VERSION_2_3_31;
		//
		Configuration configuration = new Configuration(version);
		configuration.setFallbackOnNullLoopVariable(false);
		configuration.setObjectWrapper(new SatoolFreemarkerObjectWrapper(version, converterRegistry));
		//
		configuration.addAutoInclude("satool-macros.ftl");
		configuration.addAutoInclude("confluence-layout.ftl");
		configuration.addAutoInclude("confluence-widgets.ftl");
		configuration.addAutoInclude("beholder-confluence-macros.ftl");
		configuration.setTemplateLoader(
				new ClassTemplateLoader(Thread.currentThread().getContextClassLoader(),
						"freemarker"));
		//
		Template template = configuration.getTemplate("persistent-models-page.ftl");
		//
		Object data = HashMap.of(
				"artifactManifest", ArtifactManifest.builder()
						.persistentModels(List.of(
								PersistentModel.builder()
										.attributes(List.of(
												PersistentModelAttribute.builder()
														.databaseNames(List.of("a", "b"))
														.build(),
												PersistentModelAttribute.builder()
														.build()))
										.databaseName("my_entity")
										.javaClassSignature(ClassSignature.from("my.entity:MyEntity"))
										.build(),
								PersistentModel.builder()
										.build()))
						.build());
		//
		StringWriter writer = new StringWriter();
		template.process(data, writer);
		//
		String result = writer.toString();
		//
		System.out.println(result);
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class PersistentModelsModel {

		ArtifactManifest artifactManifest;

		List<PersistentModel> persistentModels;
	}
}