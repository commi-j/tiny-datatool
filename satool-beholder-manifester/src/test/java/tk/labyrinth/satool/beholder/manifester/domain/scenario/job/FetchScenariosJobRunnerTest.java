package tk.labyrinth.satool.beholder.manifester.domain.scenario.job;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.satool.confluenceconnector.model.ConfluenceQueryLanguagePredicate;

@Disabled
@SpringBootTest
class FetchScenariosJobRunnerTest {

	@Disabled
	@Test
	void test(@Autowired FetchScenariosJobRunner fetchScenariosJobRunner) {
		// FIXME: Do we need it?
		fetchScenariosJobRunner.run(JobRunner.Context.<FetchScenariosJobRunner.Parameters>builder()
				.logSink(logEntry -> {
					// no-op
				})
				.parameters(FetchScenariosJobRunner.Parameters.builder()
						.cql(ConfluenceQueryLanguagePredicate.builder().build())
						.build())
				.build());
		//
		System.out.println();
	}
}