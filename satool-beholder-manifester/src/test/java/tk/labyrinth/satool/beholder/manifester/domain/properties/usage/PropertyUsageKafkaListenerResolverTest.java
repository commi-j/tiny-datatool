package tk.labyrinth.satool.beholder.manifester.domain.properties.usage;

import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.annotation.KafkaListener;
import spoon.Launcher;
import spoon.reflect.CtModel;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

class PropertyUsageKafkaListenerResolverTest {

	@Test
	void testResolve() {
		Launcher launcher = new Launcher();
		//
		launcher.addInputResource("src/test/java/%s.java".formatted(
				PropertyUsageKafkaListenerResolverTest.class.getName().replace(".", "/")));
		//
		launcher.buildModel();
		//
		CtModel model = launcher.getModel();
		//
		Assertions.assertEquals(
				List.of(
						PropertyUsageEntry.builder()
								.javaType(TypeDescription.of("java.lang:String"))
								.location("tk.labyrinth.satool.beholder.manifester.domain.properties.usage:PropertyUsageKafkaListenerResolverTest.TestClass@org.springframework.kafka.annotation:KafkaListener")
								.name("prefix")
								.tags(List.of("java"))
								.build(),
						PropertyUsageEntry.builder()
								.javaType(TypeDescription.of("java.lang:String"))
								.location("tk.labyrinth.satool.beholder.manifester.domain.properties.usage:PropertyUsageKafkaListenerResolverTest.TestClass@org.springframework.kafka.annotation:KafkaListener")
								.name("name")
								.tags(List.of("java"))
								.build(),
						PropertyUsageEntry.builder()
								.javaType(TypeDescription.of("java.lang:String"))
								.location("tk.labyrinth.satool.beholder.manifester.domain.properties.usage:PropertyUsageKafkaListenerResolverTest.TestClass@org.springframework.kafka.annotation:KafkaListener")
								.name("hello")
								.tags(List.of("java"))
								.build(),
						PropertyUsageEntry.builder()
								.javaType(TypeDescription.of("java.lang:String"))
								.location("tk.labyrinth.satool.beholder.manifester.domain.properties.usage:PropertyUsageKafkaListenerResolverTest.TestClass#listen(java.lang:String,java.lang:String)@org.springframework.kafka.annotation:KafkaListener")
								.name("method")
								.tags(List.of("java"))
								.build()),
				PropertyUsageKafkaListenerResolver.resolve(model));
	}

	@KafkaListener(topics = {"${prefix}.${name}", "${hello}"})
	static class TestClass {

		@KafkaListener(topics = "${method}")
		static void listen(String foo, String bar) {
			// no-op
		}
	}
}