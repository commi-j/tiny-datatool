package tk.labyrinth.satool.beholder.manifester.domain.properties;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;

import java.nio.file.Path;

@Disabled
@ExtendWithSpring
@Import({
		SpringPropertiesManifestContributor.class,
})
@PropertySource("nocommit.properties")
class SpringPropertiesManifestContributorTest {

	@Autowired
	private SpringPropertiesManifestContributor manifestContributor;

	@Value("${test.path-to-survey:#{null}}")
	private String pathToSurvey;

	@Test
	void testContributeWithCustomProject() {
		manifestContributor.contribute(Path.of(pathToSurvey), null);
	}

	@Test
	void testContributeWithTestProject() {
		List<?> contributedObjects = manifestContributor.contribute(Path.of(
						"../beholder-manifester-test-project"),
				null);
		//
		Assertions
				.assertThat(contributedObjects)
				.isEqualTo(List.of(
						PropertyDeclarationEntry.builder()
								.location("src/main/resources/application.properties")
								.name("application.properties")
								.tags(List.of("file"))
								.value("true")
								.build(),
						PropertyDeclarationEntry.builder()
								.location("src/main/resources/application.yaml")
								.name("application.yaml")
								.tags(List.of("file"))
								.value("true")
								.build(),
						PropertyDeclarationEntry.builder()
								.location("src/main/resources/application.yml")
								.name("application.yml")
								.tags(List.of("file"))
								.value("true")
								.build()));
	}
}