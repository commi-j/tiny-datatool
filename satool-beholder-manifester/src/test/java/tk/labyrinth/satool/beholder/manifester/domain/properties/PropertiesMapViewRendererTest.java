package tk.labyrinth.satool.beholder.manifester.domain.properties;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PropertiesMapViewRendererTest {

	@Test
	void testComposeValueText() {
		Assertions
				.assertThat(PropertiesMapViewRenderer.composeValueText(
						"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))
				.isEqualTo("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		Assertions
				.assertThat(PropertiesMapViewRenderer.composeValueText(
						"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))
				.isEqualTo("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa… (11 more)");
	}

	@Test
	void testCreateDiagramModel() {
		// TODO
	}
}