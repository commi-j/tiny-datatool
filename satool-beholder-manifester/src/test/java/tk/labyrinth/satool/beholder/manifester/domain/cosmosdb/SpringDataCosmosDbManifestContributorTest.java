package tk.labyrinth.satool.beholder.manifester.domain.cosmosdb;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.init.temporal.PandoraTemporalConstants;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.satool.beholder.manifester.artifact.ArtifactDefinition;
import tk.labyrinth.satool.beholder.manifester.artifact.GitBranchArtifactOrigin;
import tk.labyrinth.satool.beholder.manifester.artifact.NameArtifactDefinitionReference;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.BuildArtifactManifestJobRunner;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.GitBranch;
import tk.labyrinth.satool.beholder.manifester.domain.gitbranch.RepositoryReferenceAndNameGitBranchReference;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;
import tk.labyrinth.satool.beholder.manifester.domain.repository.Repository;
import tk.labyrinth.satool.beholder.manifester.domain.repository.UrlRepositoryReference;

@DirtiesContext
@SpringBootTest
class SpringDataCosmosDbManifestContributorTest {

	@SmartAutowired
	private TypedObjectManipulator<ArtifactDefinition> artifactDefinitionManipulator;

	@SmartAutowired
	private TypedObjectSearcher<ArtifactManifest> artifactManifestSearcher;

	@Autowired
	private BuildArtifactManifestJobRunner buildArtifactManifestJobRunner;

	@SmartAutowired
	private TypedObjectManipulator<GitBranch> gitBranchManipulator;

	@SmartAutowired
	private TypedObjectManipulator<Repository> repositoryManipulator;

	@SmartAutowired
	private TypedObjectManipulator<StoreRoute> storeRouteManipulator;

	@BeforeEach
	void beforeEach() {
		storeRouteManipulator.createWithAdjustment(StoreRoute.builder()
						.distance(Integer.MAX_VALUE)
						.name("test-store-route")
						.storeConfigurationPredicate(Predicates.equalTo(
								StoreConfiguration.DESIGNATION_ATTRIBUTE_NAME,
								StoreDesignation.TEMPORAL))
						.build(),
				genericObject -> genericObject.withAddedAttributes(
						RootObjectUtils.createNewUidAttribute(),
						PandoraTemporalConstants.ATTRIBUTE));
	}

	@org.junit.jupiter.api.Tag("web")
	@Test
	void testContribute() {
		String repositoryUrl = "https://github.com/Azure-Samples/azure-spring-data-cosmos-java-sql-api-samples";
		String branchName = "main";
		//
		repositoryManipulator.createWithGeneratedUid(
				Repository.builder()
						.name("test")
						.url(repositoryUrl)
						.build());
		gitBranchManipulator.createWithGeneratedUid(GitBranch.builder()
				.name(branchName)
				.repositoryReference(UrlRepositoryReference.of(repositoryUrl))
				.build());
		artifactDefinitionManipulator.createWithGeneratedUid(
				ArtifactDefinition.builder()
						.name("test")
						.origin(GitBranchArtifactOrigin.builder()
								.gitBranchReference(RepositoryReferenceAndNameGitBranchReference.of(
										UrlRepositoryReference.of(repositoryUrl),
										branchName))
								.build())
						.build());
		//
		buildArtifactManifestJobRunner.run(JobRunner.Context.<BuildArtifactManifestJobRunner.Parameters>builder()
				.parameters(BuildArtifactManifestJobRunner.Parameters.builder()
						.artifactDefinitionReference(NameArtifactDefinitionReference.of("test"))
						.build())
				.build());
		//
		List<ArtifactManifest> artifactManifests = artifactManifestSearcher.searchAll();
		//
		Assertions.assertThat(artifactManifests).hasSize(1);
		//
		ArtifactManifest artifactManifest = artifactManifests.get(0);
		//
		Assertions
				.assertThat(artifactManifest.getPersistentModels())
				.isEqualTo(List.of(PersistentModel.builder()
						.attributes(List.of(
								PersistentModelAttribute.builder()
										.databaseNames(List.of("id"))
										.javaFieldName("id")
										.javaFieldType(TypeDescription.ofNonParameterized(String.class))
										.tags(List.of(PersistentModelAttribute.PRIMARY_KEY_TAG))
										.build(),
								PersistentModelAttribute.builder()
										.databaseNames(List.of("firstName"))
										.javaFieldName("firstName")
										.javaFieldType(TypeDescription.ofNonParameterized(String.class))
										.tags(List.empty())
										.build(),
								PersistentModelAttribute.builder()
										.databaseNames(List.of("lastName"))
										.javaFieldName("lastName")
										.javaFieldType(TypeDescription.ofNonParameterized(String.class))
										.tags(List.of(Tag.of("PARTITION_KEY")))
										.build()))
						.databaseName("usercontainer")
						.javaClassSignature(ClassSignature.from("com.azure.cosmos.springexamples.common:User"))
						.tags(List.of(Tag.of("COSMOSDB_CONTAINER")))
						.build()));
	}
}