package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class JpaPersistentModelsManifestContributorTest {

	@Test
	void testGetExtraClasspathEntries() {
		Assertions.assertEquals(2, new JpaPersistentModelsManifestContributor().getExtraClasspathEntries().size());
	}
}