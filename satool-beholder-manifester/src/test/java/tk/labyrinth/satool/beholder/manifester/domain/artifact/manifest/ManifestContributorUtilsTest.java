package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.annotation.FunctionName;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ManifestContributorUtilsTest {

	@Test
	void testGetLocationOfClass() {
		Assertions
				.assertThat(ManifestContributorUtils.getLocationOfClassSource(FunctionName.class))
				.doesNotStartWith("file:")
				.doesNotStartWith("jar:")
				.endsWith("/.m2/repository/com/microsoft/azure/functions/azure-functions-java-library/3.0.0/azure-functions-java-library-3.0.0.jar");
		Assertions
				.assertThat(ManifestContributorUtils.getLocationOfClassSource(HttpMethod.class))
				.doesNotStartWith("file:")
				.doesNotStartWith("jar:")
				.endsWith("/.m2/repository/com/microsoft/azure/functions/azure-functions-java-core-library/1.2.0/azure-functions-java-core-library-1.2.0.jar");
	}
}