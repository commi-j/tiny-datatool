package tk.labyrinth.satool.beholder.manifester.domain.validations;

import io.vavr.collection.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import tk.labyrinth.pandora.context.PandoraContextModule;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifestBuilder;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.DisplayableModel;
import tk.labyrinth.satool.beholder.manifester.domain.restcontroller.RestControllersManifestContributor;

import java.nio.file.Path;

@Disabled
@SpringBootTest(classes = {
		ArtifactManifestBuilder.class,
		PandoraContextModule.class,
		RestControllersManifestContributor.class,
		ValidationsManifestContributor.class,
})
@TestPropertySource(locations = "/nocommit.properties")
class ValidationsManifestContributorTest {

	@Autowired
	private ArtifactManifestBuilder artifactManifestBuilder;

	@Value("${test.validations-survey-paths}")
	private java.util.List<String> validationsSurveyPaths;

	@Disabled
	@Test
	void testContribute() {
		List<ArtifactManifest> artifactManifests = List.ofAll(validationsSurveyPaths)
				.map(path -> artifactManifestBuilder.buildJavaModuleManifestFromDirectory(Path.of(path)));
		//
		List<DisplayableModel> displayableModels = artifactManifests.flatMap(ArtifactManifest::getExposedClasses)
				.map(DisplayableModel::createFrom);
		//
		System.out.println();
	}
}