package tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.mongodb;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifestBuilder;
import tk.labyrinth.satool.beholder.manifester.domain.exposed.ExposedClass;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModel;

import java.nio.file.Path;

@SpringBootTest
class SpringDataMongoDbManifestContributorTest {

	@Autowired
	private ArtifactManifestBuilder artifactManifestBuilder;

	@org.junit.jupiter.api.Tag("web")
	@Test
	void testContribute() {
		ArtifactManifest artifactManifest = artifactManifestBuilder.buildJavaModuleManifestFromDirectory(
				Path.of("../beholder-manifester-test-project"));
		//
		Assertions
				.assertThat(artifactManifest.getPersistentModels()
						.map(PersistentModel::getJavaClassSignature)
						.map(ClassSignature::toString))
				.isEqualTo(List.of("tk.labyrinth.beholder.manifestertestproject.mongodbmodel:RootDocument"));
		Assertions
				.assertThat(artifactManifest.getExposedClasses()
						.map(ExposedClass::getSignature)
						.map(ClassSignature::toString))
				.isEqualTo(List.of(
						"tk.labyrinth.beholder.manifestertestproject.mongodbmodel:SomeEnumeration",
						"tk.labyrinth.beholder.manifestertestproject.mongodbmodel:SomeItem",
						"tk.labyrinth.beholder.manifestertestproject.mongodbmodel:SomeObject",
						"tk.labyrinth.beholder.manifestertestproject.mongodbmodel:SomeNestedObject"));
		Assertions
				.assertThat(artifactManifest.getExposedClasses()
						.filter(ExposedClass::isEnum)
						.singleOption()
						.map(ExposedClass::getEnumValues)
						.get())
				.isEqualTo(List.of("ONE", "TWO", "THREE"));
	}
}