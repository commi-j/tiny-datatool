package tk.labyrinth.satool.beholder.manifester.dependencymanager;

import org.apache.maven.model.Model;
import org.junit.jupiter.api.Test;
import tk.labyrinth.satool.maven.connector.MavenRepositoryConnector;
import tk.labyrinth.satool.maven.testing.TestingConstants;

class MavenDependencyManagerTest {

	@Test
	void testCreateBom() {
		MavenRepositoryConnector mavenRepositoryConnector = new MavenRepositoryConnector();
		MavenDependencyManager mavenDependencyManager = new MavenDependencyManager(mavenRepositoryConnector);
		//
		Model model = mavenRepositoryConnector.fetchPom(
				MavenRepositoryConnector.MAVEN_CENTRAL_URL,
				TestingConstants.SPRING_BOOT_DEPENDENCIES_COORDINATES);
		//
		MavenDependencyManager.createBom(model);
	}
}