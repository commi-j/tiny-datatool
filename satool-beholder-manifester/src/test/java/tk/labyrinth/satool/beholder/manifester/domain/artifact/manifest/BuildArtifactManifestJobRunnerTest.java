package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

class BuildArtifactManifestJobRunnerTest {

	@Test
	void testComputeArtifactManifestDirectory() {
		Assertions.assertEquals(
				Path.of("foo/bar/bar"),
				BuildArtifactManifestJobRunner.computeArtifactManifestDirectory(Path.of("foo/bar"), "bar"));
	}
}