package tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.diagram;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.ArtifactManifestIndex;
import tk.labyrinth.satool.beholder.manifester.domain.artifactmanifest.RefinedPersistentModel;
import tk.labyrinth.satool.beholder.manifester.domain.confluencepublishing.ConfluenceRenderEngine;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelAttribute;
import tk.labyrinth.satool.beholder.manifester.domain.persistentmodel.PersistentModelRelation;
import tk.labyrinth.satool.beholder.manifester.domain.properties.Property;
import tk.labyrinth.satool.beholder.manifester.domain.properties.declaration.PropertyDeclarationEntry;
import tk.labyrinth.satool.beholder.manifester.domain.properties.usage.PropertyUsageEntry;

import java.time.Instant;
import java.util.Objects;

@Disabled("FIXME: Should fix runner image for this to work in pipeline")
@SpringBootTest
class ConfluenceDiagramTemplateDirectiveModelTest {

	private static final ArtifactManifestIndex manifest = ArtifactManifestIndex.empty()
			.withPersistentModels(List.of(
					RefinedPersistentModel.builder()
							.attributes(List.of(
									PersistentModelAttribute.builder()
											.databaseNames(List.of("id"))
											.javaFieldName("id")
											.javaFieldType(TypeDescription.ofNonParameterized(Long.class))
											.tags(List.of(PersistentModelAttribute.PRIMARY_KEY_TAG))
											.build(),
									PersistentModelAttribute.builder()
											.databaseNames(List.of("name"))
											.javaFieldName("name")
											.javaFieldType(TypeDescription.ofNonParameterized(String.class))
											.tags(List.empty())
											.build()))
							.databaseName("user_role")
							.javaSignature(ClassSignature.from("tk.labyrinth:UserRoleEntity"))
							.name("UserRoleEntity")
							.tags(List.empty())
							.build(),
					RefinedPersistentModel.builder()
							.attributes(List.of(
									PersistentModelAttribute.builder()
											.databaseNames(List.of("id"))
											.javaFieldName("id")
											.javaFieldType(TypeDescription.ofNonParameterized(Long.class))
											.tags(List.of(PersistentModelAttribute.PRIMARY_KEY_TAG))
											.build(),
									PersistentModelAttribute.builder()
											.databaseNames(List.of("name"))
											.javaFieldName("name")
											.javaFieldType(TypeDescription.ofNonParameterized(String.class))
											.tags(List.empty())
											.build(),
									PersistentModelAttribute.builder()
											.databaseNames(List.of("role"))
											.javaFieldName("role")
											.javaFieldType(TypeDescription.of("tk.labyrinth:UserRoleEntity"))
											.relation(PersistentModelRelation.builder()
													.hasRelationColumn(true)
													.toColumnName("id")
													.toTableName("user_role")
													.build())
											.tags(List.empty())
											.build(),
									PersistentModelAttribute.builder()
											.databaseNames(List.of("created_at"))
											.javaFieldName("createdAt")
											.javaFieldType(TypeDescription.ofNonParameterized(Instant.class))
											.tags(List.empty())
											.build(),
									PersistentModelAttribute.builder()
											.databaseNames(List.of("updated_at"))
											.javaFieldName("updatedAt")
											.javaFieldType(TypeDescription.ofNonParameterized(Instant.class))
											.tags(List.empty())
											.build()))
							.databaseName("user")
							.javaSignature(ClassSignature.from("tk.labyrinth:UserEntity"))
							.name("UserEntity")
							.tags(List.empty())
							.build()))
			.withProperties(List.of(
					Property.builder()
							.declarationEntries(List.of(PropertyDeclarationEntry.builder()
									.location("application.properties")
									.name("foo.bar")
									.tags(List.of("file", "property"))
									.value("${FOO.BAR} & ${hello}")
									.build()))
							.name("foo.bar")
							.usageEntries(List.empty())
							.build(),
					Property.builder()
							.declarationEntries(List.of(PropertyDeclarationEntry.builder()
									.location("application.properties")
									.name("FOO.BAR")
									.tags(List.of("file", "property"))
									.value("bar-foo")
									.build()))
							.name("FOO.BAR")
							.usageEntries(List.of(PropertyUsageEntry.builder()
									.location("application.properties:foo.bar")
									.name("FOO.BAR")
									.tags(List.of("file", "property"))
									.build()))
							.build(),
					Property.builder()
							.declarationEntries(List.of(PropertyDeclarationEntry.builder()
									.location("application.properties")
									.name("hello")
									.tags(List.of("file", "property"))
									.value("world")
									.build()))
							.name("hello")
							.usageEntries(List.of(PropertyUsageEntry.builder()
									.location("application.properties:foo.bar")
									.name("hello")
									.tags(List.of("file", "property"))
									.build()))
							.build()));

	@Autowired
	private ConfluenceRenderEngine confluenceRenderEngine;

	@Autowired
	private ConverterRegistry converterRegistry;

	@Test
	void testExecuteWithPersistentModelAndPlantUml() {
		Assertions
				.assertThat(confluenceRenderEngine
						.resolve(
								"""
										<@diagram kind="PERSISTENT_MODEL_MAP" strategy="PLANT_UML"/>""",
								HashMap.of(
										"manifest", manifest,
										"persistentModel", manifest.getPersistentModels()
												.find(persistentModel ->
														Objects.equals(persistentModel.getName(), "UserEntity"))
												.get()))
						.replace("\r\n", "\n"))
				.isEqualTo("""
						<ac:structured-macro ac:name="plantuml">
							<ac:plain-text-body><![CDATA['@startuml
						      
						object "**UserEntity**\\nuser" as user#lightblue {
						.. Primary Key ..
						id : Long
						.. Business Attributes ..
						name : String
						role : UserRoleEntity
						.. Utility Attributes ..
						created_at : Instant
						updated_at : Instant
						}
						      
						object "**UserRoleEntity**\\nuser_role" as user_role {
						.. Primary Key ..
						id : Long
						.. Business Attributes ..
						name : String
						}
						      
						user::role --> user_role::id
						      
						'@enduml
						]]></ac:plain-text-body>
						</ac:structured-macro>
						""");
	}

	@Test
	void testExecuteWithProperetiesAndSvg() {
		ThreadLocalConfluenceAttachmentsCache.initialize();
		//
		String resolved = confluenceRenderEngine
				.resolve(
						"""
								<@diagram kind="PROPERTY_MAP" strategy="SVG" filenamePattern="${property.name}"/>""",
						HashMap.of(
								"manifest", manifest,
								"property", manifest.getProperties()
										.find(property -> Objects.equals(property.getName(), "foo.bar"))
										.get()))
				.replace("\r\n", "\n");
		//
		Map<String, byte[]> attachments = ThreadLocalConfluenceAttachmentsCache.collect();
		//
		Assertions
				.assertThat(resolved)
				.isEqualTo("""
						<ac:image ac:border="true" ac:height="100">
							<ri:attachment ri:filename="foo.bar.svg"/>
						</ac:image>
						""");
		Assertions
				.assertThat(attachments.size())
				.isEqualTo(1);
		Assertions
				.assertThat(attachments.get()._1())
				.isEqualTo("foo.bar.svg");
		Assertions
				.assertThat(attachments.get()._2().length)
				.isEqualTo(4752);
	}

	@Test
	void testExecuteWithPropertiesAndPlantUml() {
		Assertions
				.assertThat(confluenceRenderEngine
						.resolve(
								"""
										<@diagram kind="PROPERTY_MAP" strategy="PLANT_UML"/>""",
								HashMap.of(
										"manifest", manifest,
										"property", manifest.getProperties()
												.find(property -> Objects.equals(property.getName(), "foo.bar"))
												.get()))
						.replace("\r\n", "\n"))
				.isEqualTo("""
						<ac:structured-macro ac:name="plantuml">
							<ac:plain-text-body><![CDATA['@startuml
						skinparam componentStyle rectangle
						      
						frame "Files " as files{
						frame "application.properties " as application.properties{
						component application.properties_FOO.BAR [
						FOO.BAR
						bar-foo
						]
						component application.properties_foo.bar #lightblue [
						foo.bar
						${FOO.BAR} & ${hello}
						]
						component application.properties_hello [
						hello
						world
						]
						}
						}
						      
						[application.properties_FOO.BAR] -d- [application.properties_foo.bar] #FFF
						[application.properties_foo.bar] -d- [application.properties_hello] #FFF
						[application.properties_FOO.BAR] -> [application.properties_foo.bar]
						[application.properties_hello] -> [application.properties_foo.bar]
						      
						'@enduml
						]]></ac:plain-text-body>
						</ac:structured-macro>
						""");
	}

	@Test
	void testExecuteWithPropertiesAndPng() {
		ThreadLocalConfluenceAttachmentsCache.initialize();
		//
		String resolved = confluenceRenderEngine
				.resolve(
						"""
								<@diagram kind="PROPERTY_MAP" strategy="PNG" filenamePattern="${property.name}"/>""",
						HashMap.of(
								"manifest", manifest,
								"property", manifest.getProperties()
										.find(property -> Objects.equals(property.getName(), "foo.bar"))
										.get()))
				.replace("\r\n", "\n");
		//
		Map<String, byte[]> attachments = ThreadLocalConfluenceAttachmentsCache.collect();
		//
		Assertions
				.assertThat(resolved)
				.isEqualTo("""
						<ac:image ac:border="true" ac:height="100">
							<ri:attachment ri:filename="foo.bar.png"/>
						</ac:image>
						""");
		Assertions
				.assertThat(attachments.size())
				.isEqualTo(1);
		Assertions
				.assertThat(attachments.get()._1())
				.isEqualTo("foo.bar.png");
		Assertions
				.assertThat(attachments.get()._2().length)
				.isEqualTo(10040);
	}
}