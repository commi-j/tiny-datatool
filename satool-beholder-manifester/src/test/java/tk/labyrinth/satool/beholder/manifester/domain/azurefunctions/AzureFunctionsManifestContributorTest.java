package tk.labyrinth.satool.beholder.manifester.domain.azurefunctions;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifest;
import tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest.ArtifactManifestBuilder;
import tk.labyrinth.satool.beholder.manifester.domain.function.ManifestFunction;
import tk.labyrinth.satool.beholder.manifester.domain.function.http.HttpFunctionCoordinates;
import tk.labyrinth.satool.jgit.RepositoryHandler;

import java.nio.file.Path;

@SpringBootTest
class AzureFunctionsManifestContributorTest {

	@Autowired
	private ArtifactManifestBuilder artifactManifestBuilder;

	@Tag("web")
	@Test
	void testContribute() {
		Path revisionDirectory = RepositoryHandler.loadRevision(
				"https://github.com/Azure-Samples/azure-functions-samples-java",
				"master",
				null,
				"manifester-test");
		//
		ArtifactManifest artifactManifest = artifactManifestBuilder.buildJavaModuleManifestFromDirectory(
				revisionDirectory.resolve("triggers-bindings"));
		//
		Assertions
				.assertThat(artifactManifest.getFunctions()
						.filter(ManifestFunction::isHttp)
						.flatMap(function -> function.getCoordinateses()
								.map(HttpFunctionCoordinates.class::cast)
								.map(coordinates -> "%s %s | %s"
										.formatted(coordinates.getMethod(), coordinates.getPath(), function.getName()))))
				.isEqualTo(List.of(
						"GET /api/CosmosDBInputId | CosmosDBInputId",
						"POST /api/CosmosDBInputId | CosmosDBInputId",
						"GET /api/CosmosDBInputIdPOJO | CosmosDBInputIdPOJO",
						"POST /api/CosmosDBInputIdPOJO | CosmosDBInputIdPOJO",
						"GET /api/CosmosDBInputQuery | CosmosDBInputQuery",
						"POST /api/CosmosDBInputQuery | CosmosDBInputQuery",
						"GET /api/CosmosDBInputQueryPOJOArray | CosmosDBInputQueryPOJOArray",
						"POST /api/CosmosDBInputQueryPOJOArray | CosmosDBInputQueryPOJOArray",
						"GET /api/CosmosDBInputQueryPOJOList | CosmosDBInputQueryPOJOList",
						"POST /api/CosmosDBInputQueryPOJOList | CosmosDBInputQueryPOJOList",
						"GET /api/EventGridOutputBindingJava | EventGridOutputBindingJava",
						"POST /api/EventGridOutputBindingJava | EventGridOutputBindingJava",
						"GET /api/HttpExampleRetry | HttpExampleRetry",
						"POST /api/HttpExampleRetry | HttpExampleRetry",
						"GET /api/HttpTriggerJavaVersion | HttpTriggerJavaVersion",
						"POST /api/HttpTriggerJavaVersion | HttpTriggerJavaVersion",
						"GET /api/StaticWebPage | StaticWebPage",
						"POST /api/StaticWebPage | StaticWebPage",
						"GET /api/HttpExample | HttpExample",
						"POST /api/HttpExample | HttpExample",
						"GET /api/HttpTriggerAndKafkaOutput | HttpTriggerAndKafkaOutput",
						"GET /api/QueueOutputPOJOList | QueueOutputPOJOList",
						"POST /api/QueueOutputPOJOList | QueueOutputPOJOList",
						"GET /api/ServiceBusQueueOutput | ServiceBusQueueOutput",
						"POST /api/ServiceBusQueueOutput | ServiceBusQueueOutput",
						"GET /api/ServiceBusTopicOutput | ServiceBusTopicOutput",
						"POST /api/ServiceBusTopicOutput | ServiceBusTopicOutput",
						"GET /api/TableOutput | TableOutput",
						"POST /api/TableOutput | TableOutput"));
	}
}