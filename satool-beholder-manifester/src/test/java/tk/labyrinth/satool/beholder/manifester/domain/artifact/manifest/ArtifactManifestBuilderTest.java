package tk.labyrinth.satool.beholder.manifester.domain.artifact.manifest;

import lombok.val;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import tk.labyrinth.satool.beholder.manifester.domain.properties.PropertiesMapViewRenderer;
import tk.labyrinth.satool.beholder.manifester.domain.properties.PropertiesUtils;
import tk.labyrinth.satool.plantuml.componentdiagram.ComponentDiagramModel;

import java.nio.file.Path;

@Disabled // For manual launches.
@SpringBootTest
@TestPropertySource(
		locations = "/nocommit.properties"
//		,properties = "logging.level.root=DEBUG"
)
class ArtifactManifestBuilderTest {

	@Autowired
	private ArtifactManifestBuilder artifactManifestBuilder;

	@Value("${test.path-to-survey}")
	private String pathToSurvey;

	@Test
	void testBuild() {
		ArtifactManifest artifactManifest = artifactManifestBuilder.buildJavaModuleManifestFromDirectory(Path.of(
				pathToSurvey));
		//
		ComponentDiagramModel diagramModel = PropertiesMapViewRenderer.createDiagramModel(
				artifactManifest.getPropertyDeclarationEntries(),
				artifactManifest.getPropertyUsageEntries(),
				null);
		//
		val props = PropertiesUtils.selectPropertyTree(
				artifactManifest.getPropertyDeclarationEntries(),
				artifactManifest.getPropertyUsageEntries(),
				"KAFKA_PREFIX");
		ComponentDiagramModel diagramModel2 = PropertiesMapViewRenderer.createDiagramModel(
				props.getLeft(),
				props.getRight(),
				null);
		//
		System.out.println(diagramModel.render());
	}
}